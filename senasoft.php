<?php include('header.php') ?>

    	<div class="about">
        <div class="container">
	
    	<script type="text/javascript" src="engine1/wowslider.js"></script>
    	<script type="text/javascript" src="engine1/script.js"></script>
        <br/>
        <section class="title-section">
            <h1 class="title-header">¿Qué es SenaSoft 2016?</h1> 
        </section>
        <div class="about-desc text-justify">
            <p><br><strong>SENASOFT</strong> es el mayor encuentro de tecnología realizado por el SENA, como iniciativa de la Red de Conocimiento en Informática, Diseño y Desarrollo de Software y mediante el cual la Entidad propicia entre los aprendices, espacios que les permiten transferencia de conocimiento, en las habilidades más significativas de la formación TIC.</p>
            <p>En el encuentro se convocan aprendices de los Centros que ofrecen Formación en programas en ejecución de la red de conocimiento en Informática, Diseño y Desarrollo de Software.</p>
            <p>En esta séptima versión, SENASOFT reunirá a 400 aprendices y más de 90 instructores de los centros de que ofertan programas de formación pertenecientes a dicha red.</p>
            
            <p>Durante los tres días de competencia, serán llevadas a cabo diferentes actividades, entre ellas, una agenda académica con expertos internacionales y nacionales, muestra comercial, rueda de proyección empresarial para impulsar el emprendimiento digital, entre otras.</p>
        </div>
        <section class="title-section">
            <h1 class="title-header">Dirigido a:</h1> 
        </section>
        <div class="about-desc text-justify">
            <ul>
                <li><p><b>Aprendices :</b> En etapa lectiva o productiva de los programas de formación Análisis y desarrollo de sistemas de información, Animación 3D, Producción de multimedia, Desarrollo de videojuegos, Programación de software , Gestión de redes de datos, Animación digital, Producción de medios audiovisuales digitales, Mantenimiento de equipos de cómputo, Diseño e Integración de multimedia, Mantenimiento de Equipos de Cómputo, Diseño e Instalación de Cableado Estructurado e Instalación de redes de computadores.</p></li>
                <li><p><b>Comunidad SENA</b></p></li>                            
                <li><p><b>Empresarios</b></p></li>                          
            </ul>
        </div>
                    
        <section class="title-section">
            <h1 class="title-header">¿Dónde tendrá lugar SenaSoft 2016?</h1> 
        </section>
        <div class="about-desc text-justify">
            <p>En el Centro Cultural Metropolitano de Convenciones de Armenia, ubicado en la calle 26N #11-21, del 24 al 28 de Octubre de 2016. En este escenario tendremos las siguientes actividades: </p>
            <ul>
                <li>Competencias</li>
                <li>Agenda académica nacional e internacional</li>
                <li>Muestra comercial</li>
                <li>Rueda de proyección empresarial CREAMÁS</li>
                <li>Agenda cultural</li>
            </ul>
            <br><br>

            <figure id="img_donde">
                <img src="images/centro_convenciones.jpg"/>
                <img src="images/centro_convenciones2.jpg">
                <img src="images/centro_convenciones3.png">
            </figure>

            <p class="text-center">La Regional Quindío y  el Centro de Comercio y Turismo, tiene a su cargo la organización de SENASoft 2016.</p>
        </div>
        </div>
        </div>
    </div>	

<?php include('footer.php') ?>