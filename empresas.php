 
<?php include('header.php') ?>

    <div class="portfolio_box">
        <div class="container">
            <section class="title-section">
                <h1 class="title-header">Empresas patrocinadoras y de apoyo</h1>
            </section>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://www.sena.edu.co/" target="_blank">
                    <img src="empresas/SENA.png" alt="SENA">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://www.cisco.com/c/es_co/index.html" target="_blank">
                    <img src="empresas/cisco.png" alt="CISCO">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="https://www.microsoft.com/es-co/" target="_blank">
                    <img src="empresas/microsoft.png" alt="microsoft">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="https://www.ibm.com/co-es/" target="_blank">
                    <img src="empresas/ibm.png" alt="ibm">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://digitalware.com.co/" target="_blank">
                    <img src="empresas/digital-ware.png" alt="digital-ware">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://www.telefonica.co/" target="_blank">
                    <img src="empresas/telefonica.png" alt="telefonica">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://www.computadoresparaeducar.gov.co/" target="_blank">
                    <img src="empresas/cpe.png" alt="cpe">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://www.lenovo.com/co/es/" target="_blank">
                    <img src="empresas/lenovo.png" alt="lenovo">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="https://www.amazon.com/" target="_blank">
                    <img src="empresas/amazon.png" alt="amazon">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://www.wbagadion.com/" target="_blank">
                    <img src="empresas/wbagadion.png" alt="wbagadion">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://www.naskadigital.com/" target="_blank">
                    <img src="empresas/naska.png" alt="naska digital">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="https://unity3d.com/es" target="_blank">
                    <img src="empresas/unity.png" alt="unity">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="https://www.redhat.com/es" target="_blank">
                    <img src="empresas/redhat.png" alt="redhat">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="https://www.gyffu.com/" target="_blank">
                    <img src="empresas/gyffu.png" alt="gyffu">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="" target="_blank">
                    <img src="empresas/racional.png" alt="Racional">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://elportaldelquindio.com/" target="_blank">
                    <img src="empresas/portal.png" alt="Portal del Quindío">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://www.unicentrodearmenia.com/" target="_blank">
                    <img src="empresas/unicentro.png" alt="Unicentro">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://www.armeniahotel.com.co/" target="_blank">
                    <img src="empresas/armeniahotel.png" alt="Armenia Hotel">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://fedesoft.org/" target="_blank">
                    <img src="empresas/fedesoft.png" alt="Fedesoft">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://www.consensussa.com/es/" target="_blank">
                    <img src="empresas/consensus.png" alt="Consensus">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://www.semana.com/" target="_blank">
                    <img src="empresas/semana.png" alt="semana">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://www.apuestasochoa.com.co/" target="_blank">
                    <img src="empresas/ochoa.png" alt="Apuestas ochoa">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://www.mintic.gov.co/" target="_blank">
                    <img src="empresas/mintic.png" alt="mintic">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="https://www.google.es/" target="_blank">
                    <img src="empresas/google.png" alt="google">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://www.softmanagement.com.co/" target="_blank">
                    <img src="empresas/softmanagement.png" alt="softmanagement">
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-4">
                <a href="http://www.siigo.com/" target="_blank">
                    <img src="empresas/siigo.png" alt="siigo">
                </a>
            </div>
        </div>
    </div>
 
<?php include('footer.php') ?>