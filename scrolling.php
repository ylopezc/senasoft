<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
</head>
<body style="padding:0px; margin:0px; background-color:#fff;font-family:Arial, sans-serif">

    <!-- #region Jssor Slider Begin -->

    <!-- Generator: Jssor Slider Maker -->
    <!-- Source: http://www.jssor.com/demos/scrolling-logo-thumbnail-slider.slider -->
    
    <!-- This demo works with jquery library -->

    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="js/jssor.slider-21.1.5.mini.js"></script>
    <!-- use jssor.slider-21.1.5.debug.js instead for debug -->
    <script>
        jQuery(document).ready(function ($) {
            
            var jssor_1_options = {
              $AutoPlay: true,
              $Idle: 0,
              $AutoPlaySteps: 4,
              $SlideDuration: 1600,
              $SlideEasing: $Jease$.$Linear,
              $PauseOnHover: 4,
              $SlideWidth: 170,
              $Cols: 7
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 809);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
    </script>



    <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1000px; height: 170px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=90); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('images/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1000px; height: 170px; overflow: hidden;">
            <div style="display: none;">
                <img data-u="image" src="empresas/SENA.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/cisco.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/cpe.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/digital-ware.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/ibm.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/lenovo.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/microsoft.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/telefonica.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/amazon.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/naska.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/wbagadion.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/unity.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/redhat.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/gyffu.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/racional.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/portal.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/unicentro.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/armeniahotel.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/fedesoft.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/consensus.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/semana.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/ochoa.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/mintic.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/google.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/softmanagement.png" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="empresas/siigo.png" />
            </div>
        </div>
    </div>

    <!-- #endregion Jssor Slider End -->
</body>
</html>
