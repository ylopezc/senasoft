<?php include('header.php') ?>
    	
        <br>
        <div id="wowslider-container0" class="hidden-xs">
            <div class="ws_images">
                <ul>
            		<li><img src="data0/images/banner1.jpg" alt="" title="" id="wows0_0"/></li>
            		<li><img src="data0/images/banner2.jpg" alt="" title="" id="wows0_1"/></li>
            		<li><img src="data0/images/banner3.jpg" alt="" title="" id="wows0_2"/></li>
                    <li><img src="data0/images/banner4.jpg" alt="" title="" id="wows0_3"/></li>
            	</ul>
            </div>

        	<div class="ws_bullets"><div>
        		<a href="#"><span><img src="data0/tooltips/banner1.jpg" alt="SENASOFT Quindío 2016"/>1</span></a>
        		<a href="#"><span><img src="data0/tooltips/banner2.jpg" alt="SENASOFT Quindío 2016"/>2</span></a>
        		<a href="#"><span><img src="data0/tooltips/banner3.jpg" alt="SENASOFT Quindío 2016"/>3</span></a>
                <a href="#"><span><img src="data0/tooltips/banner3.jpg" alt="SENASOFT Quindío 2016"/>4</span></a>
        	</div></div>

        <div class="ws_shadow"></div>
        </div>	
        <script type="text/javascript" src="engine0/wowslider.js"></script>
        <script type="text/javascript" src="engine0/script.js"></script>

        <div class="container-fluid">
            <div class="row">
            	<br>
                <img src="images/willys.png" class="col-xs-12 col-sm-6 col-md-3 img-responsive img-thumbnail"  />
                <div class="col-xs-12 col-sm-6 col-md-9 text-justify">
                    <p><br><strong>SENASOFT</strong> es el mayor encuentro de tecnología realizado por el SENA, como iniciativa de la Red de Conocimiento en Informática, Diseño y Desarrollo de Software y mediante el cual la Entidad propicia entre los aprendices, espacios que les permiten transferencia de conocimiento, en las habilidades más significativas de la formación TIC.</p>
                    <p>En el encuentro se convocan aprendices de los Centros que ofrecen Formación en programas en ejecución de la red de conocimiento en Informática, Diseño y Desarrollo de Software.</p>
                    <p>En esta séptima versión, SENASOFT reunirá a 400 aprendices y más de 90 instructores de los centros de que ofertan programas de formación pertenecientes a dicha red.</p>
                    <a href="archivos/proyecto_senasoft.pdf" class="btn btn-success" target="_blank">Descargar Proyecto SENASoft Quindío 2016</a>
                </div>
            </div>
            <br>
            <div class="row  img-responsive">
                <img src="images/empresas.png" class="col-xs-12">
            </div>
        </div>

        <br/>

        <div class="container-fluid">
            <div class="row">
                <?php include('scrolling.php') ?>
            </div>
        </div>

        <br><br>
 
<?php include('footer.php') ?>