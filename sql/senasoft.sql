-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 26-07-2016 a las 06:10:44
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `senasoft`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accion`
--

CREATE TABLE `accion` (
  `idAccion` smallint(6) NOT NULL,
  `accion` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `etiqueta` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL,
  `archivo` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `accion`
--

INSERT INTO `accion` (`idAccion`, `accion`, `estado`, `etiqueta`, `fechaCreacion`, `fechaModificacion`, `idUsuarioCreacion`, `idUsuarioModificacion`, `archivo`) VALUES
(1, 'adicionar', 1, 'adicionar', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 'aaaa'),
(2, 'modificar', 1, 'modificar', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 'dfsdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad`
--

CREATE TABLE `actividad` (
  `idActividad` int(11) NOT NULL,
  `idResponsable` smallint(6) NOT NULL,
  `actividad` varchar(500) NOT NULL,
  `descripcion` varchar(2000) NOT NULL,
  `idActividadEstado` tinyint(4) NOT NULL,
  `fecha` datetime NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividaddiaria`
--

CREATE TABLE `actividaddiaria` (
  `idActividadDiaria` int(11) NOT NULL,
  `idResponsable` smallint(6) NOT NULL,
  `idActividad` smallint(6) NOT NULL,
  `actividadDiaria` varchar(500) NOT NULL,
  `descripcion` varchar(2000) NOT NULL,
  `idActividadEstado` tinyint(4) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividadestado`
--

CREATE TABLE `actividadestado` (
  `idActividadEstado` tinyint(4) NOT NULL,
  `actividadEstado` varchar(100) NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aplicacion`
--

CREATE TABLE `aplicacion` (
  `idAplicacion` smallint(6) NOT NULL,
  `aplicacion` varchar(50) NOT NULL,
  `baseDatos` varchar(50) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `contrasena` varchar(100) NOT NULL,
  `puerto` smallint(6) NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aprendiz`
--

CREATE TABLE `aprendiz` (
  `idAprendiz` smallint(6) NOT NULL,
  `idPersona` int(11) NOT NULL,
  `idProgramaFormacion` smallint(6) NOT NULL,
  `idCentroFormacion` smallint(6) NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area`
--

CREATE TABLE `area` (
  `idArea` smallint(6) NOT NULL,
  `area` varchar(200) NOT NULL,
  `idAreaPadre` smallint(6) DEFAULT NULL,
  `idEmpresa` smallint(6) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistenteruedanegocio`
--

CREATE TABLE `asistenteruedanegocio` (
  `idAsistenteRuedaNegocio` smallint(6) NOT NULL,
  `asistenteRuedaNegocio` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `cargo` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `correoElectronico` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `horaAsistencia` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `idEmpresa` smallint(6) NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `asistenteruedanegocio`
--

INSERT INTO `asistenteruedanegocio` (`idAsistenteRuedaNegocio`, `asistenteRuedaNegocio`, `cargo`, `correoElectronico`, `telefono`, `horaAsistencia`, `estado`, `idEmpresa`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(4, 'Alexis', 'Desarrollador', 'alexismpv@misena.edu.co', '8702410', '9:00 PM', 1, 9, 1, 1, '2014-07-22 10:02:09', '2014-07-22 10:02:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoriaconcurso`
--

CREATE TABLE `categoriaconcurso` (
  `idCategoriaConcurso` smallint(6) NOT NULL,
  `categoriaConcurso` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `limiteNumeroAprendiz` tinyint(4) NOT NULL,
  `limiteNumeroInstructor` tinyint(4) NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categoriaconcurso`
--

INSERT INTO `categoriaconcurso` (`idCategoriaConcurso`, `categoriaConcurso`, `limiteNumeroAprendiz`, `limiteNumeroInstructor`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`, `estado`) VALUES
(1, 'Aplicaciones web PHP', 40, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(2, 'Algoritmia', 40, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(3, 'Diseño orientado a objetos ', 20, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(4, 'Base de datos', 40, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(5, 'Java web', 24, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(6, 'Aplicaciones .Net', 28, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(7, 'Aplicaciones Móviles Android', 20, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(8, 'Producción de multimedia', 20, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(9, 'Videojuegos', 16, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(10, 'Animación 3D', 16, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(11, 'Producción de medios audiovisuales', 20, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(12, 'Redes de datos', 24, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(13, 'Instalación y hardening de sistemas operativos', 36, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(14, 'Sistemas operativos de red', 20, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(15, 'Proyectos ágiles con SCRUM', 16, 6, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoriaproducto`
--

CREATE TABLE `categoriaproducto` (
  `idCategoriaProducto` smallint(6) NOT NULL,
  `CategoriaProducto` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `idUsuarioCreacion` smallint(6) DEFAULT NULL,
  `idUsuarioModificacion` smallint(6) DEFAULT NULL,
  `fechaCreacion` datetime DEFAULT NULL,
  `fechaModificacion` datetime DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centroformacion`
--

CREATE TABLE `centroformacion` (
  `idCentroFormacion` smallint(6) NOT NULL,
  `centroFormacion` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `idRegional` smallint(6) NOT NULL,
  `codigo` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL,
  `limiteAprendices` smallint(6) NOT NULL,
  `limiteInstructores` smallint(6) NOT NULL,
  `idUsuarioCreacion` int(11) DEFAULT NULL,
  `fechaCreacion` datetime DEFAULT NULL,
  `fechaModificacion` datetime DEFAULT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `centroformacion`
--

INSERT INTO `centroformacion` (`idCentroFormacion`, `centroFormacion`, `idRegional`, `codigo`, `limiteAprendices`, `limiteInstructores`, `idUsuarioCreacion`, `fechaCreacion`, `fechaModificacion`, `estado`) VALUES
(1, 'CENTRO PARA LA BIODIVERSIDAD Y EL TURISMO DEL AMAZONAS	', 1, '6b00e11b4e57974f8e625cbba687c76e', 2, 1, NULL, NULL, NULL, 1),
(2, 'CENTRO DE LOS RECURSOS NATURALES RENOVABLES - LA SALADA	', 2, '2373bbbbaf97da49d3b784b90daada8c', 2, 1, NULL, NULL, NULL, 1),
(3, 'CENTRO DE DISEÑO Y MANUFACTURA DEL CUERO', 2, 'e350e730f0cd45ed223f5c1a2ef91e61', 2, 1, NULL, NULL, NULL, 1),
(4, 'CENTRO DE FORMACION EN DISEÑO, CONFECCION Y MODA	', 2, 'bfb75971de3b39ae07dc18c4735e2cfe', 2, 1, NULL, NULL, NULL, 1),
(5, 'Centro para el Desarrollo del Hábitat y la Construcción	', 2, '0', 0, 0, NULL, NULL, NULL, 1),
(6, 'CENTRO DE TECNOLOGÍA DE LA MANUFACTURA AVANZADA	', 2, '1775c06cbbaec800c1986747552e6638', 8, 1, NULL, NULL, NULL, 1),
(7, 'CENTRO TECNOLÓGICO DEL MOBILIARIO', 2, '65703a43b57f395b774de5f5802e841d', 4, 1, NULL, NULL, NULL, 1),
(8, 'CENTRO TECNOLÓGICO DE GESTIÓN INDUSTRIAL	', 2, '3264d87e7145898ee42a2151e5cb0e06', 6, 1, NULL, NULL, NULL, 1),
(9, 'Centro de Comercio	', 2, '1', 0, 0, NULL, NULL, NULL, 1),
(10, 'Centro de Servicios de Salud	', 2, '2', 0, 0, NULL, NULL, NULL, 1),
(11, 'CENTRO DE SERVICIOS Y GESTION EMPRESARIAL', 2, '3c467d2adb1eb012ee649c82e58e12fa', 12, 1, NULL, NULL, NULL, 1),
(12, 'CENTRO DE LA INNOVACIÓN, LA AGROINDUSTRIA Y EL TURISMO	', 2, '93cf2b64a5085c3edccae9ab59ef1a97', 4, 1, NULL, NULL, NULL, 1),
(13, 'COMPLEJO TECNOLOGICO PARA LA GESTION AGROEMPRESARIAL', 2, 'db105df49b70bea0cbf05cfb303b6c62', 4, 1, NULL, NULL, NULL, 1),
(14, 'COMPLEJO TECNOLÓGICO MINERO AGROEMPRESARIAL', 2, 'd109765908c9d00577fe06bd30894fd3', 2, 1, NULL, NULL, NULL, 1),
(15, 'COMPLEJO TECNOLOGICO AGROINDUSTRIAL, PECUARIO Y TURISTICO	', 2, '3fb53774e0c3dc2c536595aad60386b5', 6, 1, NULL, NULL, NULL, 1),
(16, 'Complejo Tecnológico, Turístico y Agroindustrial del Occidente Antioqueño	', 2, '3', 0, 0, NULL, NULL, NULL, 1),
(17, 'CENTRO DE GESTION Y DESARROLLO AGROINDUSTRIAL DE ARAUCA	', 3, '9a1109caf6dbec697c55cfcfee9cc27d', 2, 1, NULL, NULL, NULL, 1),
(18, 'Centro para el Desarrollo Agroecológico y Agroindustrial	', 4, 'NULL', 0, 0, NULL, NULL, NULL, 1),
(19, 'CENTRO NACIONAL COLOMBO ALEMAN', 4, 'b9868204ad6fd575012a2dacea2659a0', 12, 1, NULL, NULL, NULL, 1),
(20, 'CENTRO INDUSTRIAL Y DE AVIACION', 4, 'ca7db5f498a1735410bd1699abd95595', 6, 1, NULL, NULL, NULL, 1),
(21, 'Centro de Comercio y Servicios	', 4, '5', 0, 0, NULL, NULL, NULL, 1),
(22, '	Centro Agroempresarial y Minero	', 5, NULL, 0, 0, NULL, NULL, NULL, 1),
(23, '	Centro Internacional Náutico Fluvial y Portuario 	', 5, NULL, 0, 0, NULL, NULL, NULL, 1),
(24, 'CENTRO PARA LA INDUSTRIA PETROQUIMICA', 5, 'ea94f74d06eb72bc8d09b3a5dc564442', 8, 1, NULL, NULL, NULL, 1),
(25, 'CENTRO DE COMERCIO Y SERVICIOS', 5, '723a80d6daa88178479a426a40286b93', 6, 1, NULL, NULL, NULL, 1),
(26, '	Centro de Desarrollo Agropecuario y Agroindustrial	', 6, NULL, 0, 0, NULL, NULL, NULL, 1),
(27, 'CENTRO MINERO', 6, '321ee3bca38d5e84a6644640f40dcc9f', 2, 1, NULL, NULL, NULL, 1),
(28, 'CENTRO DE GESTION ADMINISTRATIVA Y FORTALECIMIENTO EMPRESARIAL', 6, '05b0a4493b221f2afe4e7cf9b3d69517', 4, 1, NULL, NULL, NULL, 1),
(29, 'CENTRO INDUSTRIAL DE MANTENIMIENTO Y MANUFACTURA	', 6, 'd42669660758e3c57c408f5e58b28214', 4, 1, NULL, NULL, NULL, 1),
(30, '	Centro para la Formación Cafetera	', 7, NULL, 0, 0, NULL, NULL, NULL, 1),
(31, 'CENTRO DE AUTOMATIZACION INDUSTRIAL	', 7, 'b8519470cf15718cac8b6ffc79e14008', 8, 1, NULL, NULL, NULL, 1),
(32, 'CENTRO DE PROCESOS INDUSTRIALES', 7, '0904912ff8f7e2df2d2e9770a90f30ae', 2, 1, NULL, NULL, NULL, 1),
(33, 'CENTRO PECUARIO Y AGROEMPRESARIAL', 7, '76c1d9ece478199c59f80e6f794e9af5', 2, 1, NULL, NULL, NULL, 1),
(34, 'CENTRO DE COMERCIO Y SERVICIOS', 7, 'c23bb7e438e898874bba960ca10be383', 2, 1, NULL, NULL, NULL, 1),
(35, 'CENTRO TECNOLOGICO DE LA AMAZONIA	', 8, 'b90bc941c15b8cecccfaf18d4687a769', 2, 1, NULL, NULL, NULL, 1),
(36, 'CENTRO AGROINDUSTRIAL Y FORTALECIMIENTO EMPRESARIAL DE CASANARE', 9, 'c7a9894219bea537a28c8221c61116b2', 2, 1, NULL, NULL, NULL, 1),
(37, 'CENTRO AGROPECUARIO', 10, '8d7610ed7314aaa6c6a63cc6bc89698d', 2, 1, NULL, NULL, NULL, 1),
(38, 'CENTRO DE TELEINFORMATICA Y PRODUCCIÓN INDUSTRIAL	', 10, '6155556d10d2d701287fd01566a986ae', 12, 1, NULL, NULL, NULL, 1),
(39, 'CENTRO DE COMERCIO Y SERVICIOS', 10, '115d86f26129056dbd1fbfe9ad3cf7a1', 8, 1, NULL, NULL, NULL, 1),
(40, 'CENTRO BIOTECNOLOGICO DEL CARIBE', 11, '31a91582eee48753071b78cf7c192b15', 4, 1, NULL, NULL, NULL, 1),
(41, 'CENTRO AGROEMPRESARIAL', 11, 'c0af000e5e4b54f30a4cdaa3197b684a', 2, 1, NULL, NULL, NULL, 1),
(42, 'CENTRO DE OPERACIÓN Y MANTENIMIENTO MINERO	', 11, '598da80c420f1dbbf7b0acca07c03378', 6, 1, NULL, NULL, NULL, 1),
(43, 'CENTRO DE RECURSOS NATURALES, INDUSTRIA Y BIODIVERSIDAD', 12, '3bc7a94da93444e9356949187a89081e', 4, 1, NULL, NULL, NULL, 1),
(44, 'CENTRO AGROPECUARIO Y DE BIOTECNOLOGIA EL PORVENIR	', 13, '8bffbe37b8cc7f032e97c6aacc645171', 2, 1, NULL, NULL, NULL, 1),
(45, 'CENTRO DE COMERCIO, INDUSTRIA Y TURISMO DE CORDOBA	', 13, '3c96b151dcbfdc7cc5f3364b81aa8267', 2, 1, NULL, NULL, NULL, 1),
(46, 'Regional Cundinamarca	', 14, NULL, 0, 0, NULL, NULL, NULL, 1),
(47, 'CENTRO DE DESARROLLO AGROINDUSTRIAL Y EMPRESARIAL	', 14, '5df4279ca8ea92e4fc2d89f1cd54266c', 2, 1, NULL, NULL, NULL, 1),
(48, 'CENTRO AGROECOLOGICO Y EMPRESARIAL	', 14, 'cdec95795e5bf8e0f416e8d609038de8\r\n', 2, 1, NULL, NULL, NULL, 1),
(49, 'CENTRO DE LA TECNOLOGIA DEL DISEÑO Y LA PRODUCTIVIDAD EMPRESARIAL', 14, '8c6e794701a7342e10dc8ba042d0b224', 2, 1, NULL, NULL, NULL, 1),
(50, 'CENTRO DE BIOTECNOLOGIA AGROPECUARIA	', 14, '14e860aae8347b2338355f5dee57283a', 4, 1, NULL, NULL, NULL, 1),
(51, 'CENTRO DE DESARROLLO AGROEMPRESARIAL', 14, '2b63281328fc17992f46b62c6643a51c', 4, 1, NULL, NULL, NULL, 1),
(52, 'CENTRO INDUSTRIAL Y DESARROLLO EMPRESARIAL DE SOACHA	', 14, '1eba28a3098441891337b96f2e55a8f3', 8, 1, NULL, NULL, NULL, 1),
(53, 'Centro de Tecnologías para la Construcción y la Madera	', 15, NULL, 0, 0, NULL, NULL, NULL, 1),
(54, 'CENTRO DE ELECTRICIDAD, ELECTRÓNICA Y TELECOMUNICACIONES	', 15, '28178324b1038a4570f674b9497728fe', 12, 1, NULL, NULL, NULL, 1),
(55, 'Centro de Gestión Industrial	', 15, NULL, 0, 0, NULL, NULL, NULL, 1),
(56, 'Centro de Manufacturas en Textiles y Cuero	', 15, NULL, 0, 0, NULL, NULL, NULL, 1),
(57, 'Centro de Tecnologías del Transporte	', 15, NULL, 0, 0, NULL, NULL, NULL, 1),
(58, 'Centro Metalmecánico	', 15, NULL, 0, 0, NULL, NULL, NULL, 1),
(59, 'CENTRO DE MATERIALES Y ENSAYOS', 15, '10581833696b3e9c0150b9b9f2ead8d1', 2, 1, NULL, NULL, NULL, 1),
(60, 'CENTRO DE DISEÑO Y METROLOGIA', 15, '66079f53b102d7d9ba1b08a81b523454', 4, 1, NULL, NULL, NULL, 1),
(61, 'CENTRO PARA LA INDUSTRIA DE LA COMUNICACIÓN GRAFICA	', 15, 'e1d4f3daacff5787030ba728c67c2edf', 8, 1, NULL, NULL, NULL, 1),
(62, 'CENTRO DE GESTION DE MERCADOS, LOGISTICA Y TECNOLOGIAS DE LA INFORMACION', 15, '29a54c140cf08b7adb170700a536f0e6', 12, 1, NULL, NULL, NULL, 1),
(63, 'Centro de Formación de Talento Humano en Salud	', 15, NULL, 0, 0, NULL, NULL, NULL, 1),
(64, 'Centro de Gestión Administrativa	', 15, NULL, 0, 0, NULL, NULL, NULL, 1),
(65, 'CENTRO DE SERVICIOS FINANCIEROS', 15, 'bdd90252a8cc7bd7e2211e508c7e6eb0', 4, 1, NULL, NULL, NULL, 1),
(66, 'Centro Nacional de Hotelería, Turismo y Alimentos	', 15, NULL, 0, 0, NULL, NULL, NULL, 1),
(67, 'Centro de Formación en Actividad Física y Cultura.	', 15, NULL, 0, 0, NULL, NULL, NULL, 1),
(68, 'CENTRO AMBIENTAL Y ECOTURISTICO DEL NORORIENTE AMAZONICO	', 16, '9cc400251cfe354360223932a7284304', 2, 1, NULL, NULL, NULL, 1),
(69, 'CENTRO DE DESARROLLO AGROINDUSTRIAL, TURISTICO Y TECNOLOGICO DEL GUAVIARE', 17, '', 0, 0, NULL, NULL, NULL, 1),
(70, 'CENTRO DE FORMACION AGROINDUSTRIAL', 18, '8fb5938a5f697499d567701266b706d7', 2, 1, NULL, NULL, NULL, 1),
(71, 'CENTRO AGROEMPRESARIAL Y DESARROLLO PECUARIO DEL HUILA	', 18, 'e2c2287dd9021d3d23a1924416bf975d', 2, 1, NULL, NULL, NULL, 1),
(72, 'CENTRO DE DESARROLLO AGROEMPRESARIAL Y TURISTICO DEL HUILA', 18, '9fe752119f50dd23132ad07c8128bda4', 2, 1, NULL, NULL, NULL, 1),
(73, 'CENTRO DE LA INDUSTRIA, LA EMPRESA Y LOS SERVICIOS', 18, '3b5b8ff596f1452431286ab0493eb6b9', 10, 1, NULL, NULL, NULL, 1),
(74, 'CENTRO DE GESTION Y DESARROLLO SOSTENIBLE SURCOLOMBIANO	', 18, '88549869d29e4447c023510b67b496f6', 4, 1, NULL, NULL, NULL, 1),
(75, 'CENTRO ACUICOLA Y AGROINDUSTRIAL DE GAIRA	', 19, '74131b8bc8106d9ac5243279d7b2dba2', 2, 1, NULL, NULL, NULL, 1),
(76, 'CENTRO DE LOGISTICA Y PROMOCION ECOTURISTICA DEL MAGDALENA	', 19, '18b9549e646f65a860152fbbae1293fd', 2, 1, NULL, NULL, NULL, 1),
(77, 'CENTRO AGROINDUSTRIAL DEL META', 20, '0117f5f3f49691073928e7fed2473c24', 2, 1, NULL, NULL, NULL, 1),
(78, 'CENTRO DE INDUSTRIA Y SERVICIOS DEL META	', 20, '1a7ed575a2cfb409a3e6ecf785d2bc43', 2, 1, NULL, NULL, NULL, 1),
(79, 'CENTRO SUR COLOMBIANO DE LOGÍSTICA INTERNACIONAL', 21, '2ddccdf41aee6f1e260d8cd52331b006', 2, 1, NULL, NULL, NULL, 1),
(80, 'CENTRO AGROINDUSTRIAL Y PESQUERO DE LA COSTA PACIFICA	', 21, '3506d9710a2fd5376f1198d5581afd92', 2, 1, NULL, NULL, NULL, 1),
(81, 'CENTRO INTERNACIONAL DE PRODUCCIÓN LIMPIA - LOPE	', 21, '32c8f937f1148d7c82c9f65e17845e41', 2, 1, NULL, NULL, NULL, 1),
(82, 'CENTRO DE FORMACIÓN PARA EL DESARROLLO RURAL Y MINERO ', 22, '60c69fa447514f22599fc321e575c23c', 6, 1, NULL, NULL, NULL, 1),
(83, 'CENTRO DE LA INDUSTRIA, LA EMPRESA Y LOS SERVICIOS', 22, 'fd5232027a6da8583ed8ad20702131ee', 6, 1, NULL, NULL, NULL, 1),
(84, 'CENTRO AGROFORESTAL Y ACUICOLA ARAPAIMA	', 23, 'b3a7580539b4441098c4f1396bbfd188', 2, 1, NULL, NULL, NULL, 1),
(85, 'Centro Agroindustrial	', 24, NULL, 0, 0, NULL, NULL, NULL, 1),
(86, 'CENTRO PARA EL DESARROLLO TECNOLÓGICO DE LA CONSTRUCCIÓN Y LA INDUSTRIA', 24, '5f601db76be4a1f088d1db85b22c9118', 6, 1, NULL, NULL, NULL, 1),
(87, 'CENTRO DE COMERCIO Y TURISMO', 24, 'fbbd0445074531bbc0962bb7d1e90445', 12, 1, NULL, NULL, NULL, 1),
(88, 'CENTRO ATENCION SECTOR AGROPECUARIO	', 25, 'c413ab423cd861736b5042cb75331e39', 2, 1, NULL, NULL, NULL, 1),
(89, 'CENTRO DE DISEÑO E INNOVACIÓN TECNOLOGICA INDUSTRIAL', 25, '1abb488ccf5fce7ddc2c2e6f4c7573d8', 8, 1, NULL, NULL, NULL, 1),
(90, 'Centro de Comercio y Servicios	', 25, NULL, 0, 0, NULL, NULL, NULL, 1),
(91, 'CENTRO DE FORMACION TURISTICA, GENTE DE MAR Y DE SERVICIOS', 26, '4402a6e2f53836f82765316f392c03ee', 2, 1, NULL, NULL, NULL, 1),
(92, 'CENTRO ATENCION SECTOR AGROPECUARIO	', 27, '215b20e74c53e45f04aa6685609aa2ee', 2, 1, NULL, NULL, NULL, 1),
(93, 'CENTRO INDUSTRIAL DE MANTENIMIENTO INTEGRAL', 27, 'de576474860e19f97a866a1d792df9fb', 8, 1, NULL, NULL, NULL, 1),
(94, 'CENTRO INDUSTRIAL DEL DISEÑO Y LA MANUFACTURA', 27, '84d9e93a30b39e18aea735687d640299', 2, 1, NULL, NULL, NULL, 1),
(95, 'CENTRO DE SERVICIOS EMPRESARIALES Y TURISTICOS	', 27, 'd4a70a5139b0ef950c6a0b7e96724231', 2, 1, NULL, NULL, NULL, 1),
(96, 'CENTRO INDUSTRIAL Y DEL DESARROLLO TECNOLOGICO', 27, '5faef438a29e9c6492c9cd3f9810d594', 2, 1, NULL, NULL, NULL, 1),
(97, 'CENTRO AGROTURISTICO', 27, '971c49a143bb835528540c08dd2fa05f', 4, 1, NULL, NULL, NULL, 1),
(98, 'CENTRO AGROEMPRESARIAL Y TURISTICO DE LOS ANDES	', 27, '4c1a9b9fe8881f015cfe2992a9176145', 2, 1, NULL, NULL, NULL, 1),
(99, 'CENTRO DE GESTION AGROEMPRESARIAL DEL ORIENTE	', 27, '42fbe44de67ab3e0aec4bb2ef9a395a6', 2, 1, NULL, NULL, NULL, 1),
(100, 'CENTRO DE LA INNOVACION, LA TECNOLOGIA Y LOS SERVICIOS	', 28, 'ad6bd68d5ed6ba1f8bcaf7f63010264a', 2, 1, NULL, NULL, NULL, 1),
(101, 'CENTRO AGROPECUARIO LA GRANJA', 29, 'cd853b1db46aa0fe72e034151ceb5a0a', 2, 1, NULL, NULL, NULL, 1),
(102, 'CENTRO DE INDUSTRIA Y CONSTRUCCION	', 29, '86f293eb82d5d5f4aebde4e5d2024837', 8, 1, NULL, NULL, NULL, 1),
(103, 'CENTRO DE COMERCIO Y SERVICIOS', 29, 'bf8e446b6b51a089f68c374902c210f3', 4, 1, NULL, NULL, NULL, 1),
(104, 'CENTRO AGROPECUARIO DE BUGA', 30, '3190f200a52c5573cb0b37ed5025d737', 2, 1, NULL, NULL, NULL, 1),
(105, 'CENTRO LATINOAMERICANO DE ESPECIES MENORES	', 30, '62094dba14684c20713ed1e522293542', 2, 1, NULL, NULL, NULL, 1),
(106, 'CENTRO NAUTICO PESQUERO DE BUENAVENTURA	', 30, '8f1e9917b12ce757be4750ef65f76ae0', 2, 1, NULL, NULL, NULL, 1),
(107, 'CENTRO DE ELECTRICIDAD Y AUTOMATIZACION INDUSTRIAL - CEAI	', 30, '72f2fe3d3d4741b38e1109b99b616dec', 8, 1, NULL, NULL, NULL, 1),
(108, 'Centro de la Construcción	', 30, NULL, 0, 0, NULL, NULL, NULL, 1),
(109, 'CENTRO DE DISEÑO TECNOLOGICO INDUSTRIAL	', 30, '464466f5ae72d43681ca123fd37b9a9c', 6, 1, NULL, NULL, NULL, 1),
(110, 'Centro Nacional de Asistencia Técnica  a la Industria - Astin	', 30, '6', 0, 0, NULL, NULL, NULL, 1),
(111, 'Centro de Gestión Tecnológica de Servicios	', 30, NULL, 0, 0, NULL, NULL, NULL, 1),
(112, 'CENTRO DE TECNOLOGIAS AGROINDUSTRIALES	', 30, '2a31683c0ed454c43c636ba10eaedc4b', 8, 1, NULL, NULL, NULL, 1),
(113, 'CENTRO DE BIOTECNOLOGIA INDUSTRIAL	', 30, '48ad6a75b425478996a202b57bb91043', 2, 1, NULL, NULL, NULL, 1),
(114, 'CENTRO AGROPECUARIO Y DE SERVICIOS AMBIENTALES JIRI - JIRIMO', 31, 'cba5932b1044d5517d954afd1a7d9fa7', 2, 1, NULL, NULL, NULL, 1),
(115, 'CENTRO DE PRODUCCIÓN Y TRANSFORMACION AGROINDUSTRIAL DE LA ORINOQUIA', 32, '58c8cd0db11e28fd99940a60bebdce97', 2, 1, NULL, NULL, NULL, 1),
(116, 'CENTRO INDUSTRIAL Y DE ENERGIAS ALTERNATIVAS	', 33, 'f100417d85e0502b71af0c6dbf4bedb7', 2, 1, NULL, NULL, NULL, 1),
(117, 'CENTRO AGROEMPRESARIAL Y ACUICOLA	', 33, '4bbb6e75dbb74c65237a78382e82971e', 2, 1, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `idDepartamento` smallint(6) NOT NULL,
  `referencia` char(2) COLLATE utf8_spanish_ci DEFAULT NULL,
  `departamento` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fechaCreacion` datetime DEFAULT NULL,
  `fechaModificacion` datetime DEFAULT NULL,
  `idUsuarioCreacion` smallint(6) DEFAULT NULL,
  `idUsuarioModificacion` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`idDepartamento`, `referencia`, `departamento`, `estado`, `fechaCreacion`, `fechaModificacion`, `idUsuarioCreacion`, `idUsuarioModificacion`) VALUES
(1, NULL, 'Amazonas', 1, NULL, NULL, NULL, NULL),
(2, NULL, 'Antioquia', 1, NULL, NULL, NULL, NULL),
(3, NULL, 'Arauca', 1, NULL, NULL, NULL, NULL),
(4, NULL, 'Atlántico', 1, NULL, NULL, NULL, NULL),
(5, NULL, 'Bolívar', 1, NULL, NULL, NULL, NULL),
(6, NULL, 'Boyacá', 1, NULL, NULL, NULL, NULL),
(7, NULL, 'Caldas', 1, NULL, NULL, NULL, NULL),
(8, NULL, 'Caquetá', 1, NULL, NULL, NULL, NULL),
(9, NULL, 'Casanare', 1, NULL, NULL, NULL, NULL),
(10, NULL, 'Cauca', 1, NULL, NULL, NULL, NULL),
(11, NULL, 'Cesar', 1, NULL, NULL, NULL, NULL),
(12, NULL, 'Chocó', 1, NULL, NULL, NULL, NULL),
(13, NULL, 'Córdoba', 1, NULL, NULL, NULL, NULL),
(14, NULL, 'Cundinamarca', 1, NULL, NULL, NULL, NULL),
(15, NULL, 'Güainia', 1, NULL, NULL, NULL, NULL),
(16, NULL, 'Guaviare', 1, NULL, NULL, NULL, NULL),
(17, NULL, 'Huila', 1, NULL, NULL, NULL, NULL),
(18, NULL, 'La Guajira', 1, NULL, NULL, NULL, NULL),
(19, NULL, 'Magdalena', 1, NULL, NULL, NULL, NULL),
(20, NULL, 'Meta', 1, NULL, NULL, NULL, NULL),
(21, NULL, 'Nariño', 1, NULL, NULL, NULL, NULL),
(22, NULL, 'Norte de Santander', 1, NULL, NULL, NULL, NULL),
(23, NULL, 'Putumayo', 1, NULL, NULL, NULL, NULL),
(24, NULL, 'Quindío', 1, NULL, NULL, NULL, NULL),
(25, NULL, 'Risaralda', 1, NULL, NULL, NULL, NULL),
(26, NULL, 'San Andrés y Providencia', 1, NULL, NULL, NULL, NULL),
(27, NULL, 'Santander', 1, NULL, NULL, NULL, NULL),
(28, NULL, 'Sucre', 1, NULL, NULL, NULL, NULL),
(29, NULL, 'Tolima', 1, NULL, NULL, NULL, NULL),
(30, NULL, 'Valle del Cauca', 1, NULL, NULL, NULL, NULL),
(31, NULL, 'Vaupés', 1, NULL, NULL, NULL, NULL),
(32, NULL, 'Vichada', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `idEmpresa` smallint(6) NOT NULL,
  `nit` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `empresa` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `representanteLegal` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `telefonoCelular` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `telefonoFijo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `correoElectronico` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `url` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idMunicipio` smallint(6) NOT NULL,
  `idRegimenEmpresa` smallint(6) NOT NULL,
  `idSectorEconomico` smallint(6) NOT NULL,
  `sectorEconomico` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `idUsuarioCreacion` smallint(6) DEFAULT NULL,
  `idUsuarioModificacion` smallint(6) DEFAULT NULL,
  `fechaCreacion` datetime DEFAULT NULL,
  `fechaModificacion` datetime DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresacodigo`
--

CREATE TABLE `empresacodigo` (
  `idEmpresaCodigo` tinyint(4) NOT NULL,
  `idEmpresa` smallint(6) NOT NULL,
  `empresaCodigo` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `nit` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `empresa` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `correoElectronico` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `idEstadoEmpresa` smallint(6) NOT NULL,
  `idUsuarioCreacion` smallint(6) DEFAULT NULL,
  `idUsuarioModificacion` smallint(6) DEFAULT NULL,
  `fechaCreacion` datetime DEFAULT NULL,
  `fechaModificacion` datetime DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresaproducto`
--

CREATE TABLE `empresaproducto` (
  `idEmpresaProducto` smallint(6) NOT NULL,
  `empresaProducto` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `idEmpresa` smallint(6) NOT NULL,
  `idCategoriaProducto` smallint(6) NOT NULL,
  `idTipoEmpresaProducto` smallint(6) NOT NULL,
  `frecuencia` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `idUsuarioCreacion` smallint(6) DEFAULT NULL,
  `idUsuarioModificacion` smallint(6) DEFAULT NULL,
  `fechaCreacion` datetime DEFAULT NULL,
  `fechaModificacion` datetime DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `empresaproducto`
--

INSERT INTO `empresaproducto` (`idEmpresaProducto`, `empresaProducto`, `idEmpresa`, `idCategoriaProducto`, `idTipoEmpresaProducto`, `frecuencia`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`, `estado`) VALUES
(20, 'aaaaa', 8, 2, 3, '', NULL, NULL, '2014-07-22 10:00:22', '2014-07-22 10:00:22', NULL),
(21, '', 9, 1, 1, 'Diaria', NULL, NULL, '2014-07-22 10:02:08', '2014-07-22 10:02:08', NULL),
(22, '', 9, 1, 2, '', NULL, NULL, '2014-07-22 10:02:09', '2014-07-22 10:02:09', NULL),
(23, 'fgfgfg', 10, 1, 3, '', NULL, NULL, '2014-07-22 17:39:24', '2014-07-22 17:39:24', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empreunidanegoc`
--

CREATE TABLE `empreunidanegoc` (
  `idEmpreUnidaNegoc` smallint(6) NOT NULL,
  `empreUnidaNegoc` varchar(300) NOT NULL,
  `idEmpresa` smallint(6) NOT NULL,
  `idUnidadNegocio` tinyint(4) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` tinyint(4) NOT NULL,
  `idUsuarioModificacion` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadoempresa`
--

CREATE TABLE `estadoempresa` (
  `idEstadoEmpresa` smallint(6) NOT NULL,
  `estadoEmpresa` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `idUsuarioCreacion` smallint(6) DEFAULT NULL,
  `idUsuarioModificacion` smallint(6) DEFAULT NULL,
  `fechaCreacion` datetime DEFAULT NULL,
  `fechaModificacion` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `estadoempresa`
--

INSERT INTO `estadoempresa` (`idEstadoEmpresa`, `estadoEmpresa`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'Solicitado', 1, NULL, NULL, NULL, NULL),
(2, 'Autorizado', 1, NULL, NULL, NULL, NULL),
(3, 'Cancelado', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formulario`
--

CREATE TABLE `formulario` (
  `idFormulario` smallint(6) NOT NULL,
  `idMenu` smallint(6) NOT NULL,
  `etiqueta` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `ubicacion` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `formulario` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `formulario`
--

INSERT INTO `formulario` (`idFormulario`, `idMenu`, `etiqueta`, `ubicacion`, `formulario`, `estado`, `fechaCreacion`, `fechaModificacion`, `idUsuarioCreacion`, `idUsuarioModificacion`) VALUES
(1, 2, 'Cambiar contraseña', 'Seguridad', '/Senasoft-admin/vista/frmCambiarContrasenia.html', 1, '2014-02-27 00:00:00', '2014-04-20 23:48:06', 1, 1),
(3, 2, 'Roles', 'Seguridad', '/Senasoft-admin/vista/fbeRoles.html', 1, '2014-02-27 00:00:00', '2014-04-20 23:49:09', 1, 1),
(4, 2, 'Acciones', 'Seguridad', '/Senasoft-admin/vista/fbeAcciones.html', 1, '2014-02-27 00:00:00', '2014-04-20 23:50:19', 1, 1),
(5, 2, 'Formularios', 'Seguridad', '/Senasoft-admin/vista/fbeFormulario.html', 1, '2014-02-27 00:00:00', '2014-04-20 23:50:55', 1, 1),
(6, 2, 'Carpetas', 'Seguridad', '/Senasoft-admin/vista/fbeCarpetas.html', 1, '2014-02-27 00:00:00', '2014-04-21 00:11:37', 1, 1),
(14, 2, 'Personas', '', '/Senasoft-admin/vista/fbePersona.html', 1, '2014-02-27 00:00:00', '2014-02-27 00:00:00', 1, 1),
(29, 2, 'Usuarios', 'Seguridad', '/Senasoft-admin/vista/fbeUsuario.html', 1, '2014-04-02 22:40:48', '2014-04-02 22:40:48', 1, 1),
(30, 2, 'Actividad', '', '/Senasoft-admin/vista/fbeActividad.html', 1, '2014-04-02 22:40:48', '2014-04-02 22:40:48', 1, 1),
(35, 6, 'Autorizar Inscripción', 'Administracion', '/Senasoft-admin/vista/fbeInscripcionEmpresa.html', 1, '2014-05-07 07:55:26', '2014-05-07 07:55:26', 1, 1),
(36, 3, 'Preguntas Frecuentes', 'Administración', '/Senasoft-admin/vista/frmPreguntasFrecuentes.html', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(37, 3, 'Mensajes', 'Administracion', '/Senasoft-admin/vista/fbeMensajeContacto.html', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0),
(38, 6, 'Asistentes', 'Administracion', '/Senasoft-admin/vista/fbeAsistRuedaNegoc.html', 1, '2014-07-21 00:00:00', '2014-07-21 00:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formularioaccion`
--

CREATE TABLE `formularioaccion` (
  `idFormularioAccion` smallint(6) NOT NULL,
  `idFormulario` smallint(6) NOT NULL,
  `idAccion` smallint(6) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscricategoconcuraprend`
--

CREATE TABLE `inscricategoconcuraprend` (
  `idInscriCategoConcurAprend` smallint(6) NOT NULL,
  `idProgramaFormacion` smallint(6) NOT NULL,
  `idCentroFormacion` smallint(6) NOT NULL,
  `idCategoriaConcurso` smallint(6) NOT NULL,
  `idAprendiz` smallint(6) NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscricategoconcurinstru`
--

CREATE TABLE `inscricategoconcurinstru` (
  `idInscriCateroConcurInstru` smallint(6) NOT NULL,
  `idCentroFormacion` smallint(6) NOT NULL,
  `idCategoriaConcurso` smallint(6) NOT NULL,
  `idInstructor` smallint(6) NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instructor`
--

CREATE TABLE `instructor` (
  `idInstructor` smallint(6) NOT NULL,
  `idPersona` int(11) NOT NULL,
  `idCentroFormacion` smallint(6) NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `leguajeprogramacion`
--

CREATE TABLE `leguajeprogramacion` (
  `idLeguajeProgramacion` smallint(6) NOT NULL,
  `leguajeProgramacion` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `leguajeprogramacion`
--

INSERT INTO `leguajeprogramacion` (`idLeguajeProgramacion`, `leguajeProgramacion`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`, `estado`) VALUES
(1, 'No aplica', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(2, 'Java', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(3, 'C#', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(4, 'PHP', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(5, 'Python', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(6, 'C', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(7, 'C++', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(8, 'JavaScript', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(9, 'ActionScript', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(10, 'Visual Basic .NET', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(12, 'Ruby', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajecontacto`
--

CREATE TABLE `mensajecontacto` (
  `idMensajeContacto` int(11) NOT NULL,
  `remitente` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `mensajeContacto` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `rol` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `asunto` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `idCentroFormacion` smallint(6) NOT NULL,
  `correoElectronico` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajerespuesta`
--

CREATE TABLE `mensajerespuesta` (
  `idRespuesta` smallint(6) NOT NULL,
  `idMensajeContacto` smallint(6) NOT NULL,
  `respuesta` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `idMenu` smallint(6) NOT NULL,
  `etiqueta` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `idMenuPadre` smallint(6) DEFAULT NULL,
  `menu` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `ubicacion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`idMenu`, `etiqueta`, `idMenuPadre`, `menu`, `ubicacion`, `estado`, `fechaCreacion`, `fechaModificacion`, `idUsuarioCreacion`, `idUsuarioModificacion`) VALUES
(1, 'Inicio', NULL, 'carInicio', '', 1, '2014-01-11 00:00:00', '2014-01-11 00:00:00', 1, 1),
(2, 'Seguridad', NULL, 'carSeguridad', '', 1, '2014-01-11 00:00:00', '2014-01-11 00:00:00', 1, 1),
(3, 'Mensajería', NULL, 'carAdministracion', NULL, 1, '2014-01-11 00:00:00', '2014-01-11 00:00:00', 1, 1),
(4, 'Competencias', NULL, 'carCompetencias', NULL, 1, '2014-01-11 00:00:00', '2014-01-11 00:00:00', 1, 1),
(5, 'Proyecto', NULL, 'carProyecto', NULL, 1, '2014-01-11 00:00:00', '2014-01-11 00:00:00', 1, 1),
(6, 'Rueda de negocios', NULL, 'carRuedaNegoc', NULL, 1, '2014-01-11 00:00:00', '2014-01-11 00:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `motorbasedatos`
--

CREATE TABLE `motorbasedatos` (
  `idMotorBaseDatos` smallint(6) NOT NULL,
  `motorBaseDatos` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `motorbasedatos`
--

INSERT INTO `motorbasedatos` (`idMotorBaseDatos`, `motorBaseDatos`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`, `estado`) VALUES
(1, 'No aplica', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(2, 'MySQL', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(3, 'PostgreSQL', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(4, 'Oracle', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(5, 'SQLite', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(6, 'SQL Server', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(8, 'Informix', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(9, 'Microsoft Access', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(10, 'Sybase', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(11, 'Firebird', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(12, 'MongoDB', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipio`
--

CREATE TABLE `municipio` (
  `idMunicipio` smallint(6) NOT NULL,
  `referencia` char(5) COLLATE utf8_spanish_ci DEFAULT NULL,
  `municipio` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `idDepartamento` smallint(6) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fechaCreacion` datetime DEFAULT NULL,
  `fechaModificacion` datetime DEFAULT NULL,
  `idUsuarioCreacion` smallint(6) DEFAULT NULL,
  `idUsuarioModificacion` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `municipio`
--

INSERT INTO `municipio` (`idMunicipio`, `referencia`, `municipio`, `idDepartamento`, `estado`, `fechaCreacion`, `fechaModificacion`, `idUsuarioCreacion`, `idUsuarioModificacion`) VALUES
(12, NULL, '	Leticia	', 1, 1, NULL, NULL, NULL, NULL),
(13, NULL, '	El encanto	', 1, 1, NULL, NULL, NULL, NULL),
(14, NULL, '	La chorrera	', 1, 1, NULL, NULL, NULL, NULL),
(15, NULL, '	La pedrera	', 1, 1, NULL, NULL, NULL, NULL),
(16, NULL, '	La victoria	', 1, 1, NULL, NULL, NULL, NULL),
(17, NULL, '	Miriti - parana	', 1, 1, NULL, NULL, NULL, NULL),
(18, NULL, '	Puerto alegria	', 1, 1, NULL, NULL, NULL, NULL),
(19, NULL, '	Puerto arica	', 1, 1, NULL, NULL, NULL, NULL),
(20, NULL, '	Puerto nariño	', 1, 1, NULL, NULL, NULL, NULL),
(21, NULL, '	Puerto santander	', 1, 1, NULL, NULL, NULL, NULL),
(22, NULL, '	Tarapaca	', 1, 1, NULL, NULL, NULL, NULL),
(23, NULL, '	Medellin	', 2, 1, NULL, NULL, NULL, NULL),
(24, NULL, '	Abejorral	', 2, 1, NULL, NULL, NULL, NULL),
(25, NULL, '	Abriaqui	', 2, 1, NULL, NULL, NULL, NULL),
(26, NULL, '	Alejandria	', 2, 1, NULL, NULL, NULL, NULL),
(27, NULL, '	Amaga	', 2, 1, NULL, NULL, NULL, NULL),
(28, NULL, '	Amalfi	', 2, 1, NULL, NULL, NULL, NULL),
(29, NULL, '	Andes	', 2, 1, NULL, NULL, NULL, NULL),
(30, NULL, '	Angelopolis	', 2, 1, NULL, NULL, NULL, NULL),
(31, NULL, '	Angostura	', 2, 1, NULL, NULL, NULL, NULL),
(32, NULL, '	Anori	', 2, 1, NULL, NULL, NULL, NULL),
(33, NULL, '	Santafe de antioquia	', 2, 1, NULL, NULL, NULL, NULL),
(34, NULL, '	Anza	', 2, 1, NULL, NULL, NULL, NULL),
(35, NULL, '	Apartado	', 2, 1, NULL, NULL, NULL, NULL),
(36, NULL, '	Arboletes	', 2, 1, NULL, NULL, NULL, NULL),
(37, NULL, '	Argelia	', 2, 1, NULL, NULL, NULL, NULL),
(38, NULL, '	Armenia	', 2, 1, NULL, NULL, NULL, NULL),
(39, NULL, '	Barbosa	', 2, 1, NULL, NULL, NULL, NULL),
(40, NULL, '	Belmira	', 2, 1, NULL, NULL, NULL, NULL),
(41, NULL, '	Bello	', 2, 1, NULL, NULL, NULL, NULL),
(42, NULL, '	Betania	', 2, 1, NULL, NULL, NULL, NULL),
(43, NULL, '	Betulia	', 2, 1, NULL, NULL, NULL, NULL),
(44, NULL, '	Ciudad bolivar	', 2, 1, NULL, NULL, NULL, NULL),
(45, NULL, '	Briceño	', 2, 1, NULL, NULL, NULL, NULL),
(46, NULL, '	Buritica	', 2, 1, NULL, NULL, NULL, NULL),
(47, NULL, '	Caceres	', 2, 1, NULL, NULL, NULL, NULL),
(48, NULL, '	Caicedo	', 2, 1, NULL, NULL, NULL, NULL),
(49, NULL, '	Caldas	', 2, 1, NULL, NULL, NULL, NULL),
(50, NULL, '	Campamento	', 2, 1, NULL, NULL, NULL, NULL),
(51, NULL, '	Cañasgordas	', 2, 1, NULL, NULL, NULL, NULL),
(52, NULL, '	Caracoli	', 2, 1, NULL, NULL, NULL, NULL),
(53, NULL, '	Caramanta	', 2, 1, NULL, NULL, NULL, NULL),
(54, NULL, '	Carepa	', 2, 1, NULL, NULL, NULL, NULL),
(55, NULL, '	El carmen de viboral	', 2, 1, NULL, NULL, NULL, NULL),
(56, NULL, '	Carolina	', 2, 1, NULL, NULL, NULL, NULL),
(57, NULL, '	Caucasia	', 2, 1, NULL, NULL, NULL, NULL),
(58, NULL, '	Chigorodo	', 2, 1, NULL, NULL, NULL, NULL),
(59, NULL, '	Cisneros	', 2, 1, NULL, NULL, NULL, NULL),
(60, NULL, '	Cocorna	', 2, 1, NULL, NULL, NULL, NULL),
(61, NULL, '	Concepcion	', 2, 1, NULL, NULL, NULL, NULL),
(62, NULL, '	Concordia	', 2, 1, NULL, NULL, NULL, NULL),
(63, NULL, '	Copacabana	', 2, 1, NULL, NULL, NULL, NULL),
(64, NULL, '	Dabeiba	', 2, 1, NULL, NULL, NULL, NULL),
(65, NULL, '	Don matias	', 2, 1, NULL, NULL, NULL, NULL),
(66, NULL, '	Ebejico	', 2, 1, NULL, NULL, NULL, NULL),
(67, NULL, '	El bagre	', 2, 1, NULL, NULL, NULL, NULL),
(68, NULL, '	Entrerrios	', 2, 1, NULL, NULL, NULL, NULL),
(69, NULL, '	Envigado	', 2, 1, NULL, NULL, NULL, NULL),
(70, NULL, '	Fredonia	', 2, 1, NULL, NULL, NULL, NULL),
(71, NULL, '	Frontino	', 2, 1, NULL, NULL, NULL, NULL),
(72, NULL, '	Giraldo	', 2, 1, NULL, NULL, NULL, NULL),
(73, NULL, '	Girardota	', 2, 1, NULL, NULL, NULL, NULL),
(74, NULL, '	Gomez plata	', 2, 1, NULL, NULL, NULL, NULL),
(75, NULL, '	Granada	', 2, 1, NULL, NULL, NULL, NULL),
(76, NULL, '	Guadalupe	', 2, 1, NULL, NULL, NULL, NULL),
(77, NULL, '	Guarne	', 2, 1, NULL, NULL, NULL, NULL),
(78, NULL, '	Guatape	', 2, 1, NULL, NULL, NULL, NULL),
(79, NULL, '	Heliconia	', 2, 1, NULL, NULL, NULL, NULL),
(80, NULL, '	Hispania	', 2, 1, NULL, NULL, NULL, NULL),
(81, NULL, '	Itagui	', 2, 1, NULL, NULL, NULL, NULL),
(82, NULL, '	Ituango	', 2, 1, NULL, NULL, NULL, NULL),
(83, NULL, '	Jardin	', 2, 1, NULL, NULL, NULL, NULL),
(84, NULL, '	Jerico	', 2, 1, NULL, NULL, NULL, NULL),
(85, NULL, '	La ceja	', 2, 1, NULL, NULL, NULL, NULL),
(86, NULL, '	La estrella	', 2, 1, NULL, NULL, NULL, NULL),
(87, NULL, '	La pintada	', 2, 1, NULL, NULL, NULL, NULL),
(88, NULL, '	La union	', 2, 1, NULL, NULL, NULL, NULL),
(89, NULL, '	Liborina	', 2, 1, NULL, NULL, NULL, NULL),
(90, NULL, '	Maceo	', 2, 1, NULL, NULL, NULL, NULL),
(91, NULL, '	Marinilla	', 2, 1, NULL, NULL, NULL, NULL),
(92, NULL, '	Montebello	', 2, 1, NULL, NULL, NULL, NULL),
(93, NULL, '	Murindo	', 2, 1, NULL, NULL, NULL, NULL),
(94, NULL, '	Mutata	', 2, 1, NULL, NULL, NULL, NULL),
(95, NULL, '	Nariño	', 2, 1, NULL, NULL, NULL, NULL),
(96, NULL, '	Necocli	', 2, 1, NULL, NULL, NULL, NULL),
(97, NULL, '	Nechi	', 2, 1, NULL, NULL, NULL, NULL),
(98, NULL, '	Olaya	', 2, 1, NULL, NULL, NULL, NULL),
(99, NULL, '	Peðol	', 2, 1, NULL, NULL, NULL, NULL),
(100, NULL, '	Peque	', 2, 1, NULL, NULL, NULL, NULL),
(101, NULL, '	Pueblorrico	', 2, 1, NULL, NULL, NULL, NULL),
(102, NULL, '	Puerto berrio	', 2, 1, NULL, NULL, NULL, NULL),
(103, NULL, '	Puerto nare	', 2, 1, NULL, NULL, NULL, NULL),
(104, NULL, '	Puerto triunfo	', 2, 1, NULL, NULL, NULL, NULL),
(105, NULL, '	Remedios	', 2, 1, NULL, NULL, NULL, NULL),
(106, NULL, '	Retiro	', 2, 1, NULL, NULL, NULL, NULL),
(107, NULL, '	Rionegro	', 2, 1, NULL, NULL, NULL, NULL),
(108, NULL, '	Sabanalarga	', 2, 1, NULL, NULL, NULL, NULL),
(109, NULL, '	Sabaneta	', 2, 1, NULL, NULL, NULL, NULL),
(110, NULL, '	Salgar	', 2, 1, NULL, NULL, NULL, NULL),
(111, NULL, '	San andres de cuerquia	', 2, 1, NULL, NULL, NULL, NULL),
(112, NULL, '	San carlos	', 2, 1, NULL, NULL, NULL, NULL),
(113, NULL, '	San francisco	', 2, 1, NULL, NULL, NULL, NULL),
(114, NULL, '	San jeronimo	', 2, 1, NULL, NULL, NULL, NULL),
(115, NULL, '	San jose de la montaña	', 2, 1, NULL, NULL, NULL, NULL),
(116, NULL, '	San juan de uraba	', 2, 1, NULL, NULL, NULL, NULL),
(117, NULL, '	San luis	', 2, 1, NULL, NULL, NULL, NULL),
(118, NULL, '	San pedro	', 2, 1, NULL, NULL, NULL, NULL),
(119, NULL, '	San pedro de uraba	', 2, 1, NULL, NULL, NULL, NULL),
(120, NULL, '	San rafael	', 2, 1, NULL, NULL, NULL, NULL),
(121, NULL, '	San roque	', 2, 1, NULL, NULL, NULL, NULL),
(122, NULL, '	San vicente	', 2, 1, NULL, NULL, NULL, NULL),
(123, NULL, '	Santa barbara	', 2, 1, NULL, NULL, NULL, NULL),
(124, NULL, '	Santa rosa de osos	', 2, 1, NULL, NULL, NULL, NULL),
(125, NULL, '	Santo domingo	', 2, 1, NULL, NULL, NULL, NULL),
(126, NULL, '	El santuario	', 2, 1, NULL, NULL, NULL, NULL),
(127, NULL, '	Segovia	', 2, 1, NULL, NULL, NULL, NULL),
(128, NULL, '	Sonson	', 2, 1, NULL, NULL, NULL, NULL),
(129, NULL, '	Sopetran	', 2, 1, NULL, NULL, NULL, NULL),
(130, NULL, '	Tamesis	', 2, 1, NULL, NULL, NULL, NULL),
(131, NULL, '	Taraza	', 2, 1, NULL, NULL, NULL, NULL),
(132, NULL, '	Tarso	', 2, 1, NULL, NULL, NULL, NULL),
(133, NULL, '	Titiribi	', 2, 1, NULL, NULL, NULL, NULL),
(134, NULL, '	Toledo	', 2, 1, NULL, NULL, NULL, NULL),
(135, NULL, '	Turbo	', 2, 1, NULL, NULL, NULL, NULL),
(136, NULL, '	Uramita	', 2, 1, NULL, NULL, NULL, NULL),
(137, NULL, '	Urrao	', 2, 1, NULL, NULL, NULL, NULL),
(138, NULL, '	Valdivia	', 2, 1, NULL, NULL, NULL, NULL),
(139, NULL, '	Valparaiso	', 2, 1, NULL, NULL, NULL, NULL),
(140, NULL, '	Vegachi	', 2, 1, NULL, NULL, NULL, NULL),
(141, NULL, '	Venecia	', 2, 1, NULL, NULL, NULL, NULL),
(142, NULL, '	Vigia del fuerte	', 2, 1, NULL, NULL, NULL, NULL),
(143, NULL, '	Yali	', 2, 1, NULL, NULL, NULL, NULL),
(144, NULL, '	Yarumal	', 2, 1, NULL, NULL, NULL, NULL),
(145, NULL, '	Yolombo	', 2, 1, NULL, NULL, NULL, NULL),
(146, NULL, '	Yondo	', 2, 1, NULL, NULL, NULL, NULL),
(147, NULL, '	Zaragoza	', 2, 1, NULL, NULL, NULL, NULL),
(148, NULL, '	Arauca	', 3, 1, NULL, NULL, NULL, NULL),
(149, NULL, '	Arauquita	', 3, 1, NULL, NULL, NULL, NULL),
(150, NULL, '	Cravo norte	', 3, 1, NULL, NULL, NULL, NULL),
(151, NULL, '	Fortul	', 3, 1, NULL, NULL, NULL, NULL),
(152, NULL, '	Puerto rondon	', 3, 1, NULL, NULL, NULL, NULL),
(153, NULL, '	Saravena	', 3, 1, NULL, NULL, NULL, NULL),
(154, NULL, '	Tame	', 3, 1, NULL, NULL, NULL, NULL),
(155, NULL, '	Barranquilla	', 4, 1, NULL, NULL, NULL, NULL),
(156, NULL, '	Baranoa	', 4, 1, NULL, NULL, NULL, NULL),
(157, NULL, '	Campo de la cruz	', 4, 1, NULL, NULL, NULL, NULL),
(158, NULL, '	Candelaria	', 4, 1, NULL, NULL, NULL, NULL),
(159, NULL, '	Galapa	', 4, 1, NULL, NULL, NULL, NULL),
(160, NULL, '	Juan de acosta	', 4, 1, NULL, NULL, NULL, NULL),
(161, NULL, '	Luruaco	', 4, 1, NULL, NULL, NULL, NULL),
(162, NULL, '	Malambo	', 4, 1, NULL, NULL, NULL, NULL),
(163, NULL, '	Manati	', 4, 1, NULL, NULL, NULL, NULL),
(164, NULL, '	Palmar de varela	', 4, 1, NULL, NULL, NULL, NULL),
(165, NULL, '	Piojo	', 4, 1, NULL, NULL, NULL, NULL),
(166, NULL, '	Polonuevo	', 4, 1, NULL, NULL, NULL, NULL),
(167, NULL, '	Ponedera	', 4, 1, NULL, NULL, NULL, NULL),
(168, NULL, '	Puerto colombia	', 4, 1, NULL, NULL, NULL, NULL),
(169, NULL, '	Repelon	', 4, 1, NULL, NULL, NULL, NULL),
(170, NULL, '	Sabanagrande	', 4, 1, NULL, NULL, NULL, NULL),
(171, NULL, '	Sabanalarga	', 4, 1, NULL, NULL, NULL, NULL),
(172, NULL, '	Santa lucia	', 4, 1, NULL, NULL, NULL, NULL),
(173, NULL, '	Santo tomas	', 4, 1, NULL, NULL, NULL, NULL),
(174, NULL, '	Soledad	', 4, 1, NULL, NULL, NULL, NULL),
(175, NULL, '	Suan	', 4, 1, NULL, NULL, NULL, NULL),
(176, NULL, '	Tubara	', 4, 1, NULL, NULL, NULL, NULL),
(177, NULL, '	Usiacuri	', 4, 1, NULL, NULL, NULL, NULL),
(178, NULL, '	Cartagena	', 5, 1, NULL, NULL, NULL, NULL),
(179, NULL, '	Achi	', 5, 1, NULL, NULL, NULL, NULL),
(180, NULL, '	Altos del rosario	', 5, 1, NULL, NULL, NULL, NULL),
(181, NULL, '	Arenal	', 5, 1, NULL, NULL, NULL, NULL),
(182, NULL, '	Arjona	', 5, 1, NULL, NULL, NULL, NULL),
(183, NULL, '	Arroyohondo	', 5, 1, NULL, NULL, NULL, NULL),
(184, NULL, '	Barranco de loba	', 5, 1, NULL, NULL, NULL, NULL),
(185, NULL, '	Calamar	', 5, 1, NULL, NULL, NULL, NULL),
(186, NULL, '	Cantagallo	', 5, 1, NULL, NULL, NULL, NULL),
(187, NULL, '	Cicuco	', 5, 1, NULL, NULL, NULL, NULL),
(188, NULL, '	Cordoba	', 5, 1, NULL, NULL, NULL, NULL),
(189, NULL, '	Clemencia	', 5, 1, NULL, NULL, NULL, NULL),
(190, NULL, '	El carmen de bolivar	', 5, 1, NULL, NULL, NULL, NULL),
(191, NULL, '	El guamo	', 5, 1, NULL, NULL, NULL, NULL),
(192, NULL, '	El peñon	', 5, 1, NULL, NULL, NULL, NULL),
(193, NULL, '	Hatillo de loba	', 5, 1, NULL, NULL, NULL, NULL),
(194, NULL, '	Magangue	', 5, 1, NULL, NULL, NULL, NULL),
(195, NULL, '	Mahates	', 5, 1, NULL, NULL, NULL, NULL),
(196, NULL, '	Margarita	', 5, 1, NULL, NULL, NULL, NULL),
(197, NULL, '	Maria la baja	', 5, 1, NULL, NULL, NULL, NULL),
(198, NULL, '	Montecristo	', 5, 1, NULL, NULL, NULL, NULL),
(199, NULL, '	Mompos	', 5, 1, NULL, NULL, NULL, NULL),
(200, NULL, '	Norosi	', 5, 1, NULL, NULL, NULL, NULL),
(201, NULL, '	Morales	', 5, 1, NULL, NULL, NULL, NULL),
(202, NULL, '	Pinillos	', 5, 1, NULL, NULL, NULL, NULL),
(203, NULL, '	Regidor	', 5, 1, NULL, NULL, NULL, NULL),
(204, NULL, '	Rio viejo	', 5, 1, NULL, NULL, NULL, NULL),
(205, NULL, '	San cristobal	', 5, 1, NULL, NULL, NULL, NULL),
(206, NULL, '	San estanislao	', 5, 1, NULL, NULL, NULL, NULL),
(207, NULL, '	San fernando	', 5, 1, NULL, NULL, NULL, NULL),
(208, NULL, '	San jacinto	', 5, 1, NULL, NULL, NULL, NULL),
(209, NULL, '	San jacinto del cauca	', 5, 1, NULL, NULL, NULL, NULL),
(210, NULL, '	San juan nepomuceno	', 5, 1, NULL, NULL, NULL, NULL),
(211, NULL, '	San martin de loba	', 5, 1, NULL, NULL, NULL, NULL),
(212, NULL, '	San pablo	', 5, 1, NULL, NULL, NULL, NULL),
(213, NULL, '	Santa catalina	', 5, 1, NULL, NULL, NULL, NULL),
(214, NULL, '	Santa rosa	', 5, 1, NULL, NULL, NULL, NULL),
(215, NULL, '	Santa rosa del sur	', 5, 1, NULL, NULL, NULL, NULL),
(216, NULL, '	Simiti	', 5, 1, NULL, NULL, NULL, NULL),
(217, NULL, '	Soplaviento	', 5, 1, NULL, NULL, NULL, NULL),
(218, NULL, '	Talaigua nuevo	', 5, 1, NULL, NULL, NULL, NULL),
(219, NULL, '	Tiquisio	', 5, 1, NULL, NULL, NULL, NULL),
(220, NULL, '	Turbaco	', 5, 1, NULL, NULL, NULL, NULL),
(221, NULL, '	Turbana	', 5, 1, NULL, NULL, NULL, NULL),
(222, NULL, '	Villanueva	', 5, 1, NULL, NULL, NULL, NULL),
(223, NULL, '	Zambrano	', 5, 1, NULL, NULL, NULL, NULL),
(224, NULL, '	Tunja	', 6, 1, NULL, NULL, NULL, NULL),
(225, NULL, '	Almeida	', 6, 1, NULL, NULL, NULL, NULL),
(226, NULL, '	Aquitania	', 6, 1, NULL, NULL, NULL, NULL),
(227, NULL, '	Arcabuco	', 6, 1, NULL, NULL, NULL, NULL),
(228, NULL, '	Belen	', 6, 1, NULL, NULL, NULL, NULL),
(229, NULL, '	Berbeo	', 6, 1, NULL, NULL, NULL, NULL),
(230, NULL, '	Beteitiva	', 6, 1, NULL, NULL, NULL, NULL),
(231, NULL, '	Boavita	', 6, 1, NULL, NULL, NULL, NULL),
(232, NULL, '	Boyaca	', 6, 1, NULL, NULL, NULL, NULL),
(233, NULL, '	Briceño	', 6, 1, NULL, NULL, NULL, NULL),
(234, NULL, '	Buenavista	', 6, 1, NULL, NULL, NULL, NULL),
(235, NULL, '	Busbanza	', 6, 1, NULL, NULL, NULL, NULL),
(236, NULL, '	Caldas	', 6, 1, NULL, NULL, NULL, NULL),
(237, NULL, '	Campohermoso	', 6, 1, NULL, NULL, NULL, NULL),
(238, NULL, '	Cerinza	', 6, 1, NULL, NULL, NULL, NULL),
(239, NULL, '	Chinavita	', 6, 1, NULL, NULL, NULL, NULL),
(240, NULL, '	Chiquinquira	', 6, 1, NULL, NULL, NULL, NULL),
(241, NULL, '	Chiscas	', 6, 1, NULL, NULL, NULL, NULL),
(242, NULL, '	Chita	', 6, 1, NULL, NULL, NULL, NULL),
(243, NULL, '	Chitaraque	', 6, 1, NULL, NULL, NULL, NULL),
(244, NULL, '	Chivata	', 6, 1, NULL, NULL, NULL, NULL),
(245, NULL, '	Cienega	', 6, 1, NULL, NULL, NULL, NULL),
(246, NULL, '	Combita	', 6, 1, NULL, NULL, NULL, NULL),
(247, NULL, '	Coper	', 6, 1, NULL, NULL, NULL, NULL),
(248, NULL, '	Corrales	', 6, 1, NULL, NULL, NULL, NULL),
(249, NULL, '	Covarachia	', 6, 1, NULL, NULL, NULL, NULL),
(250, NULL, '	Cubara	', 6, 1, NULL, NULL, NULL, NULL),
(251, NULL, '	Cucaita	', 6, 1, NULL, NULL, NULL, NULL),
(252, NULL, '	Cuitiva	', 6, 1, NULL, NULL, NULL, NULL),
(253, NULL, '	Chiquiza	', 6, 1, NULL, NULL, NULL, NULL),
(254, NULL, '	Chivor	', 6, 1, NULL, NULL, NULL, NULL),
(255, NULL, '	Duitama	', 6, 1, NULL, NULL, NULL, NULL),
(256, NULL, '	El cocuy	', 6, 1, NULL, NULL, NULL, NULL),
(257, NULL, '	El espino	', 6, 1, NULL, NULL, NULL, NULL),
(258, NULL, '	Firavitoba	', 6, 1, NULL, NULL, NULL, NULL),
(259, NULL, '	Floresta	', 6, 1, NULL, NULL, NULL, NULL),
(260, NULL, '	Gachantiva	', 6, 1, NULL, NULL, NULL, NULL),
(261, NULL, '	Gameza	', 6, 1, NULL, NULL, NULL, NULL),
(262, NULL, '	Garagoa	', 6, 1, NULL, NULL, NULL, NULL),
(263, NULL, '	Guacamayas	', 6, 1, NULL, NULL, NULL, NULL),
(264, NULL, '	Guateque	', 6, 1, NULL, NULL, NULL, NULL),
(265, NULL, '	Guayata	', 6, 1, NULL, NULL, NULL, NULL),
(266, NULL, '	Gsican	', 6, 1, NULL, NULL, NULL, NULL),
(267, NULL, '	Iza	', 6, 1, NULL, NULL, NULL, NULL),
(268, NULL, '	Jenesano	', 6, 1, NULL, NULL, NULL, NULL),
(269, NULL, '	Jerico	', 6, 1, NULL, NULL, NULL, NULL),
(270, NULL, '	Labranzagrande	', 6, 1, NULL, NULL, NULL, NULL),
(271, NULL, '	La capilla	', 6, 1, NULL, NULL, NULL, NULL),
(272, NULL, '	La victoria	', 6, 1, NULL, NULL, NULL, NULL),
(273, NULL, '	La uvita	', 6, 1, NULL, NULL, NULL, NULL),
(274, NULL, '	Villa de leyva	', 6, 1, NULL, NULL, NULL, NULL),
(275, NULL, '	Macanal	', 6, 1, NULL, NULL, NULL, NULL),
(276, NULL, '	Maripi	', 6, 1, NULL, NULL, NULL, NULL),
(277, NULL, '	Miraflores	', 6, 1, NULL, NULL, NULL, NULL),
(278, NULL, '	Mongua	', 6, 1, NULL, NULL, NULL, NULL),
(279, NULL, '	Mongui	', 6, 1, NULL, NULL, NULL, NULL),
(280, NULL, '	Moniquira	', 6, 1, NULL, NULL, NULL, NULL),
(281, NULL, '	Motavita	', 6, 1, NULL, NULL, NULL, NULL),
(282, NULL, '	Muzo	', 6, 1, NULL, NULL, NULL, NULL),
(283, NULL, '	Nobsa	', 6, 1, NULL, NULL, NULL, NULL),
(284, NULL, '	Nuevo colon	', 6, 1, NULL, NULL, NULL, NULL),
(285, NULL, '	Oicata	', 6, 1, NULL, NULL, NULL, NULL),
(286, NULL, '	Otanche	', 6, 1, NULL, NULL, NULL, NULL),
(287, NULL, '	Pachavita	', 6, 1, NULL, NULL, NULL, NULL),
(288, NULL, '	Paez	', 6, 1, NULL, NULL, NULL, NULL),
(289, NULL, '	Paipa	', 6, 1, NULL, NULL, NULL, NULL),
(290, NULL, '	Pajarito	', 6, 1, NULL, NULL, NULL, NULL),
(291, NULL, '	Panqueba	', 6, 1, NULL, NULL, NULL, NULL),
(292, NULL, '	Pauna	', 6, 1, NULL, NULL, NULL, NULL),
(293, NULL, '	Paya	', 6, 1, NULL, NULL, NULL, NULL),
(294, NULL, '	Paz de rio	', 6, 1, NULL, NULL, NULL, NULL),
(295, NULL, '	Pesca	', 6, 1, NULL, NULL, NULL, NULL),
(296, NULL, '	Pisba	', 6, 1, NULL, NULL, NULL, NULL),
(297, NULL, '	Puerto boyaca	', 6, 1, NULL, NULL, NULL, NULL),
(298, NULL, '	Quipama	', 6, 1, NULL, NULL, NULL, NULL),
(299, NULL, '	Ramiriqui	', 6, 1, NULL, NULL, NULL, NULL),
(300, NULL, '	Raquira	', 6, 1, NULL, NULL, NULL, NULL),
(301, NULL, '	Rondon	', 6, 1, NULL, NULL, NULL, NULL),
(302, NULL, '	Saboya	', 6, 1, NULL, NULL, NULL, NULL),
(303, NULL, '	Sachica	', 6, 1, NULL, NULL, NULL, NULL),
(304, NULL, '	Samaca	', 6, 1, NULL, NULL, NULL, NULL),
(305, NULL, '	San eduardo	', 6, 1, NULL, NULL, NULL, NULL),
(306, NULL, '	San jose de pare	', 6, 1, NULL, NULL, NULL, NULL),
(307, NULL, '	San luis de gaceno	', 6, 1, NULL, NULL, NULL, NULL),
(308, NULL, '	San mateo	', 6, 1, NULL, NULL, NULL, NULL),
(309, NULL, '	San miguel de sema	', 6, 1, NULL, NULL, NULL, NULL),
(310, NULL, '	San pablo de borbur	', 6, 1, NULL, NULL, NULL, NULL),
(311, NULL, '	Santana	', 6, 1, NULL, NULL, NULL, NULL),
(312, NULL, '	Santa maria	', 6, 1, NULL, NULL, NULL, NULL),
(313, NULL, '	Santa rosa de viterbo	', 6, 1, NULL, NULL, NULL, NULL),
(314, NULL, '	Santa sofia	', 6, 1, NULL, NULL, NULL, NULL),
(315, NULL, '	Sativanorte	', 6, 1, NULL, NULL, NULL, NULL),
(316, NULL, '	Sativasur	', 6, 1, NULL, NULL, NULL, NULL),
(317, NULL, '	Siachoque	', 6, 1, NULL, NULL, NULL, NULL),
(318, NULL, '	Soata	', 6, 1, NULL, NULL, NULL, NULL),
(319, NULL, '	Socota	', 6, 1, NULL, NULL, NULL, NULL),
(320, NULL, '	Socha	', 6, 1, NULL, NULL, NULL, NULL),
(321, NULL, '	Sogamoso	', 6, 1, NULL, NULL, NULL, NULL),
(322, NULL, '	Somondoco	', 6, 1, NULL, NULL, NULL, NULL),
(323, NULL, '	Sora	', 6, 1, NULL, NULL, NULL, NULL),
(324, NULL, '	Sotaquira	', 6, 1, NULL, NULL, NULL, NULL),
(325, NULL, '	Soraca	', 6, 1, NULL, NULL, NULL, NULL),
(326, NULL, '	Susacon	', 6, 1, NULL, NULL, NULL, NULL),
(327, NULL, '	Sutamarchan	', 6, 1, NULL, NULL, NULL, NULL),
(328, NULL, '	Sutatenza	', 6, 1, NULL, NULL, NULL, NULL),
(329, NULL, '	Tasco	', 6, 1, NULL, NULL, NULL, NULL),
(330, NULL, '	Tenza	', 6, 1, NULL, NULL, NULL, NULL),
(331, NULL, '	Tibana	', 6, 1, NULL, NULL, NULL, NULL),
(332, NULL, '	Tibasosa	', 6, 1, NULL, NULL, NULL, NULL),
(333, NULL, '	Tinjaca	', 6, 1, NULL, NULL, NULL, NULL),
(334, NULL, '	Tipacoque	', 6, 1, NULL, NULL, NULL, NULL),
(335, NULL, '	Toca	', 6, 1, NULL, NULL, NULL, NULL),
(336, NULL, '	Togsi	', 6, 1, NULL, NULL, NULL, NULL),
(337, NULL, '	Topaga	', 6, 1, NULL, NULL, NULL, NULL),
(338, NULL, '	Tota	', 6, 1, NULL, NULL, NULL, NULL),
(339, NULL, '	Tunungua	', 6, 1, NULL, NULL, NULL, NULL),
(340, NULL, '	Turmeque	', 6, 1, NULL, NULL, NULL, NULL),
(341, NULL, '	Tuta	', 6, 1, NULL, NULL, NULL, NULL),
(342, NULL, '	Tutaza	', 6, 1, NULL, NULL, NULL, NULL),
(343, NULL, '	Umbita	', 6, 1, NULL, NULL, NULL, NULL),
(344, NULL, '	Ventaquemada	', 6, 1, NULL, NULL, NULL, NULL),
(345, NULL, '	Viracacha	', 6, 1, NULL, NULL, NULL, NULL),
(346, NULL, '	Zetaquira	', 6, 1, NULL, NULL, NULL, NULL),
(347, NULL, '	Manizales	', 7, 1, NULL, NULL, NULL, NULL),
(348, NULL, '	Aguadas	', 7, 1, NULL, NULL, NULL, NULL),
(349, NULL, '	Anserma	', 7, 1, NULL, NULL, NULL, NULL),
(350, NULL, '	Aranzazu	', 7, 1, NULL, NULL, NULL, NULL),
(351, NULL, '	Belalcazar	', 7, 1, NULL, NULL, NULL, NULL),
(352, NULL, '	Chinchina	', 7, 1, NULL, NULL, NULL, NULL),
(353, NULL, '	Filadelfia	', 7, 1, NULL, NULL, NULL, NULL),
(354, NULL, '	La dorada	', 7, 1, NULL, NULL, NULL, NULL),
(355, NULL, '	La merced	', 7, 1, NULL, NULL, NULL, NULL),
(356, NULL, '	Manzanares	', 7, 1, NULL, NULL, NULL, NULL),
(357, NULL, '	Marmato	', 7, 1, NULL, NULL, NULL, NULL),
(358, NULL, '	Marquetalia	', 7, 1, NULL, NULL, NULL, NULL),
(359, NULL, '	Marulanda	', 7, 1, NULL, NULL, NULL, NULL),
(360, NULL, '	Neira	', 7, 1, NULL, NULL, NULL, NULL),
(361, NULL, '	Norcasia	', 7, 1, NULL, NULL, NULL, NULL),
(362, NULL, '	Pacora	', 7, 1, NULL, NULL, NULL, NULL),
(363, NULL, '	Palestina	', 7, 1, NULL, NULL, NULL, NULL),
(364, NULL, '	Pensilvania	', 7, 1, NULL, NULL, NULL, NULL),
(365, NULL, '	Riosucio	', 7, 1, NULL, NULL, NULL, NULL),
(366, NULL, '	Risaralda	', 7, 1, NULL, NULL, NULL, NULL),
(367, NULL, '	Salamina	', 7, 1, NULL, NULL, NULL, NULL),
(368, NULL, '	Samana	', 7, 1, NULL, NULL, NULL, NULL),
(369, NULL, '	San jose	', 7, 1, NULL, NULL, NULL, NULL),
(370, NULL, '	Supia	', 7, 1, NULL, NULL, NULL, NULL),
(371, NULL, '	Victoria	', 7, 1, NULL, NULL, NULL, NULL),
(372, NULL, '	Villamaria	', 7, 1, NULL, NULL, NULL, NULL),
(373, NULL, '	Viterbo	', 7, 1, NULL, NULL, NULL, NULL),
(374, NULL, '	Florencia	', 8, 1, NULL, NULL, NULL, NULL),
(375, NULL, '	Albania	', 8, 1, NULL, NULL, NULL, NULL),
(376, NULL, '	Belen de los andaquies	', 8, 1, NULL, NULL, NULL, NULL),
(377, NULL, '	Cartagena del chaira	', 8, 1, NULL, NULL, NULL, NULL),
(378, NULL, '	Curillo	', 8, 1, NULL, NULL, NULL, NULL),
(379, NULL, '	El doncello	', 8, 1, NULL, NULL, NULL, NULL),
(380, NULL, '	El paujil	', 8, 1, NULL, NULL, NULL, NULL),
(381, NULL, '	La montañita	', 8, 1, NULL, NULL, NULL, NULL),
(382, NULL, '	Milan	', 8, 1, NULL, NULL, NULL, NULL),
(383, NULL, '	Morelia	', 8, 1, NULL, NULL, NULL, NULL),
(384, NULL, '	Puerto rico	', 8, 1, NULL, NULL, NULL, NULL),
(385, NULL, '	San jose del fragua	', 8, 1, NULL, NULL, NULL, NULL),
(386, NULL, '	San vicente del caguan	', 8, 1, NULL, NULL, NULL, NULL),
(387, NULL, '	Solano	', 8, 1, NULL, NULL, NULL, NULL),
(388, NULL, '	Solita	', 8, 1, NULL, NULL, NULL, NULL),
(389, NULL, '	Valparaiso	', 8, 1, NULL, NULL, NULL, NULL),
(390, NULL, '	Yopal	', 9, 1, NULL, NULL, NULL, NULL),
(391, NULL, '	Aguazul	', 9, 1, NULL, NULL, NULL, NULL),
(392, NULL, '	Chameza	', 9, 1, NULL, NULL, NULL, NULL),
(393, NULL, '	Hato corozal	', 9, 1, NULL, NULL, NULL, NULL),
(394, NULL, '	La salina	', 9, 1, NULL, NULL, NULL, NULL),
(395, NULL, '	Mani	', 9, 1, NULL, NULL, NULL, NULL),
(396, NULL, '	Monterrey	', 9, 1, NULL, NULL, NULL, NULL),
(397, NULL, '	Nunchia	', 9, 1, NULL, NULL, NULL, NULL),
(398, NULL, '	Orocue	', 9, 1, NULL, NULL, NULL, NULL),
(399, NULL, '	Paz de ariporo	', 9, 1, NULL, NULL, NULL, NULL),
(400, NULL, '	Pore	', 9, 1, NULL, NULL, NULL, NULL),
(401, NULL, '	Recetor	', 9, 1, NULL, NULL, NULL, NULL),
(402, NULL, '	Sabanalarga	', 9, 1, NULL, NULL, NULL, NULL),
(403, NULL, '	Sacama	', 9, 1, NULL, NULL, NULL, NULL),
(404, NULL, '	San luis de palenque	', 9, 1, NULL, NULL, NULL, NULL),
(405, NULL, '	Tamara	', 9, 1, NULL, NULL, NULL, NULL),
(406, NULL, '	Tauramena	', 9, 1, NULL, NULL, NULL, NULL),
(407, NULL, '	Trinidad	', 9, 1, NULL, NULL, NULL, NULL),
(408, NULL, '	Villanueva	', 9, 1, NULL, NULL, NULL, NULL),
(409, NULL, '	Popayan	', 10, 1, NULL, NULL, NULL, NULL),
(410, NULL, '	Almaguer	', 10, 1, NULL, NULL, NULL, NULL),
(411, NULL, '	Argelia	', 10, 1, NULL, NULL, NULL, NULL),
(412, NULL, '	Balboa	', 10, 1, NULL, NULL, NULL, NULL),
(413, NULL, '	Bolivar	', 10, 1, NULL, NULL, NULL, NULL),
(414, NULL, '	Buenos aires	', 10, 1, NULL, NULL, NULL, NULL),
(415, NULL, '	Cajibio	', 10, 1, NULL, NULL, NULL, NULL),
(416, NULL, '	Caldono	', 10, 1, NULL, NULL, NULL, NULL),
(417, NULL, '	Caloto	', 10, 1, NULL, NULL, NULL, NULL),
(418, NULL, '	Corinto	', 10, 1, NULL, NULL, NULL, NULL),
(419, NULL, '	El tambo	', 10, 1, NULL, NULL, NULL, NULL),
(420, NULL, '	Florencia	', 10, 1, NULL, NULL, NULL, NULL),
(421, NULL, '	Guachene	', 10, 1, NULL, NULL, NULL, NULL),
(422, NULL, '	Guapi	', 10, 1, NULL, NULL, NULL, NULL),
(423, NULL, '	Inza	', 10, 1, NULL, NULL, NULL, NULL),
(424, NULL, '	Jambalo	', 10, 1, NULL, NULL, NULL, NULL),
(425, NULL, '	La sierra	', 10, 1, NULL, NULL, NULL, NULL),
(426, NULL, '	La vega	', 10, 1, NULL, NULL, NULL, NULL),
(427, NULL, '	Lopez	', 10, 1, NULL, NULL, NULL, NULL),
(428, NULL, '	Mercaderes	', 10, 1, NULL, NULL, NULL, NULL),
(429, NULL, '	Miranda	', 10, 1, NULL, NULL, NULL, NULL),
(430, NULL, '	Morales	', 10, 1, NULL, NULL, NULL, NULL),
(431, NULL, '	Padilla	', 10, 1, NULL, NULL, NULL, NULL),
(432, NULL, '	Paez	', 10, 1, NULL, NULL, NULL, NULL),
(433, NULL, '	Patia	', 10, 1, NULL, NULL, NULL, NULL),
(434, NULL, '	Piamonte	', 10, 1, NULL, NULL, NULL, NULL),
(435, NULL, '	Piendamo	', 10, 1, NULL, NULL, NULL, NULL),
(436, NULL, '	Puerto tejada	', 10, 1, NULL, NULL, NULL, NULL),
(437, NULL, '	Purace	', 10, 1, NULL, NULL, NULL, NULL),
(438, NULL, '	Rosas	', 10, 1, NULL, NULL, NULL, NULL),
(439, NULL, '	San sebastian	', 10, 1, NULL, NULL, NULL, NULL),
(440, NULL, '	Santander de quilichao	', 10, 1, NULL, NULL, NULL, NULL),
(441, NULL, '	Santa rosa	', 10, 1, NULL, NULL, NULL, NULL),
(442, NULL, '	Silvia	', 10, 1, NULL, NULL, NULL, NULL),
(443, NULL, '	Sotara	', 10, 1, NULL, NULL, NULL, NULL),
(444, NULL, '	Suarez	', 10, 1, NULL, NULL, NULL, NULL),
(445, NULL, '	Sucre	', 10, 1, NULL, NULL, NULL, NULL),
(446, NULL, '	Timbio	', 10, 1, NULL, NULL, NULL, NULL),
(447, NULL, '	Timbiqui	', 10, 1, NULL, NULL, NULL, NULL),
(448, NULL, '	Toribio	', 10, 1, NULL, NULL, NULL, NULL),
(449, NULL, '	Totoro	', 10, 1, NULL, NULL, NULL, NULL),
(450, NULL, '	Villa rica	', 10, 1, NULL, NULL, NULL, NULL),
(451, NULL, '	Valledupar	', 11, 1, NULL, NULL, NULL, NULL),
(452, NULL, '	Aguachica	', 11, 1, NULL, NULL, NULL, NULL),
(453, NULL, '	Agustin codazzi	', 11, 1, NULL, NULL, NULL, NULL),
(454, NULL, '	Astrea	', 11, 1, NULL, NULL, NULL, NULL),
(455, NULL, '	Becerril	', 11, 1, NULL, NULL, NULL, NULL),
(456, NULL, '	Bosconia	', 11, 1, NULL, NULL, NULL, NULL),
(457, NULL, '	Chimichagua	', 11, 1, NULL, NULL, NULL, NULL),
(458, NULL, '	Chiriguana	', 11, 1, NULL, NULL, NULL, NULL),
(459, NULL, '	Curumani	', 11, 1, NULL, NULL, NULL, NULL),
(460, NULL, '	El copey	', 11, 1, NULL, NULL, NULL, NULL),
(461, NULL, '	El paso	', 11, 1, NULL, NULL, NULL, NULL),
(462, NULL, '	Gamarra	', 11, 1, NULL, NULL, NULL, NULL),
(463, NULL, '	Gonzalez	', 11, 1, NULL, NULL, NULL, NULL),
(464, NULL, '	La gloria	', 11, 1, NULL, NULL, NULL, NULL),
(465, NULL, '	La jagua de ibirico	', 11, 1, NULL, NULL, NULL, NULL),
(466, NULL, '	Manaure	', 11, 1, NULL, NULL, NULL, NULL),
(467, NULL, '	Pailitas	', 11, 1, NULL, NULL, NULL, NULL),
(468, NULL, '	Pelaya	', 11, 1, NULL, NULL, NULL, NULL),
(469, NULL, '	Pueblo bello	', 11, 1, NULL, NULL, NULL, NULL),
(470, NULL, '	Rio de oro	', 11, 1, NULL, NULL, NULL, NULL),
(471, NULL, '	La paz	', 11, 1, NULL, NULL, NULL, NULL),
(472, NULL, '	San alberto	', 11, 1, NULL, NULL, NULL, NULL),
(473, NULL, '	San diego	', 11, 1, NULL, NULL, NULL, NULL),
(474, NULL, '	San martin	', 11, 1, NULL, NULL, NULL, NULL),
(475, NULL, '	Tamalameque	', 11, 1, NULL, NULL, NULL, NULL),
(476, NULL, '	Quibdo	', 12, 1, NULL, NULL, NULL, NULL),
(477, NULL, '	Acandi	', 12, 1, NULL, NULL, NULL, NULL),
(478, NULL, '	Alto baudo	', 12, 1, NULL, NULL, NULL, NULL),
(479, NULL, '	Atrato	', 12, 1, NULL, NULL, NULL, NULL),
(480, NULL, '	Bagado	', 12, 1, NULL, NULL, NULL, NULL),
(481, NULL, '	Bahia solano	', 12, 1, NULL, NULL, NULL, NULL),
(482, NULL, '	Bajo baudo	', 12, 1, NULL, NULL, NULL, NULL),
(483, NULL, '	Bojaya	', 12, 1, NULL, NULL, NULL, NULL),
(484, NULL, '	El canton del san pablo	', 12, 1, NULL, NULL, NULL, NULL),
(485, NULL, '	Carmen del darien	', 12, 1, NULL, NULL, NULL, NULL),
(486, NULL, '	Certegui	', 12, 1, NULL, NULL, NULL, NULL),
(487, NULL, '	Condoto	', 12, 1, NULL, NULL, NULL, NULL),
(488, NULL, '	El carmen de atrato	', 12, 1, NULL, NULL, NULL, NULL),
(489, NULL, '	El litoral del san juan	', 12, 1, NULL, NULL, NULL, NULL),
(490, NULL, '	Istmina	', 12, 1, NULL, NULL, NULL, NULL),
(491, NULL, '	Jurado	', 12, 1, NULL, NULL, NULL, NULL),
(492, NULL, '	Lloro	', 12, 1, NULL, NULL, NULL, NULL),
(493, NULL, '	Medio atrato	', 12, 1, NULL, NULL, NULL, NULL),
(494, NULL, '	Medio baudo	', 12, 1, NULL, NULL, NULL, NULL),
(495, NULL, '	Medio san juan	', 12, 1, NULL, NULL, NULL, NULL),
(496, NULL, '	Novita	', 12, 1, NULL, NULL, NULL, NULL),
(497, NULL, '	Nuqui	', 12, 1, NULL, NULL, NULL, NULL),
(498, NULL, '	Rio iro	', 12, 1, NULL, NULL, NULL, NULL),
(499, NULL, '	Rio quito	', 12, 1, NULL, NULL, NULL, NULL),
(500, NULL, '	Riosucio	', 12, 1, NULL, NULL, NULL, NULL),
(501, NULL, '	San jose del palmar	', 12, 1, NULL, NULL, NULL, NULL),
(502, NULL, '	Sipi	', 12, 1, NULL, NULL, NULL, NULL),
(503, NULL, '	Tado	', 12, 1, NULL, NULL, NULL, NULL),
(504, NULL, '	Unguia	', 12, 1, NULL, NULL, NULL, NULL),
(505, NULL, '	Union panamericana	', 12, 1, NULL, NULL, NULL, NULL),
(506, NULL, '	Monteria	', 13, 1, NULL, NULL, NULL, NULL),
(507, NULL, '	Ayapel	', 13, 1, NULL, NULL, NULL, NULL),
(508, NULL, '	Buenavista	', 13, 1, NULL, NULL, NULL, NULL),
(509, NULL, '	Canalete	', 13, 1, NULL, NULL, NULL, NULL),
(510, NULL, '	Cerete	', 13, 1, NULL, NULL, NULL, NULL),
(511, NULL, '	Chima	', 13, 1, NULL, NULL, NULL, NULL),
(512, NULL, '	Chinu	', 13, 1, NULL, NULL, NULL, NULL),
(513, NULL, '	Cienaga de oro	', 13, 1, NULL, NULL, NULL, NULL),
(514, NULL, '	Cotorra	', 13, 1, NULL, NULL, NULL, NULL),
(515, NULL, '	La apartada	', 13, 1, NULL, NULL, NULL, NULL),
(516, NULL, '	Lorica	', 13, 1, NULL, NULL, NULL, NULL),
(517, NULL, '	Los cordobas	', 13, 1, NULL, NULL, NULL, NULL),
(518, NULL, '	Momil	', 13, 1, NULL, NULL, NULL, NULL),
(519, NULL, '	Montelibano	', 13, 1, NULL, NULL, NULL, NULL),
(520, NULL, '	Moñitos	', 13, 1, NULL, NULL, NULL, NULL),
(521, NULL, '	Planeta rica	', 13, 1, NULL, NULL, NULL, NULL),
(522, NULL, '	Pueblo nuevo	', 13, 1, NULL, NULL, NULL, NULL),
(523, NULL, '	Puerto escondido	', 13, 1, NULL, NULL, NULL, NULL),
(524, NULL, '	Puerto libertador	', 13, 1, NULL, NULL, NULL, NULL),
(525, NULL, '	Purisima	', 13, 1, NULL, NULL, NULL, NULL),
(526, NULL, '	Sahagun	', 13, 1, NULL, NULL, NULL, NULL),
(527, NULL, '	San andres sotavento	', 13, 1, NULL, NULL, NULL, NULL),
(528, NULL, '	San antero	', 13, 1, NULL, NULL, NULL, NULL),
(529, NULL, '	San bernardo del viento	', 13, 1, NULL, NULL, NULL, NULL),
(530, NULL, '	San carlos	', 13, 1, NULL, NULL, NULL, NULL),
(531, NULL, '	San pelayo	', 13, 1, NULL, NULL, NULL, NULL),
(532, NULL, '	Tierralta	', 13, 1, NULL, NULL, NULL, NULL),
(533, NULL, '	Valencia	', 13, 1, NULL, NULL, NULL, NULL),
(534, NULL, '	Agua de dios	', 14, 1, NULL, NULL, NULL, NULL),
(535, NULL, '	Alban	', 14, 1, NULL, NULL, NULL, NULL),
(536, NULL, '	Anapoima	', 14, 1, NULL, NULL, NULL, NULL),
(537, NULL, '	Anolaima	', 14, 1, NULL, NULL, NULL, NULL),
(538, NULL, '	Arbelaez	', 14, 1, NULL, NULL, NULL, NULL),
(539, NULL, '	Beltran	', 14, 1, NULL, NULL, NULL, NULL),
(540, NULL, '	Bituima	', 14, 1, NULL, NULL, NULL, NULL),
(541, NULL, '	Bogotá D.C.	', 14, 1, NULL, NULL, NULL, NULL),
(542, NULL, '	Bojaca	', 14, 1, NULL, NULL, NULL, NULL),
(543, NULL, '	Cabrera	', 14, 1, NULL, NULL, NULL, NULL),
(544, NULL, '	Cachipay	', 14, 1, NULL, NULL, NULL, NULL),
(545, NULL, '	Cajica	', 14, 1, NULL, NULL, NULL, NULL),
(546, NULL, '	Caparrapi	', 14, 1, NULL, NULL, NULL, NULL),
(547, NULL, '	Caqueza	', 14, 1, NULL, NULL, NULL, NULL),
(548, NULL, '	Carmen de carupa	', 14, 1, NULL, NULL, NULL, NULL),
(549, NULL, '	Chaguani	', 14, 1, NULL, NULL, NULL, NULL),
(550, NULL, '	Chia	', 14, 1, NULL, NULL, NULL, NULL),
(551, NULL, '	Chipaque	', 14, 1, NULL, NULL, NULL, NULL),
(552, NULL, '	Choachi	', 14, 1, NULL, NULL, NULL, NULL),
(553, NULL, '	Choconta	', 14, 1, NULL, NULL, NULL, NULL),
(554, NULL, '	Cogua	', 14, 1, NULL, NULL, NULL, NULL),
(555, NULL, '	Cota	', 14, 1, NULL, NULL, NULL, NULL),
(556, NULL, '	Cucunuba	', 14, 1, NULL, NULL, NULL, NULL),
(557, NULL, '	El colegio	', 14, 1, NULL, NULL, NULL, NULL),
(558, NULL, '	El peñon	', 14, 1, NULL, NULL, NULL, NULL),
(559, NULL, '	El rosal	', 14, 1, NULL, NULL, NULL, NULL),
(560, NULL, '	Facatativa	', 14, 1, NULL, NULL, NULL, NULL),
(561, NULL, '	Fomeque	', 14, 1, NULL, NULL, NULL, NULL),
(562, NULL, '	Fosca	', 14, 1, NULL, NULL, NULL, NULL),
(563, NULL, '	Funza	', 14, 1, NULL, NULL, NULL, NULL),
(564, NULL, '	Fuquene	', 14, 1, NULL, NULL, NULL, NULL),
(565, NULL, '	Fusagasuga	', 14, 1, NULL, NULL, NULL, NULL),
(566, NULL, '	Gachala	', 14, 1, NULL, NULL, NULL, NULL),
(567, NULL, '	Gachancipa	', 14, 1, NULL, NULL, NULL, NULL),
(568, NULL, '	Gacheta	', 14, 1, NULL, NULL, NULL, NULL),
(569, NULL, '	Gama	', 14, 1, NULL, NULL, NULL, NULL),
(570, NULL, '	Girardot	', 14, 1, NULL, NULL, NULL, NULL),
(571, NULL, '	Granada	', 14, 1, NULL, NULL, NULL, NULL),
(572, NULL, '	Guacheta	', 14, 1, NULL, NULL, NULL, NULL),
(573, NULL, '	Guaduas	', 14, 1, NULL, NULL, NULL, NULL),
(574, NULL, '	Guasca	', 14, 1, NULL, NULL, NULL, NULL),
(575, NULL, '	Guataqui	', 14, 1, NULL, NULL, NULL, NULL),
(576, NULL, '	Guatavita	', 14, 1, NULL, NULL, NULL, NULL),
(577, NULL, '	Guayabal de siquima	', 14, 1, NULL, NULL, NULL, NULL),
(578, NULL, '	Guayabetal	', 14, 1, NULL, NULL, NULL, NULL),
(579, NULL, '	Gutierrez	', 14, 1, NULL, NULL, NULL, NULL),
(580, NULL, '	Jerusalen	', 14, 1, NULL, NULL, NULL, NULL),
(581, NULL, '	Junin	', 14, 1, NULL, NULL, NULL, NULL),
(582, NULL, '	La calera	', 14, 1, NULL, NULL, NULL, NULL),
(583, NULL, '	La mesa	', 14, 1, NULL, NULL, NULL, NULL),
(584, NULL, '	La palma	', 14, 1, NULL, NULL, NULL, NULL),
(585, NULL, '	La peña	', 14, 1, NULL, NULL, NULL, NULL),
(586, NULL, '	La vega	', 14, 1, NULL, NULL, NULL, NULL),
(587, NULL, '	Lenguazaque	', 14, 1, NULL, NULL, NULL, NULL),
(588, NULL, '	Macheta	', 14, 1, NULL, NULL, NULL, NULL),
(589, NULL, '	Madrid	', 14, 1, NULL, NULL, NULL, NULL),
(590, NULL, '	Manta	', 14, 1, NULL, NULL, NULL, NULL),
(591, NULL, '	Medina	', 14, 1, NULL, NULL, NULL, NULL),
(592, NULL, '	Mosquera	', 14, 1, NULL, NULL, NULL, NULL),
(593, NULL, '	Nariño	', 14, 1, NULL, NULL, NULL, NULL),
(594, NULL, '	Nemocon	', 14, 1, NULL, NULL, NULL, NULL),
(595, NULL, '	Nilo	', 14, 1, NULL, NULL, NULL, NULL),
(596, NULL, '	Nimaima	', 14, 1, NULL, NULL, NULL, NULL),
(597, NULL, '	Nocaima	', 14, 1, NULL, NULL, NULL, NULL),
(598, NULL, '	Venecia	', 14, 1, NULL, NULL, NULL, NULL),
(599, NULL, '	Pacho	', 14, 1, NULL, NULL, NULL, NULL),
(600, NULL, '	Paime	', 14, 1, NULL, NULL, NULL, NULL),
(601, NULL, '	Pandi	', 14, 1, NULL, NULL, NULL, NULL),
(602, NULL, '	Paratebueno	', 14, 1, NULL, NULL, NULL, NULL),
(603, NULL, '	Pasca	', 14, 1, NULL, NULL, NULL, NULL),
(604, NULL, '	Puerto salgar	', 14, 1, NULL, NULL, NULL, NULL),
(605, NULL, '	Puli	', 14, 1, NULL, NULL, NULL, NULL),
(606, NULL, '	Quebradanegra	', 14, 1, NULL, NULL, NULL, NULL),
(607, NULL, '	Quetame	', 14, 1, NULL, NULL, NULL, NULL),
(608, NULL, '	Quipile	', 14, 1, NULL, NULL, NULL, NULL),
(609, NULL, '	Apulo	', 14, 1, NULL, NULL, NULL, NULL),
(610, NULL, '	Ricaurte	', 14, 1, NULL, NULL, NULL, NULL),
(611, NULL, '	San antonio del tequendama	', 14, 1, NULL, NULL, NULL, NULL),
(612, NULL, '	San bernardo	', 14, 1, NULL, NULL, NULL, NULL),
(613, NULL, '	San cayetano	', 14, 1, NULL, NULL, NULL, NULL),
(614, NULL, '	San francisco	', 14, 1, NULL, NULL, NULL, NULL),
(615, NULL, '	San juan de rio seco	', 14, 1, NULL, NULL, NULL, NULL),
(616, NULL, '	Sasaima	', 14, 1, NULL, NULL, NULL, NULL),
(617, NULL, '	Sesquile	', 14, 1, NULL, NULL, NULL, NULL),
(618, NULL, '	Sibate	', 14, 1, NULL, NULL, NULL, NULL),
(619, NULL, '	Silvania	', 14, 1, NULL, NULL, NULL, NULL),
(620, NULL, '	Simijaca	', 14, 1, NULL, NULL, NULL, NULL),
(621, NULL, '	Soacha	', 14, 1, NULL, NULL, NULL, NULL),
(622, NULL, '	Sopo	', 14, 1, NULL, NULL, NULL, NULL),
(623, NULL, '	Subachoque	', 14, 1, NULL, NULL, NULL, NULL),
(624, NULL, '	Suesca	', 14, 1, NULL, NULL, NULL, NULL),
(625, NULL, '	Supata	', 14, 1, NULL, NULL, NULL, NULL),
(626, NULL, '	Susa	', 14, 1, NULL, NULL, NULL, NULL),
(627, NULL, '	Sutatausa	', 14, 1, NULL, NULL, NULL, NULL),
(628, NULL, '	Tabio	', 14, 1, NULL, NULL, NULL, NULL),
(629, NULL, '	Tausa	', 14, 1, NULL, NULL, NULL, NULL),
(630, NULL, '	Tena	', 14, 1, NULL, NULL, NULL, NULL),
(631, NULL, '	Tenjo	', 14, 1, NULL, NULL, NULL, NULL),
(632, NULL, '	Tibacuy	', 14, 1, NULL, NULL, NULL, NULL),
(633, NULL, '	Tibirita	', 14, 1, NULL, NULL, NULL, NULL),
(634, NULL, '	Tocaima	', 14, 1, NULL, NULL, NULL, NULL),
(635, NULL, '	Tocancipa	', 14, 1, NULL, NULL, NULL, NULL),
(636, NULL, '	Topaipi	', 14, 1, NULL, NULL, NULL, NULL),
(637, NULL, '	Ubala	', 14, 1, NULL, NULL, NULL, NULL),
(638, NULL, '	Ubaque	', 14, 1, NULL, NULL, NULL, NULL),
(639, NULL, '	Villa de san diego de ubate	', 14, 1, NULL, NULL, NULL, NULL),
(640, NULL, '	Une	', 14, 1, NULL, NULL, NULL, NULL),
(641, NULL, '	Utica	', 14, 1, NULL, NULL, NULL, NULL),
(642, NULL, '	Vergara	', 14, 1, NULL, NULL, NULL, NULL),
(643, NULL, '	Viani	', 14, 1, NULL, NULL, NULL, NULL),
(644, NULL, '	Villagomez	', 14, 1, NULL, NULL, NULL, NULL),
(645, NULL, '	Villapinzon	', 14, 1, NULL, NULL, NULL, NULL),
(646, NULL, '	Villeta	', 14, 1, NULL, NULL, NULL, NULL),
(647, NULL, '	Viota	', 14, 1, NULL, NULL, NULL, NULL),
(648, NULL, '	Yacopi	', 14, 1, NULL, NULL, NULL, NULL),
(649, NULL, '	Zipacon	', 14, 1, NULL, NULL, NULL, NULL),
(650, NULL, '	Zipaquira	', 14, 1, NULL, NULL, NULL, NULL),
(651, NULL, '	Inirida	', 15, 1, NULL, NULL, NULL, NULL),
(652, NULL, '	Barranco minas	', 15, 1, NULL, NULL, NULL, NULL),
(653, NULL, '	Mapiripana	', 15, 1, NULL, NULL, NULL, NULL),
(654, NULL, '	San felipe	', 15, 1, NULL, NULL, NULL, NULL),
(655, NULL, '	Puerto colombia	', 15, 1, NULL, NULL, NULL, NULL),
(656, NULL, '	La guadalupe	', 15, 1, NULL, NULL, NULL, NULL),
(657, NULL, '	Cacahual	', 15, 1, NULL, NULL, NULL, NULL),
(658, NULL, '	Pana pana	', 15, 1, NULL, NULL, NULL, NULL),
(659, NULL, '	Morichal	', 15, 1, NULL, NULL, NULL, NULL),
(660, NULL, '	San jose del guaviare	', 16, 1, NULL, NULL, NULL, NULL),
(661, NULL, '	Calamar	', 16, 1, NULL, NULL, NULL, NULL),
(662, NULL, '	El retorno	', 16, 1, NULL, NULL, NULL, NULL),
(663, NULL, '	Miraflores	', 16, 1, NULL, NULL, NULL, NULL),
(664, NULL, '	Neiva	', 17, 1, NULL, NULL, NULL, NULL),
(665, NULL, '	Acevedo	', 17, 1, NULL, NULL, NULL, NULL),
(666, NULL, '	Agrado	', 17, 1, NULL, NULL, NULL, NULL),
(667, NULL, '	Aipe	', 17, 1, NULL, NULL, NULL, NULL),
(668, NULL, '	Algeciras	', 17, 1, NULL, NULL, NULL, NULL),
(669, NULL, '	Altamira	', 17, 1, NULL, NULL, NULL, NULL),
(670, NULL, '	Baraya	', 17, 1, NULL, NULL, NULL, NULL),
(671, NULL, '	Campoalegre	', 17, 1, NULL, NULL, NULL, NULL),
(672, NULL, '	Colombia	', 17, 1, NULL, NULL, NULL, NULL),
(673, NULL, '	Elias	', 17, 1, NULL, NULL, NULL, NULL),
(674, NULL, '	Garzon	', 17, 1, NULL, NULL, NULL, NULL),
(675, NULL, '	Gigante	', 17, 1, NULL, NULL, NULL, NULL),
(676, NULL, '	Guadalupe	', 17, 1, NULL, NULL, NULL, NULL),
(677, NULL, '	Hobo	', 17, 1, NULL, NULL, NULL, NULL),
(678, NULL, '	Iquira	', 17, 1, NULL, NULL, NULL, NULL),
(679, NULL, '	Isnos	', 17, 1, NULL, NULL, NULL, NULL),
(680, NULL, '	La argentina	', 17, 1, NULL, NULL, NULL, NULL),
(681, NULL, '	La plata	', 17, 1, NULL, NULL, NULL, NULL),
(682, NULL, '	Nataga	', 17, 1, NULL, NULL, NULL, NULL),
(683, NULL, '	Oporapa	', 17, 1, NULL, NULL, NULL, NULL),
(684, NULL, '	Paicol	', 17, 1, NULL, NULL, NULL, NULL),
(685, NULL, '	Palermo	', 17, 1, NULL, NULL, NULL, NULL),
(686, NULL, '	Palestina	', 17, 1, NULL, NULL, NULL, NULL),
(687, NULL, '	Pital	', 17, 1, NULL, NULL, NULL, NULL),
(688, NULL, '	Pitalito	', 17, 1, NULL, NULL, NULL, NULL),
(689, NULL, '	Rivera	', 17, 1, NULL, NULL, NULL, NULL),
(690, NULL, '	Saladoblanco	', 17, 1, NULL, NULL, NULL, NULL),
(691, NULL, '	San agustin	', 17, 1, NULL, NULL, NULL, NULL),
(692, NULL, '	Santa maria	', 17, 1, NULL, NULL, NULL, NULL),
(693, NULL, '	Suaza	', 17, 1, NULL, NULL, NULL, NULL),
(694, NULL, '	Tarqui	', 17, 1, NULL, NULL, NULL, NULL),
(695, NULL, '	Tesalia	', 17, 1, NULL, NULL, NULL, NULL),
(696, NULL, '	Tello	', 17, 1, NULL, NULL, NULL, NULL),
(697, NULL, '	Teruel	', 17, 1, NULL, NULL, NULL, NULL),
(698, NULL, '	Timana	', 17, 1, NULL, NULL, NULL, NULL),
(699, NULL, '	Villavieja	', 17, 1, NULL, NULL, NULL, NULL),
(700, NULL, '	Yaguara	', 17, 1, NULL, NULL, NULL, NULL),
(701, NULL, '	Riohacha	', 18, 1, NULL, NULL, NULL, NULL),
(702, NULL, '	Albania	', 18, 1, NULL, NULL, NULL, NULL),
(703, NULL, '	Barrancas	', 18, 1, NULL, NULL, NULL, NULL),
(704, NULL, '	Dibulla	', 18, 1, NULL, NULL, NULL, NULL),
(705, NULL, '	Distraccion	', 18, 1, NULL, NULL, NULL, NULL),
(706, NULL, '	El molino	', 18, 1, NULL, NULL, NULL, NULL),
(707, NULL, '	Fonseca	', 18, 1, NULL, NULL, NULL, NULL),
(708, NULL, '	Hatonuevo	', 18, 1, NULL, NULL, NULL, NULL),
(709, NULL, '	La jagua del pilar	', 18, 1, NULL, NULL, NULL, NULL),
(710, NULL, '	Maicao	', 18, 1, NULL, NULL, NULL, NULL),
(711, NULL, '	Manaure	', 18, 1, NULL, NULL, NULL, NULL),
(712, NULL, '	San juan del cesar	', 18, 1, NULL, NULL, NULL, NULL),
(713, NULL, '	Uribia	', 18, 1, NULL, NULL, NULL, NULL),
(714, NULL, '	Urumita	', 18, 1, NULL, NULL, NULL, NULL),
(715, NULL, '	Villanueva	', 18, 1, NULL, NULL, NULL, NULL),
(716, NULL, '	Santa marta	', 19, 1, NULL, NULL, NULL, NULL),
(717, NULL, '	Algarrobo	', 19, 1, NULL, NULL, NULL, NULL),
(718, NULL, '	Aracataca	', 19, 1, NULL, NULL, NULL, NULL),
(719, NULL, '	Ariguani	', 19, 1, NULL, NULL, NULL, NULL),
(720, NULL, '	Cerro san antonio	', 19, 1, NULL, NULL, NULL, NULL),
(721, NULL, '	Chibolo	', 19, 1, NULL, NULL, NULL, NULL),
(722, NULL, '	Cienaga	', 19, 1, NULL, NULL, NULL, NULL),
(723, NULL, '	Concordia	', 19, 1, NULL, NULL, NULL, NULL),
(724, NULL, '	El banco	', 19, 1, NULL, NULL, NULL, NULL),
(725, NULL, '	El piñon	', 19, 1, NULL, NULL, NULL, NULL),
(726, NULL, '	El reten	', 19, 1, NULL, NULL, NULL, NULL),
(727, NULL, '	Fundacion	', 19, 1, NULL, NULL, NULL, NULL),
(728, NULL, '	Guamal	', 19, 1, NULL, NULL, NULL, NULL),
(729, NULL, '	Nueva granada	', 19, 1, NULL, NULL, NULL, NULL),
(730, NULL, '	Pedraza	', 19, 1, NULL, NULL, NULL, NULL),
(731, NULL, '	Pijiño del carmen	', 19, 1, NULL, NULL, NULL, NULL),
(732, NULL, '	Pivijay	', 19, 1, NULL, NULL, NULL, NULL),
(733, NULL, '	Plato	', 19, 1, NULL, NULL, NULL, NULL),
(734, NULL, '	Puebloviejo	', 19, 1, NULL, NULL, NULL, NULL),
(735, NULL, '	Remolino	', 19, 1, NULL, NULL, NULL, NULL),
(736, NULL, '	Sabanas de san angel	', 19, 1, NULL, NULL, NULL, NULL),
(737, NULL, '	Salamina	', 19, 1, NULL, NULL, NULL, NULL),
(738, NULL, '	San sebastian de buenavista	', 19, 1, NULL, NULL, NULL, NULL),
(739, NULL, '	San zenon	', 19, 1, NULL, NULL, NULL, NULL),
(740, NULL, '	Santa ana	', 19, 1, NULL, NULL, NULL, NULL),
(741, NULL, '	Santa barbara de pinto	', 19, 1, NULL, NULL, NULL, NULL),
(742, NULL, '	Sitionuevo	', 19, 1, NULL, NULL, NULL, NULL),
(743, NULL, '	Tenerife	', 19, 1, NULL, NULL, NULL, NULL),
(744, NULL, '	Zapayan	', 19, 1, NULL, NULL, NULL, NULL),
(745, NULL, '	Zona bananera	', 19, 1, NULL, NULL, NULL, NULL),
(746, NULL, '	Villavicencio	', 20, 1, NULL, NULL, NULL, NULL),
(747, NULL, '	Acacias	', 20, 1, NULL, NULL, NULL, NULL),
(748, NULL, '	Barranca de upia	', 20, 1, NULL, NULL, NULL, NULL),
(749, NULL, '	Cabuyaro	', 20, 1, NULL, NULL, NULL, NULL),
(750, NULL, '	Castilla la nueva	', 20, 1, NULL, NULL, NULL, NULL),
(751, NULL, '	Cubarral	', 20, 1, NULL, NULL, NULL, NULL),
(752, NULL, '	Cumaral	', 20, 1, NULL, NULL, NULL, NULL),
(753, NULL, '	El calvario	', 20, 1, NULL, NULL, NULL, NULL),
(754, NULL, '	El castillo	', 20, 1, NULL, NULL, NULL, NULL),
(755, NULL, '	El dorado	', 20, 1, NULL, NULL, NULL, NULL),
(756, NULL, '	Fuente de oro	', 20, 1, NULL, NULL, NULL, NULL),
(757, NULL, '	Granada	', 20, 1, NULL, NULL, NULL, NULL),
(758, NULL, '	Guamal	', 20, 1, NULL, NULL, NULL, NULL),
(759, NULL, '	Mapiripan	', 20, 1, NULL, NULL, NULL, NULL),
(760, NULL, '	Mesetas	', 20, 1, NULL, NULL, NULL, NULL),
(761, NULL, '	La macarena	', 20, 1, NULL, NULL, NULL, NULL),
(762, NULL, '	Uribe	', 20, 1, NULL, NULL, NULL, NULL),
(763, NULL, '	Lejanias	', 20, 1, NULL, NULL, NULL, NULL),
(764, NULL, '	Puerto concordia	', 20, 1, NULL, NULL, NULL, NULL),
(765, NULL, '	Puerto gaitan	', 20, 1, NULL, NULL, NULL, NULL),
(766, NULL, '	Puerto lopez	', 20, 1, NULL, NULL, NULL, NULL),
(767, NULL, '	Puerto lleras	', 20, 1, NULL, NULL, NULL, NULL),
(768, NULL, '	Puerto rico	', 20, 1, NULL, NULL, NULL, NULL),
(769, NULL, '	Restrepo	', 20, 1, NULL, NULL, NULL, NULL),
(770, NULL, '	San carlos de guaroa	', 20, 1, NULL, NULL, NULL, NULL),
(771, NULL, '	San juan de arama	', 20, 1, NULL, NULL, NULL, NULL),
(772, NULL, '	San juanito	', 20, 1, NULL, NULL, NULL, NULL),
(773, NULL, '	San martin	', 20, 1, NULL, NULL, NULL, NULL),
(774, NULL, '	Vistahermosa	', 20, 1, NULL, NULL, NULL, NULL),
(775, NULL, '	Pasto	', 21, 1, NULL, NULL, NULL, NULL),
(776, NULL, '	Alban	', 21, 1, NULL, NULL, NULL, NULL),
(777, NULL, '	Aldana	', 21, 1, NULL, NULL, NULL, NULL),
(778, NULL, '	Ancuya	', 21, 1, NULL, NULL, NULL, NULL),
(779, NULL, '	Arboleda	', 21, 1, NULL, NULL, NULL, NULL),
(780, NULL, '	Barbacoas	', 21, 1, NULL, NULL, NULL, NULL),
(781, NULL, '	Belen	', 21, 1, NULL, NULL, NULL, NULL),
(782, NULL, '	Buesaco	', 21, 1, NULL, NULL, NULL, NULL),
(783, NULL, '	Colon	', 21, 1, NULL, NULL, NULL, NULL),
(784, NULL, '	Consaca	', 21, 1, NULL, NULL, NULL, NULL),
(785, NULL, '	Contadero	', 21, 1, NULL, NULL, NULL, NULL),
(786, NULL, '	Cordoba	', 21, 1, NULL, NULL, NULL, NULL),
(787, NULL, '	Cuaspud	', 21, 1, NULL, NULL, NULL, NULL),
(788, NULL, '	Cumbal	', 21, 1, NULL, NULL, NULL, NULL),
(789, NULL, '	Cumbitara	', 21, 1, NULL, NULL, NULL, NULL),
(790, NULL, '	Chachagsi	', 21, 1, NULL, NULL, NULL, NULL),
(791, NULL, '	El charco	', 21, 1, NULL, NULL, NULL, NULL),
(792, NULL, '	El peñol	', 21, 1, NULL, NULL, NULL, NULL),
(793, NULL, '	El rosario	', 21, 1, NULL, NULL, NULL, NULL),
(794, NULL, '	El tablon de gomez	', 21, 1, NULL, NULL, NULL, NULL),
(795, NULL, '	El tambo	', 21, 1, NULL, NULL, NULL, NULL),
(796, NULL, '	Funes	', 21, 1, NULL, NULL, NULL, NULL),
(797, NULL, '	Guachucal	', 21, 1, NULL, NULL, NULL, NULL),
(798, NULL, '	Guaitarilla	', 21, 1, NULL, NULL, NULL, NULL),
(799, NULL, '	Gualmatan	', 21, 1, NULL, NULL, NULL, NULL),
(800, NULL, '	Iles	', 21, 1, NULL, NULL, NULL, NULL),
(801, NULL, '	Imues	', 21, 1, NULL, NULL, NULL, NULL),
(802, NULL, '	Ipiales	', 21, 1, NULL, NULL, NULL, NULL),
(803, NULL, '	La cruz	', 21, 1, NULL, NULL, NULL, NULL),
(804, NULL, '	La florida	', 21, 1, NULL, NULL, NULL, NULL),
(805, NULL, '	La llanada	', 21, 1, NULL, NULL, NULL, NULL),
(806, NULL, '	La tola	', 21, 1, NULL, NULL, NULL, NULL),
(807, NULL, '	La union	', 21, 1, NULL, NULL, NULL, NULL),
(808, NULL, '	Leiva	', 21, 1, NULL, NULL, NULL, NULL),
(809, NULL, '	Linares	', 21, 1, NULL, NULL, NULL, NULL),
(810, NULL, '	Los andes	', 21, 1, NULL, NULL, NULL, NULL),
(811, NULL, '	Magsi	', 21, 1, NULL, NULL, NULL, NULL),
(812, NULL, '	Mallama	', 21, 1, NULL, NULL, NULL, NULL),
(813, NULL, '	Mosquera	', 21, 1, NULL, NULL, NULL, NULL),
(814, NULL, '	Nariño	', 21, 1, NULL, NULL, NULL, NULL),
(815, NULL, '	Olaya herrera	', 21, 1, NULL, NULL, NULL, NULL),
(816, NULL, '	Ospina	', 21, 1, NULL, NULL, NULL, NULL),
(817, NULL, '	Francisco pizarro	', 21, 1, NULL, NULL, NULL, NULL),
(818, NULL, '	Policarpa	', 21, 1, NULL, NULL, NULL, NULL),
(819, NULL, '	Potosi	', 21, 1, NULL, NULL, NULL, NULL),
(820, NULL, '	Providencia	', 21, 1, NULL, NULL, NULL, NULL),
(821, NULL, '	Puerres	', 21, 1, NULL, NULL, NULL, NULL),
(822, NULL, '	Pupiales	', 21, 1, NULL, NULL, NULL, NULL),
(823, NULL, '	Ricaurte	', 21, 1, NULL, NULL, NULL, NULL),
(824, NULL, '	Roberto payan	', 21, 1, NULL, NULL, NULL, NULL),
(825, NULL, '	Samaniego	', 21, 1, NULL, NULL, NULL, NULL),
(826, NULL, '	Sandona	', 21, 1, NULL, NULL, NULL, NULL),
(827, NULL, '	San bernardo	', 21, 1, NULL, NULL, NULL, NULL),
(828, NULL, '	San lorenzo	', 21, 1, NULL, NULL, NULL, NULL),
(829, NULL, '	San pablo	', 21, 1, NULL, NULL, NULL, NULL),
(830, NULL, '	San pedro de cartago	', 21, 1, NULL, NULL, NULL, NULL),
(831, NULL, '	Santa barbara	', 21, 1, NULL, NULL, NULL, NULL),
(832, NULL, '	Santacruz	', 21, 1, NULL, NULL, NULL, NULL),
(833, NULL, '	Sapuyes	', 21, 1, NULL, NULL, NULL, NULL),
(834, NULL, '	Taminango	', 21, 1, NULL, NULL, NULL, NULL),
(835, NULL, '	Tangua	', 21, 1, NULL, NULL, NULL, NULL),
(836, NULL, '	San andres de tumaco	', 21, 1, NULL, NULL, NULL, NULL),
(837, NULL, '	Tuquerres	', 21, 1, NULL, NULL, NULL, NULL),
(838, NULL, '	Yacuanquer	', 21, 1, NULL, NULL, NULL, NULL),
(839, NULL, '	Cucuta	', 22, 1, NULL, NULL, NULL, NULL),
(840, NULL, '	Abrego	', 22, 1, NULL, NULL, NULL, NULL),
(841, NULL, '	Arboledas	', 22, 1, NULL, NULL, NULL, NULL),
(842, NULL, '	Bochalema	', 22, 1, NULL, NULL, NULL, NULL),
(843, NULL, '	Bucarasica	', 22, 1, NULL, NULL, NULL, NULL),
(844, NULL, '	Cacota	', 22, 1, NULL, NULL, NULL, NULL),
(845, NULL, '	Cachira	', 22, 1, NULL, NULL, NULL, NULL),
(846, NULL, '	Chinacota	', 22, 1, NULL, NULL, NULL, NULL),
(847, NULL, '	Chitaga	', 22, 1, NULL, NULL, NULL, NULL),
(848, NULL, '	Convencion	', 22, 1, NULL, NULL, NULL, NULL),
(849, NULL, '	Cucutilla	', 22, 1, NULL, NULL, NULL, NULL),
(850, NULL, '	Durania	', 22, 1, NULL, NULL, NULL, NULL),
(851, NULL, '	El carmen	', 22, 1, NULL, NULL, NULL, NULL),
(852, NULL, '	El tarra	', 22, 1, NULL, NULL, NULL, NULL),
(853, NULL, '	El zulia	', 22, 1, NULL, NULL, NULL, NULL),
(854, NULL, '	Gramalote	', 22, 1, NULL, NULL, NULL, NULL),
(855, NULL, '	Hacari	', 22, 1, NULL, NULL, NULL, NULL),
(856, NULL, '	Herran	', 22, 1, NULL, NULL, NULL, NULL),
(857, NULL, '	Labateca	', 22, 1, NULL, NULL, NULL, NULL),
(858, NULL, '	La esperanza	', 22, 1, NULL, NULL, NULL, NULL),
(859, NULL, '	La playa	', 22, 1, NULL, NULL, NULL, NULL),
(860, NULL, '	Los patios	', 22, 1, NULL, NULL, NULL, NULL),
(861, NULL, '	Lourdes	', 22, 1, NULL, NULL, NULL, NULL),
(862, NULL, '	Mutiscua	', 22, 1, NULL, NULL, NULL, NULL),
(863, NULL, '	Ocaña	', 22, 1, NULL, NULL, NULL, NULL),
(864, NULL, '	Pamplona	', 22, 1, NULL, NULL, NULL, NULL),
(865, NULL, '	Pamplonita	', 22, 1, NULL, NULL, NULL, NULL),
(866, NULL, '	Puerto santander	', 22, 1, NULL, NULL, NULL, NULL),
(867, NULL, '	Ragonvalia	', 22, 1, NULL, NULL, NULL, NULL),
(868, NULL, '	Salazar	', 22, 1, NULL, NULL, NULL, NULL),
(869, NULL, '	San calixto	', 22, 1, NULL, NULL, NULL, NULL),
(870, NULL, '	San cayetano	', 22, 1, NULL, NULL, NULL, NULL),
(871, NULL, '	Santiago	', 22, 1, NULL, NULL, NULL, NULL),
(872, NULL, '	Sardinata	', 22, 1, NULL, NULL, NULL, NULL),
(873, NULL, '	Silos	', 22, 1, NULL, NULL, NULL, NULL),
(874, NULL, '	Teorama	', 22, 1, NULL, NULL, NULL, NULL),
(875, NULL, '	Tibu	', 22, 1, NULL, NULL, NULL, NULL),
(876, NULL, '	Toledo	', 22, 1, NULL, NULL, NULL, NULL),
(877, NULL, '	Villa caro	', 22, 1, NULL, NULL, NULL, NULL),
(878, NULL, '	Villa del rosario	', 22, 1, NULL, NULL, NULL, NULL),
(879, NULL, '	Mocoa	', 23, 1, NULL, NULL, NULL, NULL),
(880, NULL, '	Colon	', 23, 1, NULL, NULL, NULL, NULL),
(881, NULL, '	Orito	', 23, 1, NULL, NULL, NULL, NULL),
(882, NULL, '	Puerto asis	', 23, 1, NULL, NULL, NULL, NULL),
(883, NULL, '	Puerto caicedo	', 23, 1, NULL, NULL, NULL, NULL),
(884, NULL, '	Puerto guzman	', 23, 1, NULL, NULL, NULL, NULL),
(885, NULL, '	Leguizamo	', 23, 1, NULL, NULL, NULL, NULL),
(886, NULL, '	Sibundoy	', 23, 1, NULL, NULL, NULL, NULL),
(887, NULL, '	San francisco	', 23, 1, NULL, NULL, NULL, NULL),
(888, NULL, '	San miguel	', 23, 1, NULL, NULL, NULL, NULL),
(889, NULL, '	Santiago	', 23, 1, NULL, NULL, NULL, NULL),
(890, NULL, '	Valle del guamuez	', 23, 1, NULL, NULL, NULL, NULL),
(891, NULL, '	Villagarzon	', 23, 1, NULL, NULL, NULL, NULL),
(892, NULL, '	Armenia	', 24, 1, NULL, NULL, NULL, NULL),
(893, NULL, '	Buenavista	', 24, 1, NULL, NULL, NULL, NULL),
(894, NULL, '	Calarca	', 24, 1, NULL, NULL, NULL, NULL),
(895, NULL, '	Circasia	', 24, 1, NULL, NULL, NULL, NULL),
(896, NULL, '	Cordoba	', 24, 1, NULL, NULL, NULL, NULL);
INSERT INTO `municipio` (`idMunicipio`, `referencia`, `municipio`, `idDepartamento`, `estado`, `fechaCreacion`, `fechaModificacion`, `idUsuarioCreacion`, `idUsuarioModificacion`) VALUES
(897, NULL, '	Filandia	', 24, 1, NULL, NULL, NULL, NULL),
(898, NULL, '	Genova	', 24, 1, NULL, NULL, NULL, NULL),
(899, NULL, '	La tebaida	', 24, 1, NULL, NULL, NULL, NULL),
(900, NULL, '	Montenegro	', 24, 1, NULL, NULL, NULL, NULL),
(901, NULL, '	Pijao	', 24, 1, NULL, NULL, NULL, NULL),
(902, NULL, '	Quimbaya	', 24, 1, NULL, NULL, NULL, NULL),
(903, NULL, '	Salento	', 24, 1, NULL, NULL, NULL, NULL),
(904, NULL, '	Pereira	', 25, 1, NULL, NULL, NULL, NULL),
(905, NULL, '	Apia	', 25, 1, NULL, NULL, NULL, NULL),
(906, NULL, '	Balboa	', 25, 1, NULL, NULL, NULL, NULL),
(907, NULL, '	Belen de umbria	', 25, 1, NULL, NULL, NULL, NULL),
(908, NULL, '	Dosquebradas	', 25, 1, NULL, NULL, NULL, NULL),
(909, NULL, '	Guatica	', 25, 1, NULL, NULL, NULL, NULL),
(910, NULL, '	La celia	', 25, 1, NULL, NULL, NULL, NULL),
(911, NULL, '	La virginia	', 25, 1, NULL, NULL, NULL, NULL),
(912, NULL, '	Marsella	', 25, 1, NULL, NULL, NULL, NULL),
(913, NULL, '	Mistrato	', 25, 1, NULL, NULL, NULL, NULL),
(914, NULL, '	Pueblo rico	', 25, 1, NULL, NULL, NULL, NULL),
(915, NULL, '	Quinchia	', 25, 1, NULL, NULL, NULL, NULL),
(916, NULL, '	Santa rosa de cabal	', 25, 1, NULL, NULL, NULL, NULL),
(917, NULL, '	Santuario	', 25, 1, NULL, NULL, NULL, NULL),
(918, NULL, '	San andres	', 26, 1, NULL, NULL, NULL, NULL),
(919, NULL, '	Providencia	', 26, 1, NULL, NULL, NULL, NULL),
(920, NULL, '	Bucaramanga	', 27, 1, NULL, NULL, NULL, NULL),
(921, NULL, '	Aguada	', 27, 1, NULL, NULL, NULL, NULL),
(922, NULL, '	Albania	', 27, 1, NULL, NULL, NULL, NULL),
(923, NULL, '	Aratoca	', 27, 1, NULL, NULL, NULL, NULL),
(924, NULL, '	Barbosa	', 27, 1, NULL, NULL, NULL, NULL),
(925, NULL, '	Barichara	', 27, 1, NULL, NULL, NULL, NULL),
(926, NULL, '	Barrancabermeja	', 27, 1, NULL, NULL, NULL, NULL),
(927, NULL, '	Betulia	', 27, 1, NULL, NULL, NULL, NULL),
(928, NULL, '	Bolivar	', 27, 1, NULL, NULL, NULL, NULL),
(929, NULL, '	Cabrera	', 27, 1, NULL, NULL, NULL, NULL),
(930, NULL, '	California	', 27, 1, NULL, NULL, NULL, NULL),
(931, NULL, '	Capitanejo	', 27, 1, NULL, NULL, NULL, NULL),
(932, NULL, '	Carcasi	', 27, 1, NULL, NULL, NULL, NULL),
(933, NULL, '	Cepita	', 27, 1, NULL, NULL, NULL, NULL),
(934, NULL, '	Cerrito	', 27, 1, NULL, NULL, NULL, NULL),
(935, NULL, '	Charala	', 27, 1, NULL, NULL, NULL, NULL),
(936, NULL, '	Charta	', 27, 1, NULL, NULL, NULL, NULL),
(937, NULL, '	Chima	', 27, 1, NULL, NULL, NULL, NULL),
(938, NULL, '	Chipata	', 27, 1, NULL, NULL, NULL, NULL),
(939, NULL, '	Cimitarra	', 27, 1, NULL, NULL, NULL, NULL),
(940, NULL, '	Concepcion	', 27, 1, NULL, NULL, NULL, NULL),
(941, NULL, '	Confines	', 27, 1, NULL, NULL, NULL, NULL),
(942, NULL, '	Contratacion	', 27, 1, NULL, NULL, NULL, NULL),
(943, NULL, '	Coromoro	', 27, 1, NULL, NULL, NULL, NULL),
(944, NULL, '	Curiti	', 27, 1, NULL, NULL, NULL, NULL),
(945, NULL, '	El carmen de chucuri	', 27, 1, NULL, NULL, NULL, NULL),
(946, NULL, '	El guacamayo	', 27, 1, NULL, NULL, NULL, NULL),
(947, NULL, '	El peñon	', 27, 1, NULL, NULL, NULL, NULL),
(948, NULL, '	El playon	', 27, 1, NULL, NULL, NULL, NULL),
(949, NULL, '	Encino	', 27, 1, NULL, NULL, NULL, NULL),
(950, NULL, '	Enciso	', 27, 1, NULL, NULL, NULL, NULL),
(951, NULL, '	Florian	', 27, 1, NULL, NULL, NULL, NULL),
(952, NULL, '	Floridablanca	', 27, 1, NULL, NULL, NULL, NULL),
(953, NULL, '	Galan	', 27, 1, NULL, NULL, NULL, NULL),
(954, NULL, '	Gambita	', 27, 1, NULL, NULL, NULL, NULL),
(955, NULL, '	Giron	', 27, 1, NULL, NULL, NULL, NULL),
(956, NULL, '	Guaca	', 27, 1, NULL, NULL, NULL, NULL),
(957, NULL, '	Guadalupe	', 27, 1, NULL, NULL, NULL, NULL),
(958, NULL, '	Guapota	', 27, 1, NULL, NULL, NULL, NULL),
(959, NULL, '	Guavata	', 27, 1, NULL, NULL, NULL, NULL),
(960, NULL, '	Gsepsa	', 27, 1, NULL, NULL, NULL, NULL),
(961, NULL, '	Hato	', 27, 1, NULL, NULL, NULL, NULL),
(962, NULL, '	Jesus maria	', 27, 1, NULL, NULL, NULL, NULL),
(963, NULL, '	Jordan	', 27, 1, NULL, NULL, NULL, NULL),
(964, NULL, '	La belleza	', 27, 1, NULL, NULL, NULL, NULL),
(965, NULL, '	Landazuri	', 27, 1, NULL, NULL, NULL, NULL),
(966, NULL, '	La paz	', 27, 1, NULL, NULL, NULL, NULL),
(967, NULL, '	Lebrija	', 27, 1, NULL, NULL, NULL, NULL),
(968, NULL, '	Los santos	', 27, 1, NULL, NULL, NULL, NULL),
(969, NULL, '	Macaravita	', 27, 1, NULL, NULL, NULL, NULL),
(970, NULL, '	Malaga	', 27, 1, NULL, NULL, NULL, NULL),
(971, NULL, '	Matanza	', 27, 1, NULL, NULL, NULL, NULL),
(972, NULL, '	Mogotes	', 27, 1, NULL, NULL, NULL, NULL),
(973, NULL, '	Molagavita	', 27, 1, NULL, NULL, NULL, NULL),
(974, NULL, '	Ocamonte	', 27, 1, NULL, NULL, NULL, NULL),
(975, NULL, '	Oiba	', 27, 1, NULL, NULL, NULL, NULL),
(976, NULL, '	Onzaga	', 27, 1, NULL, NULL, NULL, NULL),
(977, NULL, '	Palmar	', 27, 1, NULL, NULL, NULL, NULL),
(978, NULL, '	Palmas del socorro	', 27, 1, NULL, NULL, NULL, NULL),
(979, NULL, '	Paramo	', 27, 1, NULL, NULL, NULL, NULL),
(980, NULL, '	Piedecuesta	', 27, 1, NULL, NULL, NULL, NULL),
(981, NULL, '	Pinchote	', 27, 1, NULL, NULL, NULL, NULL),
(982, NULL, '	Puente nacional	', 27, 1, NULL, NULL, NULL, NULL),
(983, NULL, '	Puerto parra	', 27, 1, NULL, NULL, NULL, NULL),
(984, NULL, '	Puerto wilches	', 27, 1, NULL, NULL, NULL, NULL),
(985, NULL, '	Rionegro	', 27, 1, NULL, NULL, NULL, NULL),
(986, NULL, '	Sabana de torres	', 27, 1, NULL, NULL, NULL, NULL),
(987, NULL, '	San andres	', 27, 1, NULL, NULL, NULL, NULL),
(988, NULL, '	San benito	', 27, 1, NULL, NULL, NULL, NULL),
(989, NULL, '	San gil	', 27, 1, NULL, NULL, NULL, NULL),
(990, NULL, '	San joaquin	', 27, 1, NULL, NULL, NULL, NULL),
(991, NULL, '	San jose de miranda	', 27, 1, NULL, NULL, NULL, NULL),
(992, NULL, '	San miguel	', 27, 1, NULL, NULL, NULL, NULL),
(993, NULL, '	San vicente de chucuri	', 27, 1, NULL, NULL, NULL, NULL),
(994, NULL, '	Santa barbara	', 27, 1, NULL, NULL, NULL, NULL),
(995, NULL, '	Santa helena del opon	', 27, 1, NULL, NULL, NULL, NULL),
(996, NULL, '	Simacota	', 27, 1, NULL, NULL, NULL, NULL),
(997, NULL, '	Socorro	', 27, 1, NULL, NULL, NULL, NULL),
(998, NULL, '	Suaita	', 27, 1, NULL, NULL, NULL, NULL),
(999, NULL, '	Sucre	', 27, 1, NULL, NULL, NULL, NULL),
(1000, NULL, '	Surata	', 27, 1, NULL, NULL, NULL, NULL),
(1001, NULL, '	Tona	', 27, 1, NULL, NULL, NULL, NULL),
(1002, NULL, '	Valle de san jose	', 27, 1, NULL, NULL, NULL, NULL),
(1003, NULL, '	Velez	', 27, 1, NULL, NULL, NULL, NULL),
(1004, NULL, '	Vetas	', 27, 1, NULL, NULL, NULL, NULL),
(1005, NULL, '	Villanueva	', 27, 1, NULL, NULL, NULL, NULL),
(1006, NULL, '	Zapatoca	', 27, 1, NULL, NULL, NULL, NULL),
(1007, NULL, '	Sincelejo	', 28, 1, NULL, NULL, NULL, NULL),
(1008, NULL, '	Buenavista	', 28, 1, NULL, NULL, NULL, NULL),
(1009, NULL, '	Caimito	', 28, 1, NULL, NULL, NULL, NULL),
(1010, NULL, '	Coloso	', 28, 1, NULL, NULL, NULL, NULL),
(1011, NULL, '	Corozal	', 28, 1, NULL, NULL, NULL, NULL),
(1012, NULL, '	Coveñas	', 28, 1, NULL, NULL, NULL, NULL),
(1013, NULL, '	Chalan	', 28, 1, NULL, NULL, NULL, NULL),
(1014, NULL, '	El roble	', 28, 1, NULL, NULL, NULL, NULL),
(1015, NULL, '	Galeras	', 28, 1, NULL, NULL, NULL, NULL),
(1016, NULL, '	Guaranda	', 28, 1, NULL, NULL, NULL, NULL),
(1017, NULL, '	La union	', 28, 1, NULL, NULL, NULL, NULL),
(1018, NULL, '	Los palmitos	', 28, 1, NULL, NULL, NULL, NULL),
(1019, NULL, '	Majagual	', 28, 1, NULL, NULL, NULL, NULL),
(1020, NULL, '	Morroa	', 28, 1, NULL, NULL, NULL, NULL),
(1021, NULL, '	Ovejas	', 28, 1, NULL, NULL, NULL, NULL),
(1022, NULL, '	Palmito	', 28, 1, NULL, NULL, NULL, NULL),
(1023, NULL, '	Sampues	', 28, 1, NULL, NULL, NULL, NULL),
(1024, NULL, '	San benito abad	', 28, 1, NULL, NULL, NULL, NULL),
(1025, NULL, '	San juan de betulia	', 28, 1, NULL, NULL, NULL, NULL),
(1026, NULL, '	San marcos	', 28, 1, NULL, NULL, NULL, NULL),
(1027, NULL, '	San onofre	', 28, 1, NULL, NULL, NULL, NULL),
(1028, NULL, '	San pedro	', 28, 1, NULL, NULL, NULL, NULL),
(1029, NULL, '	San luis de since	', 28, 1, NULL, NULL, NULL, NULL),
(1030, NULL, '	Sucre	', 28, 1, NULL, NULL, NULL, NULL),
(1031, NULL, '	Santiago de tolu	', 28, 1, NULL, NULL, NULL, NULL),
(1032, NULL, '	Tolu viejo	', 28, 1, NULL, NULL, NULL, NULL),
(1033, NULL, '	Ibague	', 29, 1, NULL, NULL, NULL, NULL),
(1034, NULL, '	Alpujarra	', 29, 1, NULL, NULL, NULL, NULL),
(1035, NULL, '	Alvarado	', 29, 1, NULL, NULL, NULL, NULL),
(1036, NULL, '	Ambalema	', 29, 1, NULL, NULL, NULL, NULL),
(1037, NULL, '	Anzoategui	', 29, 1, NULL, NULL, NULL, NULL),
(1038, NULL, '	Armero	', 29, 1, NULL, NULL, NULL, NULL),
(1039, NULL, '	Ataco	', 29, 1, NULL, NULL, NULL, NULL),
(1040, NULL, '	Cajamarca	', 29, 1, NULL, NULL, NULL, NULL),
(1041, NULL, '	Carmen de apicala	', 29, 1, NULL, NULL, NULL, NULL),
(1042, NULL, '	Casabianca	', 29, 1, NULL, NULL, NULL, NULL),
(1043, NULL, '	Chaparral	', 29, 1, NULL, NULL, NULL, NULL),
(1044, NULL, '	Coello	', 29, 1, NULL, NULL, NULL, NULL),
(1045, NULL, '	Coyaima	', 29, 1, NULL, NULL, NULL, NULL),
(1046, NULL, '	Cunday	', 29, 1, NULL, NULL, NULL, NULL),
(1047, NULL, '	Dolores	', 29, 1, NULL, NULL, NULL, NULL),
(1048, NULL, '	Espinal	', 29, 1, NULL, NULL, NULL, NULL),
(1049, NULL, '	Falan	', 29, 1, NULL, NULL, NULL, NULL),
(1050, NULL, '	Flandes	', 29, 1, NULL, NULL, NULL, NULL),
(1051, NULL, '	Fresno	', 29, 1, NULL, NULL, NULL, NULL),
(1052, NULL, '	Guamo	', 29, 1, NULL, NULL, NULL, NULL),
(1053, NULL, '	Herveo	', 29, 1, NULL, NULL, NULL, NULL),
(1054, NULL, '	Honda	', 29, 1, NULL, NULL, NULL, NULL),
(1055, NULL, '	Icononzo	', 29, 1, NULL, NULL, NULL, NULL),
(1056, NULL, '	Lerida	', 29, 1, NULL, NULL, NULL, NULL),
(1057, NULL, '	Libano	', 29, 1, NULL, NULL, NULL, NULL),
(1058, NULL, '	Mariquita	', 29, 1, NULL, NULL, NULL, NULL),
(1059, NULL, '	Melgar	', 29, 1, NULL, NULL, NULL, NULL),
(1060, NULL, '	Murillo	', 29, 1, NULL, NULL, NULL, NULL),
(1061, NULL, '	Natagaima	', 29, 1, NULL, NULL, NULL, NULL),
(1062, NULL, '	Ortega	', 29, 1, NULL, NULL, NULL, NULL),
(1063, NULL, '	Palocabildo	', 29, 1, NULL, NULL, NULL, NULL),
(1064, NULL, '	Piedras	', 29, 1, NULL, NULL, NULL, NULL),
(1065, NULL, '	Planadas	', 29, 1, NULL, NULL, NULL, NULL),
(1066, NULL, '	Prado	', 29, 1, NULL, NULL, NULL, NULL),
(1067, NULL, '	Purificacion	', 29, 1, NULL, NULL, NULL, NULL),
(1068, NULL, '	Rioblanco	', 29, 1, NULL, NULL, NULL, NULL),
(1069, NULL, '	Roncesvalles	', 29, 1, NULL, NULL, NULL, NULL),
(1070, NULL, '	Rovira	', 29, 1, NULL, NULL, NULL, NULL),
(1071, NULL, '	Saldaña	', 29, 1, NULL, NULL, NULL, NULL),
(1072, NULL, '	San antonio	', 29, 1, NULL, NULL, NULL, NULL),
(1073, NULL, '	San luis	', 29, 1, NULL, NULL, NULL, NULL),
(1074, NULL, '	Santa isabel	', 29, 1, NULL, NULL, NULL, NULL),
(1075, NULL, '	Suarez	', 29, 1, NULL, NULL, NULL, NULL),
(1076, NULL, '	Valle de san juan	', 29, 1, NULL, NULL, NULL, NULL),
(1077, NULL, '	Venadillo	', 29, 1, NULL, NULL, NULL, NULL),
(1078, NULL, '	Villahermosa	', 29, 1, NULL, NULL, NULL, NULL),
(1079, NULL, '	Villarrica	', 29, 1, NULL, NULL, NULL, NULL),
(1080, NULL, '	Cali	', 30, 1, NULL, NULL, NULL, NULL),
(1081, NULL, '	Alcala	', 30, 1, NULL, NULL, NULL, NULL),
(1082, NULL, '	Andalucia	', 30, 1, NULL, NULL, NULL, NULL),
(1083, NULL, '	Ansermanuevo	', 30, 1, NULL, NULL, NULL, NULL),
(1084, NULL, '	Argelia	', 30, 1, NULL, NULL, NULL, NULL),
(1085, NULL, '	Bolivar	', 30, 1, NULL, NULL, NULL, NULL),
(1086, NULL, '	Buenaventura	', 30, 1, NULL, NULL, NULL, NULL),
(1087, NULL, '	Guadalajara de buga	', 30, 1, NULL, NULL, NULL, NULL),
(1088, NULL, '	Bugalagrande	', 30, 1, NULL, NULL, NULL, NULL),
(1089, NULL, '	Caicedonia	', 30, 1, NULL, NULL, NULL, NULL),
(1090, NULL, '	Calima	', 30, 1, NULL, NULL, NULL, NULL),
(1091, NULL, '	Candelaria	', 30, 1, NULL, NULL, NULL, NULL),
(1092, NULL, '	Cartago	', 30, 1, NULL, NULL, NULL, NULL),
(1093, NULL, '	Dagua	', 30, 1, NULL, NULL, NULL, NULL),
(1094, NULL, '	El aguila	', 30, 1, NULL, NULL, NULL, NULL),
(1095, NULL, '	El cairo	', 30, 1, NULL, NULL, NULL, NULL),
(1096, NULL, '	El cerrito	', 30, 1, NULL, NULL, NULL, NULL),
(1097, NULL, '	El dovio	', 30, 1, NULL, NULL, NULL, NULL),
(1098, NULL, '	Florida	', 30, 1, NULL, NULL, NULL, NULL),
(1099, NULL, '	Ginebra	', 30, 1, NULL, NULL, NULL, NULL),
(1100, NULL, '	Guacari	', 30, 1, NULL, NULL, NULL, NULL),
(1101, NULL, '	Jamundi	', 30, 1, NULL, NULL, NULL, NULL),
(1102, NULL, '	La cumbre	', 30, 1, NULL, NULL, NULL, NULL),
(1103, NULL, '	La union	', 30, 1, NULL, NULL, NULL, NULL),
(1104, NULL, '	La victoria	', 30, 1, NULL, NULL, NULL, NULL),
(1105, NULL, '	Obando	', 30, 1, NULL, NULL, NULL, NULL),
(1106, NULL, '	Palmira	', 30, 1, NULL, NULL, NULL, NULL),
(1107, NULL, '	Pradera	', 30, 1, NULL, NULL, NULL, NULL),
(1108, NULL, '	Restrepo	', 30, 1, NULL, NULL, NULL, NULL),
(1109, NULL, '	Riofrio	', 30, 1, NULL, NULL, NULL, NULL),
(1110, NULL, '	Roldanillo	', 30, 1, NULL, NULL, NULL, NULL),
(1111, NULL, '	San pedro	', 30, 1, NULL, NULL, NULL, NULL),
(1112, NULL, '	Sevilla	', 30, 1, NULL, NULL, NULL, NULL),
(1113, NULL, '	Toro	', 30, 1, NULL, NULL, NULL, NULL),
(1114, NULL, '	Trujillo	', 30, 1, NULL, NULL, NULL, NULL),
(1115, NULL, '	Tulua	', 30, 1, NULL, NULL, NULL, NULL),
(1116, NULL, '	Ulloa	', 30, 1, NULL, NULL, NULL, NULL),
(1117, NULL, '	Versalles	', 30, 1, NULL, NULL, NULL, NULL),
(1118, NULL, '	Vijes	', 30, 1, NULL, NULL, NULL, NULL),
(1119, NULL, '	Yotoco	', 30, 1, NULL, NULL, NULL, NULL),
(1120, NULL, '	Yumbo	', 30, 1, NULL, NULL, NULL, NULL),
(1121, NULL, '	Zarzal	', 30, 1, NULL, NULL, NULL, NULL),
(1122, NULL, '	Mitu	', 31, 1, NULL, NULL, NULL, NULL),
(1123, NULL, '	Caruru	', 31, 1, NULL, NULL, NULL, NULL),
(1124, NULL, '	Pacoa	', 31, 1, NULL, NULL, NULL, NULL),
(1125, NULL, '	Taraira	', 31, 1, NULL, NULL, NULL, NULL),
(1126, NULL, '	Papunaua	', 31, 1, NULL, NULL, NULL, NULL),
(1127, NULL, '	Yavarate	', 31, 1, NULL, NULL, NULL, NULL),
(1128, NULL, '	Puerto carreño	', 32, 1, NULL, NULL, NULL, NULL),
(1129, NULL, '	La primavera	', 32, 1, NULL, NULL, NULL, NULL),
(1130, NULL, '	Santa rosalia	', 32, 1, NULL, NULL, NULL, NULL),
(1131, NULL, '	Cumaribo	', 32, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivelprogramaformacion`
--

CREATE TABLE `nivelprogramaformacion` (
  `idNivelProgramaFormacion` smallint(6) NOT NULL,
  `nivelProgramaFormacion` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioCreacion_copy1` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `nivelprogramaformacion`
--

INSERT INTO `nivelprogramaformacion` (`idNivelProgramaFormacion`, `nivelProgramaFormacion`, `idUsuarioCreacion`, `idUsuarioCreacion_copy1`, `fechaCreacion`, `fechaModificacion`, `estado`) VALUES
(1, 'Tecnólogo', 1, 2, '2014-07-05 22:11:08', '2014-07-05 22:11:11', 1),
(2, 'Técnico', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oficina`
--

CREATE TABLE `oficina` (
  `idOficina` smallint(6) NOT NULL,
  `oficina` varchar(145) NOT NULL,
  `idEmpreUnidaNegoc` smallint(6) NOT NULL,
  `idMunicipio` smallint(6) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `correoElectronico` varchar(100) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parametro`
--

CREATE TABLE `parametro` (
  `idParametro` smallint(6) NOT NULL,
  `parametro` varchar(145) COLLATE utf8_spanish_ci NOT NULL,
  `valor` varchar(145) COLLATE utf8_spanish_ci NOT NULL,
  `tipoDato` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'el tipo de valor puede ser un string con formato json o xml el cual ser? utilizado cuando se requiera de un detalle',
  `estado` tinyint(4) NOT NULL DEFAULT '1',
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL,
  `descripcion` varchar(200) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `idPersona` int(11) NOT NULL,
  `idTipoDocumentoIdentidad` tinyint(4) NOT NULL,
  `numeroIdentificacion` bigint(20) NOT NULL,
  `foto` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `primerNombre` varchar(120) COLLATE utf8_spanish_ci NOT NULL,
  `segundoNombre` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL,
  `primerApellido` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL,
  `segundoApellido` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechaNacimiento` date NOT NULL,
  `correoElectronico` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `telefonoCelular` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `telefonoFijo` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `genero` char(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personainfoadicional`
--

CREATE TABLE `personainfoadicional` (
  `idPersonaInfoAdicional` int(11) NOT NULL,
  `idPersona` int(11) NOT NULL,
  `rh` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `tallaCamiseta` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `cuidadoMedico` varchar(400) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `personainfoadicional`
--

INSERT INTO `personainfoadicional` (`idPersonaInfoAdicional`, `idPersona`, `rh`, `tallaCamiseta`, `cuidadoMedico`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`, `estado`) VALUES
(5, 9, 'A+', 'XS', '', 1, 1, '2016-07-25 11:32:45', '2016-07-25 11:32:45', 1),
(6, 10, 'A+', 'S', '', 1, 1, '2016-07-25 11:32:45', '2016-07-25 11:32:45', 1),
(7, 11, 'A+', 'XS', '', 1, 1, '2016-07-25 11:34:10', '2016-07-25 11:34:10', 1),
(8, 12, 'A+', 'XS', '', 1, 1, '2016-07-25 13:22:10', '2016-07-25 13:22:10', 1),
(9, 13, 'A-', 'S', '', 1, 1, '2016-07-25 13:22:10', '2016-07-25 13:22:10', 1),
(10, 14, 'A+', 'S', '', 1, 1, '2016-07-25 13:27:58', '2016-07-25 13:27:58', 1),
(11, 15, 'O+', 'M', '', 1, 1, '2016-07-25 13:27:58', '2016-07-25 13:27:58', 1),
(12, 16, 'A+', 'S', '', 1, 1, '2016-07-25 17:00:28', '2016-07-25 17:00:28', 1),
(13, 17, 'A-', 'M', '', 1, 1, '2016-07-25 17:07:52', '2016-07-25 17:07:52', 1),
(14, 18, 'B+', 'M', '', 1, 1, '2016-07-25 17:07:52', '2016-07-25 17:07:52', 1),
(15, 19, 'A-', 'M', '', 1, 1, '2016-07-25 17:09:11', '2016-07-25 17:09:11', 1),
(16, 20, 'A+', 'L', '', 1, 1, '2016-07-25 22:33:06', '2016-07-25 22:33:06', 1),
(17, 21, 'A-', 'M', '', 1, 1, '2016-07-25 22:33:06', '2016-07-25 22:33:06', 1),
(18, 22, 'A+', 'XS', '', 1, 1, '2016-07-25 22:36:11', '2016-07-25 22:36:11', 1),
(19, 23, 'O-', 'M', '', 1, 1, '2016-07-25 22:36:11', '2016-07-25 22:36:11', 1),
(20, 24, 'A+', 'M', '', 1, 1, '2016-07-25 22:37:11', '2016-07-25 22:37:11', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntafrecuente`
--

CREATE TABLE `preguntafrecuente` (
  `idPreguntaFrecuente` smallint(6) NOT NULL,
  `preguntaFrecuente` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `respuesta` varchar(400) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `preguntafrecuente`
--

INSERT INTO `preguntafrecuente` (`idPreguntaFrecuente`, `preguntaFrecuente`, `respuesta`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(5, '¿Qué es?', 'SENASOFT es el mayor encuentro de tecnología realizado anualmente por el SENA, como iniciativa de la Red de Conocimiento en Informática, Diseño y Desarrollo de Software, para propiciar en los aprendices un escenario de sana competencia en las habilidades más significativas de la formación TIC.', 1, 1, 1, '2014-07-18 18:11:12', '2014-07-18 18:11:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso`
--

CREATE TABLE `proceso` (
  `idProceso` smallint(6) NOT NULL,
  `proceso` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioCreacion_copy1` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `proceso`
--

INSERT INTO `proceso` (`idProceso`, `proceso`, `idUsuarioCreacion`, `idUsuarioCreacion_copy1`, `fechaCreacion`, `fechaModificacion`, `estado`) VALUES
(1, 'Direccionamiento Estratégico\r\n', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(2, 'Gestión del Talento Humano', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(3, 'Gestión de Tecnologías de la información\r\n', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(4, 'Gestión de contratación y convenios\r\n', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(5, 'Gestión de comunicaciones', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(6, 'Gestión documental\r\n', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(7, 'Gestión de Infraestructura y Logística', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(8, 'Gestión de Evaluación y control', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(9, 'Gestión Jurídica', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(10, 'Gestión de recursos financieros', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(11, 'Gestión de emprendimiento y empresarismo', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(12, 'Relacionamiento empresarial y gestión del cliente \r\n', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(13, 'Gestión de formación  profesional integral', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(14, 'Gestión por competencias para las cualificaciones', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(15, 'Gestión  de la innovación y la competitividad', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(16, 'Gestión de empleo , orientación ocupacional y empleabilidad', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(17, 'Gestión de articulación regional', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programaformacion`
--

CREATE TABLE `programaformacion` (
  `idProgramaFormacion` smallint(6) NOT NULL,
  `programaFormacion` varchar(400) COLLATE utf8_spanish_ci NOT NULL,
  `idNivelProgramaFormacion` smallint(6) NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioCreacion_copy1` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `programaformacion`
--

INSERT INTO `programaformacion` (`idProgramaFormacion`, `programaFormacion`, `idNivelProgramaFormacion`, `idUsuarioCreacion`, `idUsuarioCreacion_copy1`, `fechaCreacion`, `fechaModificacion`, `estado`) VALUES
(1, 'Análisis y desarrollo de sistemas de información', 1, 1, 1, '2014-07-05 22:11:41', '2014-07-05 22:11:44', 1),
(2, 'Animación digital', 1, 1, 1, '2014-07-05 22:17:26', '0000-00-00 00:00:00', 1),
(3, 'Animación 3D', 1, 1, 1, '2014-07-28 00:00:00', '2014-07-30 00:00:00', 1),
(4, 'Desarrollo de videojuegos', 1, 1, 1, '2014-07-01 00:00:00', '2014-07-28 00:00:00', 1),
(5, 'Diseño e integración de multimedia', 2, 1, 1, '2016-07-25 00:00:00', '2016-07-25 00:00:00', 1),
(6, 'Gestión de redes de datos', 1, 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(7, 'Instalación de redes de computadores', 2, 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(8, 'Mantenimiento de equipos de cómputo', 2, 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(9, 'Mantenimiento de equipos de cómputo, diseño e instalación de cableado estructurado', 1, 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(10, 'Producción de medios audiovisuales digitales', 1, 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(11, 'Produccion de multimedia', 1, 1, 1, '2016-07-25 00:00:00', '2016-07-25 00:00:00', 1),
(12, 'Programación de software', 2, 1, 1, '2016-07-25 00:00:00', '2016-07-25 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE `proyecto` (
  `idProyecto` smallint(6) NOT NULL,
  `proyecto` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(400) COLLATE utf8_spanish_ci NOT NULL,
  `url` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `sistemaRelacionado` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ubicacion` varchar(150) COLLATE utf8_spanish_ci NOT NULL COMMENT 'alojamiento',
  `urlVideo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `tiempoProduccion` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `idProcesoImpacto` smallint(6) NOT NULL,
  `idTipoSolucion` smallint(6) NOT NULL,
  `idMotorBaseDatos` smallint(6) NOT NULL,
  `idLenguajeProgramacion` smallint(6) NOT NULL,
  `informacionAdicional` varchar(400) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipoLicenciamiento` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `derechosAutor` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `NroImplementaciones` tinyint(4) NOT NULL,
  `idAprendiz` smallint(6) NOT NULL,
  `idCentroFormacion` smallint(6) NOT NULL,
  `cartaImplementacion` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `nroImplementacionFuturo` tinyint(4) NOT NULL,
  `desarrolloFuturo` varchar(300) COLLATE utf8_spanish_ci DEFAULT NULL,
  `equipoDesarroll` varchar(400) COLLATE utf8_spanish_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regimenempresa`
--

CREATE TABLE `regimenempresa` (
  `idRegimenEmpresa` smallint(6) NOT NULL,
  `regimenEmpresa` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `regimenempresa`
--

INSERT INTO `regimenempresa` (`idRegimenEmpresa`, `regimenEmpresa`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'Gran contribuyente', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(2, 'Régimen común', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(3, 'Régimen simplificado', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regional`
--

CREATE TABLE `regional` (
  `idRegional` smallint(6) NOT NULL,
  `regional` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `idUsuarioCreacion` int(11) DEFAULT NULL,
  `idUsuarioCreacion_copy1` int(11) DEFAULT NULL,
  `fechaCreacion` datetime DEFAULT NULL,
  `fechaModificacion` datetime DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `regional`
--

INSERT INTO `regional` (`idRegional`, `regional`, `idUsuarioCreacion`, `idUsuarioCreacion_copy1`, `fechaCreacion`, `fechaModificacion`, `estado`) VALUES
(1, 'Amazonas', NULL, NULL, NULL, NULL, 1),
(2, 'Antioquia', NULL, NULL, NULL, NULL, 1),
(3, 'Arauca', NULL, NULL, NULL, NULL, 1),
(4, 'Atlántico', NULL, NULL, NULL, NULL, 1),
(5, 'Bolívar', NULL, NULL, NULL, NULL, 1),
(6, 'Boyacá', NULL, NULL, NULL, NULL, 1),
(7, 'Caldas', NULL, NULL, NULL, NULL, 1),
(8, 'Caquetá', NULL, NULL, NULL, NULL, 1),
(9, 'Casanare', NULL, NULL, NULL, NULL, 1),
(10, 'Cauca', NULL, NULL, NULL, NULL, 1),
(11, 'Cesar', NULL, NULL, NULL, NULL, 1),
(12, 'Choco', NULL, NULL, NULL, NULL, 1),
(13, 'Córdoba', NULL, NULL, NULL, NULL, 1),
(14, 'Cundinamarca', NULL, NULL, NULL, NULL, 1),
(15, 'Distrito Capital', NULL, NULL, NULL, NULL, 1),
(16, 'Guainía', NULL, NULL, NULL, NULL, 1),
(17, 'Guaviare', NULL, NULL, NULL, NULL, 1),
(18, 'Huila', NULL, NULL, NULL, NULL, 1),
(19, 'Magdalena', NULL, NULL, NULL, NULL, 1),
(20, 'Meta', NULL, NULL, NULL, NULL, 1),
(21, 'Nariño', NULL, NULL, NULL, NULL, 1),
(22, 'Norte De Santander', NULL, NULL, NULL, NULL, 1),
(23, 'Putumayo', NULL, NULL, NULL, NULL, 1),
(24, 'Quindio', NULL, NULL, NULL, NULL, 1),
(25, 'Risaralda', NULL, NULL, NULL, NULL, 1),
(26, 'San Andres', NULL, NULL, NULL, NULL, 1),
(27, 'Santander', NULL, NULL, NULL, NULL, 1),
(28, 'Sucre', NULL, NULL, NULL, NULL, 1),
(29, 'Tolima', NULL, NULL, NULL, NULL, 1),
(30, 'Valle', NULL, NULL, NULL, NULL, 1),
(31, 'Vaupés', NULL, NULL, NULL, NULL, 1),
(32, 'Vichada', NULL, NULL, NULL, NULL, 1),
(33, 'Guajira', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `idRol` smallint(6) NOT NULL,
  `rol` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rolformuaccio`
--

CREATE TABLE `rolformuaccio` (
  `idRolFormuAccio` smallint(6) NOT NULL,
  `idRol` smallint(6) NOT NULL,
  `idFormularioAccion` smallint(6) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sectoreconomico`
--

CREATE TABLE `sectoreconomico` (
  `idSectorEconomico` smallint(6) NOT NULL,
  `sectorEconomico` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sectoreconomico`
--

INSERT INTO `sectoreconomico` (`idSectorEconomico`, `sectorEconomico`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'Agroindustria', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(2, 'Hoteleria  y turismo', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(3, 'Comercio', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(4, 'Gastronomía', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(5, 'Construcción', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(6, 'Financiera', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(7, 'Gobierno', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(8, 'Marroquinería, calzado y cueros', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(9, 'Industria', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(10, 'Publicidad y marketing', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(11, 'Minería o energético', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(12, 'Servicios', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(13, 'Solidario(Cooperativas)', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(14, 'Edición y publicación de libros', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(15, 'Farmacéuticos', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(16, 'Transporte carga o pasajeros ', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tercero`
--

CREATE TABLE `tercero` (
  `idTercero` int(11) NOT NULL,
  `nit` bigint(20) NOT NULL,
  `razonSocial` varchar(300) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefonoCelular` int(11) NOT NULL,
  `telefonoFijo` int(11) NOT NULL,
  `correoElectronico` varchar(300) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodocumentoidentidad`
--

CREATE TABLE `tipodocumentoidentidad` (
  `idTipoDocumentoIdentidad` smallint(3) NOT NULL,
  `codigo` varchar(8) COLLATE utf8_spanish_ci NOT NULL,
  `tipodocumentoidentidadcol` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioModificacion` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipodocumentoidentidad`
--

INSERT INTO `tipodocumentoidentidad` (`idTipoDocumentoIdentidad`, `codigo`, `tipodocumentoidentidadcol`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`, `estado`) VALUES
(1, 'CC', 'Cédula', 1, 1, '2014-07-19 19:07:04', '2014-07-19 19:07:08', 1),
(2, 'TI', 'Tarjeta de Identidad', 1, 1, '2014-07-19 19:07:04', '2014-07-19 19:07:04', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoempresaproducto`
--

CREATE TABLE `tipoempresaproducto` (
  `idTipoEmpresaProducto` smallint(6) NOT NULL,
  `tipoEmpresaProducto` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tipoempresaproducto`
--

INSERT INTO `tipoempresaproducto` (`idTipoEmpresaProducto`, `tipoEmpresaProducto`, `estado`, `idUsuarioCreacion`, `idUsuarioModificacion`, `fechaCreacion`, `fechaModificacion`) VALUES
(1, 'Cliente ha comprado', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(2, 'Cliente desea comprar', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00'),
(3, 'Cliente ofrece o vende', 1, 1, 1, '2014-07-20 00:00:00', '2014-07-20 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposolucion`
--

CREATE TABLE `tiposolucion` (
  `idTipoSolucion` smallint(6) NOT NULL,
  `tipoSolucion` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `idUsuarioCreacion` int(11) NOT NULL,
  `idUsuarioCreacion_copy1` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tiposolucion`
--

INSERT INTO `tiposolucion` (`idTipoSolucion`, `tipoSolucion`, `idUsuarioCreacion`, `idUsuarioCreacion_copy1`, `fechaCreacion`, `fechaModificacion`, `estado`) VALUES
(1, 'Aplicación multimedia', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(2, 'Videojuego', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(3, 'Aplicación web', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(4, 'Aplicación móvil', 1, 1, '2014-07-09 00:00:00', '2014-07-09 00:00:00', 1),
(8, 'Aplicación de escritorio', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1),
(9, 'Aplicación networking', 1, 1, '2014-07-14 00:00:00', '2014-07-14 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidadnegocio`
--

CREATE TABLE `unidadnegocio` (
  `idUnidadNegocio` tinyint(4) NOT NULL,
  `unidadNegocio` varchar(300) NOT NULL,
  `estado` tinyint(4) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` smallint(6) NOT NULL,
  `idPersona` smallint(6) DEFAULT NULL,
  `usuario` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `perfil` tinyint(4) NOT NULL,
  `fechaActivacion` datetime NOT NULL,
  `fechaExpiracion` datetime DEFAULT NULL,
  `contrasenia` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `idEmpresa` smallint(6) DEFAULT NULL,
  `codigoActivacion` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `idPersona`, `usuario`, `perfil`, `fechaActivacion`, `fechaExpiracion`, `contrasenia`, `idEmpresa`, `codigoActivacion`, `estado`, `fechaCreacion`, `fechaModificacion`, `idUsuarioCreacion`, `idUsuarioModificacion`) VALUES
(1, 1, 'admin', 1, '2014-07-07 00:00:00', '2014-07-07 00:00:00', '202cb962ac59075b964b07152d234b70', 1, '111', 1, '2014-07-07 00:00:00', '2014-05-07 10:34:09', 1, 1),
(2, 2, 'adminC', 2, '2014-07-24 00:00:00', '2014-07-24 00:00:00', '202cb962ac59075b964b07152d234b70', 1, '111', 1, '2014-07-24 00:00:00', '2014-07-24 00:00:00', 1, 1),
(3, 3, 'adminP', 3, '2014-07-24 00:00:00', '2014-07-24 00:00:00', '202cb962ac59075b964b07152d234b70', 1, '111', 1, '2014-07-24 00:00:00', '2014-07-24 00:00:00', 1, 1),
(4, 4, 'adminR', 4, '2014-07-24 00:00:00', '2014-07-24 00:00:00', '202cb962ac59075b964b07152d234b70', 1, '111', 1, '2014-07-24 00:00:00', '2014-07-24 00:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarioarea`
--

CREATE TABLE `usuarioarea` (
  `idUsuarioArea` smallint(6) NOT NULL,
  `idUsuario` smallint(6) NOT NULL,
  `idArea` smallint(6) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL,
  `principal` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariooficina`
--

CREATE TABLE `usuariooficina` (
  `idUsuarioOficina` smallint(6) NOT NULL,
  `idUsuario` smallint(6) NOT NULL,
  `principal` tinyint(1) NOT NULL,
  `idOficina` smallint(6) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariorol`
--

CREATE TABLE `usuariorol` (
  `idUsuarioRol` smallint(6) NOT NULL,
  `idUsuario` smallint(6) NOT NULL,
  `idRol` smallint(6) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `idUsuarioCreacion` smallint(6) NOT NULL,
  `idUsuarioModificacion` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accion`
--
ALTER TABLE `accion`
  ADD PRIMARY KEY (`idAccion`);

--
-- Indices de la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD PRIMARY KEY (`idActividad`);

--
-- Indices de la tabla `actividaddiaria`
--
ALTER TABLE `actividaddiaria`
  ADD PRIMARY KEY (`idActividadDiaria`);

--
-- Indices de la tabla `actividadestado`
--
ALTER TABLE `actividadestado`
  ADD PRIMARY KEY (`idActividadEstado`);

--
-- Indices de la tabla `aplicacion`
--
ALTER TABLE `aplicacion`
  ADD PRIMARY KEY (`idAplicacion`);

--
-- Indices de la tabla `aprendiz`
--
ALTER TABLE `aprendiz`
  ADD PRIMARY KEY (`idAprendiz`);

--
-- Indices de la tabla `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`idArea`);

--
-- Indices de la tabla `asistenteruedanegocio`
--
ALTER TABLE `asistenteruedanegocio`
  ADD PRIMARY KEY (`idAsistenteRuedaNegocio`);

--
-- Indices de la tabla `categoriaconcurso`
--
ALTER TABLE `categoriaconcurso`
  ADD PRIMARY KEY (`idCategoriaConcurso`);

--
-- Indices de la tabla `categoriaproducto`
--
ALTER TABLE `categoriaproducto`
  ADD PRIMARY KEY (`idCategoriaProducto`);

--
-- Indices de la tabla `centroformacion`
--
ALTER TABLE `centroformacion`
  ADD PRIMARY KEY (`idCentroFormacion`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`idDepartamento`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`idEmpresa`);

--
-- Indices de la tabla `empresacodigo`
--
ALTER TABLE `empresacodigo`
  ADD PRIMARY KEY (`idEmpresaCodigo`);

--
-- Indices de la tabla `empresaproducto`
--
ALTER TABLE `empresaproducto`
  ADD PRIMARY KEY (`idEmpresaProducto`);

--
-- Indices de la tabla `empreunidanegoc`
--
ALTER TABLE `empreunidanegoc`
  ADD PRIMARY KEY (`idEmpreUnidaNegoc`);

--
-- Indices de la tabla `estadoempresa`
--
ALTER TABLE `estadoempresa`
  ADD PRIMARY KEY (`idEstadoEmpresa`);

--
-- Indices de la tabla `formulario`
--
ALTER TABLE `formulario`
  ADD PRIMARY KEY (`idFormulario`),
  ADD KEY `UQ_formulario_formulario_ubicacion` (`formulario`,`ubicacion`),
  ADD KEY `FK_formulario_menu_idMenu_idx` (`idMenu`);

--
-- Indices de la tabla `formularioaccion`
--
ALTER TABLE `formularioaccion`
  ADD PRIMARY KEY (`idFormularioAccion`),
  ADD KEY `FK_formulario_accion_idFormulario_idx` (`idFormulario`),
  ADD KEY `FK_formulario_accion_idAccion_idx` (`idAccion`);

--
-- Indices de la tabla `inscricategoconcuraprend`
--
ALTER TABLE `inscricategoconcuraprend`
  ADD PRIMARY KEY (`idInscriCategoConcurAprend`);

--
-- Indices de la tabla `inscricategoconcurinstru`
--
ALTER TABLE `inscricategoconcurinstru`
  ADD PRIMARY KEY (`idInscriCateroConcurInstru`);

--
-- Indices de la tabla `instructor`
--
ALTER TABLE `instructor`
  ADD PRIMARY KEY (`idInstructor`);

--
-- Indices de la tabla `leguajeprogramacion`
--
ALTER TABLE `leguajeprogramacion`
  ADD PRIMARY KEY (`idLeguajeProgramacion`);

--
-- Indices de la tabla `mensajecontacto`
--
ALTER TABLE `mensajecontacto`
  ADD PRIMARY KEY (`idMensajeContacto`);

--
-- Indices de la tabla `mensajerespuesta`
--
ALTER TABLE `mensajerespuesta`
  ADD PRIMARY KEY (`idRespuesta`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`idMenu`);

--
-- Indices de la tabla `motorbasedatos`
--
ALTER TABLE `motorbasedatos`
  ADD PRIMARY KEY (`idMotorBaseDatos`);

--
-- Indices de la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD PRIMARY KEY (`idMunicipio`),
  ADD KEY `Fk_municipio_departamento_idDepartamento_idx` (`idDepartamento`);

--
-- Indices de la tabla `nivelprogramaformacion`
--
ALTER TABLE `nivelprogramaformacion`
  ADD PRIMARY KEY (`idNivelProgramaFormacion`);

--
-- Indices de la tabla `oficina`
--
ALTER TABLE `oficina`
  ADD PRIMARY KEY (`idOficina`);

--
-- Indices de la tabla `parametro`
--
ALTER TABLE `parametro`
  ADD PRIMARY KEY (`idParametro`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idPersona`);

--
-- Indices de la tabla `personainfoadicional`
--
ALTER TABLE `personainfoadicional`
  ADD PRIMARY KEY (`idPersonaInfoAdicional`),
  ADD KEY `idPersonaInfoAdicional` (`idPersonaInfoAdicional`);

--
-- Indices de la tabla `preguntafrecuente`
--
ALTER TABLE `preguntafrecuente`
  ADD PRIMARY KEY (`idPreguntaFrecuente`);

--
-- Indices de la tabla `proceso`
--
ALTER TABLE `proceso`
  ADD PRIMARY KEY (`idProceso`);

--
-- Indices de la tabla `programaformacion`
--
ALTER TABLE `programaformacion`
  ADD PRIMARY KEY (`idProgramaFormacion`);

--
-- Indices de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD PRIMARY KEY (`idProyecto`);

--
-- Indices de la tabla `regimenempresa`
--
ALTER TABLE `regimenempresa`
  ADD PRIMARY KEY (`idRegimenEmpresa`);

--
-- Indices de la tabla `regional`
--
ALTER TABLE `regional`
  ADD PRIMARY KEY (`idRegional`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`idRol`);

--
-- Indices de la tabla `rolformuaccio`
--
ALTER TABLE `rolformuaccio`
  ADD PRIMARY KEY (`idRolFormuAccio`),
  ADD UNIQUE KEY `idRolAccioFormu_UNIQUE` (`idRolFormuAccio`),
  ADD KEY `FK_rol_accio_formu_idFormularioAccion_idx` (`idFormularioAccion`),
  ADD KEY `FK_rol_accio_formu_idRol_idx` (`idRol`);

--
-- Indices de la tabla `sectoreconomico`
--
ALTER TABLE `sectoreconomico`
  ADD PRIMARY KEY (`idSectorEconomico`);

--
-- Indices de la tabla `tercero`
--
ALTER TABLE `tercero`
  ADD PRIMARY KEY (`idTercero`);

--
-- Indices de la tabla `tipodocumentoidentidad`
--
ALTER TABLE `tipodocumentoidentidad`
  ADD PRIMARY KEY (`idTipoDocumentoIdentidad`);

--
-- Indices de la tabla `tipoempresaproducto`
--
ALTER TABLE `tipoempresaproducto`
  ADD PRIMARY KEY (`idTipoEmpresaProducto`);

--
-- Indices de la tabla `tiposolucion`
--
ALTER TABLE `tiposolucion`
  ADD PRIMARY KEY (`idTipoSolucion`);

--
-- Indices de la tabla `unidadnegocio`
--
ALTER TABLE `unidadnegocio`
  ADD PRIMARY KEY (`idUnidadNegocio`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`),
  ADD KEY `IU_usuario_idPersona` (`idPersona`);

--
-- Indices de la tabla `usuarioarea`
--
ALTER TABLE `usuarioarea`
  ADD PRIMARY KEY (`idUsuarioArea`);

--
-- Indices de la tabla `usuariooficina`
--
ALTER TABLE `usuariooficina`
  ADD PRIMARY KEY (`idUsuarioOficina`);

--
-- Indices de la tabla `usuariorol`
--
ALTER TABLE `usuariorol`
  ADD PRIMARY KEY (`idUsuarioRol`),
  ADD KEY `FK_usuario_rol_idUsuario_idx` (`idUsuario`),
  ADD KEY `FK_usuario_rol_idRol_idx` (`idRol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accion`
--
ALTER TABLE `accion`
  MODIFY `idAccion` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `actividad`
--
ALTER TABLE `actividad`
  MODIFY `idActividad` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `actividaddiaria`
--
ALTER TABLE `actividaddiaria`
  MODIFY `idActividadDiaria` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `actividadestado`
--
ALTER TABLE `actividadestado`
  MODIFY `idActividadEstado` tinyint(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `aplicacion`
--
ALTER TABLE `aplicacion`
  MODIFY `idAplicacion` smallint(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `aprendiz`
--
ALTER TABLE `aprendiz`
  MODIFY `idAprendiz` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `area`
--
ALTER TABLE `area`
  MODIFY `idArea` smallint(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `asistenteruedanegocio`
--
ALTER TABLE `asistenteruedanegocio`
  MODIFY `idAsistenteRuedaNegocio` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `categoriaconcurso`
--
ALTER TABLE `categoriaconcurso`
  MODIFY `idCategoriaConcurso` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `categoriaproducto`
--
ALTER TABLE `categoriaproducto`
  MODIFY `idCategoriaProducto` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `centroformacion`
--
ALTER TABLE `centroformacion`
  MODIFY `idCentroFormacion` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;
--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `idDepartamento` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `idEmpresa` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `empresacodigo`
--
ALTER TABLE `empresacodigo`
  MODIFY `idEmpresaCodigo` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `empresaproducto`
--
ALTER TABLE `empresaproducto`
  MODIFY `idEmpresaProducto` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `empreunidanegoc`
--
ALTER TABLE `empreunidanegoc`
  MODIFY `idEmpreUnidaNegoc` smallint(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `estadoempresa`
--
ALTER TABLE `estadoempresa`
  MODIFY `idEstadoEmpresa` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `formulario`
--
ALTER TABLE `formulario`
  MODIFY `idFormulario` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT de la tabla `formularioaccion`
--
ALTER TABLE `formularioaccion`
  MODIFY `idFormularioAccion` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT de la tabla `inscricategoconcuraprend`
--
ALTER TABLE `inscricategoconcuraprend`
  MODIFY `idInscriCategoConcurAprend` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `inscricategoconcurinstru`
--
ALTER TABLE `inscricategoconcurinstru`
  MODIFY `idInscriCateroConcurInstru` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `instructor`
--
ALTER TABLE `instructor`
  MODIFY `idInstructor` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `leguajeprogramacion`
--
ALTER TABLE `leguajeprogramacion`
  MODIFY `idLeguajeProgramacion` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `mensajecontacto`
--
ALTER TABLE `mensajecontacto`
  MODIFY `idMensajeContacto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `mensajerespuesta`
--
ALTER TABLE `mensajerespuesta`
  MODIFY `idRespuesta` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `idMenu` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `motorbasedatos`
--
ALTER TABLE `motorbasedatos`
  MODIFY `idMotorBaseDatos` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `municipio`
--
ALTER TABLE `municipio`
  MODIFY `idMunicipio` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1132;
--
-- AUTO_INCREMENT de la tabla `nivelprogramaformacion`
--
ALTER TABLE `nivelprogramaformacion`
  MODIFY `idNivelProgramaFormacion` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `oficina`
--
ALTER TABLE `oficina`
  MODIFY `idOficina` smallint(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `parametro`
--
ALTER TABLE `parametro`
  MODIFY `idParametro` smallint(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `idPersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `personainfoadicional`
--
ALTER TABLE `personainfoadicional`
  MODIFY `idPersonaInfoAdicional` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `preguntafrecuente`
--
ALTER TABLE `preguntafrecuente`
  MODIFY `idPreguntaFrecuente` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `proceso`
--
ALTER TABLE `proceso`
  MODIFY `idProceso` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  MODIFY `idProyecto` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `regional`
--
ALTER TABLE `regional`
  MODIFY `idRegional` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `idRol` smallint(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `rolformuaccio`
--
ALTER TABLE `rolformuaccio`
  MODIFY `idRolFormuAccio` smallint(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tercero`
--
ALTER TABLE `tercero`
  MODIFY `idTercero` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tiposolucion`
--
ALTER TABLE `tiposolucion`
  MODIFY `idTipoSolucion` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `unidadnegocio`
--
ALTER TABLE `unidadnegocio`
  MODIFY `idUnidadNegocio` tinyint(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `usuarioarea`
--
ALTER TABLE `usuarioarea`
  MODIFY `idUsuarioArea` smallint(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuariooficina`
--
ALTER TABLE `usuariooficina`
  MODIFY `idUsuarioOficina` smallint(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuariorol`
--
ALTER TABLE `usuariorol`
  MODIFY `idUsuarioRol` smallint(6) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
