<?php include('header.php') ?>

<?php   
    try {
        //$con = new PDO('mysql:host=localhost;dbname=agenda', 'root', '');
        $con = new PDO('mysql:host=creamas.co;dbname=creamas_senasoft', 'creamas_senasoft', 'ubicua', array(PDO::ATTR_PERSISTENT=>TRUE));
        $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch(PDOException $e) {
      echo 'Error de conexión con la base de datos: ' . $e->getMessage();
    }
?>

    <div class="portfolio_box">
        <div class="container">
            <section class="title-section">
                <h1 class="title-header">Agenda Académica</h1>
            </section>
        </div>
    </div>

    <div class="container">
        <div class="row table-responsive">
            <table class="table table-bordered table-hover">
                <tr class="bg-success">
                    <td>Fecha</td>
                    <td>Hora</td>
                    <td>Tema</td>
                    <td>Modalidad</td>
                    <td>Conferencista</td>
                    <td>Cargo</td>
                    <td>Empresa</td>
                    <td>Lugar</td>
                </tr>
                <?php 

                $datos = $con->query('SELECT * FROM agenda');
                
                foreach($datos as $row){?>
                    <?php 
                        if($row[1] == '25'){
                                    echo '<tr class="bg-default">';
                                }else if($row[1] == '26'){
                                    echo '<tr class="bg-success">';
                                }else if($row[1] == '27'){
                                    echo '<tr class="bg-default">';
                                }?>
                        <td>
                            <?php 
                                if($row[1] == '25'){
                                    echo '<img src="images/martes.png">';
                                }else if($row[1] == '26'){
                                    echo '<img src="images/miercoles.png">';
                                }else if($row[1] == '27'){
                                    echo '<img src="images/jueves.png">';
                                }
                            ?>
                        </td>
                        <td><?php echo $row[2]; ?></td>
                        <td><?php echo utf8_encode($row[3]); ?></td>
                        <td><?php echo utf8_encode($row[4]); ?></td>
                        <td><?php echo utf8_encode($row[5]); ?></td>
                        <td><?php echo utf8_encode($row[6]); ?></td>
                        <td><?php echo utf8_encode($row[7]); ?></td>
                        <td><?php echo utf8_encode($row[8]); ?></td>
                    </tr>
                    <?php
                }?>
            </table>
        </div>
    </div>
 
<?php include('footer.php') ?>



