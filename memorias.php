<?php include('header.php') ?>

    <div class="portfolio_box">
        <div class="container">
            <section class="title-section">
            	<h1 class="title-header">Histórico SENASoft</h1>
            </section>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="panel panel-success"> 
                <div class="panel-heading">
                    <h3 class="panel-title">Video SENASoft Quindío 2016</h3> 
                </div> 
                <div class="panel-body">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/btfsubfIod4"></iframe>
                    </div>
                </div> 
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="panel panel-success"> 
                <div class="panel-heading">
                    <h3 class="panel-title">Video SENASoft Santander 2015</h3> 
                </div> 
                <div class="panel-body">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/rZOFtKMGhoA"></iframe>
                    </div>
                </div> 
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="panel panel-success"> 
                <div class="panel-heading">
                    <h3 class="panel-title">Video SENASoft Huila 2014</h3> 
                </div> 
                <div class="panel-body">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/eulIHDwu64w"></iframe>
                    </div>
                </div> 
            </div>
        </div>
    </div>
            
<?php include('footer.php') ?>