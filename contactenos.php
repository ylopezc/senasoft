﻿ 
<?php include('header.php') ?>

    <div class="portfolio_box">
        <div class="container">
            <section class="title-section">
                <h1 class="title-header">Contacto</h1>
            </section>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="panel panel-success"> 
                <div class="panel-heading">
                </div> 
                <div class="panel-body">
                    <img src="images/willys.png" class="col-xs-12 col-md-3 img-responsive img-thumbnail"  />
                    <p class="col-xs-12 col-md-9 text-center">
                        <br>
                        Yovanny López<br>
                        Instructor<br>
                        Centro de Comercio y Turismo, Regional Quindío<br>
                        hlopezy@sena.edu.co
			<br><br>
                        <a href="archivos/lideres.pdf" target="_blank">Listado de líderes de categoría</a>
                    </p>
                </div> 
            </div>
        </div>
    </div>
 
<?php include('footer.php') ?>