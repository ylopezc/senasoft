 
<?php include('header.php') ?>

    <div class="portfolio_box">
        <div class="container">
                <img src="images/creamas.png" class="img-responsive">
        </div>
    </div>

    <div class="container">
        <div class="row text-justify">
            <section class="title-section">
                <h1 class="title-header">Descripción</h1>
            </section>
            <p>SENASoft es el mayor encuentro de tecnología realizado por el SENA. Una de las
iniciativas del evento es incentivar el desarrollo de ideas productivas generadas por los
aprendices a partir del desarrollo de sus competencias en los ambientes de formación.
Para 2016 esta actividad estará a cargo del proyecto líder en generación de ideas de
negocios en la regional Quindio, Creamás.</p>
<p>
La proyección de ideas de negocios Creamás - SENASoft 2016 es un espacio abierto para
todo el SENA, donde los aprendices participantes, provenientes de todo el país tendrán
la posibilidad de tener contacto con empresarios asistentes al evento. Adicionalmente,
los proyectos participantes estarán en una vitrina a la que asistirán aprendices,
instructores, directivos y visitantes de todo el país.</p>
        </div>
    </div><br>

    <div class="container">
        <div class="row text-justify">
            <section class="title-section">
                <h1 class="title-header">¿Quiénes pueden participar?</h1>
            </section>
            <p>La Proyección de ideas de negocios Creamás - SENASoft 2016 está abierta para todos los
aprendices del SENA a nivel nacional (en etapa lectiva o productiva) de cualquier
programa de nivel técnico o tecnológico, que estén cursando como mínimo el segundo trimestre de formación.
            </p>
        </div>
    </div><br>

    <div class="container">
        <div class="row text-justify">
            <section class="title-section">
                <h1 class="title-header">¿Cómo participar?</h1>
            </section>
            <p>Creamás ha desarrollado un proceso de generación y validación de ideas que se ha
puesto en marcha y perfeccionado durante el desarrollo de las tres versiones: 2013, 2014
y 2015 y que se usará como metodología para la participación en la rueda de negocios
Creamás - SENASoft 2016. Este se desarrolla con los siguientes pasos:
            </p>
            <p>a. Los centros de formación deben realizar convocatoria, análisis y filtrado de las
ideas de negocio de sus aprendices, para seleccionar las de mayor proyección en
el mercado. Se recomienda implementar el <a href="archivos/proceso.pdf" target="_blank">"Proceso Creamás"</a>.</p>
            <p>b. Las ideas seleccionadas en cada centro deben realizar su inscripción a través del
sitio web: creamas.co/inscripcion. El formulario incluye preguntas básicas de la
idea de negocio como: nombre de la idea, descripción en un texto no superior a
500 caracteres, mercado objetivo, estado de desarrollo de la idea y datos
generales del participante.</p>
            <p>c. El proceso de inscripción a nivel nacional incluye la realización de un vídeo no
superior a 3 minutos donde describan la idea de negocio y su estado de desarrollo.
El participante debe anexar el link del video, previamente cargado a una plataforma
como Youtube, Vimeo, etc.</p>
            <p>d. El comité Creamás - SENASoft 2016 realizará la selección de las ideas que entrarán
en proceso de fortalecimiento para su participación en la rueda de proyección
empresarial, en la que sólo podrá participar 1 integrante por idea seleccionada.</p>
            <p>e. Cada centro de formación puede presentar un máximo de 3 ideas de negocio.</p>
            <p>El proceso de fortalecimiento debe desarrollarse por instructores del centro de
formación al que pertenece la idea de negocio. Lo recomendado por el comité de
Creamás es conformar un equipo multidisciplinario para garantizar una evaluación
justa y con criterios profesionales desde diferentes áreas del conocimiento como:
finanzas, marketing, identidad corporativa, innovación, TIC y otras que se consideren
indispensables para el fortalecimiento de las ideas. Cada centro de formación
asignará los instructores que conformarán este equipo que se encargará del proceso
de fortalecimiento de las ideas seleccionadas.</p>
            <p>El proceso de fortalecimiento incluye varios momentos y evidencias que requieren el
acompañamiento y evaluación de los padrinos o mentores de cada idea.</p>

        </div>
    </div><br>

    <div class="container">
        <div class="row text-justify">
            <section class="title-section">
                <h1 class="title-header">Momentos y evidencias</h1>
            </section>
            <ul>
                <li><strong>Compromiso:</strong> Los aprendices creadores de las ideas y el
instructor mentor asignado por el centro de formación, deberán
firmar el compromiso de fortalecimiento y participación que puede
descargar <a href="archivos/acta_compromiso.docx" target="_blank">aquí</a> y deben enviar soporte de
éste al correo rueda.creamas@gmail.com</li>
                <li><strong>Modelo de negocios</strong> Creamás desde el 2013 ha utilizado con
excelentes resultados el modelo Canvas y es el que el comité
recomienda para el proceso. Este modelo se puede descargar 
<a href="archivos/canvas.pdf" target="_blank">aquí</a></li>
                <li><strong>Pitch o discurso</strong> Creamás utiliza como base el “Elevator
Pitch” y desarrolla con base en este, versiones para rueda de
negocios, para presentación y para captación de clientes.</li>
                <li><strong>PMV (producto mínimo viable)</strong> Una de las fortalezas más
importantes del modelo Creamás es el desarrollo de un PMV que
permita la validación de la idea, tanto en rueda de negocios como en
el contexto del mercado, previa presentación del producto. Quienes
lleguen a la rueda de negocios deben llevar demo de la idea o
prototipo funcional.</li>
                <li><strong>Estructura financiera básica</strong>
                Inversión requerida para la idea, discriminando el valor de los activos
tangibles e intangibles; costos de producción fijos y variables y un
precio de venta tentativo. Es importante tener un estimado de la
capacidad de producción y el punto de equilibrio.</li>
                <li><strong>Identidad corporativa:</strong> Inversión requerida para la idea, discriminando el valor de los activos
tangibles e intangibles; costos de producción fijos y variables y un
precio de venta tentativo. Es importante tener un estimado de la
capacidad de producción y el punto de equilibrio.</li>
            </ul>
            </p>
        </div>
    </div><br>

    <div class="container">
        <div class="row">
            <section class="title-section">
                <h1 class="title-header">Cronograma de participación</h1>
            </section>
            <img src="images/cronograma.png" class="img-responsive text-center">
        </div>
    </div><br>

    <div class="container">
        <div class="row text-justify">
            <section class="title-section">
                <h1 class="title-header">Consideraciones especiales</h1>
            </section>
            <ul>
                <li>Para garantizar la participación de ideas de negocios que cumplan con los
requisitos mínimos para generar procesos efectivos de validación con
empresarios, el proceso de filtrado y selección de las ideas lo realizará el comité
Creamás - SENASoft 2016.</li>
                <li>La participación en la Rueda de Proyección Empresarial Creamás está sujeta a
los avances que se realicen durante el proceso de fortalecimiento al interior de
cada centro. El comité de Creamás tiene la potestad de retirar de la rueda a
aquellos proyectos que no muestren avances o que no evidencien
fortalecimiento en cualquiera de las etapas del proceso. El comité de Creamás
solicitará avances del fortalecimiento de las ideas.
El retiro puede acarrear sanciones por parte de las directivas de los centros a
los que pertenezcan las ideas o planes de mejoramiento recomendados de
acuerdo a los lineamientos del reglamento del aprendiz.</li>
                <li>El comité de Creamás se compromete a no divulgar las ideas ni a hacer uso de
estas para su desarrollo. Para garantizar la custodia de los secretos industriales
se firmará un acta de confidencialidad entre los desarrolladores de las ideas, los
instructores mentores y el comité de Creamás del centro de Comercio y
Turismo.</li>
                <li>La participación en la Proyección de ideas de negocios no garantiza cierre de
negociaciones o inversiones para las ideas, estas están limitadas al criterio de
los empresarios que asistan al evento. La negociación se da exclusivamente
entre las ideas y los empresarios e inversionistas sin intervención de Creamás o
de los mentores de los centros de formación.</li>
                <li>
                    El proceso de Creamás desarrollado en el Centro de Comercio y Turismo de la
regional Quindío es un modelo de generación de ideas que se recomienda para
su aplicación en el proceso, esta no es una imposición para la participación en
la estrategia; cada centro tiene la libertad de implementar el modelo que
considere más oportuno, siempre y cuando las ideas cumplan con los
requerimientos para la participación en la Rueda de Proyección Empresarial.
                </li>
                <li>El cumplimiento del compromiso firmado es obligatorio, su no cumplimiento
acarrea sanciones de acuerdo con el reglamento del aprendiz.</li>
            </ul>
        </div>
    </div><br>

    <div class="container">
        <div class="row">
            <section class="title-section">
                <h1 class="title-header">Validación CREAMÁS</h1>
            </section>
            <a href="archivos/validacion.pdf" target="_blank">Descargar lineamientos proceso de validación</a>
        </div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <section class="title-section">
                <h1 class="title-header">Contacto</h1>
            </section>
            <p>Líder Creamás<br>
            Ana María Romero Medina<br>
            amromerom@misena.edu.co</p>
        </div>
    </div>

    <br>
 
<?php include('footer.php') ?>