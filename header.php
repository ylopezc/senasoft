<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="content" description="SENASOFT es el mayor encuentro de tecnología realizado por el SENA, como iniciativa de la Red de Conocimiento en Informática, Diseño y Desarrollo de Software. El objetivo de este evento es propiciar entre los aprendices espacios que permitan transferencia de conocimiento, en las habilidades más significativas de la formación TIC.">
       
        <title>SENASoft Quindío 2016</title>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/style.css"> 
        <link rel="stylesheet" href="css/styles_jquery_anuncios_principal.css"/>
          <!-- Start WOWSlider.com HEAD section -->
        <link rel="stylesheet" type="text/css" href="engine0/style.css" />
        <script type="text/javascript" src="engine0/jquery.js"></script>
        <!-- End WOWSlider.com HEAD section -->      
        
        <link type="image/x-icon" href="images/logo.ico" rel="icon" >
        <script src="js/jquery.min.js"></script>
    </head>
    
    <body>
    	<div class="header">
            <div class="container">
                <div class="header-top">
                    <div class="logo">
                        <a href="index.php"><img src="images/LogoSenasoft.png" alt="SENASOFT" class="img-responsive" /></a>
                    </div>
                    <!-- Inicio MENÚ -->
                    <div class="h_menu4">
                        <a class="toggleMenu" href="#">Menú</a>
                            <ul class="nav">
                                <li><a href="index.php">Inicio</a>
                                <li><a href="senasoft.php">SENASOFT 2016</a>
                                </li>
                                <li><a href="memorias.php">Videos - Histórico</a></li>
                                <li><a href="categorias.php">Categorías</a>  </li>
                                <li><a href="archivos/convocatoria_instructores_senasoft.pdf" target="_blank">Convocatoria Silicon Valley</a></li>
                                <li><a href="agenda.php">Agenda Académica</a>
                                </li>
                                <li><a href="#">Empresas</a>
                                    <ul>
                                        <li><a href="empresas.php">Empresas</a></li>
                                        <li><a href="creamas.php">Rueda de proyección empresarial CREAMÁS</a></li>
                                    </ul>
                                </li>
                        
                                <li><a href="contactenos.php">Contáctenos</a></li>
 
                            </ul>

                        <script type="text/javascript" src="js/nav.js"></script>

                    </div><!-- FIN MENÚ -->
                    <div class="clearfix"> </div>

                </div>
            </div>
        </div>