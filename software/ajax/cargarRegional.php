<?php
ini_set("display_errors", 0);
require_once '../entidad/CentroFormacion.php';
require_once '../modelo/CentroFormacion.php';
$retorno = array('exito'=>1,'mensaje'=>'','data'=>null,'numeroRegistros'=>0);
try {
	$centroFormacionE = new \entidad\CentroFormacion();
	
	$centroFormacionM = new \modelo\CentroFormacion($centroFormacionE);
	$retorno['data'] = $centroFormacionM->consultarRegional();
	$retorno['numeroRegistros'] = $centroFormacionM->conexion->obtenerNumeroRegistros();
} catch (Exception $e) {
	$retorno['exito'] = 0;
	$retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
?>