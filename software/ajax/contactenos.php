<?php
ini_set("display_errors", 0);
$retorno = array('exito'=>1 , 'enviado'=>0 , 'mensaje'=>'El mensaje se envió correctamente.');
try {    
    $nombres = $_POST['nombres'];
    $correoElectronico = $_POST['correoElectronico'];   
    $asunto = $_POST['asunto'];
    $rol = $_POST['rol'];
    $regional = $_POST['regional'];
    $centroFormacion = $_POST['centroFormacion'];
    $idCentroFormacion = $_POST['idCentroFormacion'];
    $mensaje = $_POST['mensaje'];
	
    date_default_timezone_set('America/Bogota');
    $destinatario = "contacto@senasoft.co";
    
    $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    
    $cabeceras .= "From: ".$nombres." <".$correoElectronico.">\r\n";
    
    $mensajeEnviar = '<b>Regional: </b>'.$regional;
    $mensajeEnviar .= '<br><b>Centro de formación: </b>'.$centroFormacion;
    $mensajeEnviar .= '<br><b>Rol: </b>'.$rol;
    $mensajeEnviar .= '<br><br>'.$mensaje;
    
    if(mail($destinatario, $asunto, $mensajeEnviar , $cabeceras)){
        $retorno['enviado'] = 1;
    }else{
        $retorno['enviado'] = 0;
    }
}catch (Exception $ex) {
    $retorno['mensaje'] = $ex->getMessage();
    $retorno['exito'] = 0;
}
echo json_encode($retorno);
?>