<?php
ini_set("display_errors", 0);
require_once '../entidad/SectorEconomico.php';
require_once '../modelo/SectorEconomico.php';
$retorno = array('exito'=>1,'data'=>null,'numeroRegistros'=>null,'mensaje'=>'');

try {
	$sectorEconomicoE = new \entidad\SectorEconomico();
	$sectorEconomicoE->setEstado(1);
	
	$sectorEconomicoM = new \modelo\SectorEconomico($sectorEconomicoE);
	$retorno['data'] = $sectorEconomicoM->consultar();
	$retorno['numeroRegistros'] = $sectorEconomicoM->conexion->obtenerNumeroRegistros();
}catch(Exception $e){
	$retorno['exito'] =0 ;
	$retorno['mensaje'] =$e->getMessage();
}
echo json_encode($retorno);
?>