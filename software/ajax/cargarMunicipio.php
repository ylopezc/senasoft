<?php
ini_set("display_errors", 0);
require_once '../entorno/Conexion.php';
$nombreMunicipio = $_GET['term'];
echo json_encode(buscarMunicipio($nombreMunicipio));

function buscarMunicipio($nombreMunicipio){
    $conexion = new \Conexion();
    $datos = array();
    $sentenciaSql = "
                    SELECT
                        m.idMunicipio
                        ,m.municipio
                        ,d.departamento
                    FROM
                        municipio as m
                        INNER JOIN departamento as d ON d.idDepartamento = m.idDepartamento    
                    WHERE
                        CONCAT_WS(' - ',m.municipio,d.departamento) LIKE '%$nombreMunicipio%'
                        
                    ";
    $conexion->ejecutar($sentenciaSql);
    while($fila = $conexion->obtenerObjeto()){
        $datos[] = array("value"=>$fila->municipio." - ".$fila->departamento,
			"municipio"=>$fila->municipio,
                        "idMunicipio"=>$fila->idMunicipio
                        );
    }
    return $datos;
}
?>