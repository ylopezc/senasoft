<?php
ini_set("display_errors", 0);
require_once '../entidad/Proyecto.php';
require_once '../modelo/Proyecto.php';
ini_set("display_errors", 0);
$proyectoE = new \entidad\Proyecto();

$proyectoM = new \modelo\Proyecto($proyectoE);
$nombreProyecto = $_GET['term'];
echo json_encode($proyectoM->buscarProyecto($nombreProyecto));
?>