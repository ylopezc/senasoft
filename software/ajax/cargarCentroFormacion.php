<?php
ini_set("display_errors", 0);
require_once '../entidad/CentroFormacion.php';
require_once '../modelo/CentroFormacion.php';
require_once '../entidad/Regional.php';
$retorno = array('exito'=>1,'mensaje'=>'','data'=>null,'numeroRegistros'=>0);
try {
	$idRegional = $_POST['idRegional'];
	
	$centroFormacionE = new \entidad\CentroFormacion();
	
	$regionalE = new \entidad\Regional();
	$regionalE->setIdRegional($idRegional);
	$centroFormacionE->setRegional($regionalE);
	
	$centroFormacionM = new \modelo\CentroFormacion($centroFormacionE);
	$retorno['data'] = $centroFormacionM->consultar();
	$retorno['numeroRegistros'] = $centroFormacionM->conexion->obtenerNumeroRegistros();
} catch (Exception $e) {
	$retorno['exito'] = 0;
	$retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
?>