<?php
ini_set("display_errors", 0);
session_start();
require_once '../lib/securimage/securimage.php';
$retorno = array('exito'=>1,'mensaje'=>'');
try {
    $imagen = new Securimage();
    $captcha = $_POST['captcha'];
    $validarCaptcha = $imagen->check($captcha);
    
    if($validarCaptcha){
        $retorno['exito'] = 1;
        $retorno['mensaje'] = 'El captcha es correcto';
    }else{
        $retorno['exito'] = 0;
        $retorno['mensaje'] = 'El texto de la imagen con caracteres especiales ingresado es incorrecto.';
    }
} catch (Exception $exc) {
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $exc->getMessage();
}
echo json_encode($retorno);
?>