<?php
ini_set("display_errors", 0);
require_once '../entidad/EmpresaCodigo.php';
require_once '../modelo/EmpresaCodigo.php';
$retorno = array('exito'=>1,'mensaje'=>'','data'=>null,'numeroRegistros'=>0);
try {
	$empresaCodigoE = new \entidad\EmpresaCodigo();
	
	$empresaCodigoM = new \modelo\EmpresaCodigo($empresaCodigoE);
	$retorno['data'] = $empresaCodigoM->consultarEstadoEmpresa();
	$retorno['numeroRegistros'] = $empresaCodigoM->conexion->obtenerNumeroRegistros();
} catch (Exception $e) {
	$retorno['exito'] = 0;
	$retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
?>