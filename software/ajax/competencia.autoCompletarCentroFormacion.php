<?php
ini_set("display_errors", 0);
require_once '../entidad/CentroFormacion.php';
require_once '../modelo/CentroFormacion.php';
$centroFormacionE = new \entidad\CentroFormacion();

$centroFormacionM = new \modelo\CentroFormacion($centroFormacionE);
$centroFormacion = $_GET['term'];
echo json_encode($centroFormacionM->buscarCentroFormacion($centroFormacion));
?>