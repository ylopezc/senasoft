<?php
ini_set("display_errors", 0);
require_once('../entidad/Menu.php');
require_once('../modelo/Menu.php');
try {
    $menu = '';
    $menuEC = new \entidad\Menu();
    $menuMC = new \modelo\Menu($menuEC);
    
    $menuE = new \entidad\Menu();
    $menuM = new \modelo\Menu($menuE);
    
    $menuMC->cargarCarpetasPadres();
    //Creo la etiqueta del menu
    $menu .= '<div class="collapse navbar-collapse navbar-ex1-collapse">';
    $menu .= '<ul class="nav navbar-nav">';
    while($fila = $menuMC->conexion->obtenerObjeto()){
        if($fila->idMenu == 1){
            $menu .= '<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" id="'.$fila->idMenu.'"><span class="fa fa-home fa-2x iconosMenu"></span></a>';
        }else{
            $menu .= '<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">'.$fila->etiqueta.'</a>';
        }
        //En caso tal de que esa carpeta tenga formularios
        $menu .= $menuM->obtenerFormulario($fila->idMenu);
        //En caso tal de que esa carpeta tenga más carpetas
        $menu .= $menuM->obtenerMenuDinamico($fila->idMenu);
        $menu .= '</li>';
    }
    //Cierro la etiqueta del menu
    $menu .= '</ul>';
    //Nombre del usuario y el vinculo de cerrarSesion
	
	$nombre = $_SESSION['nombre'];
	$apellido = $_SESSION['apellido'];
    $foto = $_SESSION['foto'];
    $menu .= '<ul class="nav navbar-nav navbar-right">
			  <li><i class="fa fa-envelope iconoMensaje"></i></li>
			  <li class="liFotoUsuario"><img width="40px" height="48px" style="border-radius:50px;" src="'. $foto .'"></li>
              <li><p class="navbar-text"><span class="nombreUsuario">'.$_SESSION['nombre'].' '.$_SESSION['apellido'].'</span></p></li>
              <li><a href="/Senasoft-admin/entorno/cerrarSesion.php"><i class ="fa fa-sign-out fa-2x iconosMenu"></i></a></li>
            </ul>';
    $menu .= '</div>';
   
} catch (Exception $e) {
    echo $e->getMessage();
}
 echo $menu;
?>