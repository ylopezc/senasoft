<?php
ini_set("display_errors", 0);
require_once '../entorno/Conexion.php';
$nombreEmpresa = $_GET['term'];
echo json_encode(buscarEmpresa($nombreEmpresa));

function buscarEmpresa($nombreEmpresa){
    $conexion = new \Conexion();
    $datos = array();
    $sentenciaSql = "
                    SELECT DISTINCT
                        e.idEmpresa
                        ,e.empresa
                    FROM
                        empresa AS e
						INNER JOIN empresaproducto AS ep ON ep.idEmpresa = e.idEmpresa
					WHERE
                        empresa LIKE '%$nombreEmpresa%'
						AND (ep.idTipoEmpresaProducto = 1 OR ep.idTipoEmpresaProducto = 2)
						
                    ";
    $conexion->ejecutar($sentenciaSql);
    while($fila = $conexion->obtenerObjeto()){
        $datos[] = array("value"=>$fila->empresa,
			"empresa"=>$fila->empresa,
                        "idEmpresa"=>$fila->idEmpresa
                        );
    }
    return $datos;
}
?>