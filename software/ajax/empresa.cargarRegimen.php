<?php
ini_set("display_errors", 0);
require_once '../entidad/RegimenEmpresa.php';
require_once '../modelo/RegimenEmpresa.php';
$retorno = array('exito'=>1,'data'=>null,'numeroRegistros'=>null,'mensaje'=>'');

try {
	$regimenEmpresaE = new \entidad\RegimenEmpresa();
	$regimenEmpresaE->setEstado(1);
	
	$regimenEmpresaM = new \modelo\RegimenEmpresa($regimenEmpresaE);
	$retorno['data'] = $regimenEmpresaM->consultar();
	$retorno['numeroRegistros'] = $regimenEmpresaM->conexion->obtenerNumeroRegistros();
}catch(Exception $e){
	$retorno['exito'] =0 ;
	$retorno['mensaje'] =$e->getMessage();
}
echo json_encode($retorno);
?>