// JavaScript Document
var nit = null;
var empresa = null;
var correo =  null;
var data = null;

var data = null;
$(function(){
	cargarDialogMensaje();
	$("#btnRegistrar").bind({
		"click":function(){
			if(validarVacios(document.frmInscripcionEmpresa) == false)
				return false;
			if(validarCorreoElectronico("txtCorreoElectronico") == false){
				alerta("El correo electrónico de la empresa no es válido");
				return false;
			}
			obtenerDatosEnvio();
			$.ajax({
				url:'../controlador/empresaCodigo.adicionar.php',
				type:'POST',
				dataType:"json",
				data:data,
				success: function(json){
					var exito = json.exito;
					var mensaje = json.mensaje;
					var numeroRegistros = json.numeroRegistros;
					
					if(exito == 0){
						alerta("Error adicionando la empresa");
						return false;
					}
					alerta(mensaje);
					limpiarVariables();
					limpiarControlesFormulario(document.frmInscripcionEmpresa);
				},error: function(xhr, opciones, error){
					alerta(error);
				}
			});
		}
	});
});
function asignarValoresEnvio(){
	nit = $("#txtNit").val();
	empresa = $("#txtEmpresa").val();
	correo = $("#txtCorreoElectronico").val();
}
function obtenerDatosEnvio(){
	asignarValoresEnvio();
	data = 'nit=' + nit+'&empresa=' +empresa+'&correoElectronico='+correo;
}
function limpiarVariables(){
	nit = '';
	empresa = '';
	correo =  '';
	data = '';
}