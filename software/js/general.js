﻿// JavaScript Document
// JavaScript Document
var nombreFormulario = '';
var idEventos = new Array();
var banderaDialogo = 1;
var banderaFormulario = 0;
var contadorCarpetas=0;
var nombreApellidoUser ='';
var banderaInterval = 0;

function validarVacios(f){
	//validaLogueo();
	for (var i = 0; i < f.elements.length; i++){
		if(f.elements[i].accessKey.match('V') != 'V'){
			switch(f.elements[i].type){
				case "text":
					var valorControl = $.trim(f.elements[i].value);
					if( valorControl == "" ){
						if(f.elements[i].id != "txtUbicacion" || banderaFormulario != 1){
							alerta("Por favor digite " + f.elements[i].title);
							f.elements[i].focus();
							return false;
						}
					}
				break;
				case "password":
					var valorControl = $.trim(f.elements[i].value);
					if( valorControl == "" ){
						alerta("Por favor digite " + f.elements[i].title);
						f.elements[i].focus();
						return false;
					}
				break;
				case "select-one":
					if(f.elements[i].options[0].selected == true && f.elements[i].options[0].text != 'ACTIVO' && f.elements[i].id != 'selCarpetaDestino' && f.elements[i].id != "selRoles"){
						alerta("Por favor escoja " + f.elements[i].title);
						return false;
					}
				break;
				case "textarea":
					var valorControl = $.trim(f.elements[i].value);
					if( valorControl == "" ){
						alerta("Por favor digite " + f.elements[i].title);
						f.elements[i].focus();
						return false;
					}
				break;
				case "file":
					if(f.elements[i].value == ""){
						alerta("Por favor seleccione " + f.elements[i].title);
						f.elements[i].focus();
						return false;
					}
				break;
				case "radio2":
					var opciones = document.getElementsByName(f.elements[i].name); 
					var seleccionado = false;
					for(var i=0; i<opciones.length; i++) {    
					  if(opciones[i].checked) {
						seleccionado = true;
					  }
					}
					 
					if(!seleccionado) {
					  alerta("Por escoja " + f.elements[i].title);		
					  return false;
					}
				break;
			}
		}
	}
	
	limpiarCaracteresEspeciales(f);
	
	return true;
}
function validarVaciosConsultar(f){
	var numeroControlesVacios = 0;
	validaLogueo();
	for (var i = 0; i < f.elements.length; i++){
		switch(f.elements[i].type){
			case "text":
				if(f.elements[i].value == "" ){
					if(f.elements[i].id != "txtUbicacion" || banderaFormulario != 1){
						numeroControlesVacios++;
					}
				}
			break;
			case "select-one":
				if(f.elements[i].options[0].selected == true && f.elements[i].options[0].text != 'ACTIVO' && f.elements[i].id != 'selAcciones' && f.elements[i].id != 'selCarpetaDestino'){
					numeroControlesVacios++;
				}
			break;
			case "textarea":
				if(f.elements[i].value == ""){
					numeroControlesVacios++;
				}
			break;
			case "checkbox":
				if(f.elements[i].checked == false){
                                    numeroControlesVacios++;
                                };
			break;
			case "radio":
				if(f.elements[i].checked == false){
					numeroControlesVacios++;
				};
			break;
		}
	}
	return numeroControlesVacios;
}

function limpiarControlesFormulario(f){
	for(var i = 0; i < f.elements.length; i++){
		switch(f.elements[i].type){
			case "text":
				f.elements[i].value = "";
			break;
			case "select-one":
				f.elements[i].options[0].selected = true;
			break;
			case "tel":
				f.elements[i].value = "";
			break;
			case "file":
				f.elements[i].value = "";
			break;
			case "password":
				f.elements[i].value = "";
			break;
			case "checkbox":
				f.elements[i].checked = 0;
			break;
			case "textarea":
				f.elements[i].value = '';
			break;
			case "email":
				f.elements[i].value = "";
			break;
			case "radio":
				f.elements[i].checked = false;
			break;
			case "file":
				f.elements[i].value = '';
			break;
		}
	}
}
function obtenerSelectEstado(control){
	control.empty();
	control.append('<option value="1">ACTIVO</option>');
	control.append('<option value="0">INACTIVO</option>');
}

function obtenerSelectEstadoSeleccione(control){
	control.empty();
	control.append('<option value="">---SELECCIONE---</option>');
	control.append('<option value="1">ACTIVO</option>');
	control.append('<option value="0">INACTIVO</option>');
}
function validarNumeros(t){//
	document.getElementById(t).onkeypress = function (event){
		if (event.which >= 48 && event.which <=58 || event.which >= 0 && event.which <= 31){
			return true;
		}else{
			return false;
		}
	}
}

function variableUrl(){
	var src = '';
	var cadenaPrueba = '';
	if(String( window.location.href ).split('?')[1]){
		src = String( window.location.href ).split('?')[1];
		src = src.replace("%C2%A0%C2%A0%C2%A0","");
		src = decodeURI(src);
		var srcDos = src.split('=');
		if(srcDos[0] == 'id'){
			return srcDos;
		}
		src = src.split('&');
		for(i=0; i < src.length; i++){
			src[i] = src[i].substring(src[i].indexOf('=')+1);
		}
		if(src[3] != '1'){
			cadena = src[0].indexOf(' ');	
			src[0] = src[0].substring(cadena+1);
		}
	}
	return src;
}
function validarLetras(t){
	document.getElementById(t).onkeypress = function (event){
		if (event.which >= 97 && event.which <= 122 || event.which >= 65 && event.which <= 90 || event.which >= 0 && event.which <= 32 || event.which >= 46 && event.which <= 47)
			return true;
		else
			return false;
	}
}
function obtenerHoraFechaActual(){
	var fecha = new Date();
	var anio = fecha.getFullYear();
	var mes = fecha.getMonth() + 1;
	var dia = fecha.getDate();
	var hora = fecha.getHours();
	var minuto = fecha.getMinutes();
	
	var fechaHora = anio + "-" + mes + "-" + dia + " " + hora + ":" + minuto;
	return fechaHora;
}
function abrirVentanaSeguridad(cargarVentanaModal, id){
	$.ajax({
		url:'/Senasoft-admin/vista/'+cargarVentanaModal,
		data:null,
		dataType:"html",
		success: function(html){
			$(function(){
				w = $(window).width();
				h = $(window).height();
				
				var ruta = '/Senasoft-admin/vista/'+ cargarVentanaModal+'?id='+ id;
				var iframe = '<iframe id="ventanaModal" src="' + ruta + '" frameborder="0" style="width:100%; height:' + h + 'px;"/>';
				$("#modalBody").html(iframe);
				$(".modal-dialog").css("width","100%");
				$("#abrirVentanaModal").click();
			});
		}
	});
}
function abrirVentana(cargarVentanaModal, id){
	$.ajax({
		url:'../vista/'+cargarVentanaModal,
		data:null,
		dataType:"html",
		success: function(html){
			$(function(){
				w = $(window).width();
				h = $(window).height();

				var iframe = '<iframe id="ventanaModal" src="'+ cargarVentanaModal+'?id='+ id+'" frameborder="0" style="width:100%; height:' + h + 'px;"/>';
				$("#modalBody").html(iframe);
				$(".modal-dialog").css("width","100%");
				$("#abrirVentanaModal").click();
				
			});
		}
	});
}
function validarCorreoElectronico(control){
	var correoElectronico = document.getElementById(control).value;
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(correoElectronico))
        return false
}
$(function(){
	$('#imgCancelar').click(function(){
		//$("#btnCerrarDialogo").click();
		$("#btnCancelar").click();
	});
});
function alerta(mensaje){
	$("#textoMensaje").html(mensaje);
	$("#mensaje").dialog("open");
}
function cargarDialogMensaje(){
	
	$( "#mensaje" ).dialog({
      autoOpen: false, // no abrir automáticamente
      resizable: false, //permite cambiar el tamaño
      height:200,
	  width:500,
	  show: "fold",
      hide: "scale", // altura
      modal: true,
	  dialogClass: 'noTitleStuff',
	  open: function() {
            jQuery('.ui-widget-overlay').bind('click', function() {
                jQuery('#mensaje').dialog('close');
            })
        }
    });
}
function validarCaptcha(captcha){
	var retorno = true;
	$.ajax({
		async:false,
		url:'../ajax/validarCaptcha.php',
		type:'POST',
		dataType:"json",
		data:{captcha:captcha},
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;
			
			if(exito == 0){
				retorno = false;
				alerta(mensaje);
				return false;
			}
		},error: function(xhr, opciones, error){
			alerta(error);
		}
	});
	return retorno;
}
function validarNumeroCelular(celular){
	  var expresionRegular1=/^([0-9]+){10}$/;//<--- con esto vamos a validar el numero
	  var expresionRegular2=/\s/;//<--- con esto vamos a validar que no tenga espacios en blanco
	 
	  if(expresionRegular2.test(celular))
	    return false
	  else if(!expresionRegular1.test(celular))
		return false
	  return true;
}
function validarTamanoArchivo(id,peso){
	var input = document.getElementById(id);
	var file = input.files[0];
	var tamano = file.size/1024;

	if(tamano > peso){
		alerta("El tamaño del archivo supera los " + peso + " Kbs.");
		input.value = "";
		return false;
	}
	return true;
}
function validarExtensionImagen(id) {
	var value = document.getElementById(id).value;	

	if(value.match(/.(jpg)|(jpeg)|(png)|(gif)|(JPG)|(JPEG)|(PNG)|(GIF)$/))
		return true;
	document.getElementById(id).value = '';	
	return false;
}
function validarExtensionArchivo(id) {
	var value = document.getElementById(id).value;	

	if(value.match(/.(pdf)|(PDF)$/))
		return true;
	document.getElementById(id).value = '';	
	return false;
}
function limpiarCaracteresEspeciales(f){
	for (var i = 0; i < f.elements.length; i++){
		switch(f.elements[i].type){
			case "text":
				var cadena = $.trim(f.elements[i].value);
				var resultado = cadena.replace(/"/g, "");
				resultado = resultado.replace(/'/g, "");
				resultado = resultado.replace(/;/g, "");
				resultado = resultado.replace(/--/g, "");
				resultado = resultado.replace(/%/g, "");
				resultado = resultado.replace(/#/g, "");
				resultado = resultado.replace(/$/g, "");
				f.elements[i].value = $.trim(resultado);
			break;
			case "textarea":
				var cadena = $.trim(f.elements[i].value);
				var resultado = cadena.replace(/"/g, "");
				resultado = resultado.replace(/'/g, "");
				resultado = resultado.replace(/;/g, "");
				resultado = resultado.replace(/--/g, "");
				resultado = resultado.replace(/%/g, "");
				resultado = resultado.replace(/#/g, "");
				resultado = resultado.replace(/$/g, "");
				f.elements[i].value = $.trim(resultado);
			break;
		}
	}
}