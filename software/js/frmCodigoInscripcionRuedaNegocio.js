// JavaScript Document
var codigoInscripcion = null;

var data = null;
$(function(){
	cargarDialogMensaje();
	$("#txtCodigoInscripcion").focus();
	
	$("#txtCodigoInscripcion").bind({
		"keypress":function(e){
			if(e.keyCode == 13){
				$("#btnEnviar").click();
				return false;
			}
		}
	});
	
	$("#btnEnviar").bind({
		"click":function(){
			obtenerDatosEnvio();
			if(validar() == false)
					return false;
			$.ajax({
				url:'../controlador/ruedaNegocio.validarCodigo.php',
				type:'POST',
				dataType:"json",
				data:data,
				success: function(json){
					var exito = json.exito;
					var mensaje = json.mensaje;
					var numeroRegistros = json.numeroRegistros;
					
					if(numeroRegistros != 0){
						window.open("frmInscrRuedaNegoc.html","_parent");
					}else{
						alerta("Debe ingresar un código válido para la inscripción de su empresa.");
						$("#txtCodigoInscripcion").focus();
						return false;
					}
				},error: function(xhr, opciones, error){
					alerta(error);
				}
			});
		}
	});
});
function asignarValoresEnvio(){
	codigoInscripcion = $("#txtCodigoInscripcion").val();
}
function obtenerDatosEnvio(){
	asignarValoresEnvio();
	data = 'codigoInscripcion=' + codigoInscripcion;
}
function validar(){
	if($("#txtCodigoInscripcion").val() == ""){
		alerta("Debe ingresar un código de inscripción.");
		$("#txtCodigoInscripcion").focus();
		return false;
	}
}