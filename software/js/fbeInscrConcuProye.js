﻿// JavaScript Document

var idProyecto = null;

var numeroIdentificacion = null;

var idTipoSolucion = null;

var idRegional = null;

var idCentroFormacion = null;



var data = null;

$(function(){

	$("body").css("overflow-y","scroll");

	validarNumeros("txtDocumentoAprendiz");

	cargarDialogMensaje();

	cargarTipoSolucion();

	autocompletarNombreProyecto();

	cargarRegional();

	$("#selRegional").change(function(){

		cargarCentroFormacion($("#selRegional").val());

	});

	$("#btnConsultar").click(function(){

		obtenerDatosEnvio();

		$.ajax({

			url:'../controlador/inscripcionProyecto.consultar.php',

			type:'POST',

			dataType:"json",

			data:data,

			success: function(json){

				var exito = json.exito;

				var mensaje = json.mensaje;

				var numeroRegistros = json.numeroRegistros;

				

				if(exito == 0){

					alerta(mensaje);

					return false;

				}

				if(numeroRegistros == 0){

					alerta("No se encontraron registros con los parámetros indicados");

					return false;

				}

				crearListado(json);

			},error: function(xhr, opciones, error){

				alerta(error);

			}

		});

	});

	$("#btnLimpiar").click(function(){

		limpiarControlesFormulario(document.frmConsltarProyecto);

		limpiarVariables();

		$("#tablaProyectos").html("");

	});

	$("#txtNombreProyecto").bind({

		"keypress":function(e){

			if(e.keyCode == 08 || e.keyCode == 46){

				idProyecto = '';

			}

		}

	});

	$("#txtCentroFormacion").bind({

		"keypress":function(e){

			if(e.keyCode == 08 || e.keyCode == 46){

				idCentroFormacion = '';

			}

		}

	});

});

function cargarTipoSolucion(){

	$.ajax({

		url:'../controlador/inscripcionProyecto.cargarTipoSolucion.php',

		type:'POST',

		dataType:"json",

		data:null,

		success: function(json){

			var exito = json.exito;

			var mensaje = json.mensaje;

			var numeroRegistros = json.numeroRegistros;

			

			if(exito == 0){

				alerta(mensaje);

				return false;

			}

			if(numeroRegistros == 0){

				return false;

			}

			var control = $("#selTipoSolucion");

			control.empty();

			control.append('<option value="">--Seleccione el tipo de solución--</option>');

			$.each(json.data,function(contador, fila){

				control.append('<option value="' + fila.idTipoSolucion + '">' + fila.tipoSolucion + '</option>');

			});

			

		},error: function(xhr, opciones, error){

			alerta(error);

		}

	});

}

function autocompletarNombreProyecto(){

	$("body").css("text-align","left");

	$("#txtNombreProyecto").autocomplete({

		source:'../ajax/inscripcionProyecto.autoCompletarNombreProyecto.php',

		select:function(event, ui){

			idProyecto = ui.item.idProyecto;		

		}

	});

}

function limpiarVariables(){

	idProyecto = '';
	numeroIdentificacion = '';
	idTipoSolucion = '';
	idRegional = '';
	idCentroFormacion = '';
	
	data = '';
}

function asignarValoresEnvio(){

	numeroIdentificacion = $("#txtDocumentoAprendiz").val();

	idTipoSolucion = $("#selTipoSolucion").val();

	idRegional = $("#selRegional").val();

	idCentroFormacion = $("#selCentroFormacion").val();

}

function obtenerDatosEnvio(){

	asignarValoresEnvio();

	data = 'idProyecto=' + idProyecto + '&numeroIdentificacion=' + numeroIdentificacion + '&idTipoSolucion=' + idTipoSolucion + '&idRegional=' + idRegional + '&idCentroFormacion=' + idCentroFormacion;

}

function crearListado(json){

	$("#tablaProyectos").html("");

	var tabla = '<table class="consultaTabla">';

	tabla += '<tr>';

	tabla += '<th>Nombre del Proyecto</th>';

	tabla += '<th>URL Proyecto</th>';

	tabla += '<th>Descripción</th>';

	tabla += '<th>Centro de Formación</th>';

	tabla += '<th>URL Video</th>';

	tabla += '<th>Aprendiz Responsable</th>';

	tabla += '</tr>';

	$.each(json.data,function(contador, fila){

		tabla += '<tr>';

		tabla += '<td>' + fila.proyecto + '</td>';

		tabla += '<td><a href="http://' + fila.url + '" target="_blank">' + fila.url + '</a></td>';

		tabla += '<td>' + fila.descripcion + '</td>';

		tabla += '<td>' + fila.centroFormacion + '</td>';

		tabla += '<td><a href="http://' + fila.urlVideo + '" target="_blank">' + fila.urlVideo + '</a></td>';

		tabla += '<td>' + fila.nombreAprendiz + '</td>';

		tabla += '</tr>';

	});

	tabla += '</table>';

	$("#tablaProyectos").html(tabla);

}

function cargarRegional(){

	$.ajax({

		   url:'../ajax/cargarRegional.php',

		   type:'POST',

		   dataType:"json",

		   data:null,

		   success: function(json){

			   var exito = json.exito;

			   var mensaje = json.mensaje;

			   

				if(exito == 0){

					alerta(mensaje);

					return false;

		   		}

				if(json.numeroRegistros == 0){

					alerta("No se encontraron registros con los parámetros indicados");

					retorno = false;

					return false;

				}

				var control = $("#selRegional");

				control.empty();

				control.append('<option value="">--Seleccione la regional--</option>');

				$.each(json.data, function(contador,fila){

					control.append('<option value="' + fila.idRegional + '">' + fila.regional + '</option>');

				});

		   },error:function(xhr,opciones,error){

				alerta("error"+ error);

			}

		});



}

function cargarCentroFormacion(id){

	if(id == ""){

		var control = $("#selCentroFormacion");

		control.empty();

		control.append('<option value="">--Seleccione el centro de formación--</option>');

		return false;

	}

	$.ajax({

	   url:'../ajax/cargarCentroFormacion.php',

	   type:'POST',

	   dataType:"json",

	   data:{idRegional:id},

	   success: function(json){

		   var exito = json.exito;

		   var mensaje = json.mensaje;

		   

			if(exito == 0){

				alerta(mensaje);

				return false;

	   		}

			if(json.numeroRegistros == 0){

				alerta("No se encontraron registros con los parámetros indicados");

				retorno = false;

				return false;

			}

			var control = $("#selCentroFormacion");

			control.empty();

			control.append('<option value="">--Seleccione el centro de formación--</option>');

			$.each(json.data, function(contador,fila){

				control.append('<option value="' + fila.idCentroFormacion + '">' + fila.centroFormacion + '</option>');

			});

	   },error:function(xhr,opciones,error){

			alerta("error"+ error);

		}

	});



}