var idEmpresa = null;
var nit = null;
var empresa = null;
var empresaProducto = null;
var representanteLegal = null;
var direccion = null; 
var celular = null;
var telefonoFijo = null;
var correoElectronico = null;
var urlEmpresa = null;
var estado = null;
var idMunicipio = null;
var municipio = null;
var idDepartamento = null
var departamento = null;

var idCategoriaProducto = new Array();
var categoriaProducto = new Array();
var producto = new Array();

var data= '';

$(function(){
	montarTabla();
	cargarDialogMensaje();
	
	$("#tabCliente").tabs();
	$( "body" ).css("overflow-y","scroll");	
		
	$.ajax({
		url:'../controlador/ruedaNegocio.cargarCategoria.php',
		type:'POST',
		dataType:'json',
		data:null,
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;	
			
			if(exito == 0){
				alerta(mensaje);
				return false;
			}
			
			var control1 = $("#selCategoriaVenta");
			var control2 = $("#selCategoriaCompra");
			control1.empty();
			control2.empty();
			control1.append('<option value="">---Seleccione la categoría de su producto---</option>');
			control2.append('<option value="">---Seleccione la categoría de su producto---</option>');
			$.each(json.data, function(contador, fila){
				if(fila.categoriaProducto)
					control1.append('<option value="'+fila.idCategoriaProducto+'">'+fila.categoriaProducto+'</option>');
					control2.append('<option value="'+fila.idCategoriaProducto+'">'+fila.categoriaProducto+'</option>');
			});
		},error:function(xhr,opciones,error){
			alerta(error);
		}
	});	
	
	$("#btnAdicionarProducto").click(function(){
		if(document.getElementById("selCategoria").options[0].selected == true){
			alerta("Por favor escoja una categoría.");
			return false;
		}
		if(document.getElementById("txaProduEmpre").value == ""){
			alerta("Por favor digite el producto.");
			return false;
		}
		
		idCategoriaProducto.push($("#selCategoria").val());
		categoriaProducto.push($("#selCategoria option:selected").text());
		producto.push($("#txaProduEmpre").val());
		
		$("#selCategoria").val('');
		$("#txaProduEmpre").val('');
		
		crearListadoProductos();
	});
	
	$("#btnGuardarInforEmpre").bind({
		"click":function(){
			obtenerDatosEnvio();
			if( validarVacios(document.frmInforEmpre) == false){
				$("#tabRuedaNegocios").tabs( "option","active",0);
				return false
			}
			if(validarCorreoElectronico("txtCorreo") == false){
				alerta("El correo electrónico ingresado no es válido");
				return false;	
			}
			if(validarNumeroCelular($('#txtCelular').val()) == false){
				alerta("El número de celular no es válido");
				return false;	
			}
			if($("#selCategoria").val()!= '' && $("#txaProduEmpre").val() != '' ){
				$("#tabRuedaNegocios").tabs( "option","active",1);
				alerta("Debe seleccionar la opción de adicionar ");
				return false;
			}
			if($("#selCategoria").val()!= '' || $("#txaProduEmpre").val() != '' ){
				$("#tabRuedaNegocios").tabs( "option","active",1);
				alerta("Debe seleccionar la opción de adicionar ");
				return false;
			}
			if(idCategoriaProducto.length == 0){
				$("#tabRuedaNegocios").tabs( "option","active",1);
				alerta("Debe ingresar al menos un producto o servicio.");
				return false;
			}
			if(idMunicipio == ""){
				alerta("Por favor digite el municipio .");
				return false;
			}
			if($("#txtCaptcha").val() == ""){
				alerta("Debe ingresar los caracteres especiales mostrados en la imagen");
				$("#txtCaptcha").focus();
				return false;
			}
			if(validarCaptcha($("#txtCaptcha").val()) == false)
				return false;
			$.ajax({
				async:false,
				url:"../controlador/ruedaNegocioEmpresa.guardar.php",
				type:'POST',
				dataType:"json",
				data:data,
				success: function(json){
					var exito = json.exito;
					var mensaje = json.mensaje;
					
					if (exito == 0) 
                        return false;                                                   
					
					idEmpresa = json.idEmpresa;
					montarTabla();
				 		
				},error: function(xhr, opciones, error){
					alerta (error);
				}
			});

			$.ajax({
				async:false,
				url:"../controlador/ruedaNegocioEmpresaProducto.guardar.php",
				type:'POST',
				dataType:"json",
				data:{idEmpresa:idEmpresa,
					idCategoriaProducto:idCategoriaProducto,
					empresaProducto: producto},
				success: function(json){
					var exito = json.exito;
					var mensaje = json.mensaje;
					
					if (exito == 0) 
                        return false;
						                     
					idEmpresaProducto = json.idEmpresaProducto;
				 		
				},error: function(xhr, opciones, error){
					alerta (error);
				}
			});
			
			alerta("La empresa se registró correctamente.");	
			$('#tablaProductos').html('');
			limpiarVariables();
			limpiarControlesFormulario(document.frmInforEmpre);
			$("#imgRecargarCaptcha").click();
		}
	});
});

function asignarValores(){
    nit = $("#txtNit").val();
    empresa = $("#txtEmpresa").val();
    representanteLegal = $("#txtRepreLegal").val();
	direccion = $("#txtDireccion").val();
	telefonoCelular = $("#txtCelular").val();
	telefonoFijo = $("#txtTeleFijo").val();
	correoElectronico = $("#txtCorreo").val();
	url = $("#txtUrlEmpre").val();
	idDepartamento= $("#selDepartamento").val();
	idMunicipio = $("#selMunicipio").val();

}

function asignarValoresProducto(){
    empresaProducto = $("#txaProduEmpre").val();
	idCategoriaProducto = $("#selCategoria").val();
	idDepartamento= $("#selDepartamento").val();
	idMunicipio = $("#selMunicipio").val();
	
	
}
    
function obtenerDatosEnvioProducto(){
    asignarValoresProducto();
    data = 'idEmpresa='+idEmpresa+'&empresaProducto='+empresaProducto+'&idCategoriaProducto='+idCategoriaProducto;
}

    
function obtenerDatosEnvio(){
    asignarValores();
    data = 'nit='+nit+'&empresa='+empresa+'&representanteLegal='+representanteLegal+'&direccion='+direccion+'&telefonoCelular='+telefonoCelular+'&telefonoFijo='+telefonoFijo+'&correoElectronico='+correoElectronico+'&url='+url+'&idDepartamento='+idDepartamento+'&idMunicipio='+idMunicipio;
}

function cargarMunicipios(){
	if($('#selDepartamento').val() == ""){
		var municipio = $('#selMunicipio');
		municipio.empty();
		municipio.append('<option value="">--Seleccione municipio--</option>');
		return false;
	}
	$.ajax({
		url:'../controlador/ruedaNegocio.cargarDepartamentosMunicipio.php',
		type:'POST',
		dataType:"json",
		data:{idDepartamento:$('#selDepartamento').val()},
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;
			
			if(exito == 0){
				alerta(mensaje);
				return false;
			}
			if(json.numeroRegistros == 0){
				alerta('No se encontraron registros con los parámetros indicados por el usuario.');
				return false;
			}
			
			var municipio = $('#selMunicipio');
			municipio.empty();
			municipio.append('<option value="">--Seleccione municipio--</option>');
			$.each(json.municipio, function(contador, fila){
				municipio.append('<option value="'+fila.idMunicipio+'">'+fila.municipio+'</option>');
			});
		}
	});
}

function limpiarVariables(){
	idEmpresa = '';
	nit = '';
	empresa = '';
	empresaProducto = '';
	representanteLegal = '';
	direccion = ''; 
	celular = '';
	telefonoFijo = '';
	correo = '';
	urlEmpresa = '';
	estado = '';
	idMunicipio = '';
	idDepartamento = '';
	idCategoriaProducto = new Array();
	categoriaProducto = new Array();
	producto = new Array();
	data= '';
}
function crearListadoProductos(){
	var tabla = '<table class="consultaTabla" style="margin-left: -1%;">';
	tabla += '<tr>';
	tabla += '<th align="center">Categoría </th>';
	tabla += '<th align="center">Producto </th>';
	tabla += '<th align="center">Acción </th>';
	tabla += '<tr>';

	if(idCategoriaProducto.length == 0){
		$("#tablaProductos").html("");	
		montarTabla();
		return false;
	}
		
	for(var i = 0; i < idCategoriaProducto.length; i++){
		tabla += '<tr>';
		tabla += '<td width="200px" style="max-width:200px">' + categoriaProducto[i] + '</td>';
		tabla += '<td width="400px" style="max-width:400px;overflow:hidden">' + producto[i] + '</td>';
		tabla += '<td width="20px" style="max-width:20px;text-align:center"><img src="../imagenes/eliminar2.png" title="Eliminar" width="20px" style="cursor:pointer" onclick="eliminarProductos(' + idCategoriaProducto[i] + ')" ></td>';
		tabla += '</tr>';	
	}
	tabla += '</table>';
	$("#tablaProductos").html(tabla);
}
function eliminarProductos(id){
	id = id.toString();
	var posBorrar = idCategoriaProducto.indexOf(id);
	
	idCategoriaProducto.splice(posBorrar, 1);
	categoriaProducto.splice(posBorrar, 1);
	producto.splice(posBorrar, 1);

	crearListadoProductos();	
}

function borrarTabla(){
	var tabla = '<table>';
	tabla += '</table>';
	
	$("#tablaProductos").html(tabla);
	
}

function validarCodigo(){
	$.ajax({
		url:'../controlador/ruedaNegocio.validarExistenciaCodigo.php',
		type:'POST',
		dataType:"json",
		data:data,
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;
			var numeroRegistros = json.numeroRegistros;
			
			if(exito == 0){
				window.open("frmCodigoInscripcionRuedaNegocio.html","_parent");
			}else{
				$.each(json.data, function(contador, fila){
					$("#txtNit").val(fila.nit);
				    $("#txtEmpresa").val(fila.empresa);
					$("#txtCorreo").val(fila.correoElectronico);
				});
			}
			
			
		},error: function(xhr, opciones, error){
			alerta(error);
		}
	});
}
function montarTabla(){
	var tabla = '<table class="consultaTabla" style="margin-left: -1%;">';
	tabla += '<tr>';
	tabla += '<th align="center">Categoría </th>';
	tabla += '<th align="center">Producto </th>';
	tabla += '<th align="center">Acción </th>';
	tabla += '</tr>';
	tabla += '<tr>';
	tabla += '<td>&nbsp;</td>';
	tabla += '<td></td>';
	tabla += '<td></td>';
	tabla += '</tr>';
	$("#tablaProductosCompra").html(tabla);
	$("#tablaProductosVenta").html(tabla);
}