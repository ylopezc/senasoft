var nit = null;
var idEmpresa = null;
var empresa = null;
var idEmpresaCodigo = null;
var idEstadoEmpresa = null;

var data = ''

$(function(){
	cargarDialogMensaje();
	$( "body" ).css("overflow-y","scroll");
	cargarEstadoEmpresa();
	montarTabla();
	
	$("#btnConsultar").bind({
		"click":function(){
			obtenerDatosEnvio();			
			$.ajax({
				async:false,
				url:"../controlador/empresaCodigo.consultar.php",
				type:'POST',
				dataType:"json",
				data:data,
				success: function(json){
					var exito = json.exito;
					var mensaje = json.mensaje;
					var numeroRegistros = json.numeroRegistros; 
					if (exito == 0){
						//alerta(mensaje);
						return false;
					}
					if( numeroRegistros == 0){
						montarTabla();
						alerta("No se encontraron registros con los parámetros indicados por el usuario");
						return false;
					}
                    crearListado(json);  
				},error: function(xhr, opciones, error){
					 alerta(error);
				}
			});
		}
	});
	
	$("#btnLimpiar").bind({
		"click":function(){
			limpiarVariables();
			limpiarControlesFormulario(document.frmConsultarEmpresaCodigo);
			$("#consultaTabla").html("");
		}
	});
});

function limpiarVariables(){
	nit = '';
	idEmpresa = '';
	empresa = '';
	idEstadoEmpresa = '';

    data = '';
}
function asignarDatosEnvio(){
    nit = $("#txtNitEmpresa").val();
    empresa = $("#txtEmpresa").val();
	idEstadoEmpresa = $("#selEstadoEmpresa").val();
}
function obtenerDatosEnvio(){
    asignarDatosEnvio();
    data = 'idEmpresaCodigo='+idEmpresaCodigo+'&empresa='+empresa+'&nit='+nit+'&idEstadoEmpresa='+idEstadoEmpresa;
}
		
function crearListado(json){
	$("#consultaTabla").html("");
	var tabla = '<tr>';
		tabla += '<th><p>Código</p></th>';
		tabla += '<th><p>Empresa</p></th>';
		tabla += '<th><p>Correo Electrónico</p></th>';
		tabla += '<th><p>Estado</p></th>';
		tabla += '<th><p>Accion</p></th>';
		tabla += '</tr>';
		
	   $.each(json.data,function(contador, fila){
			   color = '#FFF'
				if(contador % 2 == 0){
					color = '#FFF';
				}
				tabla += '<tr bgcolor="' + color + '">';
				tabla += '<td>' + fila.empresaCodigo + '</td>';
				tabla += '<td>'+ fila.nit + ' - ' + fila.empresa + '</td>';
				tabla +='<td>'+ fila.correoElectronico+'</td>';
				tabla += '<td>'+fila.estadoEmpresa+'</td>';
				
				if(fila.idEstadoEmpresa == 1){
					tabla += '<td><input type="button" value="Autorizar" onclick="autorizarEmpresaCodigo('+fila.idEmpresaCodigo+',2)"><input type="button" value="Cancelar" onclick="inactivarEmpresaCodigo('+fila.idEmpresaCodigo+' ,3)"></td>';	
				
				}
				
				if(fila.idEstadoEmpresa == 2){
					tabla += '<td><input type="button" value="Cancelar" onclick="inactivarEmpresaCodigo('+fila.idEmpresaCodigo+' ,3)"></td>';	
				
				}
				if(fila.idEstadoEmpresa == 3){
					tabla += '<td><input type="button" value="Autorizar" onclick="autorizarEmpresaCodigo('+fila.idEmpresaCodigo+',2)"></td>';
				}
				
				
				tabla += '<tr>';
		});
		$("#consultaTabla").html(tabla);
}
function borrarVariableAutocomplete(){
	$("#txtMunicipio").autocomplete.val = '';
	$("#txtEmpresa").autocomplete.val = '';
	municipio = '';
	idMunicipio = '';
	empresa = '';
	idEmpresa = '';
}
function cargarEstadoEmpresa(){
	$.ajax({
		   url:'../ajax/cargarEstadoEmpresa.php',
		   type:'POST',
		   dataType:"json",
		   data:null,
		   success: function(json){
			   
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					return false;
		   		}
				if(json.numeroRegistros == 0){
					alerta("No se encontraron registros con los parámetros indicados");
					retorno = false;
					return false;
				}
				var control = $("#selEstadoEmpresa");
				control.empty();
				control.append('<option value="">--Seleccione el estado de la empresa--</option>');
				$.each(json.data, function(contador,fila){
					control.append('<option value="' + fila.idEstadoEmpresa + '">' + fila.estadoEmpresa + '</option>');
				});
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});

}
function montarTabla(){
	$("#consultaTabla").html("");
	var tabla = '<tr>';
	tabla += '<th><p>Código</p></th>';
	tabla += '<th><p>Empresa</p></th>';
	tabla += '<th><p>Correo Electrónico</p></th>';
	tabla += '<th><p>Estado</p></th>';
	tabla += '</tr>';
	tabla += '<tr>';
	tabla += '<td>&nbsp;</td>';
	tabla += '<td>&nbsp;</td>';
	tabla += '<td>&nbsp;</td>';
	tabla += '<td>&nbsp;</td>';
	tabla += '</tr>';
	$("#consultaTabla").html(tabla);
}
function autorizarEmpresaCodigo(id , idEstadoEmpresa){
	idEmpresaCodigo = id ;
	idEstadoEmpresa = idEstadoEmpresa;
	obtenerDatosEnvio();
	$.ajax({
		   url:'../controlador/empresaCodigo.autorizar.php',
		   type:'POST',
		   dataType:"json",
		   data:{idEmpresaCodigo:idEmpresaCodigo ,idEstadoEmpresa:idEstadoEmpresa},
		   success: function(json){			   
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   var enviado = json.enviado;
			   
				if(exito == 0){
					alerta(mensaje);
					return false;
		   		}
				if(enviado == 0){
					alerta("Error enviando el mensaje");
				}else{
					alerta("Se ha autorizado correctamente , y se ha enviado un correo electrónico a la empresa para que termine su inscripción");
				}
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
	});
	$("#btnConsultar").click();
}
function inactivarEmpresaCodigo(id , idEstadoEmpresa){
	idEmpresaCodigo = id ;
	idEstadoEmpresa = idEstadoEmpresa;
	obtenerDatosEnvio();
	$.ajax({
		   url:'../controlador/empresaCodigo.inactivar.php',
		   type:'POST',
		   dataType:"json",
		   data:{idEmpresaCodigo:idEmpresaCodigo ,idEstadoEmpresa:idEstadoEmpresa},
		   success: function(json){			   
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					return false;
		   		}
				alerta(mensaje);
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
	});
	$("#btnConsultar").click();
}