// JavaScript Document

var idProyecto = null;

var proyecto = null;

var descripcion = null;

var url = null;

var foto = null;

var sistemaRelacionado = null;

var ubicacion = null;

var urlVideo = null;

var tiempoProduccion = null;

var idProcesoImpacto = null;

var idTipoSolucion = null;

var idMotorBaseDatos = null;

var idLenguajeProgramacion = null;

var tipoLicenciamiento = null;

var derechosAutor = null;

var nroImplementaciones = null;

var idAprendiz = null;

var idCentroFormacion = null;

var cartaImplementacion = null;

var nroImplementacionFuturo = null;

var desarrolloFuturo = null;

var equipoDesarrollo = null;
var informacionAdicional = null;


var idPersona = null;

var idCategoria = null;

var idTipoDocumentoIdentidad = null;

var numeroIdentificacion = null;

var idProgramaFormacion = null;

var primerNombre = null;

var segundoNombre = '';

var primerApellido = null;

var segundoApellido = null;

var fechaNacimiento = null;

var rh = null;

var rutaFoto = null;

var tallaCamiseta = null;

var correoElectronico = null;

var telefonoFijo = null;

var telefonoCelular = null;

var genero = null;

var cuidadoMedico = null;

var data = null;

var banderaAprendizEvaluar = null;

var mensajeErrorFoto = null;

var data = null;

$(function(){

	//Oculto el campo de implementaciones a futuro

	$("#tdNumeroImplementacionesFuturo").hide();

	crearCalendario("txtFechaNacimAprendiz");
	validarNumeros("txtTiempoProduc");

	

	validarNumeros("txtDocumIdentAprendiz");

	validarNumeros("txtTelefFijoAprendiz");

	validarNumeros("txtTelefCelulAprendiz");

	

	validarNumeros("txtNumerImple");

	validarNumeros("txtNumerImpleFutur");

	

	$("body").css("overflow-y","scroll");

	

	$("#flCartaImple").change(function(){

		if(validarTamanoArchivo('flCartaImple', 500) == false){

			return false;

		}

		

		if(validarExtensionArchivo('flCartaImple') == false){

			alerta("Extensión inválida.");

			return false;

		}

		

	});

	$("#fleFotoAprendiz").change(function(){

		

		if(validarTamanoArchivo('fleFotoAprendiz', 200) == false){

			$("#divFotoAprendiz").html("");

			return false;

		}

		

		if(validarExtensionImagen('fleFotoAprendiz') == false){

			alerta("Extensión inválida.");

			return false;

		}

		subirFoto("aprendiz","fleFotoAprendiz");

		if(foto == ''){

			$("#fleFotoAprendiz").val('');

			alerta(mensajeErrorFoto);

			$("#divFotoAprendiz").empty();

			$("#divFotoAprendiz").hide();

			return false;

		}

		$("#divFotoAprendiz").empty();

		$("#divFotoAprendiz").show();

		//$("#divFotoAprendiz").html('<br><br><iframe id="iframeLogo" height="auto" width="auto" src="' + foto + '" scrolling="no" frameborder="0" style="margin-top: -5%;margin-left: 40%;"></iframe>');

		$("#divFotoAprendiz").html('<br><br><img src="' + foto + '" width="150px" height="150px" style="margin-top: -5%;margin-left: 40%;">');

	});

	cargarDialogMensaje();

	cargarProgramaFormacion();

	cargarRh();

	cargarTallaCamiseta();

	cargarTipoDocumIdent();

	

	//Consulto el centro de formacion dependiendo del código ingresado

	obtenerCentroFormacion();

	

	//Cargo los select

	cargarProceseoImpacto();

	cargarTipoSolucion();

	cargarMotorBaseDatos();

	cargarLenguajeProgramacion();

	

	$("#tabProyecto").tabs();

	

	$("#selImplementarOtroCentro").change(function(){

		if ($("#selImplementarOtroCentro").val() == ""){

			$("#tdNumeroImplementacionesFuturo").hide();

			return false;

		}

			

		if ($("#selImplementarOtroCentro").val() == 1){

			$("#tdNumeroImplementacionesFuturo").show();

			document.getElementById("txtNumerImpleFutur").focus();

		}else{

			$("#tdNumeroImplementacionesFuturo").hide();

		}

	});

	$("#txtDocumIdentAprendiz").keypress(function(e){
		if(e.keyCode == 8 || e.keyCode == 46){
			idPersona = '';
			idAprendiz = '';
			var conteCampoNumerDocum = $("#txtDocumIdentAprendiz").val();
			limpiarControlesFormulario(document.frmInscrAprendiz);
			$("#txtDocumIdentAprendiz").val(conteCampoNumerDocum);
			$("#divFotoAprendiz").html("");
			$("#fleFotoAprendiz").attr("accesskey","L");
			quitarReadonlyCampos();
		}
	});
	
	$("#txtDocumIdentAprendiz").change(function(){
		if($.trim($("#txtDocumIdentAprendiz").val()) == "")
			return false;
		consultarAprendiz($("#txtDocumIdentAprendiz").val());
	});

	$("#btnGuardar").click(function(){	



		if(validarVacios(document.frmProyecto) == false){

			$("#tabProyecto").tabs( "option","active",0);

			return false;	

		}

		

			

		subirCartaImplementacion();

		if(cartaImplementacion == ''){

			return false;

		}

			

		if($("#selImplementarOtroCentro").val() == 1 && $("#txtNumerImpleFutur").val() == ""){

			alerta("Usted ha seleccionado que su proyecto se piensa implementar en otros centros, por favor especifique en cuantos.");

			$("#txtNumerImpleFutur").focus();

			return false;

		}

		

		if(validarVacios(document.frmInscrAprendiz) == false){

			$("#tabProyecto").tabs( "option","active",1);	

			return false;

		}

			

		

		if(validarCorreoElectronico("txtCorreElectAprendiz") == false){

			alerta("El correo electrónico del aprendiz  no es válido");

			return false;

		}

		if(validarNumeroCelular($('#txtTelefCelulAprendiz').val()) == false){

			alerta("El número de celular no es válido");

			return false;	

		}

		

		if($("#txtCaptcha").val() == ""){

			alerta("Ingrese los caracteres especiales mostrados en la imagen");

			$("#txtCaptcha").focus();

			return false;

		}

		if(validarCaptcha($("#txtCaptcha").val()) == false)

			return false;
			
		if(idPersona == null || idPersona == '' || idPersona == 'null'){
			asignDatosEnvioAprendiz();
			obtenerDatosEnvioPersona();
			adicionarPersona();
			obtenerDatosEnvioPersonaInfoAdicional();
			adicionarPersonaInfoAdicional();
			obtenerDatosEnvioAprendiz();
			adicionarAprendiz();
		}
		
		obtenerDatosEnvio();
		
		$.ajax({

			async:false,

			url:'../controlador/inscripcionProyecto.adicionar.php',

			type:'POST',

			dataType:"json",

			data:data,

			success: function(json){

				var exito = json.exito;

				var mensaje = json.mensaje;

				

				if(exito == 0){

					alerta(mensaje);

					return false;

				}

				alerta(mensaje);

			},error: function(xhr, opciones, error){

				alerta(error);

			}

		});

		

		limpiarControlesFormulario(document.frmProyecto);

		limpiarControlesFormulario(document.frmInscrAprendiz);

		$("#divFotoAprendiz").html("");

		limpiarVariables();

		$("#txtCaptcha").val("");

		$("#imgRefrescarCaptcha").click();

		$("#tdNumeroImplementacionesFuturo").hide();

	});

});

function obtenerCentroFormacion(){

	$.ajax({

		async:false,

		url:'../controlador/inscripcionProyecto.obtenerCentroFormacion.php',

		type:'POST',

		dataType:"json",

		data:null,

		success: function(json){

			var exito = json.exito;

			var mensaje = json.mensaje;

			var informacionCentro = json.informacionCentro;

			

			if(exito == 0){

				alerta(mensaje);

				window.open("frmCodigoInscripcionProyecto.html","_parent");

			}

			

			idCentroFormacion = informacionCentro.idCentroFormacion;

			

			var html = '';

			html += '<p><b>Regional ' + informacionCentro.regional + '</b></p>';

			html += '<p><b>Centro de formación:</b> ' + informacionCentro.centroFormacion + '</p>';			

			$("#spanCentroFormacion").html(html);

			

		},error: function(xhr, opciones, error){

			alerta(error);

		}

	});

}

function asignarValoresEnvio(){

	proyecto = $("#txtNombreProyecto").val();

	descripcion = $("#txaDescrProyec").val();

	url = $("#txtUrlProyec").val();

	sistemaRelacionado = $("#txaSisteRelac").val();

	ubicacion = $("#txtUbicacion").val();

	urlVideo = $("#txtUrlVideo").val();

	tiempoProduccion = $("#txtTiempoProduc").val();

	idProcesoImpacto = $("#selProcesoImpacto").val();

	idTipoSolucion = $("#selTipoSolucion").val();

	idMotorBaseDatos = $("#selMotorBaseDatos").val();

	idLenguajeProgramacion = $("#selLenguajeProgramacion").val();

	tipoLicenciamiento = $("#txaTipoLicen").val();

	nroImplementaciones = $("#txtNumerImple").val();
	informacionAdicional = $("#txaInformacionAdicional").val();
	

	if($("#selImplementarOtroCentro").val() == 1){

		nroImplementacionFuturo = $("#txtNumerImpleFutur").val();	

	}else{

		nroImplementacionFuturo = 0;

	}

	desarrolloFuturo = $("#txtDesarFutur").val();

	equipoDesarrollo = $("#txaEquipoDesarr").val();

}

function obtenerDatosEnvio(){

	asignarValoresEnvio();

	data = 'idProyecto=' + idProyecto + '&proyecto=' + proyecto + '&descripcion=' + descripcion + '&url=' + url + '&sistemaRelacionado=' + sistemaRelacionado + '&ubicacion=' + ubicacion + '&urlVideo=' + urlVideo + '&tiempoProduccion=' + tiempoProduccion + '&idProcesoImpacto=' + idProcesoImpacto + '&idTipoSolucion=' + idTipoSolucion + '&idMotorBaseDatos=' + idMotorBaseDatos + '&idLenguajeProgramacion=' + idLenguajeProgramacion + '&tipoLicenciamiento=' + tipoLicenciamiento + '&nroImplementaciones=' + nroImplementaciones + '&idAprendiz=' + idAprendiz + '&idCentroFormacion=' + idCentroFormacion + '&cartaImplementacion=' + cartaImplementacion + '&nroImplementacionFuturo=' + nroImplementacionFuturo + '&desarrolloFuturo=' + desarrolloFuturo + '&equipoDesarrollo=' + equipoDesarrollo + '&informacionAdicional=' + informacionAdicional;

}

function cargarProceseoImpacto(){

	$.ajax({

		async:false,

		url:'../controlador/inscripcionProyecto.cargarProcesoImpacto.php',

		type:'POST',

		dataType:"json",

		data:null,

		success: function(json){

			var exito = json.exito;

			var mensaje = json.mensaje;

			var numeroRegistros = json.numeroRegistros;

			

			if(exito == 0){

				alerta(mensaje);

				return false;

			}

			if(numeroRegistros == 0){

				return false;

			}

			var control = $("#selProcesoImpacto");

			control.empty();

			control.append('<option value="">--Seleccione el proceso de impacto--</option>');

			$.each(json.data,function(contador, fila){

				control.append('<option value="' + fila.idProceso + '">' + fila.proceso + '</option>');

			});

			

		},error: function(xhr, opciones, error){

			alerta(error);

		}

	});

}

function cargarTipoSolucion(){

	$.ajax({

		async:false,

		url:'../controlador/inscripcionProyecto.cargarTipoSolucion.php',

		type:'POST',

		dataType:"json",

		data:null,

		success: function(json){

			var exito = json.exito;

			var mensaje = json.mensaje;

			var numeroRegistros = json.numeroRegistros;

			

			if(exito == 0){

				alerta(mensaje);

				return false;

			}

			if(numeroRegistros == 0){

				return false;

			}

			var control = $("#selTipoSolucion");

			control.empty();

			control.append('<option value="">--Seleccione el tipo de solución--</option>');

			$.each(json.data,function(contador, fila){

				control.append('<option value="' + fila.idTipoSolucion + '">' + fila.tipoSolucion + '</option>');

			});

			

		},error: function(xhr, opciones, error){

			alerta(error);

		}

	});

}

function cargarMotorBaseDatos(){

	$.ajax({

		async:false,

		url:'../controlador/inscripcionProyecto.cargarMotorBaseDatos.php',

		type:'POST',

		dataType:"json",

		data:null,

		success: function(json){

			var exito = json.exito;

			var mensaje = json.mensaje;

			var numeroRegistros = json.numeroRegistros;

			

			if(exito == 0){

				alerta(mensaje);

				return false;

			}

			if(numeroRegistros == 0){

				return false;

			}

			var control = $("#selMotorBaseDatos");

			control.empty();

			control.append('<option value="">--Seleccione el motor de base de datos--</option>');

			$.each(json.data,function(contador, fila){

				control.append('<option value="' + fila.idMotorBaseDatos + '">' + fila.motorBaseDatos + '</option>');

			});

			

		},error: function(xhr, opciones, error){

			alerta(error);

		}

	});

}

function cargarLenguajeProgramacion(){

	$.ajax({

		async:false,

		url:'../controlador/inscripcionProyecto.cargarLenguajeProgramacion.php',

		type:'POST',

		dataType:"json",

		data:null,

		success: function(json){

			var exito = json.exito;

			var mensaje = json.mensaje;

			var numeroRegistros = json.numeroRegistros;

			

			if(exito == 0){

				alerta(mensaje);

				return false;

			}

			if(numeroRegistros == 0){

				return false;

			}

			var control = $("#selLenguajeProgramacion");

			control.empty();

			control.append('<option value="">--Seleccione el lenguaje de programación--</option>');

			$.each(json.data,function(contador, fila){

				control.append('<option value="' + fila.idLeguajeProgramacion + '">' + fila.leguajeProgramacion + '</option>');

			});

			

		},error: function(xhr, opciones, error){

			alerta(error);

		}

	});

}

function limpiarVariables(){	

	idProyecto = '';

	proyecto = '';

	descripcion = '';

	url = '';

	sistemaRelacionado = '';

	ubicacion = '';

	urlVideo = '';

	tiempoProduccion = '';

	idProcesoImpacto = '';

	idTipoSolucion = '';

	idMotorBaseDatos = '';

	idLenguajeProgramacion = '';

	tipoLicenciamiento = '';

	derechosAutor = '';

	nroImplementaciones = '';

	idAprendiz = '';

	//idCentroFormacion = '';

	cartaImplementacion = '';

	nroImplementacionFuturo = '';

	desarrolloFuturo = '';

	equipoDesarrollo = '';
	informacionAdicional = '';
	

	idPersona = '';
	
	idCategoria = '';

	idTipoDocumentoIdentidad = '';

	numeroIdentificacion = '';

	idProgramaFormacion = '';

	primerNombre = '';

	segundoNombre = '';

	primerApellido = '';

	segundoApellido = '';

	fechaNacimiento = '';

	rh = '';

	rutaFoto = '';

	tallaCamiseta = '';

	correoElectronico = '';

	telefonoFijo = '';

	telefonoCelular = '';

	genero = '';

	cuidadoMedico = '';

	data = '';

	banderaAprendizEvaluar = '';

	

	data = '';
	
	quitarReadonlyCampos();
}

function subirCartaImplementacion(){

	var archivos = document.getElementById("flCartaImple");

	var archivo = archivos.files; 

	var data = new FormData();

	for(i=0; i < archivo.length; i++){

		data.append('archivo'+i,archivo[i]);

	}

	$.ajax({

		async: false,

		url:'../controlador/inscripcionProyecto.subirCartaImplementacion.php', 

		type:'POST', 

		dataType:"json",

		contentType:false, 

		data:data,

		processData:false, 

		cache:false,

		success: function(json){

			var exito = json.exito;

			var mensaje = json.mensaje;

			cartaImplementacion = json.ruta;

			

			if(exito == 0){

				alerta(mensaje);

				cartaImplementacion = '';

				return false;

			}

		} 

	});

}

function subirFoto(nombreFotoTemporal,control){

	var archivos = document.getElementById(control);

	var archivo = archivos.files; 

	var data = new FormData();

	for(i=0; i < archivo.length; i++){

		data.append('archivo'+i,archivo[i]);

	}

	data.append('nombreTemporal',nombreFotoTemporal);

	$.ajax({

		async: false,

		url:"../controlador/inscripcionProyecto.subirFoto.php", 

		type:'POST', 

		dataType:"json",

		contentType:false, 

		data:data,

		processData:false, 

		cache:false,

		success: function(json){

			var exito = json.exito;

			mensajeErrorFoto = json.mensaje;

			foto = json.ruta;

	

			if(exito == 0){

				mensajeErrorFoto = json.mensaje;

				foto = "";

				return false;

			}

		} 

	});

}

function cargarProgramaFormacion(){

	$.ajax({

		async:false

		,url:'../controlador/inscripcionProyecto.cargarProgramaFormacion.php',

		type:'POST',

		dataType:"json",

		data:null,

		success: function(json){

			var exito = json.exito;

			var mensaje = json.mensaje;

			var numeroRegistros = json.numeroRegistros;

			

			if(exito == 0){

				alerta(mensaje);

				return false;

			}

			if(numeroRegistros == 0){

				alerta("No se encontraron registros con los parámetros indicados por el usuario.");

				limpiarVariables();

				return false;

			}

			var control = $('#selProgrAprendiz');

			

			control.empty();

			

			control.append('<option value="">--Seleccione el programa de formación--</option>');

			

			$.each(json.data,function(contador, fila){

				control.append('<option value="'+fila.idProgramaFormacion+'">'+fila.programaFormacion+'</option>');

			});

			

		}	

	});	

}



function cargarRh(){

	$.ajax({

		async:false,

	   url:'../controlador/inscripcionProyecto.cargarRh.php',

	   type:'POST',

	   dataType:"json",

	   data:null,

	   success: function(json){

		   var exito = json.exito;

		   var mensaje = json.mensaje;

		   

			if(exito == 0){

				alerta(mensaje);

				return false;

	   		}

			var control = $("#selRhAprendiz");

			

			

			control.empty();

			

			

			control.append('<option value="">--Seleccione RH--</option>');

			

			

			$.each(json.data, function(contador,fila){

				control.append('<option value="' + fila + '">' + fila + '</option>');

			});

	   },error:function(xhr,opciones,error){

			alerta("error"+ error);

		}

	});

}

function cargarTallaCamiseta(){

	$.ajax({

		async:false,

	   url:'../controlador/inscripcionProyecto.cargarTalla.php',

	   type:'POST',

	   dataType:"json",

	   data:null,

	   success: function(json){

		   var exito = json.exito;

		   var mensaje = json.mensaje;

		   

			if(exito == 0){

				alerta(mensaje);

				return false;

	   		}

			var control = $("#selTallaAprendiz");

			

			control.empty();

			

			control.append('<option value="">--Seleccione su talla de camiseta--</option>');

			

			$.each(json.data, function(contador,fila){

				control.append('<option value="' + fila + '">' + fila + '</option>');

			});

	   },error:function(xhr,opciones,error){

			alerta("error"+ error);

		}

	});

}



function cargarTipoDocumIdent(){

	$.ajax({

		async:false

		,url:'../controlador/inscripcionProyecto.cargarTipoDocumIdent.php',

		type:'POST',

		dataType:"json",

		data:null,

		success: function(json){

			var exito = json.exito;

			var mensaje = json.mensaje;

			var numeroRegistros = json.numeroRegistros;

			

			if(exito == 0){

				alerta(mensaje);

				return false;

			}

			if(numeroRegistros == 0){

				alerta("No se encontraron registros con los parámetros indicados por el usuario.");

				limpiarVariables();

				return false;

			}

			var control = $('#selTipoDocumAprendiz');

			

			control.empty();

			

			control.append('<option value="">--Seleccione el tipo de documento--</option>');

			

			$.each(json.data,function(contador, fila){

				control.append('<option value="'+fila.idTipoDocumIdent+'">'+fila.tipoDocumIdent+'</option>');

			});

			

		}	

	});	

}

function asignDatosEnvioAprendiz(){

	idTipoDocumentoIdentidad = $('#selTipoDocumAprendiz').val();

	numeroIdentificacion = $('#txtDocumIdentAprendiz').val();

	idProgramaFormacion = $('#selProgrAprendiz').val();

	primerNombre = $('#txtPrimeNombrAprendiz').val();

	primerApellido = $('#txtPrimeApellAprendiz').val();

	segundoApellido = $('#txtSegunApellAprendiz').val();

	fechaNacimiento = $('#txtFechaNacimAprendiz').val();

	rh = $('#selRhAprendiz').val();

	tallaCamiseta = $('#selTallaAprendiz').val();

	correoElectronico = $('#txtCorreElectAprendiz').val();

	telefonoFijo = $('#txtTelefFijoAprendiz').val();

	telefonoCelular = $('#txtTelefCelulAprendiz').val();

	genero = $('#selGenerAprendiz').val();

	cuidadoMedico = $('#txaCuidadoAprendiz').val();

}

function obtenerDatosEnvioPersona(){

	data = "idTipoDocumentoIdentidad="+idTipoDocumentoIdentidad+"&numeroIdentificacion="+numeroIdentificacion+"&primerNombre="+ primerNombre+"&segundoNombre="+ segundoNombre+"&primerApellido="+ primerApellido+"&segundoApellido="+ segundoApellido+"&fechaNacimiento="+ fechaNacimiento+"&correoElectronico="+ correoElectronico+"&telefonoFijo="+ telefonoFijo+"&telefonoCelular="+ telefonoCelular+"&genero="+ genero +"&foto="+ foto;

}

function obtenerDatosEnvioPersonaInfoAdicional(){
	var n = rh.indexOf("+"); 
	if(n != "-1"){
		rh = rh.replace("+","1");
	}
	data = "idPersona="+idPersona+"&rh="+rh+"&tallaCamiseta="+ tallaCamiseta+"&cuidadoMedico="+ cuidadoMedico;

}

function obtenerDatosEnvioAprendiz(){

	data = "idPersona="+idPersona+"&idProgramaFormacion="+idProgramaFormacion+"&idCentroFormacion="+ idCentroFormacion;

}

function adicionarPersona(){

	$.ajax({

		   async:false,

		   url:'../controlador/persona.adicionar.php',

		   type:'POST',

		   dataType:"json",

		   data:data,

		   success: function(json){

			   var exito = json.exito;

			   var mensaje = json.mensaje;

			   

				if(exito == 0){

					alerta(mensaje);

					return false;

		   		}

				idPersona = json.idPersona;

		   },error:function(xhr,opciones,error){

				alerta("error"+ error);

			}

		});

	

} 

function adicionarPersonaInfoAdicional(){

	$.ajax({

		   async:false,

		   url:'../controlador/personaInfoAdicional.adicionar.php',

		   type:'POST',

		   dataType:"json",

		   data:data,

		   success: function(json){

			   var exito = json.exito;

			   var mensaje = json.mensaje;

			   

				if(exito == 0){

					alerta(mensaje);

					return false;

		   		}

		   },error:function(xhr,opciones,error){

				alerta("error"+ error);

			}

		});
} 

function adicionarAprendiz(){

	$.ajax({

		   async:false,

		   url:'../controlador/aprendiz.adicionar.php',

		   type:'POST',

		   dataType:"json",

		   data:data,

		   success: function(json){

			   var exito = json.exito;

			   var mensaje = json.mensaje;

			   

				if(exito == 0){

					alerta(mensaje);

					return false;

		   		}

				idAprendiz = json.idAprendiz;

		   },error:function(xhr,opciones,error){

				alerta("error"+ error);

			}

		});
}
function consultarAprendiz(numeroDocumento){
	var data = 'numeroDocumento=' + numeroDocumento;
	$.ajax({

		   async:false,

		   url:'../controlador/inscripcionProyecto.consultarAprendiz.php',

		   type:'POST',

		   dataType:"json",

		   data:data,

		   success: function(json){

			   var exito = json.exito;

			   var mensaje = json.mensaje;
				var numeroRegistros = json.numeroRegistros;
			   

				if(exito == 0){

					alerta(mensaje);

					return false;

		   		}
				if(numeroRegistros == 0){
					if(idAprendiz != '' && idAprendiz != 'null' && idAprendiz != null){
						var conteCampoNumerDocum = $("#txtDocumIdentAprendiz").val();
						limpiarControlesFormulario(document.frmInscrAprendiz);
						$("#txtDocumIdentAprendiz").val(conteCampoNumerDocum);
						$("#divFotoAprendiz").html("");
					}
					idPersona = '';
					idAprendiz = '';
					$("#fleFotoAprendiz").attr("accesskey","L");
					quitarReadonlyCampos();
					return false;
				}
				asignarDatosAprendiz(json);
		   },error:function(xhr,opciones,error){

				alerta("error"+ error);

			}

		});
}
function asignarDatosAprendiz(json){
	$.each(json.data,function(contador, fila){
		
		idPersona = fila.idPersona;
		
		idAprendiz = fila.idAprendiz;
		
		$("#fleFotoAprendiz").attr("accesskey","V");
		$("#divFotoAprendiz").html('<br><br><img src="' + fila.foto + '" width="150px" height="150px" style="margin-top: -5%;margin-left: 40%;">');
		$('#selTipoDocumAprendiz').val(fila.idTipoDocumentoIdentidad);
		//$('#txtDocumIdentAprendiz').val();
		$('#selProgrAprendiz').val(fila.idProgramaFormacion);
		$('#txtPrimeNombrAprendiz').val(fila.nombre);
		$('#txtPrimeApellAprendiz').val(fila.primerApellido);
		$('#txtSegunApellAprendiz').val(fila.segundoApellido);
		$('#txtFechaNacimAprendiz').val(fila.fechaNacimiento);
		$('#selRhAprendiz').val(fila.rh);
		$('#selTallaAprendiz').val(fila.tallaCamiseta);
		$('#txtCorreElectAprendiz').val(fila.correoElectronico);
		$('#txtTelefFijoAprendiz').val(fila.telefonoFijo);
		$('#txtTelefCelulAprendiz').val(fila.telefonoCelular);
		$('#selGenerAprendiz').val(fila.genero);
		$('#txaCuidadoAprendiz').val(fila.cuidadoMedico);	
	});
	
	asignarReadonlyCampos();
}
function asignarReadonlyCampos(){
	$("#fleFotoAprendiz").attr("disabled","true");
	$('#selTipoDocumAprendiz').attr("disabled","true");
	$('#selProgrAprendiz').attr("disabled","true");
	$('#txtPrimeNombrAprendiz').attr("disabled","true");
	$('#txtPrimeApellAprendiz').attr("disabled","true");
	$('#txtSegunApellAprendiz').attr("disabled","true");
	$('#txtFechaNacimAprendiz').attr("disabled","true");
	$('#selRhAprendiz').attr("disabled","true");
	$('#selTallaAprendiz').attr("disabled","true");
	$('#txtCorreElectAprendiz').attr("disabled","true");
	$('#txtTelefFijoAprendiz').attr("disabled","true");
	$('#txtTelefCelulAprendiz').attr("disabled","true");
	$('#selGenerAprendiz').attr("disabled","true");
	$('#txaCuidadoAprendiz').attr("disabled","true");
}
function quitarReadonlyCampos(){
	$("#fleFotoAprendiz").removeAttr("disabled");
	$('#selTipoDocumAprendiz').removeAttr("disabled");
	$('#selProgrAprendiz').removeAttr("disabled");
	$('#txtPrimeNombrAprendiz').removeAttr("disabled");
	$('#txtPrimeApellAprendiz').removeAttr("disabled");
	$('#txtSegunApellAprendiz').removeAttr("disabled");
	$('#txtFechaNacimAprendiz').removeAttr("disabled");
	$('#selRhAprendiz').removeAttr("disabled");
	$('#selTallaAprendiz').removeAttr("disabled");
	$('#txtCorreElectAprendiz').removeAttr("disabled");
	$('#txtTelefFijoAprendiz').removeAttr("disabled");
	$('#txtTelefCelulAprendiz').removeAttr("disabled");
	$('#selGenerAprendiz').removeAttr("disabled");
	$('#txaCuidadoAprendiz').removeAttr("disabled");
}