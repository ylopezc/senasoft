﻿// JavaScript Document
var usuario = null;
var contrasena = null;

var data = null;
$(function(){
	document.getElementById("txtUsuario").focus();
	$("#divIniciarSesionAdmin").dialog({
		width:400,
		height:300,
		autoOpen:false
	});
	
	$("#iniciarSesionAdministrador").click(function(){
		$("#divIniciarSesionAdmin").dialog("open");
	});
	
	$("#pwdContrasena").bind({
		"keypress":function(e){
			if(e.keyCode == 13){
				if(validar() == false)
					return false;
				$("#btnIngresar").click();
				return false;
			}
		}
	});
	$("#txtUsuario").bind({
		"keypress":function(e){
			if(e.keyCode == 13){
				if(validar() == false)
					return false;
				$("#btnIngresar").click();
				return false;
			}
		}
	});
	
	$("#btnIngresar").click(function(){
		if(validar() == false)
			return false
		obtenerDatosEnvio();
		$.ajax({
			url:'../Senasoft-admin/entorno/validarIngreso.php',
			type:'POST',
			data:data,
			dataType:"json",
			success: function(json){
				var exito = json.exito;
				var mensaje = json.mensaje;
				
				if(exito == 0)
					alert(mensaje);
				else
					window.location = '../Senasoft-admin/vista/frmInicio.html';
				
			},error: function(xhr, opciones, error){
				alert(error);
			}
		});
	});
});
function asignarValoresEnvio(){
	usuario = $("#txtUsuario").val();
	contrasena = $("#pwdContrasena").val();
}
function obtenerDatosEnvio(){
	asignarValoresEnvio();
	data = "Usuario=" + usuario + "&Contrasena=" + contrasena;
}
function validar(){
	if($("#txtUsuario").val() == ""){
		alert("Por favor ingrese el usuario");
		$("#txtUsuario").focus();
		return false;
	}
	if($("#pwdContrasena").val() == ""){
		alert("Por favor ingrese la contraseña");
		$("#pwdContrasena").focus();
		return false;
	}
}