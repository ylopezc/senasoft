$(function(){
	//$("input[type=button]").button();
	//$("input[type=text]").text();
	//$("input[type=file]").button();
	//Permite crear los tooltip
	$('.tooltip').tooltipster({
		   animation: 'grow',
		   delay: 200,
		   contentAsHTML : true,
		   touchDevices: false,
		   trigger: 'hover'
	});
});
function crearCalendario(id){
	$("#"+id).datepicker({
		format: "yy-mm-dd",
		dateFormat: "yy-mm-dd",
		changeMonth: true,
	    changeYear: true,
	    yearRange: '1930:2000'
	});
}