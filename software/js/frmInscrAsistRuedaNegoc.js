var idEmpresa = null;
var idSectorEconomico = null;
var idRegimenEmpresa = null;
var sectorEconomico = null;
var nit = null;
var empresa = null;
var empresaProducto = null;
var representanteLegal = null;
var direccion = null; 
var celular = null;
var telefonoFijo = null;
var correoElectronico = null;
var urlEmpresa = null;
var estado = null;
var idMunicipio = null;
var municipio = null;
var idDepartamento = null
var departamento = null;

var nombreAsistente = null;
var cargo = null;
var correoElectronicoAsistente = null;
var telefonoAsistente = null;
var horaAsistencia = null;	

var idCategoriaProductoCompra = new Array();
var idCategoriaProductoVenta = new Array();
var categoriaProductoCompra = new Array();
var categoriaProductoVenta = new Array();
var frecuencia = new Array();
var producto = new Array();

var data= '';


$(function(){
	$('#txtHoraAsistencia').ptTimeSelect();
	$('#contenidoSector').hide();
	montarTablaCompra();
	montarTablaVenta();
	validarCodigo();
	cargarCategoria();
	cargarDialogMensaje();
	cargarRegimenEmpresa();
	cargarFrecuencia();
	cargarSectorEconomico();
	validarNumeros("txtCelular");
	validarNumeros("txtTeleFijo");
	validarNumeros("txtTelefonoAsistente");
	$("#tabRuedaNegocios").tabs();
	$( "body" ).css("overflow-y","scroll");	
		
	
	
	$.ajax({
		url:'../controlador/ruedaNegocio.cargarDepartamento.php',
		type:'POST',
		dataType:'json',
		data:null,
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;	
			
			if(exito == 0){
				alerta(mensaje);
				return false;
			}
			
			var departamento = $("#selDepartamento");
			var municipio = $("#selMunicipio");
			departamento.empty();
			municipio.empty();
			departamento.append('<option value="">---Seleccione departamento---</option>');
			municipio.append('<option value="">---Seleccione municipio---</option>');
			$.each(json.data, function(contador, fila){
				if(fila.departamento)
					departamento.append('<option value="'+fila.idDepartamento+'">'+fila.departamento+'</option>');
			});
		},error:function(xhr,opciones,error){
			alerta(error);
		}
	});
	
	$("#selSectorEconomico").change(function(){
		if($("#selSectorEconomico").val() == '0'){
			$('#contenidoSector').show();
			$("#txtSectorEconomico").attr("accesskey","L");
			$("#txtSectorEconomico").val('');
		}else{
			$("#txtSectorEconomico").attr("accesskey","V");
			$('#contenidoSector').hide();
			$("#txtSectorEconomico").val('');
		}		
	});
	
	$("#btnAdicionarProductoCompra").click(function(){
		if(document.getElementById("selCategoriaCompra").options[0].selected == true){
			alerta("Por favor escoja una categoría de los productos que ha comprado");
			return false;
		}
		if(document.getElementById("selFrecuenciaCompra").value == ""){
			alerta("Por favor seleccione la frecuencia de compra del producto");
			return false;
		}
		var idCategoriaProducto = $("#selCategoriaCompra").val();
		idCategoriaProductoCompra.push($("#selCategoriaCompra").val());
		$('#selCategoriaCompra option[value='+idCategoriaProducto+']').hide();
		categoriaProductoCompra.push($("#selCategoriaCompra option:selected").text());
		frecuencia.push($("#selFrecuenciaCompra").val());
		
		$("#selCategoriaCompra").val('');
		$("#selFrecuenciaCompra").val('');
		
		crearListadoProductosCompra();
	});
	
	$("#btnAdicionarProductoVenta").click(function(){
		if(document.getElementById("selCategoriaVenta").options[0].selected == true){
			alerta("Por favor escoja una categoría de los productos a comprar.");
			return false;
		}		
		var idCategoriaProducto = $("#selCategoriaVenta").val();
		$('#selCategoriaVenta option[value='+idCategoriaProducto+']').hide();
		idCategoriaProductoVenta.push($("#selCategoriaVenta").val());
		categoriaProductoVenta.push($("#selCategoriaVenta option:selected").text());
	
		$("#selCategoriaVenta").val('');
		crearListadoProductosVenta();
	});
	
	$("#btnGuardarInforEmpre").bind({
		"click":function(){
			obtenerDatosEnvio();
			if( validarVacios(document.frmInforEmpre) == false){
				$("#tabRuedaNegocios").tabs( "option","active",0);
				return false
			}
			if(validarCorreoElectronico("txtCorreo") == false){
				alerta("El correo electrónico de la empresa ingresado no es válido");
				return false;	
			}
			if(validarCorreoElectronico("txtCorreElectAsist") == false){
				alerta("El correo electrónico ingresado del asistente no es válido");
				return false;	
			}
			if(validarNumeroCelular($('#txtCelular').val()) == false){
				alerta("El número de celular no es válido");
				return false;	
			}
			if($("#selCategoriaCompra").val()!= '' && $("#selFrecuenciaCompra").val() != '' ){
				$("#tabRuedaNegocios").tabs( "option","active",1);
				alerta("Debe seleccionar la opción de adicionar el producto que la empresa ha comprado");
				return false;
			}
			if($("#selCategoriaVenta").val()!= ''){
				$("#tabRuedaNegocios").tabs( "option","active",2);
				alerta("Debe seleccionar la opción de adicionar el producto que la empresa desea comprar");
				return false;
			}
			if(idCategoriaProductoCompra.length == 0){
				$("#tabRuedaNegocios").tabs( "option","active",1);
				alerta("Debe ingresar al menos un producto o servicio que la empresa ha comprado.");
				return false;
			}
			if(idCategoriaProductoVenta.length == 0){
				$("#tabRuedaNegocios").tabs( "option","active",1);
				alerta("Debe ingresar al menos un producto o servicio que la empresa desea comprar.");
				return false;
			}
			if(idMunicipio == ""){
				alerta("Por favor digite el municipio .");
				return false;
			}
			if($("#txtCaptcha").val() == ""){
				alerta("Debe ingresar los caracteres especiales mostrados en la imagen");
				$("#txtCaptcha").focus();
				$("#tabRuedaNegocios").tabs( "option","active",3);
				return false;
			}
			if(validarCaptcha($("#txtCaptcha").val()) == false)
				return false;
			$.ajax({
				async:false,
				url:"../controlador/ruedaNegocioEmpresa.guardar.php",
				type:'POST',
				dataType:"json",
				data:data,
				success: function(json){
					var exito = json.exito;
					var mensaje = json.mensaje;
					
					if (exito == 0) 
                        return false;                                                   
					
					idEmpresa = json.idEmpresa;
					//montarTabla();
				 		
				},error: function(xhr, opciones, error){
					alerta (error);
				}
			});
			adicionarProductoCompra();
			adicionarProductoVenta();
			adicionarAsistente();
			cargarCategoria();
			cargarFrecuencia();
			$('#tablaProductosCompra').html('');
			$('#tablaProductosVenta').html('');
			montarTablaCompra();
			montarTablaVenta();
			limpiarVariables();
			limpiarControlesFormulario(document.frmInforEmpre);
			$("#imgRecargarCaptcha").click();
		}
	});
});

function asignarValores(){
    nit = $("#txtNit").val();
    empresa = $("#txtEmpresa").val();
    representanteLegal = $("#txtRepreLegal").val();
	direccion = $("#txtDireccion").val();
	telefonoCelular = $("#txtCelular").val();
	telefonoFijo = $("#txtTeleFijo").val();
	correoElectronico = $("#txtCorreo").val();
	url = $("#txtUrlEmpre").val();
	idDepartamento= $("#selDepartamento").val();
	idMunicipio = $("#selMunicipio").val();
	idSectorEconomico = $("#selSectorEconomico").val();
	idRegimenEmpresa = $("#selRegimenEmpresa").val();
	sectorEconomico = $("#txtSectorEconomico").val();

}

function asignarValoresProducto(){
    empresaProducto = $("#txaProduEmpre").val();
	idCategoriaProducto = $("#selCategoria").val();
	idDepartamento= $("#selDepartamento").val();
	idMunicipio = $("#selMunicipio").val();	
}

function asignarValoresAsistente(){
    nombreAsistente = $("#txtNombreAsistente").val();
    cargo = $("#txtCargoAsistente").val();
	correoElectronicoAsistente = $("#txtCorreElectAsist").val();
	telefonoAsistente= $("#txtTelefonoAsistente").val();
	horaAsistencia = $("#txtHoraAsistencia").val();	
}

    
function obtenerDatosEnvioProducto(){
    asignarValoresProducto();
    data = 'idEmpresa='+idEmpresa+'&empresaProducto='+empresaProducto+'&idCategoriaProducto='+idCategoriaProducto;
}
function obtenerDatosEnvioAsistente(){
	asignarValoresAsistente();
    data = 'asistenteRuedaNegocio='+nombreAsistente+'&cargo='+cargo+'&correoElectronico='+correoElectronicoAsistente+'&telefono='+telefonoAsistente+'&horaAsistencia='+horaAsistencia+'&idEmpresa='+idEmpresa;
}
function obtenerDatosEnvio(){
    asignarValores();
    data = 'nit='+nit+'&empresa='+empresa+'&representanteLegal='+representanteLegal+'&direccion='+direccion+'&telefonoCelular='+telefonoCelular+'&telefonoFijo='+telefonoFijo+'&correoElectronico='+correoElectronico+'&url='+url+'&idDepartamento='+idDepartamento+'&idMunicipio='+idMunicipio +'&idRegimenEmpresa='+idRegimenEmpresa +'&idSectorEconomico='+idSectorEconomico +'&sectorEconomico='+sectorEconomico;
}

function cargarMunicipios(){
	if($('#selDepartamento').val() == ""){
		var municipio = $('#selMunicipio');
		municipio.empty();
		municipio.append('<option value="">--Seleccione municipio--</option>');
		return false;
	}
	$.ajax({
		url:'../controlador/ruedaNegocio.cargarDepartamentosMunicipio.php',
		type:'POST',
		dataType:"json",
		data:{idDepartamento:$('#selDepartamento').val()},
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;
			
			if(exito == 0){
				alerta(mensaje);
				return false;
			}
			if(json.numeroRegistros == 0){
				alerta('No se encontraron registros con los parámetros indicados por el usuario.');
				return false;
			}
			
			var municipio = $('#selMunicipio');
			municipio.empty();
			municipio.append('<option value="">--Seleccione municipio--</option>');
			$.each(json.municipio, function(contador, fila){
				municipio.append('<option value="'+fila.idMunicipio+'">'+fila.municipio+'</option>');
			});
		}
	});
}

function limpiarVariables(){
	idEmpresa = '';
	nit = '';
	empresa = '';
	empresaProducto = '';
	representanteLegal = '';
	direccion = ''; 
	celular = '';
	telefonoFijo = '';
	correo = '';
	urlEmpresa = '';
	estado = '';
	idMunicipio = '';
	idDepartamento = '';
	idCategoriaProductoCompra = new Array();
	categoriaProductoCompra = new Array();
	idCategoriaProductoVenta = new Array();
	categoriaProductoCompraVenta = new Array();
	frecuencia = new Array();
	producto = new Array();
	data= '';
	idSectorEconomico = '';
	idRegimenEmpresa = '';
	sectorEconomico = '';
	nombreAsistente = '';
	cargo = '';
	correoElectronicoAsistente = '';
	telefonoAsistente = '';
	horaAsistencia = '';
}
function crearListadoProductosCompra(){
	var tabla = '<table class="consultaTabla" style="margin-left: -1%;">';
	tabla += '<tr>';
	tabla += '<th align="center">Categoría </th>';
	tabla += '<th align="center">Frecuencia </th>';
	tabla += '<th align="center">Acción </th>';
	tabla += '<tr>';

	if(idCategoriaProductoCompra.length == 0){
		$("#tablaProductosCompra").html("");	
		montarTablaCompra();
		return false;
	}
		
	for(var i = 0; i < idCategoriaProductoCompra.length; i++){
		tabla += '<tr>';
		tabla += '<td width="200px" style="max-width:200px">' + categoriaProductoCompra[i] + '</td>';
		tabla += '<td width="400px" style="max-width:400px;overflow:hidden">' + frecuencia[i] + '</td>';
		tabla += '<td width="20px" style="max-width:20px;text-align:center"><img src="../imagenes/eliminar2.png" title="Eliminar" width="20px" style="cursor:pointer" onclick="eliminarProductosCompra(' + idCategoriaProductoCompra[i] + ')" ></td>';
		tabla += '</tr>';	
	}
	tabla += '</table>';
	$("#tablaProductosCompra").html(tabla);
}
function eliminarProductosCompra(id){
	id = id.toString();
	var posBorrar = idCategoriaProductoCompra.indexOf(id);
	
	idCategoriaProductoCompra.splice(posBorrar, 1);
	categoriaProductoCompra.splice(posBorrar, 1);
	frecuencia.splice(posBorrar, 1);
	
	$('#selCategoriaCompra option[value='+id+']').show();

	crearListadoProductosCompra();	
}

function crearListadoProductosVenta(){
	var tabla = '<table class="consultaTabla" style="margin-left: -1%;">';
	tabla += '<tr>';
	tabla += '<th align="center">Categoría </th>';
	tabla += '<th align="center">Acción </th>';
	tabla += '<tr>';

	if(idCategoriaProductoVenta.length == 0){
		$("#tablaProductosVenta").html("");	
		montarTablaCompra();
		return false;
	}
		
	for(var i = 0; i < idCategoriaProductoVenta.length; i++){
		tabla += '<tr>';
		tabla += '<td width="200px" style="max-width:200px">' + categoriaProductoVenta[i] + '</td>';
		tabla += '<td width="20px" style="max-width:20px;text-align:center"><img src="../imagenes/eliminar2.png" title="Eliminar" width="20px" style="cursor:pointer" onclick="eliminarProductosVenta(' + idCategoriaProductoVenta[i] + ')" ></td>';
		tabla += '</tr>';	
	}
	tabla += '</table>';
	$("#tablaProductosVenta").html(tabla);
}
function eliminarProductosVenta(id){
	id = id.toString();
	var posBorrar = idCategoriaProductoCompra.indexOf(id);
	
	idCategoriaProductoVenta.splice(posBorrar, 1);
	categoriaProductoVenta.splice(posBorrar, 1);
	
	$('#selCategoriaVenta option[value='+id+']').show();
	
	crearListadoProductosVenta();	
}
function validarCodigo(){
	$.ajax({
		url:'../controlador/ruedaNegocio.validarExistenciaCodigo.php',
		type:'POST',
		dataType:"json",
		data:data,
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;
			var numeroRegistros = json.numeroRegistros;
			
			if(exito == 0){
				window.open("frmCodigInscrAsistRuedaNegoc.html","_parent");
			}else{
				$.each(json.data, function(contador, fila){
					$("#txtNit").val(fila.nit);
				    $("#txtEmpresa").val(fila.empresa);
					$("#txtCorreo").val(fila.correoElectronico);
				});
			}
			
			
		},error: function(xhr, opciones, error){
			alerta(error);
		}
	});
}
function montarTablaCompra(){
	var tabla = '<table class="consultaTabla" style="margin-left: -1%;">';
	tabla += '<tr>';
	tabla += '<th align="center">Categoría </th>';
	tabla += '<th align="center">Frecuencia </th>';
	tabla += '<th align="center">Acción </th>';
	tabla += '</tr>';
	tabla += '<tr>';
	tabla += '<td>&nbsp;</td>';
	tabla += '<td></td>';
	tabla += '<td></td>';
	tabla += '</tr>';
	$("#tablaProductosCompra").html(tabla);
}
function montarTablaVenta(){
	var tabla = '<table class="consultaTabla" style="margin-left: -1%;">';
	tabla += '<tr>';
	tabla += '<th align="center">Categoría </th>';
	tabla += '<th align="center">Acción </th>';
	tabla += '</tr>';
	tabla += '<tr>';
	tabla += '<td>&nbsp;</td>';
	tabla += '<td></td>';
	tabla += '</tr>';
	$("#tablaProductosVenta").html(tabla);
}
function cargarRegimenEmpresa(){
	$.ajax({
		url:'../ajax/empresa.cargarRegimen.php',
		type:'POST',
		dataType:'json',
		data:null,
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;	
			
			if(exito == 0){
				alerta(mensaje);
				return false;
			}
			
		var control = $("#selRegimenEmpresa");
			control.empty();
			control.append('<option value="">---Seleccione el régimen---</option>');
			$.each(json.data, function(contador, fila){
				control.append('<option value="'+fila.idRegimenEmpresa+'">'+fila.regimenEmpresa+'</option>');
			});
		},error:function(xhr,opciones,error){
			alerta(error);
		}
	});
	
}
function cargarSectorEconomico(){
	$.ajax({
		url:'../ajax/empresa.cargarSectorEconomico.php',
		type:'POST',
		dataType:'json',
		data:null,
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;	
			
			if(exito == 0){
				alerta(mensaje);
				return false;
			}
			
		var control = $("#selSectorEconomico");
			control.empty();
			control.append('<option value="">---Seleccione el sector económico---</option>');
			$.each(json.data, function(contador, fila){
				control.append('<option value="'+fila.idSectorEconomico+'">'+fila.sectorEconomico+'</option>');
			});
			control.append('<option value="0">Otro</option>');
		},error:function(xhr,opciones,error){
			alerta(error);
		}
	});
	
}
function cargarFrecuencia(){
	$.ajax({
	   url:'../controlador/empresa.cargarFrecuencia.php',
	   type:'POST',
	   dataType:"json",
	   data:null,
	   success: function(json){
		   var exito = json.exito;
		   var mensaje = json.mensaje;
		   
			if(exito == 0){
				alerta(mensaje);
				return false;
	   		}
			var control1 = $("#selFrecuenciaCompra");
			control1.empty();			
			control1.append('<option value="">--Seleccione Frecuencia--</option>');
			$.each(json.data, function(contador,fila){
				control1.append('<option value="' + fila + '">' + fila + '</option>');
			});
	   },error:function(xhr,opciones,error){
			alerta("error"+ error);
		}
	});
}
function adicionarProductoCompra(){
	$.ajax({
		async:false,
		url:"../controlador/asistente.adicionarProductoCompra.php",
		type:'POST',
		dataType:"json",
		data:{idEmpresa:idEmpresa,
			idCategoriaProductoCompra:idCategoriaProductoCompra,
			frecuencia: frecuencia},
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;
			
			if (exito == 0) 
                return false;
				                    
		},error: function(xhr, opciones, error){
			alerta (error);
		}
	});
	
}
function adicionarProductoVenta(){
	$.ajax({
		async:false,
		url:"../controlador/asistente.adicionarProductoVenta.php",
		type:'POST',
		dataType:"json",
		data:{idEmpresa:idEmpresa,
			idCategoriaProductoVenta:idCategoriaProductoVenta},
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;
			
			if (exito == 0) 
                return false;
				                    
		},error: function(xhr, opciones, error){
			alerta (error);
		}
	});
	
}
function adicionarAsistente(){
	obtenerDatosEnvioAsistente();
	$.ajax({
		async:false,
		url:"../controlador/asistente.adicionar.php",
		type:'POST',
		dataType:"json",
		data:data,
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;
			
			if (exito == 0) 
                return false;
			alerta(mensaje);
				                    
		},error: function(xhr, opciones, error){
			alerta (error);
		}
	});
	
}
function cargarCategoria(){
	$.ajax({
		url:'../controlador/ruedaNegocio.cargarCategoria.php',
		type:'POST',
		dataType:'json',
		data:null,
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;	
			
			if(exito == 0){
				alerta(mensaje);
				return false;
			}
			
		var control1 = $("#selCategoriaVenta");
		var control2 = $("#selCategoriaCompra");
			control1.empty();
			control2.empty();
			control1.append('<option value="">---Seleccione la categoría de su producto---</option>');
			control2.append('<option value="">---Seleccione la categoría de su producto---</option>');
			$.each(json.data, function(contador, fila){
				if(fila.categoriaProducto)
					control1.append('<option value="'+fila.idCategoriaProducto+'">'+fila.categoriaProducto+'</option>');
					control2.append('<option value="'+fila.idCategoriaProducto+'">'+fila.categoriaProducto+'</option>');
			});
			control2.append('<option value="8">No lo he hecho aún</option>');
		},error:function(xhr,opciones,error){
			alerta(error);
		}
	});
}