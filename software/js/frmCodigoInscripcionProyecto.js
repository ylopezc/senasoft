// JavaScript Document
var codigoInscripcion = null;

var data = null;
$(function(){
	cargarDialogMensaje();
	$("#txtCodigoInscripcion").focus();
	
	$("#txtCodigoInscripcion").bind({
		"keypress":function(e){
			if(e.keyCode == 13){
				$("#btnEnviar").click();
				return false;
			}
		}
	});
	
	$("#btnEnviar").bind({
		"click":function(){
			obtenerDatosEnvio();
			if(validar() == false)
					return false;
			$.ajax({
				url:'../controlador/inscripcionProyecto.validarCodigo.php',
				type:'POST',
				dataType:"json",
				data:data,
				success: function(json){
					var exito = json.exito;
					var mensaje = json.mensaje;
					var validarCodigo = json.validarCodigo;
					
					if(validarCodigo == 1){
						window.open("frmInscrConcuProye.html","_parent");
					}else{
						alerta("Por favor ingrese un código válido para su centro de formación.");
						$("#txtCodigoInscripcion").focus();
						return false;
					}
				},error: function(xhr, opciones, error){
					alerta(error);
				}
			});
		}
	});
});
function asignarValoresEnvio(){
	codigoInscripcion = $("#txtCodigoInscripcion").val();
}
function obtenerDatosEnvio(){
	asignarValoresEnvio();
	data = 'codigoInscripcion=' + codigoInscripcion;
}
function validar(){
	if($("#txtCodigoInscripcion").val() == ""){
		alerta("Debe ingresar un código de inscripción.");
		$("#txtCodigoInscripcion").focus();
		return false;
	}
}