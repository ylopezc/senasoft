// JavaScript Document
var nombres;
var correoElectronico;
var asunto;
var rol;
var regional;
var centroFormacion;
var idCentroFormacion;
var mensaje;

var data;
$(function(){
	cargarDialogMensaje();
	cargarRegionales();
	$("#selRegional").change(function(){
		cargarCentroFormacion($("#selRegional").val());
	});
	$("#btnEnviar").click(function(){
		obtenerDatosEnvio();
		if(validarVacios(document.frmContactenos) == false){
			return false;
		}
		if(validarCorreoElectronico("txtEmail") == false){
			alerta("Por favor ingrese un correo de electrónico válido");
			return false;
		}
		$.ajax({
			async:false,
		   url:'software/ajax/contactenos.php',
		   type:'POST',
		   dataType:"json",
		   data:data,
		   success: function(json){
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   var enviado = json.enviado;
			   
				if(exito == 0){
					alerta(mensaje);
					return false;
				}
				if(enviado == 0){
					alerta("Ocurrio un error enviando el mensaje.");
					return false;	
				}

		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});

		$.ajax({
			async:false,
		   url:'software/controlador/mensajeContacto.adicionar.php',
		   type:'POST',
		   dataType:"json",
		   data:data,
		   success: function(json){
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					return false;
				}
				alerta("El mensaje se envió correctamente");
				limpiarVariables();
				limpiarControlesFormulario(document.frmContactenos);
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
	});
});
function asignarValoresEnvio(){
	nombres = $("#txtNombres").val();
	correoElectronico = $("#txtEmail").val();
	asunto = $("#txtAsunto").val();
	rol = $("#selRol option:selected").text();
	regional = $.trim($("#selRegional option:selected").text());
	centroFormacion = $.trim($("#selCentroFormacion option:selected").text());
	idCentroFormacion = $("#selCentroFormacion").val();
	mensaje = $("#txaMensaje").val();
}
function obtenerDatosEnvio(){
	asignarValoresEnvio();
	data = 'nombres=' + nombres + '&correoElectronico=' + correoElectronico + '&asunto=' + asunto + '&rol=' + rol + '&regional=' + regional + '&centroFormacion=' + centroFormacion + '&idCentroFormacion=' + idCentroFormacion + '&mensaje=' + mensaje;
}
function limpiarVariables(){
	nombres = '';
	correoElectronico = '';
	asunto = '';
	rol = '';
	regional = '';
	centroFormacion = '';
	idCentroFormacion = '';
	mensaje = '';
	
	data = '';
}
function cargarRegionales(){
	$.ajax({
	   url:'software/ajax/cargarRegional.php',
	   type:'POST',
	   dataType:"json",
	   data:null,
	   success: function(json){
		   var exito = json.exito;
		   var mensaje = json.mensaje;
		   
			if(exito == 0){
				alerta(mensaje);
				return false;
			}
			if(json.numeroRegistros == 0){
				alerta("No se encontraron registros con los parámetros indicados");
				retorno = false;
				return false;
			}
			var control = $("#selRegional");
			control.empty();
			control.append('<option value="">--Seleccione la regional--</option>');
			$.each(json.data, function(contador,fila){
				control.append('<option value="' + fila.idRegional + '">' + fila.regional + '</option>');
			});
	   },error:function(xhr,opciones,error){
			alerta("error"+ error);
		}
	});
}
function cargarCentroFormacion(id){
	if(id == ""){
		var control = $("#selCentroFormacion");
		control.empty();
		control.append('<option value="">--Seleccione el centro de formación--</option>');
		return false;
	}
	$.ajax({
	   url:'software/ajax/cargarCentroFormacion.php',
	   type:'POST',
	   dataType:"json",
	   data:{idRegional:id},
	   success: function(json){
		   var exito = json.exito;
		   var mensaje = json.mensaje;
		   
			if(exito == 0){
				alerta(mensaje);
				return false;
	   		}
			if(json.numeroRegistros == 0){
				alerta("No se encontraron registros con los parámetros indicados");
				retorno = false;
				return false;
			}
			var control = $("#selCentroFormacion");
			control.empty();
			control.append('<option value="">--Seleccione el centro de formación--</option>');
			$.each(json.data, function(contador,fila){
				control.append('<option value="' + fila.idCentroFormacion + '">' + fila.centroFormacion + '</option>');
			});
	   },error:function(xhr,opciones,error){
			alerta("error"+ error);
		}
	});
}