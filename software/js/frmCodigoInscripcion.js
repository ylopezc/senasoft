// JavaScript Document
var codigoCentroFormacion = null;
$(function(){
	cargarDialogMensaje();
	$("#txtCodigoCentroFormacion").focus();
	$("#txtCodigoCentroFormacion").bind({
		"keypress":function(e){
			if(e.keyCode == 13){
				$("#btnEnviar").click();
				return false;
			}
		}
	});
	$("#btnEnviar").bind({
		"click":function(){
			if(validarVacios(document.frmCodigoInscripcion) == false)
	                return false;
			validarCodigoCentroFormacion();
		}
	});
});
function asignarValoresEnvio(){
	codigoCentroFormacion = $('#txtCodigoCentroFormacion').val();
}
function validarCodigoCentroFormacion(){
	asignarValoresEnvio();
	$.ajax({
		url:'../controlador/centroFormacion.validarCodigo.php',
		type:'POST',
		dataType:"json",
		data:{codigoCentroFormacion:codigoCentroFormacion},
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;
			var numeroRegistros = json.numeroRegistros;
			
			if(exito == 0){
				alerta(mensaje);
				return false;
			}
			if(numeroRegistros == 1){
				window.open("frmInscripcionCompetencias.html","_parent");
			}
			
		}	
	});	
}

