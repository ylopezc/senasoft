﻿// JavaScript Document
var documentoIdentidad =  null;
var rol = null;
var idCentroFormacion =  null;
var idRegional = null;
var idCategoria = null;
var data = null;
$(function() {
	cargarDialogMensaje();
	validarNumeros("txtNumeroDocumento");	
	//autocompletarCentroFormacion();
	cargarRegional();
	cargarCategoriasCompetencia();
	$("#selRegional").change(function(){
		cargarCentroFormacion($("#selRegional").val());
	});
	//cargarDialogMensaje();	
	$( "body" ).css("overflow-y","scroll");
	$("#btnConsultar").bind({
		"click":function(){
			obtenerDatosEnvio();
			rol = $('#selRol').val();
			if(rol == "optAprendiz"){
				if(consultarAprendiz() == false){
					return false;
				}
				
			}else{
				if(consultarInstructor() == false){
					return false;
				}
			}	
		}
	});
	$("#btnLimpiar").bind({
		"click":function(){
			limpiarControlesFormulario(document.frmConsultaInscripcion);
			limpiarVariables();
			$("#secListadoParticipantes").html('');
		}
	});
});
function cargarCategoriasCompetencia(){
	$.ajax({
		async:false
		,url:'../controlador/competencia.cargarCategoria.php',
		type:'POST',
		dataType:"json",
		data:null,
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;
			var numeroRegistros = json.numeroRegistros;
			
			if(exito == 0){
				alerta(mensaje);
				return false;
			}
			if(numeroRegistros == 0){
				alerta("No se encontraron registros con los parámetros indicados por el usuario.");
				limpiarVariables();
				return false;
			}
			var control = $('#selCategoria');

			control.empty();
	
			control.append('<option value="" class="placeholderSelect">--Seleccione la categoría--</option>');
		
			$.each(json.data,function(contador, fila){
				control.append('<option value="'+fila.idCategoriaConcurso+'">'+fila.categoriaConcurso+'</option>');
			});
			
		}	
	});	
}
function asignDatosEnvio(){
	documentoIdentidad = $('#txtNumeroDocumento').val();
	idCategoria = $('#selCategoria').val();
	idRegional = $("#selRegional").val();
	idCentroFormacion = $("#selCentroFormacion").val();
}
function consultarAprendiz(){
	var retorno = '';
	$.ajax({
		   async:false,
		   url:'../controlador/aprendiz.consultar.php',
		   type:'POST',
		   dataType:"json",
		   data:data,
		   success: function(json){
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					return false;
		   		}
				if(json.numeroRegistros == 0){
					alerta("No se encontraron registros con los parámetros indicados");
					retorno = false;
					return false;
				}
				if(json.numeroRegistros == 1){
					crearListadoAprendizUnico(json);
				}else{
					crearListadoAprendiz(json)	
				}
				
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
	return retorno; 
	
}
function consultarInstructor(){
	var retorno = '';
	$.ajax({
		   async:false,
		   url:'../controlador/instructor.consultar.php',
		   type:'POST',
		   dataType:"json",
		   data:data,
		   success: function(json){
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					return false;
		   		}
				if(json.numeroRegistros == 0){
					alerta("No se encontraron registros con los parámetros indicados");
					retorno = false;
					return false;
				}
				crearListadoInstructor(json);
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
	return retorno;
}
function obtenerDatosEnvio(){
	asignDatosEnvio();
	data = "idCategoria="+idCategoria+"&documentoIdentidad="+documentoIdentidad+"&idCentroFormacion="+ idCentroFormacion+"&idRegional="+ idRegional;
}
function limpiarVariables(){	
	documentoIdentidad =  '';
	rol = '';
	idCentroFormacion =  '';
	idCategoria = '';
	idRegional = '';
	data = '';
}

function crearListadoInstructor(json){
	$("#consultaTabla").html('');
	$("#secListadoParticipantes").html('');
	var tabla = '<table class="tblConsultaCompetencia">';
	$.each(json.data, function(contador, fila){
		tabla  += '<tr>';
		tabla += '<td class="spanTablaInscripcion">' + fila.regional + ' - ' + fila.centroFormacion +  '</td>';
		tabla  += '</tr>';
		tabla  += '<tr>';
		tabla += '<td><img src="' + fila.foto + '" width="150" height="150" /></td>';
		tabla  += '</tr>';
		tabla  += '<tr>';
		tabla += '<td><b>' + fila.nombreCompleto + '</b></td>';
		tabla  += '</tr>';
		tabla  += '<tr>';
		tabla += '<td>' + fila.categoriaConcurso + '</td>';
		tabla  += '<tr>';	
	});
	tabla += '</table>';	
	$("#secListadoParticipantes").html(tabla);
}
function crearListadoAprendiz(json){
	$("#consultaTabla").html('');
	$("#secListadoParticipantes").html('');
	var tabla = '<table class="tblConsultaCompetencia">';
	$.each(json.data, function(contador, fila){
		tabla  += '<tr>';
		tabla += '<td colspan="2" class="spanTablaInscripcion">' + fila.regionalApren1 + ' - ' + fila.centroFormacionApren1 +  '</td>';
		tabla  += '</tr>';
		tabla  += '<tr>';
		tabla += '<td colspan="2" class="spanCategoria">' + fila.categoriaConcursoApren1 + '</td>';
		//tabla += '<td>' + fila.categoriaConcursoApren2 + '</td>';
		tabla  += '</tr>';
		tabla  += '<tr>';
		tabla += '<td><img src="' + fila.fotoApren1 + '" width="150" height="150" /></td>';
		tabla += '<td><img src="' + fila.fotoApren2 + '" width="150" height="150" /></td>';
		tabla  += '</tr>';
		tabla  += '<tr>';
		tabla  += '<td><b>' + fila.nombreCompletoApren1 + '</td></b>';
		tabla  += '<td><b>' + fila.nombreCompletoApren2 + '</td></b>';
		tabla  += '</tr>';
		
	});
	tabla += '</table>';
	$("#secListadoParticipantes").html(tabla);
}
function crearListadoAprendizUnico(json){
	$("#consultaTabla").html('');
	$("#secListadoParticipantes").html('');
	var tabla = '<table class="tblConsultaCompetencia">';
	$.each(json.data, function(contador, fila){
		tabla  += '<tr>';
		tabla += '<td class="spanTablaInscripcion">' + fila.regionalApren1 + ' - ' + fila.centroFormacionApren1 +  '</td>';
		tabla  += '</tr>';
		tabla  += '<tr>';
		tabla += '<td><img src="' + fila.fotoApren1 + '" width="150" height="150" /></td>';
		tabla  += '</tr>';
		tabla  += '<tr>';
		tabla  += '<td><b>' + fila.nombreCompletoApren1 + '</td></b>';
		tabla  += '</tr>';
		tabla  += '<tr>';
		tabla += '<td>' + fila.categoriaConcursoApren1 + '</td>';
		tabla  += '</tr>';
	});
	tabla += '</table>';
	$("#secListadoParticipantes").html(tabla);
}
function cargarRegional(){
	$.ajax({
		   url:'../ajax/cargarRegional.php',
		   type:'POST',
		   dataType:"json",
		   data:null,
		   success: function(json){
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					return false;
		   		}
				if(json.numeroRegistros == 0){
					alerta("No se encontraron registros con los parámetros indicados");
					retorno = false;
					return false;
				}
				var control = $("#selRegional");
				control.empty();
				control.append('<option value="">--Seleccione la regional--</option>');
				$.each(json.data, function(contador,fila){
					control.append('<option value="' + fila.idRegional + '">' + fila.regional + '</option>');
				});
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});

}
function cargarCentroFormacion(id){
	if(id == ""){
		var control = $("#selCentroFormacion");
		control.empty();
		control.append('<option value="">--Seleccione el centro de formación--</option>');
		return false;
	}
	$.ajax({
	   url:'../ajax/cargarCentroFormacion.php',
	   type:'POST',
	   dataType:"json",
	   data:{idRegional:id},
	   success: function(json){
		   var exito = json.exito;
		   var mensaje = json.mensaje;
		   
			if(exito == 0){
				alerta(mensaje);
				return false;
	   		}
			if(json.numeroRegistros == 0){
				alerta("No se encontraron registros con los parámetros indicados");
				retorno = false;
				return false;
			}
			var control = $("#selCentroFormacion");
			control.empty();
			control.append('<option value="">--Seleccione el centro de formación--</option>');
			$.each(json.data, function(contador,fila){
				control.append('<option value="' + fila.idCentroFormacion + '">' + fila.centroFormacion + '</option>');
			});
	   },error:function(xhr,opciones,error){
			alerta("error"+ error);
		}
	});
}