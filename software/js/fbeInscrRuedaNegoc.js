var nit = null;
var idEmpresa = null;
var empresa = null;
var representanteLegal = null;
var direccion = null; 
var celular = null;
var telefonoFijo = null;
var correoElectronico = null;
var urlEmpresa = null;
var idMunicipio = null;
var municipio = null;
var numeroControlesVacios = null;

var data = ''

$(function(){
	$( "body" ).css("overflow-y","scroll");
	autocompletarEmpresa();
	autocompletarMunicipio();
	cargarDialogMensaje();

	
	$("#btnConsultar").bind({
		"click":function(){
			obtenerDatosEnvio();			
			$.ajax({
				async:false,
				url:"../controlador/ruedaNegocio.consultarEmpresa.php",
				type:'POST',
				dataType:"json",
				data:data,
				success: function(json){
					var exito = json.exito;
					var mensaje = json.mensaje;
					
					if (exito == 0){
						alerta(mensaje);
						return false;
					}
					//idEmpresa = json.idEmpresa;
					
					if(json.numeroRegistros == 0){
						alerta('No se encontraron registros con los parámetros indicados por el usuario');
						return false;
					}
						limpiarControlesFormulario(document.frmConsultarRuedaNegocio);
						limpiarVariables();
                      	crearListado(json);  
				},error: function(xhr, opciones, error){
					 alerta(error);
				}
			});
		}
	});
	
	$("#btnLimpiar").bind({
		"click":function(){
			limpiarVariables();
			limpiarControlesFormulario(document.frmConsultarRuedaNegocio);
			$("#consultaTabla").html("");
		}
	});
});

function limpiarVariables(){
	nit = '';
	idEmpresa = '';
	idMunicipio = '';
	representanteLegal = '';
	direccion = ''; 
	celular = '';
	telefonoFijo = '';
	correoElectronico = '';
	urlEmpresa = '';

    data = '';
}

function autocompletarEmpresa(){
	$("body").css("text-align","left");
	$("#txtEmpresa").autocomplete({
		source:'../ajax/ruedaNegocio.autocompletarEmpresa.php',
		select:function(event, ui){
			idEmpresa = ui.item.idEmpresa;		
		}
	});
}

function autocompletarMunicipio(){
	$("#txtMunicipio").autocomplete({
		source:'../ajax/cargarMunicipio.php',
		select:function(event, ui){
			idMunicipio = ui.item.idMunicipio;		
		}
	});
}
function asignarDatosEnvio(){
    nit = $("#txtNitEmpresa").val();
	municipio = $("#txtMunicipio").val();
}

function obtenerDatosEnvio(){
    asignarDatosEnvio();
    data = 'idEmpresa='+idEmpresa+'&nit='+nit+'&idMunicipio='+idMunicipio+'&municipio='+municipio;
}

		
function crearListado(json){
$("#consultaTabla").html("");
	var tabla = '<tr>';
		tabla += '<th><p>Empresa</p></th>';
		tabla += '<th><p>Nit</p></th>';
		tabla += '<th><p>Representante Legal</p></th>';
		tabla += '<th><p>Direccion</p></th>';
		tabla += '<th><p>Celular</p></th>';
		tabla += '<th><p>Teléfono Fijo</p></th>';
		tabla += '<th><p>Correo</p></th>';
		tabla += '<th><p>Url</p></th>';
		tabla += '<th><p>Municipio</p></th>';
		tabla += '</tr>';
		
	   $.each(json.data,function(contador, fila){
			   color = '#D6D6D8'
				if(contador % 2 == 0){
					color = '#FFF';
				}
				tabla += '<tr bgcolor="' + color + '">';
				tabla += '<td>' + fila.empresa + '</td>';
				tabla += '<td>'+ fila.nit+'</td>';4
				tabla +='<td>'+ fila.representanteLegal+'</td>';
				tabla += '<td>'+fila.direccion+'</td>';
				tabla += '<td>'+fila.telefonoCelular+'</td>'; 
				tabla += '<td>'+fila.telefonoFijo+'</td>'; 
				tabla += '<td>'+fila.correoElectronico+'</td>'; 
				tabla += '<td><a href="http://'+fila.url+'" target="_blank">'+fila.url+'</a> </td>'; 
				tabla += '<td>'+fila.municipio+'</td>'; 
				tabla += '<tr>';
		});
		$("#consultaTabla").html(tabla);
}

function borrarVariableAutocomplete(){
	$("#txtMunicipio").autocomplete.val = '';
	$("#txtEmpresa").autocomplete.val = '';
	municipio = '';
	idMunicipio = '';
	empresa = '';
	idEmpresa = '';
}

