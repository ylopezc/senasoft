$(function(){
	cargarDialogMensaje();
	cargarPreguntasFrecuentes();
});
function crearListado(json){
	var contenidoPregunta = '';
	$("#secListadoPreguntas").html("");
	$.each(json.data,function(contador, fila){
		contenidoPregunta += '<h2 class="title-header">'+fila.preguntaFrecuente+'</h2>';
		contenidoPregunta += '<p>'+fila.respuesta+'</p>';
		contenidoPregunta += '<br>';
	});
		$("#secListadoPreguntas").html(contenidoPregunta);
}
function cargarPreguntasFrecuentes(){
	$.ajax({
		   url:'../controlador/preguntaFrecuente.consultar.php',
		   type:'POST',
		   dataType:"json",
		   data:{estado:1},
		   success: function(json){
			   
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					return false;
		   		}
				if(json.numeroRegistros == 0){
					$("#secListadoPreguntas").html('No hay preguntas disponibles');
					return false;
				}
				crearListado(json);
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
}