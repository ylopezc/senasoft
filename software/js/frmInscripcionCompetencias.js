// JavaScript Document
var idInstructor = null;
var idPersona = null;
var idAprendiz = null;
var idCentroFormacion = null;
var idCategoria = null;
var idTipoDocumentoIdentidad = null;
var numeroIdentificacion = null;
var idProgramaFormacion = null;
var primerNombre = null;
var segundoNombre = '';
var primerApellido = null;
var segundoApellido = null;
var fechaNacimiento = null;
var rh = null;
var rutaFoto = null;
var tallaCamiseta = null;
var correoElectronico = null;
var telefonoFijo = null;
var telefonoCelular = null;
var genero = null;
var cuidadoMedico = null;
var data = null;
var banderaAprendizEvaluar = null;
var fotoApren1 = null;
var fotoApren2 = null;
var fotoInstructor = null;
var mensajeErrorFoto = null;
$(function() {
	validarNumeros("txtDocumIdentApren1");
	validarNumeros("txtTelefFijoApren1");
	validarNumeros("txtTelefCelulApren1");
	
	validarNumeros("txtDocumIdentApren2");
	validarNumeros("txtTelefFijoApren2");
	validarNumeros("txtTelefCelulApren2");
	
	validarNumeros("txtDocumIdentInstr");
	validarNumeros("txtTelefFijoInstr");
	validarNumeros("txtTelefCelulInstr");
	
	crearCalendario("txtFechaNacimApren1");
	crearCalendario("txtFechaNacimApren2");
	crearCalendario("txtFechaNacimInstr");
	
	
	cargarInforCentrForma();
	cargarProgramaFormacion();
	cargarRh();
	cargarTallaCamiseta();
	cargarCategoriasCompetencia();
	cargarTipoDocumIdent();
	cargarDialogMensaje();
	
	$( "body" ).css("overflow-x","scroll");
	
    $( "#tabInscripcionCompetencia" ).tabs();
	$( "#tabInscripcionCompetencia" ).hide();
	$("#fleFotoApren1").change(function(){
		if(validarTamanoArchivo('fleFotoApren1', 200) == false){
			$("#divFotoApren1").html("");
			return false;
		}
		
		if(validarExtensionImagen('fleFotoApren1') == false){
			$("#divFotoApren1").html("");
			alerta("Extensión inválida.");
			return false;
		}
		subirFoto("apren1","fleFotoApren1");
		if(rutaFoto == ''){
			$("#fleFotoApren1").val('');
			alerta(mensajeErrorFoto);
			$("#divFotoApren1").empty();
			$("#divFotoApren1").hide();
			return false;
		}
		fotoApren1 = rutaFoto;
		banderaExtension = true;
		$("#divFotoApren1").empty();
		$("#divFotoApren1").show();
		//$("#divFotoApren1").html('<br><br><iframe id="iframeLogo"  width="auto" src="' + rutaFoto + '" scrolling="no" frameborder="0" style="margin-top: -5%;margin-left: 40%;height: 90%;"></iframe>');
		$("#divFotoApren1").html('<br><br><img src="' + rutaFoto + '" width="150px" height="150px" style="margin-top: -5%;margin-left: 40%;">');
	});
	$("#fleFotoApren2").change(function(){
		if(validarTamanoArchivo('fleFotoApren2', 200) == false){
			$("#divFotoApren2").html("");
			return false;
		}
		
		if(validarExtensionImagen('fleFotoApren2') == false){
			$("#divFotoApren2").html("");
			alerta("Extensión inválida.");
			return false;
		}
		subirFoto("apren2","fleFotoApren2");
		if(rutaFoto == ''){
			$("#fleFotoApren2").val('');
			alerta(mensajeErrorFoto);
			$("#divFotoApren2").empty();
			$("#divFotoApren2").hide();
			return false;
		}
		fotoApren2 = rutaFoto;
		banderaExtension = true;
		$("#divFotoApren2").empty();
		$("#divFotoApren2").show();
		//$("#divFotoApren2").html('<br><br><iframe id="iframeLogo" width="auto" src="' + rutaFoto + '" scrolling="no" frameborder="0" style="margin-top: -5%;margin-left: 40%;height: 120%;"></iframe>');
		$("#divFotoApren2").html('<br><br><img src="' + rutaFoto + '" width="150px" height="150px" style="margin-top: -5%;margin-left: 0%;">');
	});
	$("#fleFotoInstructor").change(function(){
		if(validarTamanoArchivo('fleFotoInstructor', 200) == false){
			$("#divFotoInstructor").html("");
			return false;
		}
		
		if(validarExtensionImagen('fleFotoInstructor') == false){
			$("#divFotoInstructor").html("");
			alerta("Extensión inválida.");
			return false;
		}
		subirFoto("instructor","fleFotoInstructor");
		if(rutaFoto == ''){
			alerta(mensajeErrorFoto);
			$("#fleFotoInstructor").val('');
			$("#divFotoInstructor").empty();
			$("#divFotoInstructor").hide();
			return false;
		}
		fotoInstructor = rutaFoto;
		banderaExtension = true;
		$("#divFotoInstructor").empty();
		$("#divFotoInstructor").show();
		//$("#divFotoInstructor").html('<br><br><iframe id="iframeLogo"  width="auto" src="' + rutaFoto + '" scrolling="no" frameborder="0" style="margin-top: -5%;margin-left: 40%;height: 90%;"></iframe>');
		$("#divFotoInstructor").html('<br><br><img src="' + rutaFoto + '" width="150px" height="150px" style="margin-top: -5%;margin-left: 40%;">');
	});
	$("#selRol").change(function(){
		visualizarTabSeleccionado();
	});
	
	$("#btnRegistrar").bind({
		"click":function(){
			if(validarVacios(document.frmDatosPrincipales) == false){
				return false;
			}
			var rol = $('#selRol').val();
			if(rol == "optAprendiz"){
				if(validarVacios(document.frmInscrApren1) == false){
					$("#tabInscripcionCompetencia").tabs( "option","active",0);
					return false;
				}
				if(validarVacios(document.frmInscrApren2) == false){
					$("#tabInscripcionCompetencia").tabs( "option","active", 1 );
					return false;
				}
				if(validarCorreoElectronico("txtCorreElectApren1") == false){
					alerta("El correo electrónico del aprendiz 1 no es válido");
					return false;
				}
				if(validarCorreoElectronico("txtCorreElectApren2") == false){
					alerta("El correo electrónico del aprendiz 2  no es válido");
					return false;
				}
				if(validarNumeroCelular($('#txtTelefCelulApren1').val()) == false){
					alerta("El número de celular del aprendiz 1 no es válido");
					return false;	
				}
				if(validarNumeroCelular($('#txtTelefCelulApren2').val()) == false){
					alerta("El número de celular del aprendiz 2 no es válido");
					return false;	
				}
				if($("#txtCaptcha").val() == ""){
					alerta("Ingrese los caracteres especiales mostrados en la imagen");
					$("#txtCaptcha").focus();
					return false;
				}
				if(validarCaptcha($("#txtCaptcha").val()) == false)
					return false;
				
				var retorno;
				/* Adicionar Aprendiz 1 */
				foto  = fotoApren1;
				asignDatosEnvioApren1();
				banderaAprendizEvaluar = 1;
				retorno = validarAprenInscrCateg();
				if( retorno == false){
					return false;
				}
				
				asignDatosEnvioApren2();
				banderaAprendizEvaluar = 2;
				if(validarAprenInscrCateg() == false){
					return false;
				}
				if(validarInscripcionAprendiz() == false){
					return false;
				}

				asignDatosEnvioApren1();
				obtenerDatosEnvioPersona();
				adicionarPersona();
				obtenerDatosEnvioPersonaInfoAdicional();
				adicionarPersonaInfoAdicional();
				obtenerDatosEnvioAprendiz();
				adicionarAprendiz();
				obtenerDatosEnvioInscrCategoConcuAprend();
				adicionarInscrCategoConcuAprend();
				
				/* Adicionar Aprendiz 2 */
				foto  = fotoApren2;
				asignDatosEnvioApren2();
				obtenerDatosEnvioPersona();
				adicionarPersona();
				obtenerDatosEnvioPersonaInfoAdicional();
				adicionarPersonaInfoAdicional();
				obtenerDatosEnvioAprendiz();
				adicionarAprendiz();
				obtenerDatosEnvioInscrCategoConcuAprend();
				adicionarInscrCategoConcuAprend();
				
				limpiarControlesFormulario(document.frmInscrApren1);
				limpiarControlesFormulario(document.frmInscrApren2);
				$("#divFotoApren1").html("");
				$("#divFotoApren2").html("");
				$( "#tabInscripcionCompetencia" ).hide();
				limpiarControlesFormulario(document.frmDatosPrincipales);	
				limpiarVariables();
				$("#txtCaptcha").val("");
				$("#imgRecargar").click();
				
			}else{
				if(validarVacios(document.frmInscripcionInstructor) == false){
					$("#tabInscripcionCompetencia").tabs( "option","active",2);
					return false;
				}	
				if(validarCorreoElectronico("txtCorreElectInstr") == false){
					alerta("El correo electrónico del instructor no es válido");
					return false;
				}
				if(validarNumeroCelular($('#txtTelefCelulInstr').val()) == false){
					alerta("El número de celular del instructor no es válido");
					return false;	
				}
				if($("#txtCaptcha").val() == ""){
					alerta("Ingrese los caracteres especiales mostrados en la imagen");
					$("#txtCaptcha").focus();
					return false;
				}
				if(validarCaptcha($("#txtCaptcha").val()) == false)
					return false;
				
				foto  = fotoInstructor;
				asignDatosEnvioInstr();
				if(validarInstrInscrCateg() == false){
					return false;
				}
				if(validarInscripcionInstructor() == false){
					return false;
				}
				
				asignDatosEnvioInstr();
				obtenerDatosEnvioPersona();
				adicionarPersona();
				obtenerDatosEnvioPersonaInfoAdicional();
				adicionarPersonaInfoAdicional();
				obtenerDatosEnvioInstructor();
				adicionarInstructor();
				obtenerDatosEnvioInscrCategoConcuInstr();
				adicionarInscrCategoConcuInstru();
				
				limpiarControlesFormulario(document.frmInscripcionInstructor);
				limpiarControlesFormulario(document.frmDatosPrincipales);
				$("#divFotoInstructor").html("");
				$( "#tabInscripcionCompetencia" ).hide();
				limpiarVariables();
				$("#txtCaptcha").val("");
				$("#imgRecargar").click();
			}
			
		}
	});
});
/*Función que permite  cargar la información de el centro de formación y la regional asociada al código ingresado */
function cargarInforCentrForma(){
	$.ajax({
		url:'../controlador/inscripcionCategoria.cargar.php',
		type:'POST',
		dataType:"json",
		data:null,
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;
			var numeroRegistros = json.numeroRegistros;
			
			if(exito == 0){
				alerta(mensaje);
				return false;
			}
			if(numeroRegistros == 1){
				$.each(json.data,function(contador, fila){
					idCentroFormacion = fila.idCentroFormacion;
					$('#nombreCentroFormacion').html(fila.centroFormacion);
					$('#nombreRegional').html(fila.regional);
				});
			}else{
				//alerta("Debe ingresar el código del centro de formación");
				window.open("frmCodigoInscripcion.html","_parent");
			}
		}	
	});	
	
}		
/*Muestra y oculta la información a partir del tab que haya seleccionado el usuario */
function visualizarTabSeleccionado(){
	var valorTabSeleccionado = $('#selRol').val();
	
	if(valorTabSeleccionado == ''){
		$( "#tabInscripcionCompetencia" ).hide();
	}
	if(valorTabSeleccionado == 'optAprendiz'){
		$( "#tabInscripcionCompetencia" ).show();
		$('#tabAprendiz1Link').show();
		$('#tabAprendiz2Link').show();
		$('#tabInstructorLink').hide();
		$("#tabInscripcionCompetencia").tabs( "option","active", 0 );
	}
	if(valorTabSeleccionado == 'optInstructor'){
		$( "#tabInscripcionCompetencia" ).show();
		$('#tabAprendiz1Link').hide();
		$('#tabAprendiz2Link').hide();
		$('#tabInstructorLink').show();
		$("#tabInscripcionCompetencia").tabs( "option","active", 2 );
	}		
}
/*Función que permite cargar las categorías del formulario*/
function cargarCategoriasCompetencia(){
	$.ajax({
		async:false
		,url:'../controlador/competencia.cargarCategoria.php',
		type:'POST',
		dataType:"json",
		data:null,
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;
			var numeroRegistros = json.numeroRegistros;
			
			if(exito == 0){
				alerta(mensaje);
				return false;
			}
			if(numeroRegistros == 0){
				alerta("No se encontraron registros con los parámetros indicados por el usuario.");
				limpiarVariables();
				return false;
			}
			var control = $('#selCategoria');

			control.empty();
	
			control.append('<option value="">--Seleccione la categoría--</option>');
		
			$.each(json.data,function(contador, fila){
				control.append('<option value="'+fila.idCategoriaConcurso+'">'+fila.categoriaConcurso+'</option>');
				
				
			});
			
		}	
	});	
}
/*Función que permite cargar los tipos de documentos  del formulario */
function cargarTipoDocumIdent(){
	$.ajax({
		async:false
		,url:'../controlador/competencia.cargarTipoDocumIdent.php',
		type:'POST',
		dataType:"json",
		data:null,
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;
			var numeroRegistros = json.numeroRegistros;
			
			if(exito == 0){
				alerta(mensaje);
				return false;
			}
			if(numeroRegistros == 0){
				alerta("No se encontraron registros con los parámetros indicados por el usuario.");
				limpiarVariables();
				return false;
			}
			var control1 = $('#selTipoDocumApren1');
			var control2 = $('#selTipoDocumApren2');
			var control3 = $('#selTipoDocumInstr');
			control1.empty();
			control2.empty();
			control3.empty();
			control1.append('<option value="">--Seleccione el tipo de documento--</option>');
			control2.append('<option value="">--Seleccione el tipo de documento--</option>');
			control3.append('<option value="">--Seleccione el tipo de documento--</option>');
			$.each(json.data,function(contador, fila){
				control1.append('<option value="'+fila.idTipoDocumIdent+'">'+fila.tipoDocumIdent+'</option>');
				control2.append('<option value="'+fila.idTipoDocumIdent+'">'+fila.tipoDocumIdent+'</option>');
				control3.append('<option value="'+fila.idTipoDocumIdent+'">'+fila.tipoDocumIdent+'</option>');
			});
			
		}	
	});	
}
/*Función que permite cargar Rh */
function cargarRh(){
	$.ajax({
	   url:'../controlador/competencia.cargarRh.php',
	   type:'POST',
	   dataType:"json",
	   data:null,
	   success: function(json){
		   var exito = json.exito;
		   var mensaje = json.mensaje;
		   
			if(exito == 0){
				alerta(mensaje);
				return false;
	   		}
			var control1 = $("#selRhApren1");
			var control2 = $("#selRhApren2");
			var control3 = $("#selRhInstructor");
			
			control1.empty();
			control2.empty();
			control3.empty();
			
			control1.append('<option value="">--Seleccione RH--</option>');
			control2.append('<option value="">--Seleccione RH--</option>');
			control3.append('<option value="">--Seleccione RH--</option>');
			
			$.each(json.data, function(contador,fila){
				control1.append('<option value="' + fila + '">' + fila + '</option>');
				control2.append('<option value="' + fila + '">' + fila + '</option>');
				control3.append('<option value="' + fila + '">' + fila + '</option>');
			});
	   },error:function(xhr,opciones,error){
			alerta("error"+ error);
		}
	});
}
function cargarTallaCamiseta(){
	$.ajax({
	   url:'../controlador/competencia.cargarTalla.php',
	   type:'POST',
	   dataType:"json",
	   data:null,
	   success: function(json){
		   var exito = json.exito;
		   var mensaje = json.mensaje;
		   
			if(exito == 0){
				alerta(mensaje);
				return false;
	   		}
			var control1 = $("#selTallaApren1");
			var control2 = $("#selTallaApren2");
			var control3 = $("#selTallaInstr");
			
			control1.empty();
			control2.empty();
			control3.empty();
			
			control1.append('<option value="">--Seleccione su talla de camiseta--</option>');
			control2.append('<option value="">--Seleccione su talla de camiseta--</option>');
			control3.append('<option value="">--Seleccione su talla de camiseta--</option>');
			
			$.each(json.data, function(contador,fila){
				control1.append('<option value="' + fila + '">' + fila + '</option>');
				control2.append('<option value="' + fila + '">' + fila + '</option>');
				control3.append('<option value="' + fila + '">' + fila + '</option>');
			});
	   },error:function(xhr,opciones,error){
			alerta("error"+ error);
		}
	});
}

function cargarProgramaFormacion(){
	$.ajax({
		async:false
		,url:'../controlador/competencia.cargarProgramaFormacion.php',
		type:'POST',
		dataType:"json",
		data:null,
		success: function(json){
			var exito = json.exito;
			var mensaje = json.mensaje;
			var numeroRegistros = json.numeroRegistros;
			
			if(exito == 0){
				alerta(mensaje);
				return false;
			}
			if(numeroRegistros == 0){
				alerta("No se encontraron registros con los parámetros indicados por el usuario.");
				limpiarVariables();
				return false;
			}
			var control1 = $('#selProgrApren1');
			var control2 = $('#selProgrApren2');
			
			control1.empty();
			control2.empty();
			
			control1.append('<option value="">--Seleccione el programa de formación--</option>');
			control2.append('<option value="">--Seleccione el programa de formación--</option>');
			
			$.each(json.data,function(contador, fila){
				control1.append('<option value="'+fila.idProgramaFormacion+'">'+fila.programaFormacion+'</option>');
				control2.append('<option value="'+fila.idProgramaFormacion+'">'+fila.programaFormacion+'</option>');
				
				
			});
			
		}	
	});	
}
function asignDatosEnvioApren1(){
	idCategoria = $('#selCategoria').val();
	idTipoDocumentoIdentidad = $('#selTipoDocumApren1').val();
	numeroIdentificacion = $('#txtDocumIdentApren1').val();
	idProgramaFormacion = $('#selProgrApren1').val();
	primerNombre = $('#txtPrimeNombrApren1').val();
	primerApellido = $('#txtPrimeApellApren1').val();
	segundoApellido = $('#txtSegunApellApren1').val();
	fechaNacimiento = $('#txtFechaNacimApren1').val();
	rh = $('#selRhApren1').val();
	tallaCamiseta = $('#selTallaApren1').val();
	correoElectronico = $('#txtCorreElectApren1').val();
	telefonoFijo = $('#txtTelefFijoApren1').val();
	telefonoCelular = $('#txtTelefCelulApren1').val();
	genero = $('#selGenerApren1').val();
	cuidadoMedico = $('#txaCuidadoApren1').val();
}
function asignDatosEnvioApren2(){
	idCategoria = $('#selCategoria').val();
	idTipoDocumentoIdentidad = $('#selTipoDocumApren2').val();
	numeroIdentificacion = $('#txtDocumIdentApren2').val();
	idProgramaFormacion = $('#selProgrApren2').val();
	primerNombre = $('#txtPrimeNombrApren2').val();
	primerApellido = $('#txtPrimeApellApren2').val();
	segundoApellido = $('#txtSegunApellApren2').val();
	fechaNacimiento = $('#txtFechaNacimApren2').val();
	rh = $('#selRhApren2').val();
	tallaCamiseta = $('#selTallaApren2').val();
	correoElectronico = $('#txtCorreElectApren2').val();
	telefonoFijo = $('#txtTelefFijoApren2').val();
	telefonoCelular = $('#txtTelefCelulApren2').val();
	genero = $('#selGenerApren2').val();
	cuidadoMedico = $('#txaCuidadoApren2').val();
}
function asignDatosEnvioInstr(){
	idCategoria = $('#selCategoria').val();
	idTipoDocumentoIdentidad = $('#selTipoDocumInstr').val();
	numeroIdentificacion = $('#txtDocumIdentInstr').val();
	primerNombre = $('#txtPrimeNombrInstr').val();	
	primerApellido = $('#txtPrimeApellInstr').val();
	segundoApellido = $('#txtSegunApellInstr').val();
	fechaNacimiento = $('#txtFechaNacimInstr').val();
	rh = $('#selRhInstructor').val();
	tallaCamiseta = $('#selTallaInstr').val();
	correoElectronico = $('#txtCorreElectInstr').val();
	telefonoFijo = $('#txtTelefFijoInstr').val();
	telefonoCelular = $('#txtTelefCelulInstr').val();
	genero = $('#selGenerInstr').val();
	cuidadoMedico = $('#txaCuidadoInstr').val();
}
/*Función que permite validar el nùmero de aprendices que se permiten registrar a una determinada categorìa */
function validarLimiteAprendices(){
	var banderaAjax;
	$.ajax({
		   async:false,
		   url:'../controlador/competencia.validarLimiteAprendiz.php',
		   type:'POST',
		   dataType:"json",
		   data:{idCategoria:idCategoria},
		   success: function(json){
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					banderaAjax = false;
					return false;
		   		}
				var numeroLimiteAprendiz;
				var aprendizInscritos;
				
				if(json.data != null){
					$.each(json.data,function(contador, fila){
						aprendizInscritos = parseFloat(fila.numeroAprendicesCategoria);
						numeroLimiteAprendiz =  parseFloat(fila.limiteNumeroAprendiz);
					});
					if(numeroLimiteAprendiz <= aprendizInscritos){
						alerta("Lo sentimos , no hay cupos disponibles para esta categoría");
						banderaAjax = false;
						return false;
					}
				}
				
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
		if(banderaAjax == false){
			return false;
		}
}

/*Función que permite validar el nùmero de instructores que se permiten registrar a una determinada categorìa */
function validarLimiteInstructor(){
	var banderaAjax;
	$.ajax({
		   async:false,
		   url:'../controlador/competencia.validarLimiteInstructor.php',
		   type:'POST',
		   dataType:"json",
		   data:{idCategoria:idCategoria},
		   success: function(json){
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					banderaAjax = false;
					return false;
		   		}
				var numeroLimiteInstructor;
				var instructorInscritos;
				if(json.data != null){
					$.each(json.data,function(contador, fila){
						instructorInscritos = parseFloat(fila.numeroInstructorCategoria);
						numeroLimiteInstructor =  parseFloat(fila.limiteNumeroInstructor);
					});
					if(numeroLimiteInstructor == instructorInscritos){
						alerta("Lo sentimos , no hay cupos disponibles para esta categoría");
						banderaAjax = false;
						return false;
					}
				}
				
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
		if(banderaAjax == false){
			return false;
		}
}
/*Función que permite validar si el centro de formación ya ha registrado una pareja a una categoría determinada */
function validarAprenInscrCentrForma(){
	var banderaAjax;
	$.ajax({
		   async:false,
		   url:'../controlador/competencia.validarAprenInscrCentrForma.php',
		   type:'POST',
		   dataType:"json",
		   data:{idCategoria:idCategoria,idCentroFormacion:idCentroFormacion},
		   success: function(json){
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					banderaAjax = false;
					return false;
		   		}
				var aprendicesInscritosCategoria;
				$.each(json.data,function(contador, fila){
					aprendicesInscritosCategoria = fila.numeroAprendicesCategoria;
					
				});
				if(aprendicesInscritosCategoria != 0){
					alerta("Lo sentimos , ya ha sido registrada una pareja de aprendices de este centro de formación para esta categoría");
					banderaAjax = false;
					return false;
				}
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
		if(banderaAjax == false){
			return false;
		}
}

/*Función que permite validar el número máximo de aprendices inscritos por centro de formacion */
function validarLimitAprenInscrCentrForma(){
	var banderaAjax;
	$.ajax({
		   async:false,
		   url:'../controlador/competencia.validarLimitAprenInscrCentrForma.php',
		   type:'POST',
		   dataType:"json",
		   data:{idCentroFormacion:idCentroFormacion},
		   success: function(json){
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					banderaAjax = false;
					return false;
		   		}
				var limiteMaximoAprendices;
				var aprendicesInscritosCentroFormacion;
				$.each(json.data,function(contador, fila){
					limiteMaximoAprendices = parseFloat(fila.limiteAprendices);
					aprendicesInscritosCentroFormacion = parseFloat(fila.numeroAprendicesInscritos);
					
				});
				if(limiteMaximoAprendices <= aprendicesInscritosCentroFormacion){
					alerta("Lo sentimos , el centro de formación  alcanzó el cupo máximo de aprendices inscritos.");
					banderaAjax = false;
					return false;
				}
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
		if(banderaAjax == false){
			return false;
		}
}



/*Función que permite validar el número máximo de instructores inscritos por centro de formacion */
function validarLimitInstrInscrCentrForma(){
	var banderaAjax;
	$.ajax({
		   async:false,
		   url:'../controlador/competencia.validarLimitInstrInscrCentrForma.php',
		   type:'POST',
		   dataType:"json",
		   data:{idCentroFormacion:idCentroFormacion},
		   success: function(json){
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					banderaAjax = false;
					return false;
		   		}
				var limiteMaximoInstructores;
				var instructoresInscritosCentroFormacion;
				$.each(json.data,function(contador, fila){
					limiteMaximoInstructores = parseFloat(fila.limiteInstructor);
					instructoresInscritosCentroFormacion = parseFloat(fila.numeroInstructoresInscritos);
					
				});
				if(limiteMaximoInstructores <= instructoresInscritosCentroFormacion){
					alerta("Lo sentimos , el centro de formación  alcanzó el cupo máximo de instructores inscritos.");
					banderaAjax = false;
					return false;
				}
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
		if(banderaAjax == false){
			return false;
		}
}




/*Función que permite validar si el centro de formación ya ha registrado un instructor a una categoría determinada */
function validarInstrInscrCentrForma(){
	var banderaAjax; 
	$.ajax({
		   async:false,
		   url:'../controlador/competencia.validarInstrInscrCentrForma.php',
		   type:'POST',
		   dataType:"json",
		   data:{idCategoria:idCategoria,idCentroFormacion:idCentroFormacion},
		   success: function(json){
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					banderaAjax = false;
					return false;
		   		}
				
				var instructoresInscritosCategoria;
				$.each(json.data,function(contador, fila){
					instructoresInscritosCategoria = fila.numeroInstructoresCategoria;
					
				});
				if(instructoresInscritosCategoria != 0){
					alerta("Lo sentimos , ya ha sido registrado un instructor de este centro de formación para esta categoría");
					banderaAjax = false;
					return false;
				}
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
		if(banderaAjax == false){
			return false;
		}
}
/*Función que permite validar si el aprendiz a registrar esta asociada a una categoría determinada */
function validarAprenInscrCateg(){
	var banderaAjax; 
	$.ajax({
		   async:false,
		   url:'../controlador/competencia.validarAprenInscrCateg.php',
		   type:'POST',
		   dataType:"json",
		   data:{numeroIdentificacion:numeroIdentificacion},
		   success: function(json){
			   var exito = json.exito;
			   var numeroRegistros = json.numeroRegistros;
			   
				if(exito == 0){
					alerta(mensaje);
					banderaAjax = false;
					return false;
		   		}
				if(numeroRegistros != 0){
					if(banderaAprendizEvaluar == 1){
						alerta("Lo sentimos , el aprendiz 1 ya ha sido registrado");
					}else{
						alerta("Lo sentimos , el aprendiz 2 ya ha sido registrado");
					}
					
					banderaAjax = false;
					return false;
				}
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
		if(banderaAjax == false){
			return false;
		}
}
/*Función que permite validar si el instructor a registrar esta asociada a una categoría determinada */
function validarInstrInscrCateg(){
	var banderaAjax;
	$.ajax({
		   async:false,
		   url:'../controlador/competencia.validarInstrInscrCateg.php',
		   type:'POST',
		   dataType:"json",
		   data:{numeroIdentificacion:numeroIdentificacion},
		   success: function(json){
			   var exito = json.exito;
			   var numeroRegistros = json.numeroRegistros;
			   
				if(exito == 0){
					alerta(mensaje);
					banderaAjax = false;
					return false;
		   		}
				if(numeroRegistros != 0){
					alerta("Lo sentimos , ya ha sido registrado este instructor ");
					banderaAjax = false;
					return false;
				}
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
		if(banderaAjax == false){
			return false;
		}
}
function adicionarPersona(){
	$.ajax({
		   async:false,
		   url:'../controlador/persona.adicionar.php',
		   type:'POST',
		   dataType:"json",
		   data:data,
		   success: function(json){
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					return false;
		   		}
				idPersona = json.idPersona;
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
	
} 
function adicionarPersonaInfoAdicional(){
	$.ajax({
		   async:false,
		   url:'../controlador/personaInfoAdicional.adicionar.php',
		   type:'POST',
		   dataType:"json",
		   data:data,
		   success: function(json){
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					return false;
		   		}
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
	
} 
function adicionarAprendiz(){
	$.ajax({
		   async:false,
		   url:'../controlador/aprendiz.adicionar.php',
		   type:'POST',
		   dataType:"json",
		   data:data,
		   success: function(json){
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					return false;
		   		}
				idAprendiz = json.idAprendiz;
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
	
}
function adicionarInstructor(){
	$.ajax({
		   async:false,
		   url:'../controlador/instructor.adicionar.php',
		   type:'POST',
		   dataType:"json",
		   data:data,
		   success: function(json){
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					return false;
		   		}
				idInstructor = json.idInstructor;
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
	
}
function adicionarInscrCategoConcuAprend(){
	$.ajax({
		   async:false,
		   url:'../controlador/inscriCategoConcurAprend.adicionar.php',
		   type:'POST',
		   dataType:"json",
		   data:data,
		   success: function(json){
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					return false;
		   		}
				alerta(mensaje);
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
	
}
function adicionarInscrCategoConcuInstru(){
	$.ajax({
		   async:false,
		   url:'../controlador/inscriCategoConcurInstru.adicionar.php',
		   type:'POST',
		   dataType:"json",
		   data:data,
		   success: function(json){
			   var exito = json.exito;
			   var mensaje = json.mensaje;
			   
				if(exito == 0){
					alerta(mensaje);
					return false;
		   		}
				alerta(mensaje);
		   },error:function(xhr,opciones,error){
				alerta("error"+ error);
			}
		});
	
}
function obtenerDatosEnvioPersona(){
	data = "idTipoDocumentoIdentidad="+idTipoDocumentoIdentidad+"&numeroIdentificacion="+numeroIdentificacion+"&primerNombre="+ primerNombre+"&segundoNombre="+ segundoNombre+"&primerApellido="+ primerApellido+"&segundoApellido="+ segundoApellido+"&fechaNacimiento="+ fechaNacimiento+"&correoElectronico="+ correoElectronico+"&telefonoFijo="+ telefonoFijo+"&telefonoCelular="+ telefonoCelular+"&genero="+ genero +"&foto="+ foto;
}
function obtenerDatosEnvioPersonaInfoAdicional(){
	var n = rh.indexOf("+"); 
	if(n != "-1"){
		rh = rh.replace("+","1");
	}
	data = "idPersona="+idPersona+"&rh="+rh+"&tallaCamiseta="+ tallaCamiseta+"&cuidadoMedico="+ cuidadoMedico;
}
function obtenerDatosEnvioAprendiz(){
	data = "idPersona="+idPersona+"&idProgramaFormacion="+idProgramaFormacion+"&idCentroFormacion="+ idCentroFormacion;
}
function obtenerDatosEnvioInscrCategoConcuAprend(){
	data = "idAprendiz="+idAprendiz+"&idProgramaFormacion="+idProgramaFormacion+"&idCentroFormacion="+ idCentroFormacion + "&idCategoriaConcurso="+ idCategoria;
}
function obtenerDatosEnvioInstructor(){
	data = "idPersona="+idPersona+"&idCentroFormacion="+ idCentroFormacion;
}
function obtenerDatosEnvioInscrCategoConcuInstr(){
	data = "idInstructor="+idInstructor+"&idCentroFormacion="+ idCentroFormacion + "&idCategoriaConcurso="+ idCategoria;
}
function validarInscripcionAprendiz(){
	if(validarLimitAprenInscrCentrForma() == false){
		return false;
	}
	if(validarAprenInscrCentrForma() == false){
		return false;
	};
	if(validarLimiteAprendices() == false){
		return false;
	};
}
function validarInscripcionInstructor(){
	if(validarLimitInstrInscrCentrForma() == false){
		return false;
	}
	if(validarInstrInscrCentrForma() == false){
		return false;
	};
	if(validarLimiteInstructor() == false){
		return false;
	};
}
function subirFoto(nombreFotoTemporal,control){
	var archivos = document.getElementById(control);
	var archivo = archivos.files; 
	var data = new FormData();
	for(i=0; i < archivo.length; i++){
		data.append('archivo'+i,archivo[i]);
	}
	data.append('nombreTemporal',nombreFotoTemporal);
	$.ajax({
		async: false,
		url:"../controlador/competencia.subirFoto.php", 
		type:'POST', 
		dataType:"json",
		contentType:false, 
		data:data,
		processData:false, 
		cache:false,
		success: function(json){
			var exito = json.exito;
			mensajeErrorFoto = json.mensaje;
			rutaFoto = json.ruta;
			
			if(exito == 0){
				mensajeErrorFoto = json.mensaje;
				rutaFoto = '';
				return false;
			}
		} 
	});
}
function limpiarVariables(){	
	idInstructor = '';
	idPersona = '';
	idAprendiz = '';	
	idCategoria = '';
	idTipoDocumentoIdentidad = '';
	numeroIdentificacion = '';
	idProgramaFormacion = '';
	primerNombre = '';
	segundoNombre = '';
	primerApellido = '';
	segundoApellido = '';
	fechaNacimiento = '';
	rh = '';
	rutaFoto = '';
	tallaCamiseta = '';
	correoElectronico = '';
	telefonoFijo = '';
	telefonoCelular = '';
	genero = '';
	cuidadoMedico = '';
	data = '';
	banderaAprendizEvaluar = '';
	fotoApren1 = '';
	fotoApren2 = '';
	fotoInstructor = '';

}