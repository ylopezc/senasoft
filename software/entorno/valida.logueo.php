<?php
session_start();
$retorno = array();
$_SESSION['autenticado'] = 1;
$_SESSION['idUsuario'] = 1;
$_SESSION['nombre'] = 'Administrador';
$_SESSION['apellido'] = 'Eventment';
if (@$_SESSION['autenticado'] != 1) {
    $retorno['autenticado'] = 0;
    //exit();
}
if(isset($_SESSION['idUsuario'])){
    $retorno['idUsuario'] = $_SESSION['idUsuario'];
    $retorno['usuario'] = $_SESSION['usuario'];
    $retorno['nombre'] = $_SESSION['nombre'];
    $retorno['apellido'] = $_SESSION['apellido'];
}
echo json_encode($retorno);
?>