<?php
session_start();
require_once 'Conexion.php';
//Establezco la zona horaria por defecto para todas las funciones de fecha y hora utilizadas en el script
date_default_timezone_set('America/Bogota');
$conexion = new Conexion();
$retorno = array();
$_SESSION['autenticado'] = 0;
$usuario = $_POST['Usuario'];
$contrasena = md5($_POST['Contrasena']);
//Obtengo la fecha actual
$fechaActual = date("Y-m-d");

//llamo el procedimiento almacenado que me consulta el usuario
$sentenciaSql = "
                SELECT
                    u.*
                    , DATE_FORMAT(u.fechaActivacion,'%Y-%m-%d') AS fechaActivacion
                    , DATE_FORMAT(u.fechaExpiracion,'%Y-%m-%d') AS fechaExpiracion
                    , p.primerNombre AS nombre
  					,p.foto
                    , p.primerApellido AS apellidos
                    ,u.perfil
                FROM
                        usuario AS u
                        INNER JOIN persona AS p ON p.idPersona = u.idPersona
                WHERE
                        usuario = '$usuario';
                ";
$conexion->ejecutar($sentenciaSql);

//Valido si el usuario está correcto
if($conexion->obtenerNumeroRegistros() == 1){
    $fila = $conexion->obtenerObjeto();

    //Valido si la contraseña está correcta
    if($fila->contrasenia == $contrasena){
        //Valido si el usuario está activo
        if($fila->estado != 0){
            $_SESSION['autenticado'] = 1;
            $_SESSION['nombre'] = $fila->nombre;
            $_SESSION['idUsuario'] = $fila->idUsuario;
			$_SESSION['foto'] = $fila->foto;
            $_SESSION['apellido'] = $fila->apellidos;
            $_SESSION['usuario'] = $usuario;
            $_SESSION['perfil'] = $fila->perfil;
            $retorno['exito'] = 1;
        }else{
            $retorno['exito'] = 0;
            $retorno['mensaje'] = 'El usuario está inactivo';
        }
    }else{
        $retorno['exito'] = 0;
        $retorno['mensaje'] = 'Usuario y/o contraseña inválida.';
    }
}else{
    $retorno['exito'] = 0;
    $retorno['mensaje'] = 'Usuario y/o contraseña inválida';
}
echo json_encode($retorno);
?>