<?php
require_once ('configuracion.php');

class Conexion {
    
    private $conn = null;
    private $recordSet=null;
    
    function __construct() {
        $this->conn = new PDO('mysql:host='.SERVER_NAME.';port='.PORT.';dbname='.DATABASE.';charset=utf8', USER, PASSWORD);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    
    public function ejecutar($sentenciaSql) {
        $this->recordSet = $this->conn->query($sentenciaSql);
        if ($this->recordSet == false)
            throw new Exception('Error ejecutando la consulta. '.$this->conn->error,E_USER_ERROR);
    }
    
    public function obtenerObjeto() {
        return $this->recordSet->fetch(PDO::FETCH_OBJ);
    }
    
    public function obtenerNumeroRegistros() {
        return $this->recordSet->rowCount();
    }
    
    function __destruct() {
        if ($this->recordSet)
            $this->recordSet = null;
        
        if ($this->conn)
            $this->conn = null;
    }
    
    public function obtenerNuevoHtml($etiqueta, $acciones) {
        $nuevoHtml = '<!doctype html>
<html><!-- InstanceBegin template="/Templates/frmFormulario.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="utf-8">

<!-- InstanceBeginEditable name="doctitle" --><title>'.strtoupper($etiqueta).' - EVENTMENT</title><!-- InstanceEndEditable -->
<link href="../css/general.css" rel="stylesheet">
<link href="../css/Formulario/Formulario.css" rel="stylesheet">
<link href="../jquery/css/ui/jquery-ui-1.10.3.custom.css" rel="stylesheet">
<link rel="icon" type="image/png" href="../imagenes/favicon.png" />
<script src="../jquery/js/jquery-1.9.1.js" ></script>
<script src="../jquery/js/jquery-ui-1.10.3.custom.js" ></script>
<script src="../js/general.js"></script>
<script src="../js/disenio.js" ></script>
<!-- InstanceBeginEditable name="redVinculo" -->

<!-- InstanceEndEditable -->
</head>

<body>
        <header class="encabezadoPrincipal">
                <div class="imgEncabezado">
                <img src="../imagenes/Encabezado.png">
            <div class="dialog-link">
                  <p><a id="cerrarSesion" href="#"><img src="../imagenes/PNG/unlock.png">Cerrar Sesión</a></p>
                        </div>
                </div>
                <nav id="menu" class="menu">
            <ul id="menuPrinc">
                <li>
                    <a href="frmPrimerPantallazo.html"><span><img src="../imagenes/PNG/folder_home.png"></span>INICIO</a>
                    <ul id="INICIO"></ul>
                </li>
                <li class="menuSeparado"><a href="" >SEGURIDAD</a>
                    <ul>
                        <li><a href="frmCambiarContrasenia.html" >Cambiar Contraseña</a></li>
                        <li><a href="fbeUsuario.html" >Usuario</a></li>
                        <li><a href="fbeRoles.html" > Rol</a></li>
                        <li><a href="fbeAcciones.html" >Acciones</a></li>
                        <li><a href="fbeFormulario.html" >Formulario</a></li>
                        <li><a href="fbeCarpetas.html" >Carpetas</a></li>
                        <li><a href="fbePersona.html" >Registro Personas</a></li>
                        <div id="SEGURIDAD"></div>
                    </ul>
                </li>
                <li><a href="" >ACTIVIDADES</a>
                    <ul>
                        <li><a href="frmCalendarioEventos.html" >Calendario De Eventos</a></li>
                        <li><a href="fbeActividad.html" >Actividad</a></li>
                        <li>
                            <a href="" >Resportes</a>
                            <ul>
                                <li><a href="frmHistorialActividades.html">Historial De Actividades</a></li>
                                <li><a href="fbeEvento.html">Consultar Eventos</a>
                                <div id="REPORTES"></div>
                                    <ul>
                                        <li><a href="fbePublicacionesGenerales.html" >Publicaciones Generales</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <div id="ACTIVIDADES"></div>
                    </ul>
                </li>
                <div id="nuevosMenu"></div>
                <li class="menuSeparado">
                    <a href="#">CONTÁCTENOS</a>
                    <div id="CONTACTENOS"></div>
                </li>
            </ul>
        </nav>
        </header>

        <section id="secPrincipal" class="secPrincipal">
            <div class="tituloFormulario">
                <!-- InstanceBeginEditable name="redTituloFormulario" -->'.strtoupper($etiqueta).'<!-- InstanceEndEditable -->
                <nav id="navAcciones" class="navAcciones">
                <!-- InstanceBeginEditable name="redAccionesFormulario" -->
                    <a href="#"><img src="../imagenes/Drive USB.png" title="Guardar" id="imgGuardar" name="imgGuardar"></a>
                    <a href="#"><img src="../imagenes/Icon_15.ico" title="Consultar" id="imgConsultar" name="imgConsultar"></a>
                    <a href="#"><img src="../imagenes/Clean-Master-Icon.png" title="Limpiar" id="imgLimpiar" name="imgLimpiar"></a>
                    <span id="nuevasAcciones">
                        '.$acciones.'
                    </span>
                <!-- InstanceEndEditable -->
                </nav>
                <!--Título del formulario ej: CREAR ACCIONES-->                
            </div>
        <section id="secFormulario" class="secFormulario">
            <form id="frmFormulario" method="post" action="">
            <!--Si no necesitan un tab deben eliminar un li y eliminar el div del tab correspondiente-->
            <!--Si necesitan un tab deben adicionar un li y adicionar el div del tab correspondiente-->
            <!-- InstanceBeginEditable name="redSecFormulario" -->
            <!-- InstanceEndEditable -->
            </form>
            <section id="secListado" class="secListado">
                                <!-- InstanceBeginEditable name="redListado" --><!-- InstanceEndEditable -->
            </section>
            <div id="exportar" title="Exportar a..." align="center">
                <div id="exportar-form" align="center">
                    <p><img src="../imagenes/acroread.png" title="pdf" id="imgPdf" name="imgPdf"></p>
                </div>
            </div> 
        </section>
    </section>
    <footer class="footer">
        <p>SENA Centro de la Industria la Empresa y los Servicios - Análisis y Desarrollo de Sistemas de Información 430959 - Copyright 2013 &copy;</p>
    </footer>
</body>
<!-- InstanceEnd --></html>';
        return $nuevoHtml;
    }
	public function obtenerNuevoPhp(){
		$nuevoPhp = "<?php ?>";
		return $nuevoPhp;
	}
	public function obtenerJsYCss(){
		$nuevo = "	";
		return $nuevo;
	}
}
?>
