<?php
    session_start();
    unset($_SESSION['usuario']); 
    unset($_SESSION['autenticado']);
    unset($_SESSION['nombre']);
    unset($_SESSION['apellido']);
    session_destroy();
    header('location: ../Index.html');
?>
