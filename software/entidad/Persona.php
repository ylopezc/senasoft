<?php
namespace entidad;
class Persona {
    
    private $idPersona;
    private $tipoDocumentoIdentidad;
    private $numeroIdentificacion;
    private $foto;
    private $primerNombre;
    private $segundoNombre;
    private $primerApellido;
    private $segundoApellido;
    private $fechaNacimiento;
    private $correoElectronico;
    private $telefonoCelular;
    private $telefonoFijo;
    private $estado;
    private $idUsuarioCreacion;
    private $fechaCreacion;
    private $idUsuarioModificacion;
    private $fechaModificacion;
    private $genero;
    
        
    public function getGenero() 
    {
      return $this->genero;
    }
    
    public function setGenero($genero) 
    {
      $this->genero = $genero;
    }
    
    public function getIdPersona() {
        return $this->idPersona;
    }

    public function setIdPersona($idPersona) {
        $this->idPersona = $idPersona;
    }

    public function getTipoDocumentoIdentidad() {
        return $this->tipoDocumentoIdentidad;
    }

    public function setTipoDocumentoIdentidad(\entidad\TipoDocumentoIdentidad $tipoDocumentoIdentidad) {
        $this->tipoDocumentoIdentidad = $tipoDocumentoIdentidad;
    }

    public function getNumeroIdentificacion() {
        return $this->numeroIdentificacion;
    }

    public function setNumeroIdentificacion($numeroIdentificacion) {
        $this->numeroIdentificacion = $numeroIdentificacion;
    }

    public function getFoto() {
        return $this->foto;
    }

    public function setFoto($foto) {
        $this->foto = $foto;
    }

    public function getPrimerNombre() {
        return $this->primerNombre;
    }

    public function setPrimerNombre($primerNombre) {
        $this->primerNombre = $primerNombre;
    }

    public function getSegundoNombre() {
        return $this->segundoNombre;
    }

    public function setSegundoNombre($segundoNombre) {
        $this->segundoNombre = $segundoNombre;
    }

    public function getPrimerApellido() {
        return $this->primerApellido;
    }

    public function setPrimerApellido($primerApellido) {
        $this->primerApellido = $primerApellido;
    }

    public function getSegundoApellido() {
        return $this->segundoApellido;
    }

    public function setSegundoApellido($segundoApellido) {
        $this->segundoApellido = $segundoApellido;
    }

    public function getFechaNacimiento() {
        return $this->fechaNacimiento;
    }

    public function setFechaNacimiento($fechaNacimiento) {
        $this->fechaNacimiento = $fechaNacimiento;
    }

    public function getCorreoElectronico() {
        return $this->correoElectronico;
    }

    public function setCorreoElectronico($correoElectronico) {
        $this->correoElectronico = $correoElectronico;
    }

    public function getTelefonoCelular() {
        return $this->telefonoCelular;
    }

    public function setTelefonoCelular($telefonoCelular) {
        $this->telefonoCelular = $telefonoCelular;
    }

    public function getTelefonoFijo() {
        return $this->telefonoFijo;
    }

    public function setTelefonoFijo($telefonoFijo) {
        $this->telefonoFijo = $telefonoFijo;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }

    public function getIdUsuarioCreacion() {
        return $this->idUsuarioCreacion;
    }

    public function setIdUsuarioCreacion($idUsuarioCreacion) {
        $this->idUsuarioCreacion = $idUsuarioCreacion;
    }

    public function getFechaCreacion() {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion($fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;
    }

    public function getIdUsuarioModificacion() {
        return $this->idUsuarioModificacion;
    }

    public function setIdUsuarioModificacion($idUsuarioModificacion) {
        $this->idUsuarioModificacion = $idUsuarioModificacion;
    }

    public function getFechaModificacion() {
        return $this->fechaModificacion;
    }

    public function setFechaModificacion($fechaModificacion) {
        $this->fechaModificacion = $fechaModificacion;
    }    
}

?>
