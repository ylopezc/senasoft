<?php
namespace entidad;

class EmpresaCodigo{
        private $idEmpresaCodigo;
        private $empresaCodigo;
        private $nit;
        private $empresa;
        private $correoElectronico;
        private $idEstadoEmpresa;
        private $estado;
        private $idUsuarioCreacion;
        private $idUsuarioModificacion;
        private $fechaCreacion;
        private $fechaModificacion;
        
        
            
        public function getIdEstadoEmpresa() 
        {
          return $this->idEstadoEmpresa;
        }
        
        public function setIdEstadoEmpresa($idEstadoEmpresa) 
        {
          $this->idEstadoEmpresa = $idEstadoEmpresa;
        }
        
        
            
        public function getCorreoElectronico() 
        {
          return $this->correoElectronico;
        }
        
        public function setCorreoElectronico($correoElectronico) 
        {
          $this->correoElectronico = $correoElectronico;
        }
        
            
        public function getEmpresa() 
        {
          return $this->empresa;
        }
        
        public function setEmpresa($empresa) 
        {
          $this->empresa = $empresa;
        }
        
            
        public function getNit() 
        {
          return $this->nit;
        }
        
        public function setNit($nit) 
        {
          $this->nit = $nit;
        }
        
        public function getIdEmpresaCodigo() {
          return $this->idEmpresaCodigo;
        }

        public function setIdEmpresaCodigo($idEmpresaCodigo) {
              $this->idEmpresaCodigo = $idEmpresaCodigo;
        }
	
        public function getEmpresaCodigo() {
          return $this->empresaCodigo;
        }

        public function setEmpresaCodigo($empresaCodigo) {
              $this->empresaCodigo = $empresaCodigo;
        }

        public function getEstado() {
           return $this->estado;
        }

        public function setEstado($estado) {
           $this->estado = $estado;
        }
        
        public function getIdUsuarioCreacion() {
           return $this->idUsuarioCreacion;
        }

        public function setIdUsuarioCreacion($idUsuarioCreacion) {
            $this->idUsuarioCreacion = $idUsuarioCreacion;
        }
        
        public function getIdUsuarioModificacion() {
           return $this->idUsuarioModificacion;
        }

        public function setIdUsuarioModificacion($idUsuarioModificacion) {
            $this->idUsuarioModificacion = $idUsuarioModificacion;
        }
        
        public function getFechaCreacion() {
           return $this->fechaCreacion;
        }

        public function setFechaCreacion($fechaCreacion) {
            $this->fechaCreacion = $fechaCreacion;
        }
        
        public function getFechaModificacion() {
           return $this->fechaModificacion;
        }

        public function setFechaModificacion($fechaModificacion) {
            $this->fechaModificacion = $fechaModificacion;
        }
	
}
?>