<?php

namespace entidad;
class Empresa {
    private $idEmpresa;
    private $nit;
    private $empresa;
    private $representanteLegal;
    private $direccion;
    private $telefonoCelular;
    private $telefonoFijo;
    private $correoElectronico;
    private $url;
    private $idMunicipio;
    private $estado;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idRegimenEmpresa;
    private $idSectorEconomico;
    private $sectorEconomico;
    
        
    public function getSectorEconomico() 
    {
      return $this->sectorEconomico;
    }
    
    public function setSectorEconomico($sectorEconomico) 
    {
      $this->sectorEconomico = $sectorEconomico;
    }
    
        
    public function getIdSectorEconomico() 
    {
      return $this->idSectorEconomico;
    }
    
    public function setIdSectorEconomico($idSectorEconomico) 
    {
      $this->idSectorEconomico = $idSectorEconomico;
    }
        
    public function getIdRegimenEmpresa() 
    {
      return $this->idRegimenEmpresa;
    }
    
    public function setIdRegimenEmpresa($idRegimenEmpresa) 
    {
      $this->idRegimenEmpresa = $idRegimenEmpresa;
    }
    
   
    
    public function getIdEmpresa(){
        return $this->idEmpresa;
    }
    
    public function setIdEmpresa($idEmpresa){
         $this->idEmpresa = $idEmpresa;
    }
    
     public function getNit(){
        return $this->nit;
    }
    
    public function setNit($nit){
         $this->nit = $nit;
    }
    
    public function getEmpresa(){
        return $this->empresa;
    }
    
    public function setEmpresa($empresa){
         $this->empresa = $empresa;
    }
    
    public function getRepresentanteLegal(){
        return $this->representanteLegal;
    }
    
    public function setRepresentanteLegal($representanteLegal){
         $this->representanteLegal = $representanteLegal;
    }
    
    public function getDireccion(){
        return $this->direccion;
    }
    
    public function setDireccion($direccion){
         $this->direccion = $direccion;
    }
    
    public function getTelefonoCelular(){
        return $this->telefonoCelular;
    }
    
    public function setTelefonoCelular($telefonoCelular){
         $this->telefonoCelular = $telefonoCelular;
    }
    
    public function getTelefonoFijo(){
        return $this->telefonoFijo;
    }
    
    public function setTelefonoFijo($telefonoFijo){
         $this->telefonoFijo = $telefonoFijo;
    }
    
    public function getCorreoElectronico(){
        return $this->correoElectronico;
    }
    
    public function setCorreoElectronico($correoElectronico){
         $this->correoElectronico = $correoElectronico;
    }
    
    public function getUrl(){
        return $this->url;
    }
    
    public function setUrl($url){
         $this->url = $url;
    }
    
    public function getIdMunicipio(){
        return $this->idMunicipio;
    }
    
    public function setIdMunicipio($idMunicipio){
         $this->idMunicipio = $idMunicipio;
    }
    
    public function getEstado() {
           return $this->estado;
    }

    public function setEstado($estado) {
       $this->estado = $estado;
    }

    public function getIdUsuarioCreacion() {
       return $this->idUsuarioCreacion;
    }

    public function setIdUsuarioCreacion($idUsuarioCreacion) {
        $this->idUsuarioCreacion = $idUsuarioCreacion;
    }

    public function getIdUsuarioModificacion() {
       return $this->idUsuarioModificacion;
    }

    public function setIdUsuarioModificacion($idUsuarioModificacion) {
        $this->idUsuarioModificacion = $idUsuarioModificacion;
    }

    public function getFechaCreacion() {
       return $this->fechaCreacion;
    }

    public function setFechaCreacion($fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;
    }

    public function getFechaModificacion() {
       return $this->fechaModificacion;
    }

    public function setFechaModificacion($fechaModificacion) {
        $this->fechaModificacion = $fechaModificacion;
    }
}

?>
