<?php
namespace entidad;
class LenguajeProgramacion {
    private $idLenguajeProgramacion;
    private $lenguajeProgramacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    private $estado;
    
    public function getIdLenguajeProgramacion() {
        return $this->idLenguajeProgramacion;
    }

    public function getLenguajeProgramacion() {
        return $this->lenguajeProgramacion;
    }

    public function getIdUsuarioCreacion() {
        return $this->idUsuarioCreacion;
    }

    public function getIdUsuarioModificacion() {
        return $this->idUsuarioModificacion;
    }

    public function getFechaCreacion() {
        return $this->fechaCreacion;
    }

    public function getFechaModificacion() {
        return $this->fechaModificacion;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function setIdLenguajeProgramacion($idLenguajeProgramacion) {
        $this->idLenguajeProgramacion = $idLenguajeProgramacion;
    }

    public function setLenguajeProgramacion($lenguajeProgramacion) {
        $this->lenguajeProgramacion = $lenguajeProgramacion;
    }

    public function setIdUsuarioCreacion($idUsuarioCreacion) {
        $this->idUsuarioCreacion = $idUsuarioCreacion;
    }

    public function setIdUsuarioModificacion($idUsuarioModificacion) {
        $this->idUsuarioModificacion = $idUsuarioModificacion;
    }

    public function setFechaCreacion($fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;
    }

    public function setFechaModificacion($fechaModificacion) {
        $this->fechaModificacion = $fechaModificacion;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }


}
