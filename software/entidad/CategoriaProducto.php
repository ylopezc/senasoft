<?php
    namespace entidad;

    class CategoriaProducto{
        private $idCategoriaProducto;
        private $categoriaProducto;
        private $estado;
        private $idUsuarioCreacion;
        private $idUsuarioModificacion;
        private $fechaCreacion;
        private $fechaModificacion;
        
        
        public function getIdCategoriaProducto(){
            return $this->idCategoriaProducto;
        }
    
        public function setIdCategoriaProducto($idCategoriaProducto){
             $this->idCategoriaProducto = $idCategoriaProducto;
        }
    
        public function getCategoriaProducto(){
            return $this->categoriaProducto;
        }

        public function setCategoriaProducto($categoriaProducto){
             $this->categoriaProducto = $categoriaProducto;
        }
        
        public function getEstado() {
           return $this->estado;
        }

        public function setEstado($estado) {
           $this->estado = $estado;
        }

        public function getIdUsuarioCreacion() {
           return $this->idUsuarioCreacion;
        }

        public function setIdUsuarioCreacion($idUsuarioCreacion) {
            $this->idUsuarioCreacion = $idUsuarioCreacion;
        }

        public function getIdUsuarioModificacion() {
           return $this->idUsuarioModificacion;
        }

        public function setIdUsuarioModificacion($idUsuarioModificacion) {
            $this->idUsuarioModificacion = $idUsuarioModificacion;
        }

        public function getFechaCreacion() {
           return $this->fechaCreacion;
        }

        public function setFechaCreacion($fechaCreacion) {
            $this->fechaCreacion = $fechaCreacion;
        }

        public function getFechaModificacion() {
           return $this->fechaModificacion;
        }

        public function setFechaModificacion($fechaModificacion) {
            $this->fechaModificacion = $fechaModificacion;
        }


    }

?>