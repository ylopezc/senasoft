<?php
namespace entidad;
class CategoriaConcurso{
	private $idCategoriaConcurso;
	private $categoriaConcurso;
	private $limiteNumeroAprendiz;
	private $limiteNumeroInstructor;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	private $estado;
	
	 
	public function getEstado()
	{
		return $this->estado;
	}
	
	public function setEstado($estado)
	{
		$this->estado = $estado;
	}
	
	
	public function getFechaModificacion()
	{
		return $this->fechaModificacion;
	}
	
	public function setFechaModificacion($fechaModificacion)
	{
		$this->fechaModificacion = $fechaModificacion;
	}
	
	
	public function getFechaCreacion()
	{
		return $this->fechaCreacion;
	}
	
	public function setFechaCreacion($fechaCreacion)
	{
		$this->fechaCreacion = $fechaCreacion;
	}
	
	public function getIdUsuarioModificacion()
	{
		return $this->idUsuarioModificacion;
	}
	
	public function setIdUsuarioModificacion($idUsuarioModificacion)
	{
		$this->idUsuarioModificacion = $idUsuarioModificacion;
	}
	
	
	public function getIdUsuarioCreacion()
	{
		return $this->idUsuarioCreacion;
	}
	
	public function setIdUsuarioCreacion($idUsuarioCreacion)
	{
		$this->idUsuarioCreacion = $idUsuarioCreacion;
	}
	    
	public function getLimiteNumeroInstructor() 
	{
	  return $this->limiteNumeroInstructor;
	}
	
	public function setLimiteNumeroInstructor($limiteNumeroInstructor) 
	{
	  $this->limiteNumeroInstructor = $limiteNumeroInstructor;
	}
	    
	public function getLimiteNumeroAprendiz() 
	{
	  return $this->limiteNumeroAprendiz;
	}
	
	public function setLimiteNumeroAprendiz($limiteNumeroAprendiz) 
	{
	  $this->limiteNumeroAprendiz = $limiteNumeroAprendiz;
	}
	    
	public function getCategoriaConcurso() 
	{
	  return $this->categoriaConcurso;
	}
	
	public function setCategoriaConcurso($categoriaConcurso) 
	{
	  $this->categoriaConcurso = $categoriaConcurso;
	}
	
	    
	public function getIdCategoriaConcurso() 
	{
	  return $this->idCategoriaConcurso;
	}
	
	public function setIdCategoriaConcurso($idCategoriaConcurso) 
	{
	  $this->idCategoriaConcurso = $idCategoriaConcurso;
	}
}
?>