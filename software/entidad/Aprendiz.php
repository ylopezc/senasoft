<?php
namespace  entidad;
class Aprendiz{
	
	private $idAprendiz;
	private $persona;
	private $programaFormacion;
	private $centroFormacion;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	private $estado;
	
	    
	public function getEstado() 
	{
	  return $this->estado;
	}
	
	public function setEstado($estado) 
	{
	  $this->estado = $estado;
	}
	
	
	public function getFechaModificacion()
	{
		return $this->fechaModificacion;
	}
	
	public function setFechaModificacion($fechaModificacion)
	{
		$this->fechaModificacion = $fechaModificacion;
	}
	
	
	public function getFechaCreacion()
	{
		return $this->fechaCreacion;
	}
	
	public function setFechaCreacion($fechaCreacion)
	{
		$this->fechaCreacion = $fechaCreacion;
	}
	
	public function getIdUsuarioModificacion()
	{
		return $this->idUsuarioModificacion;
	}
	
	public function setIdUsuarioModificacion($idUsuarioModificacion)
	{
		$this->idUsuarioModificacion = $idUsuarioModificacion;
	}
	
	
	public function getIdUsuarioCreacion()
	{
		return $this->idUsuarioCreacion;
	}
	
	public function setIdUsuarioCreacion($idUsuarioCreacion)
	{
		$this->idUsuarioCreacion = $idUsuarioCreacion;
	}
	
	    
	public function getCentroFormacion() 
	{
	  return $this->centroFormacion;
	}
	
	public function setCentroFormacion(\entidad\CentroFormacion $centroFormacion) 
	{
	  $this->centroFormacion = $centroFormacion;
	}
	
	
	    
	public function getProgramaFormacion() 
	{
	  return $this->programaFormacion;
	}
	
	public function setProgramaFormacion(\entidad\ProgramaFormacion $programaFormacion) 
	{
	  $this->programaFormacion = $programaFormacion;
	}
	
	    
	public function getPersona() 
	{
	  return $this->persona;
	}
	
	public function setPersona(\entidad\Persona $persona) 
	{
	  $this->persona = $persona;
	}
	    
	public function getIdAprendiz() 
	{
	  return $this->idAprendiz;
	}
	
	public function setIdAprendiz($idAprendiz) 
	{
	  $this->idAprendiz = $idAprendiz;
	}
	
	
	
}

?>