<?php
namespace entidad;
class TipoSolucion {
    private $idTipoSolucion;
    private $tipoSolucion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    private $estado;
    
    public function getIdTipoSolucion() {
        return $this->idTipoSolucion;
    }

    public function getTipoSolucion() {
        return $this->tipoSolucion;
    }

    public function getIdUsuarioCreacion() {
        return $this->idUsuarioCreacion;
    }

    public function getIdUsuarioModificacion() {
        return $this->idUsuarioModificacion;
    }

    public function getFechaCreacion() {
        return $this->fechaCreacion;
    }

    public function getFechaModificacion() {
        return $this->fechaModificacion;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function setIdTipoSolucion($idTipoSolucion) {
        $this->idTipoSolucion = $idTipoSolucion;
    }

    public function setTipoSolucion($tipoSolucion) {
        $this->tipoSolucion = $tipoSolucion;
    }

    public function setIdUsuarioCreacion($idUsuarioCreacion) {
        $this->idUsuarioCreacion = $idUsuarioCreacion;
    }

    public function setIdUsuarioModificacion($idUsuarioModificacion) {
        $this->idUsuarioModificacion = $idUsuarioModificacion;
    }

    public function setFechaCreacion($fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;
    }

    public function setFechaModificacion($fechaModificacion) {
        $this->fechaModificacion = $fechaModificacion;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }


}
