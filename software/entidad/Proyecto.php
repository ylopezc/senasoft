<?php
namespace entidad;
class Proyecto {
    private $idProyecto;
    private $proyecto;
    private $descripcion;
    private $url;
    private $sistemaRelacionado;
    private $ubicacion;

    private $urlVideo;

    private $tiempoProduccion;

    private $procesoImpacto;

    private $tipoSolucion;

    private $motorBaseDatos;

    private $lenguajeProgramacion;

    private $tipoLicenciamiento;

    private $derechosAutor;

    private $nroImplementaciones;

    private $aprendiz;

    private $centroFormacion;

    private $cartaImplementacion;

    private $nroImplementacionFuturo;

    private $desarrolloFuturo;

    private $equipoDesarrollo;
    
    private $informacionAdicional;
    
    private $idUsuarioCreacion;

    private $idUsuarioModificacion;

    private $fechaCreacion;

    private $fechaModificacion;

    private $estado;
    
    public function getIdProyecto() {
        return $this->idProyecto;
    }

    public function getProyecto() {
        return $this->proyecto;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getUrl() {
        return $this->url;
    }

    public function getSistemaRelacionado() {
        return $this->sistemaRelacionado;
    }

    public function getUbicacion() {
        return $this->ubicacion;
    }

    public function getUrlVideo() {
        return $this->urlVideo;
    }

    public function getTiempoProduccion() {
        return $this->tiempoProduccion;
    }

    public function getProcesoImpacto() {
        return $this->procesoImpacto;
    }

    public function getTipoSolucion() {
        return $this->tipoSolucion;
    }

    public function getMotorBaseDatos() {
        return $this->motorBaseDatos;
    }

    public function getLenguajeProgramacion() {
        return $this->lenguajeProgramacion;
    }

    public function getTipoLicenciamiento() {
        return $this->tipoLicenciamiento;
    }

    public function getDerechosAutor() {
        return $this->derechosAutor;
    }

    public function getNroImplementaciones() {
        return $this->nroImplementaciones;
    }

    public function getAprendiz() {
        return $this->aprendiz;
    }

    public function getCentroFormacion() {
        return $this->centroFormacion;
    }

    public function getCartaImplementacion() {
        return $this->cartaImplementacion;
    }

    public function getNroImplementacionFuturo() {
        return $this->nroImplementacionFuturo;
    }

    public function getDesarrolloFuturo() {
        return $this->desarrolloFuturo;
    }

    public function getEquipoDesarrollo() {
        return $this->equipoDesarrollo;
    }

    public function getInformacionAdicional() {
        return $this->informacionAdicional;
    }

    public function getIdUsuarioCreacion() {
        return $this->idUsuarioCreacion;
    }

    public function getIdUsuarioModificacion() {
        return $this->idUsuarioModificacion;
    }

    public function getFechaCreacion() {
        return $this->fechaCreacion;
    }

    public function getFechaModificacion() {
        return $this->fechaModificacion;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function setIdProyecto($idProyecto) {
        $this->idProyecto = $idProyecto;
    }

    public function setProyecto($proyecto) {
        $this->proyecto = $proyecto;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function setSistemaRelacionado($sistemaRelacionado) {
        $this->sistemaRelacionado = $sistemaRelacionado;
    }

    public function setUbicacion($ubicacion) {
        $this->ubicacion = $ubicacion;
    }

    public function setUrlVideo($urlVideo) {
        $this->urlVideo = $urlVideo;
    }

    public function setTiempoProduccion($tiempoProduccion) {
        $this->tiempoProduccion = $tiempoProduccion;
    }

    public function setProcesoImpacto($procesoImpacto) {
        $this->procesoImpacto = $procesoImpacto;
    }

    public function setTipoSolucion($tipoSolucion) {
        $this->tipoSolucion = $tipoSolucion;
    }

    public function setMotorBaseDatos($motorBaseDatos) {
        $this->motorBaseDatos = $motorBaseDatos;
    }

    public function setLenguajeProgramacion($lenguajeProgramacion) {
        $this->lenguajeProgramacion = $lenguajeProgramacion;
    }

    public function setTipoLicenciamiento($tipoLicenciamiento) {
        $this->tipoLicenciamiento = $tipoLicenciamiento;
    }

    public function setDerechosAutor($derechosAutor) {
        $this->derechosAutor = $derechosAutor;
    }

    public function setNroImplementaciones($nroImplementaciones) {
        $this->nroImplementaciones = $nroImplementaciones;
    }

    public function setAprendiz($aprendiz) {
        $this->aprendiz = $aprendiz;
    }

    public function setCentroFormacion($centroFormacion) {
        $this->centroFormacion = $centroFormacion;
    }

    public function setCartaImplementacion($cartaImplementacion) {
        $this->cartaImplementacion = $cartaImplementacion;
    }

    public function setNroImplementacionFuturo($nroImplementacionFuturo) {
        $this->nroImplementacionFuturo = $nroImplementacionFuturo;
    }

    public function setDesarrolloFuturo($desarrolloFuturo) {
        $this->desarrolloFuturo = $desarrolloFuturo;
    }

    public function setEquipoDesarrollo($equipoDesarrollo) {
        $this->equipoDesarrollo = $equipoDesarrollo;
    }

    public function setInformacionAdicional($informacionAdicional) {
        $this->informacionAdicional = $informacionAdicional;
    }

    public function setIdUsuarioCreacion($idUsuarioCreacion) {
        $this->idUsuarioCreacion = $idUsuarioCreacion;
    }

    public function setIdUsuarioModificacion($idUsuarioModificacion) {
        $this->idUsuarioModificacion = $idUsuarioModificacion;
    }

    public function setFechaCreacion($fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;
    }

    public function setFechaModificacion($fechaModificacion) {
        $this->fechaModificacion = $fechaModificacion;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }


}
