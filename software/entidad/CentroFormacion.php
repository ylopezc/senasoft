<?php
namespace entidad;
class CentroFormacion{
	private $idCentroFormacion;
	private $centroFormacion;
	private $regional;
	private $codigo;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	private $estado;
	
	 
	public function getEstado()
	{
		return $this->estado;
	}
	
	public function setEstado($estado)
	{
		$this->estado = $estado;
	}
	
	
	public function getFechaModificacion()
	{
		return $this->fechaModificacion;
	}
	
	public function setFechaModificacion($fechaModificacion)
	{
		$this->fechaModificacion = $fechaModificacion;
	}
	
	
	public function getFechaCreacion()
	{
		return $this->fechaCreacion;
	}
	
	public function setFechaCreacion($fechaCreacion)
	{
		$this->fechaCreacion = $fechaCreacion;
	}
	
	public function getIdUsuarioModificacion()
	{
		return $this->idUsuarioModificacion;
	}
	
	public function setIdUsuarioModificacion($idUsuarioModificacion)
	{
		$this->idUsuarioModificacion = $idUsuarioModificacion;
	}
	
	
	public function getIdUsuarioCreacion()
	{
		return $this->idUsuarioCreacion;
	}
	
	public function setIdUsuarioCreacion($idUsuarioCreacion)
	{
		$this->idUsuarioCreacion = $idUsuarioCreacion;
	}
	
	    
	public function getCodigo() 
	{
	  return $this->codigo;
	}
	
	public function setCodigo($codigo) 
	{
	  $this->codigo = $codigo;
	}
	
	    
	public function getRegional() 
	{
	  return $this->regional;
	}
	
	public function setRegional(\entidad\Regional $regional) 
	{
	  $this->regional = $regional;
	}
	    
	public function getCentroFormacion() 
	{
	  return $this->centroFormacion;
	}
	
	public function setCentroFormacion($centroFormacion) 
	{
	  $this->centroFormacion = $centroFormacion;
	}
	
	    
	public function getIdCentroFormacion() 
	{
	  return $this->idCentroFormacion;
	}
	
	public function setIdCentroFormacion($idCentroFormacion) 
	{
	  $this->idCentroFormacion = $idCentroFormacion;
	}
	
}
?>