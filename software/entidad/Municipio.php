<?php
namespace entidad;
class Municipio {
    private $idMunicipio;
    private $referencia;
    private $municipio;
    private $idDepartamento;
    private $estado;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    
    
    
    public function getIdMunicipio(){
        return $this->idMunicipio;
    }
    
    public function setIdMunicipio($idMunicipio){
         $this->idMunicipio = $idMunicipio;
    }
    
     public function getReferencia(){
        return $this->referencia;
    }
    
    public function setReferencia($referencia){
         $this->referencia = $referencia;
    }
    
    public function getMunicipio(){
        return $this->municipio;
    }
    
    public function setMunicipio($municipio){
         $this->municipio = $municipio;
    }
    
    public function getIdDepartamento(){
        return $this->idDepartamento;
    }
    
    public function setIdDepartamento($idDepartamento){
         $this->idDepartamento = $idDepartamento;
    }
    
    public function getEstado() {
           return $this->estado;
    }

    public function setEstado($estado) {
       $this->estado = $estado;
    }

    public function getIdUsuarioCreacion() {
       return $this->idUsuarioCreacion;
    }

    public function setIdUsuarioCreacion($idUsuarioCreacion) {
        $this->idUsuarioCreacion = $idUsuarioCreacion;
    }

    public function getIdUsuarioModificacion() {
       return $this->idUsuarioModificacion;
    }

    public function setIdUsuarioModificacion($idUsuarioModificacion) {
        $this->idUsuarioModificacion = $idUsuarioModificacion;
    }

    public function getFechaCreacion() {
       return $this->fechaCreacion;
    }

    public function setFechaCreacion($fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;
    }

    public function getFechaModificacion() {
       return $this->fechaModificacion;
    }

    public function setFechaModificacion($fechaModificacion) {
        $this->fechaModificacion = $fechaModificacion;
    }
   
}

?>
