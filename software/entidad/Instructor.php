<?php
namespace  entidad;
class Instructor{
	private $idInstructor;
	private $persona;
	private $centroFormacion;
	private $estado;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	
	public function getEstado()
	{
		return $this->estado;
	}
	
	public function setEstado($estado)
	{
		$this->estado = $estado;
	}
	
	public function getFechaModificacion()
	{
		return $this->fechaModificacion;
	}
	
	public function setFechaModificacion($fechaModificacion)
	{
		$this->fechaModificacion = $fechaModificacion;
	}
	
	
	public function getFechaCreacion()
	{
		return $this->fechaCreacion;
	}
	
	public function setFechaCreacion($fechaCreacion)
	{
		$this->fechaCreacion = $fechaCreacion;
	}
	
	public function getIdUsuarioModificacion()
	{
		return $this->idUsuarioModificacion;
	}
	
	public function setIdUsuarioModificacion($idUsuarioModificacion)
	{
		$this->idUsuarioModificacion = $idUsuarioModificacion;
	}
	
	
	public function getIdUsuarioCreacion()
	{
		return $this->idUsuarioCreacion;
	}
	
	public function setIdUsuarioCreacion($idUsuarioCreacion)
	{
		$this->idUsuarioCreacion = $idUsuarioCreacion;
	} 
	public function getCentroFormacion() 
	{
	  return $this->centroFormacion;
	}
	
	public function setCentroFormacion(\entidad\CentroFormacion $centroFormacion) 
	{
	  $this->centroFormacion = $centroFormacion;
	}
	
	    
	public function getPersona() 
	{
	  return $this->persona;
	}
	
	public function setPersona(\entidad\Persona $persona) 
	{
	  $this->persona = $persona;
	}
	
	    
	public function getIdInstructor() 
	{
	  return $this->idInstructor;
	}
	
	public function setIdInstructor($idInstructor) 
	{
	  $this->idInstructor = $idInstructor;
	}	

}
?>