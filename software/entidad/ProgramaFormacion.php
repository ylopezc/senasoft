<?php
namespace  entidad;
class ProgramaFormacion{
	private $idProgramaFormacion;
	private $nivelProgramaFormacion;
	private $programaFormacion;
	private $estado;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	
	public function getEstado()
	{
		return $this->estado;
	}
	
	public function setEstado($estado)
	{
		$this->estado = $estado;
	}
	public function getFechaModificacion()
	{
		return $this->fechaModificacion;
	}
	
	public function setFechaModificacion($fechaModificacion)
	{
		$this->fechaModificacion = $fechaModificacion;
	}
	
	 
	public function getFechaCreacion()
	{
		return $this->fechaCreacion;
	}
	
	public function setFechaCreacion($fechaCreacion)
	{
		$this->fechaCreacion = $fechaCreacion;
	}
	
	public function getIdUsuarioModificacion()
	{
		return $this->idUsuarioModificacion;
	}
	
	public function setIdUsuarioModificacion($idUsuarioModificacion)
	{
		$this->idUsuarioModificacion = $idUsuarioModificacion;
	}
	
	 
	public function getIdUsuarioCreacion()
	{
		return $this->idUsuarioCreacion;
	}
	
	public function setIdUsuarioCreacion($idUsuarioCreacion)
	{
		$this->idUsuarioCreacion = $idUsuarioCreacion;
	}
	    
	public function getProgramaFormacion() 
	{
	  return $this->programaFormacion;
	}
	
	public function setProgramaFormacion($programaFormacion) 
	{
	  $this->programaFormacion = $programaFormacion;
	}
	
	    
	public function getNivelProgramaFormacion() 
	{
	  return $this->nivelProgramaFormacion;
	}
	
	public function setNivelProgramaFormacion(\entidad\NivelProgramaFormacion $nivelProgramaFormacion) 
	{
	  $this->nivelProgramaFormacion = $nivelProgramaFormacion;
	}
	    
	public function getIdProgramaFormacion() 
	{
	  return $this->idProgramaFormacion;
	}
	
	public function setIdProgramaFormacion($idProgramaFormacion) 
	{
	  $this->idProgramaFormacion = $idProgramaFormacion;
	}
	
}
?>