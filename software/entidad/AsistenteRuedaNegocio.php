<?php
namespace  entidad;
class AsistenteRuedaNegocio{
	private $idAsistenteRuedaNegocio;
	private $asistenteRuedaNegocio;
	private $cargo;
	private $correoElectronico;
	private $telefono;
	private $horaAsistencia;
	private $idEmpresa;
	private $estado;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	    
	public function getIdEmpresa() 
	{
	  return $this->idEmpresa;
	}
	
	public function setIdEmpresa($idEmpresa) 
	{
	  $this->idEmpresa = $idEmpresa;
	}
		 
	public function getIdUsuarioCreacion() {
		return $this->idUsuarioCreacion;
	}
	
	public function setIdUsuarioCreacion($idUsuarioCreacion) {
		$this->idUsuarioCreacion = $idUsuarioCreacion;
	}
	
	public function getIdUsuarioModificacion() {
		return $this->idUsuarioModificacion;
	}
	
	public function setIdUsuarioModificacion($idUsuarioModificacion) {
		$this->idUsuarioModificacion = $idUsuarioModificacion;
	}
	
	public function getFechaCreacion() {
		return $this->fechaCreacion;
	}
	
	public function setFechaCreacion($fechaCreacion) {
		$this->fechaCreacion = $fechaCreacion;
	}
	
	public function getFechaModificacion() {
		return $this->fechaModificacion;
	}
	
	public function setFechaModificacion($fechaModificacion) {
		$this->fechaModificacion = $fechaModificacion;
	}
	
	public function getEstado() 
	{
	  return $this->estado;
	}
	
	public function setEstado($estado) 
	{
	  $this->estado = $estado;
	}
	
	    
	public function getHoraAsistencia() 
	{
	  return $this->horaAsistencia;
	}
	public function setHoraAsistencia($horaAsistencia) 
	{
	  $this->horaAsistencia = $horaAsistencia;
	}
	
	    
	public function getTelefono() 
	{
	  return $this->telefono;
	}
	
	public function setTelefono($telefono) 
	{
	  $this->telefono = $telefono;
	}
	
	    
	public function getCorreoElectronico() 
	{
	  return $this->correoElectronico;
	}
	
	public function setCorreoElectronico($correoElectronico) 
	{
	  $this->correoElectronico = $correoElectronico;
	}
	    
	public function getCargo() 
	{
	  return $this->cargo;
	}
	
	public function setCargo($cargo) 
	{
	  $this->cargo = $cargo;
	}
	    
	public function getAsistenteRuedaNegocio() 
	{
	  return $this->asistenteRuedaNegocio;
	}
	
	public function setAsistenteRuedaNegocio($asistenteRuedaNegocio) 
	{
	  $this->asistenteRuedaNegocio = $asistenteRuedaNegocio;
	}
	    
	public function getIdAsistenteRuedaNegocio() 
	{
	  return $this->idAsistenteRuedaNegocio;
	}
	
	public function setIdAsistenteRuedaNegocio($idAsistenteRuedaNegocio) 
	{
	  $this->idAsistenteRuedaNegocio = $idAsistenteRuedaNegocio;
	}
}
?>