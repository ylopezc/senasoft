<?php
namespace entidad;
class MotorBaseDatos {
    private $idMotorBaseDatos;
    private $motorBaseDatos;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    private $estado;
    
    public function getIdMotorBaseDatos() {
        return $this->idMotorBaseDatos;
    }

    public function getMotorBaseDatos() {
        return $this->motorBaseDatos;
    }

    public function getIdUsuarioCreacion() {
        return $this->idUsuarioCreacion;
    }

    public function getIdUsuarioModificacion() {
        return $this->idUsuarioModificacion;
    }

    public function getFechaCreacion() {
        return $this->fechaCreacion;
    }

    public function getFechaModificacion() {
        return $this->fechaModificacion;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function setIdMotorBaseDatos($idMotorBaseDatos) {
        $this->idMotorBaseDatos = $idMotorBaseDatos;
    }

    public function setMotorBaseDatos($motorBaseDatos) {
        $this->motorBaseDatos = $motorBaseDatos;
    }

    public function setIdUsuarioCreacion($idUsuarioCreacion) {
        $this->idUsuarioCreacion = $idUsuarioCreacion;
    }

    public function setIdUsuarioModificacion($idUsuarioModificacion) {
        $this->idUsuarioModificacion = $idUsuarioModificacion;
    }

    public function setFechaCreacion($fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;
    }

    public function setFechaModificacion($fechaModificacion) {
        $this->fechaModificacion = $fechaModificacion;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }


}
