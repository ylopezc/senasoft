<?php

namespace entidad;
class Departamento {
    private $idDepartamento;
    private $departamento;
    private $referencia;
    private $estado;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    
    
    public function getIdDepartamento(){
        return $this->idDepartamento;
    }
    
    public function setIdDepartamento($idDepartamento){
         $this->idDepartamento = $idDepartamento;
    }
    
     public function getDepartamento(){
        return $this->departamento;
    }
    
    public function setDepartamento($departamento){
         $this->departamento = $departamento;
    } 
    
    public function getReferencia(){
        return $this->referencia;
    }
    
    public function setReferencia($referencia){
         $this->referencia = $referencia;
    }
    
    public function getEstado() {
           return $this->estado;
    }

    public function setEstado($estado) {
       $this->estado = $estado;
    }

    public function getIdUsuarioCreacion() {
       return $this->idUsuarioCreacion;
    }

    public function setIdUsuarioCreacion($idUsuarioCreacion) {
        $this->idUsuarioCreacion = $idUsuarioCreacion;
    }

    public function getIdUsuarioModificacion() {
       return $this->idUsuarioModificacion;
    }

    public function setIdUsuarioModificacion($idUsuarioModificacion) {
        $this->idUsuarioModificacion = $idUsuarioModificacion;
    }

    public function getFechaCreacion() {
       return $this->fechaCreacion;
    }

    public function setFechaCreacion($fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;
    }

    public function getFechaModificacion() {
       return $this->fechaModificacion;
    }

    public function setFechaModificacion($fechaModificacion) {
        $this->fechaModificacion = $fechaModificacion;
    
    }
}

?>
