<?php
namespace entidad;
class MensajeContacto {
    private $idMensajeContacto;
    private $remitente;
    private $mensajeContacto;
    private $rol;
    private $asunto;
    private $centroFormacion;
    private $correoElectronico;
    private $estado;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    
    public function getCentroFormacion() {
        return $this->centroFormacion;
    }

    public function setCentroFormacion(\entidad\CentroFormacion $centroFormacion) {
        $this->centroFormacion = $centroFormacion;
    }

        
    public function getIdMensajeContacto() {
        return $this->idMensajeContacto;
    }

    public function getRemitente() {
        return $this->remitente;
    }

    public function getMensajeContacto() {
        return $this->mensajeContacto;
    }

    public function getRol() {
        return $this->rol;
    }

    public function getAsunto() {
        return $this->asunto;
    }

    public function getCorreoElectronico() {
        return $this->correoElectronico;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function getIdUsuarioCreacion() {
        return $this->idUsuarioCreacion;
    }

    public function getIdUsuarioModificacion() {
        return $this->idUsuarioModificacion;
    }

    public function getFechaCreacion() {
        return $this->fechaCreacion;
    }

    public function getFechaModificacion() {
        return $this->fechaModificacion;
    }

    public function setIdMensajeContacto($idMensajeContacto) {
        $this->idMensajeContacto = $idMensajeContacto;
    }

    public function setRemitente($remitente) {
        $this->remitente = $remitente;
    }

    public function setMensajeContacto($mensajeContacto) {
        $this->mensajeContacto = $mensajeContacto;
    }

    public function setRol($rol) {
        $this->rol = $rol;
    }

    public function setAsunto($asunto) {
        $this->asunto = $asunto;
    }

    public function setCorreoElectronico($correoElectronico) {
        $this->correoElectronico = $correoElectronico;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }

    public function setIdUsuarioCreacion($idUsuarioCreacion) {
        $this->idUsuarioCreacion = $idUsuarioCreacion;
    }

    public function setIdUsuarioModificacion($idUsuarioModificacion) {
        $this->idUsuarioModificacion = $idUsuarioModificacion;
    }

    public function setFechaCreacion($fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;
    }

    public function setFechaModificacion($fechaModificacion) {
        $this->fechaModificacion = $fechaModificacion;
    }


}
?>
