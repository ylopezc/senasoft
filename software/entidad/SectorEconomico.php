<?php
namespace  entidad;
class SectorEconomico {
	private $idSectorEconomico;
	private $sectorEconomico;
	private $estado;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	
	
	public function getIdUsuarioCreacion() {
		return $this->idUsuarioCreacion;
	}
	
	public function setIdUsuarioCreacion($idUsuarioCreacion) {
		$this->idUsuarioCreacion = $idUsuarioCreacion;
	}
	
	public function getIdUsuarioModificacion() {
		return $this->idUsuarioModificacion;
	}
	
	public function setIdUsuarioModificacion($idUsuarioModificacion) {
		$this->idUsuarioModificacion = $idUsuarioModificacion;
	}
	
	public function getFechaCreacion() {
		return $this->fechaCreacion;
	}
	
	public function setFechaCreacion($fechaCreacion) {
		$this->fechaCreacion = $fechaCreacion;
	}
	
	public function getFechaModificacion() {
		return $this->fechaModificacion;
	}
	
	public function setFechaModificacion($fechaModificacion) {
		$this->fechaModificacion = $fechaModificacion;
	}
	
	    
	public function getEstado() 
	{
	  return $this->estado;
	}
	
	public function setEstado($estado) 
	{
	  $this->estado = $estado;
	}
	
	    
	public function getSectorEconomico() 
	{
	  return $this->sectorEconomico;
	}
	
	public function setSectorEconomico($sectorEconomico) 
	{
	  $this->sectorEconomico = $sectorEconomico;
	}
	
	    
	public function getIdSectorEconomico() 
	{
	  return $this->idSectorEconomico;
	}
	
	public function setIdSectorEconomico($idSectorEconomico) 
	{
	  $this->idSectorEconomico = $idSectorEconomico;
	}
}
?>