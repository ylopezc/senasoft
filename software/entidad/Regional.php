<?php
namespace entidad;
class Regional{
	private $idRegional;
	private $regional;
	private $estado;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	
	
	public function getFechaModificacion()
	{
		return $this->fechaModificacion;
	}
	
	public function setFechaModificacion($fechaModificacion)
	{
		$this->fechaModificacion = $fechaModificacion;
	}
	
	
	public function getFechaCreacion()
	{
		return $this->fechaCreacion;
	}
	
	public function setFechaCreacion($fechaCreacion)
	{
		$this->fechaCreacion = $fechaCreacion;
	}
	
	public function getIdUsuarioModificacion()
	{
		return $this->idUsuarioModificacion;
	}
	
	public function setIdUsuarioModificacion($idUsuarioModificacion)
	{
		$this->idUsuarioModificacion = $idUsuarioModificacion;
	}
	
	
	public function getIdUsuarioCreacion()
	{
		return $this->idUsuarioCreacion;
	}
	
	public function setIdUsuarioCreacion($idUsuarioCreacion)
	{
		$this->idUsuarioCreacion = $idUsuarioCreacion;
	}
	
	    
	public function getRegional() 
	{
	  return $this->regional;
	}
	
	public function setRegional($regional) 
	{
	  $this->regional = $regional;
	}
	    
	public function getIdRegional() 
	{
	  return $this->idRegional;
	}
	
	public function setIdRegional($idRegional) 
	{
	  $this->idRegional = $idRegional;
	}
	
}
?>