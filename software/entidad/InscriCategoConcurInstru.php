<?php
namespace entidad;
class InscriCategoConcurInstru{
	
	private $idInscriCategoConcurInstru;
	private $categoriaConcurso;
	private $centroFormacion;
	private $instructor;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	private $estado;
	
	 
	public function getEstado()
	{
		return $this->estado;
	}
	
	public function setEstado($estado)
	{
		$this->estado = $estado;
	}
	
	
	public function getFechaModificacion()
	{
		return $this->fechaModificacion;
	}
	
	public function setFechaModificacion($fechaModificacion)
	{
		$this->fechaModificacion = $fechaModificacion;
	}
	
	
	public function getFechaCreacion()
	{
		return $this->fechaCreacion;
	}
	
	public function setFechaCreacion($fechaCreacion)
	{
		$this->fechaCreacion = $fechaCreacion;
	}
	
	public function getIdUsuarioModificacion()
	{
		return $this->idUsuarioModificacion;
	}
	
	public function setIdUsuarioModificacion($idUsuarioModificacion)
	{
		$this->idUsuarioModificacion = $idUsuarioModificacion;
	}
	
	
	public function getIdUsuarioCreacion()
	{
		return $this->idUsuarioCreacion;
	}
	
	public function setIdUsuarioCreacion($idUsuarioCreacion)
	{
		$this->idUsuarioCreacion = $idUsuarioCreacion;
	}
	
	    
	public function getInstructor() 
	{
	  return $this->instructor;
	}
	
	public function setInstructor(\entidad\Instructor $instructor) 
	{
	  $this->instructor = $instructor;
	}    
	
	public function getCategoriaConcurso()
	{
		return $this->categoriaConcurso;
	}
	
	public function setCategoriaConcurso(\entidad\CategoriaConcurso $categoriaConcurso)
	{
		$this->categoriaConcurso = $categoriaConcurso;
	}
	
	 
	public function getCentroFormacion()
	{
		return $this->centroFormacion;
	}
	
	public function setCentroFormacion(\entidad\CentroFormacion $centroFormacion)
	{
		$this->centroFormacion = $centroFormacion;
	}
	public function getIdInscriCategoConcurInstru() 
	{
	  return $this->idInscriCategoConcurInstru;
	}
	
	public function setIdInscriCategoConcurInstru($idInscriCategoConcurInstru) 
	{
	  $this->idInscriCategoConcurInstru = $idInscriCategoConcurInstru;
	}
}
?>