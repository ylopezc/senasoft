<?php
namespace  entidad;
class InscriCategoConcurAprend{
	
	private $idInscriCategoConcurAprend;
	private $programaFormacion;
	private $centroFormacion;
	private $categoriaConcurso;
	private $aprendiz;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	private $estado;
	
	 
	public function getEstado()
	{
		return $this->estado;
	}
	
	public function setEstado($estado)
	{
		$this->estado = $estado;
	}
	
	
	public function getFechaModificacion()
	{
		return $this->fechaModificacion;
	}
	
	public function setFechaModificacion($fechaModificacion)
	{
		$this->fechaModificacion = $fechaModificacion;
	}
	
	
	public function getFechaCreacion()
	{
		return $this->fechaCreacion;
	}
	
	public function setFechaCreacion($fechaCreacion)
	{
		$this->fechaCreacion = $fechaCreacion;
	}
	
	public function getIdUsuarioModificacion()
	{
		return $this->idUsuarioModificacion;
	}
	
	public function setIdUsuarioModificacion($idUsuarioModificacion)
	{
		$this->idUsuarioModificacion = $idUsuarioModificacion;
	}
	
	
	public function getIdUsuarioCreacion()
	{
		return $this->idUsuarioCreacion;
	}
	
	public function setIdUsuarioCreacion($idUsuarioCreacion)
	{
		$this->idUsuarioCreacion = $idUsuarioCreacion;
	}
	
	    
	public function getAprendiz() 
	{
	  return $this->aprendiz;
	}
	
	public function setAprendiz(\entidad\Aprendiz $aprendiz) 
	{
	  $this->aprendiz = $aprendiz;
	}
	    
	public function getCategoriaConcurso() 
	{
	  return $this->categoriaConcurso;
	}
	
	public function setCategoriaConcurso(\entidad\CategoriaConcurso $categoriaConcurso) 
	{
	  $this->categoriaConcurso = $categoriaConcurso;
	}
	
	    
	public function getCentroFormacion() 
	{
	  return $this->centroFormacion;
	}
	
	public function setCentroFormacion(\entidad\CentroFormacion $centroFormacion) 
	{
	  $this->centroFormacion = $centroFormacion;
	}
	
	    
	public function getProgramaFormacion() 
	{
	  return $this->programaFormacion;
	}
	
	public function setProgramaFormacion(\entidad\ProgramaFormacion $programaFormacion) 
	{
	  $this->programaFormacion = $programaFormacion;
	}
	    
	public function getIdInscriCategoConcurAprend() 
	{
	  return $this->idInscriCategoConcurAprend;
	}
	
	public function setIdInscriCategoConcurAprend($idInscriCategoConcurAprend) 
	{
	  $this->idInscriCategoConcurAprend = $idInscriCategoConcurAprend;
	}
}
?>