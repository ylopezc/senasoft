<?php
namespace entidad;
class NivelProgramaFormacion{
	
	private $idNivelProgramaFormacion;
	private $nivelProgramaFormacion;
	private $estado;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	
	    
	public function getFechaModificacion() 
	{
	  return $this->fechaModificacion;
	}
	
	public function setFechaModificacion($fechaModificacion) 
	{
	  $this->fechaModificacion = $fechaModificacion;
	}
	
	    
	public function getFechaCreacion() 
	{
	  return $this->fechaCreacion;
	}
	
	public function setFechaCreacion($fechaCreacion) 
	{
	  $this->fechaCreacion = $fechaCreacion;
	}
		    
	public function getIdUsuarioModificacion() 
	{
	  return $this->idUsuarioModificacion;
	}
	
	public function setIdUsuarioModificacion($idUsuarioModificacion) 
	{
	  $this->idUsuarioModificacion = $idUsuarioModificacion;
	}
	
	    
	public function getIdUsuarioCreacion() 
	{
	  return $this->idUsuarioCreacion;
	}
	
	public function setIdUsuarioCreacion($idUsuarioCreacion) 
	{
	  $this->idUsuarioCreacion = $idUsuarioCreacion;
	}
	
	    
	public function getEstado() 
	{
	  return $this->estado;
	}
	
	public function setEstado($estado) 
	{
	  $this->estado = $estado;
	}
	
	    
	public function getNivelProgramaFormacion() 
	{
	  return $this->nivelProgramaFormacion;
	}
	
	public function setNivelProgramaFormacion($nivelProgramaFormacion) 
	{
	  $this->nivelProgramaFormacion = $nivelProgramaFormacion;
	}
	
	    
	public function getIdNivelProgramaFormacion() 
	{
	  return $this->idNivelProgramaFormacion;
	}
	
	public function setIdNivelProgramaFormacion($idNivelProgramaFormacion) 
	{
	  $this->idNivelProgramaFormacion = $idNivelProgramaFormacion;
	}
}
?>