<?php
namespace entidad;
class TipoDocumentoIdentidad{
   private $idTipoDocumentoIdentidad;
   private $tipoDocumentoIdentidad;
   private $codigo;
   private $estado;
   private $idUsuarioCreacion;
   private $idUsuarioModificacion;
   private $fechaCreacion;
   private $fechaModificacion;
   
   public function getEstado() {
       return $this->estado;
   }

   public function setEstado($estado) {
       $this->estado = $estado;
   }   
   public function getIdTipoDocumentoIdentidad() {
       return $this->idTipoDocumentoIdentidad;
   }

   public function setIdTipoDocumentoIdentidad($idTipoDocumentoIdentidad) {
       $this->idTipoDocumentoIdentidad = $idTipoDocumentoIdentidad;
   }

   public function getTipoDocumentoIdentidad() {
       return $this->tipoDocumentoIdentidad;
   }

   public function setTipoDocumentoIdentidad($tipoDocumentoIdentidad) {
       $this->tipoDocumentoIdentidad = $tipoDocumentoIdentidad;
   }

   public function getCodigo() {
       return $this->codigo;
   }

   public function setCodigo($codigo) {
       $this->codigo = $codigo;
   }

   public function getIdUsuarioCreacion() {
       return $this->idUsuarioCreacion;
   }

   public function setIdUsuarioCreacion($idUsuarioCreacion) {
       $this->idUsuarioCreacion = $idUsuarioCreacion;
   }

   public function getIdUsuarioModificacion() {
       return $this->idUsuarioModificacion;
   }

   public function setIdUsuarioModificacion($idUsuarioModificacion) {
       $this->idUsuarioModificacion = $idUsuarioModificacion;
   }

   public function getFechaCreacion() {
       return $this->fechaCreacion;
   }

   public function setFechaCreacion($fechaCreacion) {
       $this->fechaCreacion = $fechaCreacion;
   }

   public function getFechaModificacion() {
       return $this->fechaModificacion;
   }

   public function setFechaModificacion($fechaModificacion) {
       $this->fechaModificacion = $fechaModificacion;
   }   
   
}
?>
