<?php
namespace  entidad;
class PreguntaFrecuente{
	private $idPreguntaFrecuente;
	private $preguntaFrecuente;
	private $respuesta;
	private $estado;
	private $fechaCreacion;
	private $fechaModificacion;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	
	    
	public function getIdUsuarioModificacion() 
	{
	  return $this->idUsuarioModificacion;
	}
	
	public function setIdUsuarioModificacion($idUsuarioModificacion) 
	{
	  $this->idUsuarioModificacion = $idUsuarioModificacion;
	}
	
	    
	public function getIdUsuarioCreacion() 
	{
	  return $this->idUsuarioCreacion;
	}
	
	public function setIdUsuarioCreacion($idUsuarioCreacion) 
	{
	  $this->idUsuarioCreacion = $idUsuarioCreacion;
	}
	    
	public function getFechaModificacion() 
	{
	  return $this->fechaModificacion;
	}
	
	public function setFechaModificacion($fechaModificacion) 
	{
	  $this->fechaModificacion = $fechaModificacion;
	}
	
	    
	public function getFechaCreacion() 
	{
	  return $this->fechaCreacion;
	}
	
	public function setFechaCreacion($fechaCreacion) 
	{
	  $this->fechaCreacion = $fechaCreacion;
	}
	
	    
	public function getEstado() 
	{
	  return $this->estado;
	}
	
	public function setEstado($estado) 
	{
	  $this->estado = $estado;
	}
	
	    
	public function getRespuesta() 
	{
	  return $this->respuesta;
	}
	
	public function setRespuesta($respuesta) 
	{
	  $this->respuesta = $respuesta;
	}
	    
	public function getPreguntaFrecuente() 
	{
	  return $this->preguntaFrecuente;
	}
	
	public function setPreguntaFrecuente($preguntaFrecuente) 
	{
	  $this->preguntaFrecuente = $preguntaFrecuente;
	}
	
	    
	public function getIdPreguntaFrecuente() 
	{
	  return $this->idPreguntaFrecuente;
	}
	
	public function setIdPreguntaFrecuente($idPreguntaFrecuente) 
	{
	  $this->idPreguntaFrecuente = $idPreguntaFrecuente;
	}	
}
?>