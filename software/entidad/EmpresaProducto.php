<?php
namespace entidad;
class EmpresaProducto {
    private $idEmpresaProducto;
    private $empresaProducto;
    private $idEmpresa;
    private $idCategoriaProducto;
    private $estado;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    private $idTipoEmpresaProducto;
    private $frecuencia;
    
        
    public function getFrecuencia() 
    {
      return $this->frecuencia;
    }
    
    public function setFrecuencia($frecuencia) 
    {
      $this->frecuencia = $frecuencia;
    }
       
    public function getIdTipoEmpresaProducto() 
    {
      return $this->idTipoEmpresaProducto;
    }
    
    public function setIdTipoEmpresaProducto($idTipoEmpresaProducto) 
    {
      $this->idTipoEmpresaProducto = $idTipoEmpresaProducto;
    }
    
    
    public function getIdEmpresaProducto(){
        return $this->idEmpresaProducto;
    }
    
    public function setIdEmpresaProducto($idEmpresaProducto){
         $this->idEmpresaProducto = $idEmpresaProducto;
    }
    
    public function getEmpresaProducto(){
        return $this->empresaProducto;
    }
    
    public function setEmpresaProducto($empresaProducto){
         $this->empresaProducto = $empresaProducto;
    }
    
     public function getIdEmpresa(){
        return $this->idEmpresa;
    }
    
    public function setIdEmpresa($idEmpresa){
         $this->idEmpresa = $idEmpresa;
    }
    
    public function getIdCategoriaProducto(){
        return $this->idCategoriaProducto;
    }
    
    public function setIdCategoriaProducto($idCategoriaProducto){
         $this->idCategoriaProducto = $idCategoriaProducto;
    }
    public function getEstado() {
           return $this->estado;
    }

    public function setEstado($estado) {
       $this->estado = $estado;
    }

    public function getIdUsuarioCreacion() {
       return $this->idUsuarioCreacion;
    }

    public function setIdUsuarioCreacion($idUsuarioCreacion) {
        $this->idUsuarioCreacion = $idUsuarioCreacion;
    }

    public function getIdUsuarioModificacion() {
       return $this->idUsuarioModificacion;
    }

    public function setIdUsuarioModificacion($idUsuarioModificacion) {
        $this->idUsuarioModificacion = $idUsuarioModificacion;
    }

    public function getFechaCreacion() {
       return $this->fechaCreacion;
    }

    public function setFechaCreacion($fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;
    }

    public function getFechaModificacion() {
       return $this->fechaModificacion;
    }

    public function setFechaModificacion($fechaModificacion) {
        $this->fechaModificacion = $fechaModificacion;
    }
    
}

?>
