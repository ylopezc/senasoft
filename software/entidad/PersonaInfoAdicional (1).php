<?php
namespace entidad;
class PersonaInfoAdicional{
	
	private $idPersonaInfoAdicional;
	private $persona;
	private $rh;
	private $tallaCamiseta;
	private $cuidadoMedico;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	private $estado;
	
	 
	public function getEstado()
	{
		return $this->estado;
	}
	
	public function setEstado($estado)
	{
		$this->estado = $estado;
	}
	
	
	public function getFechaModificacion()
	{
		return $this->fechaModificacion;
	}
	
	public function setFechaModificacion($fechaModificacion)
	{
		$this->fechaModificacion = $fechaModificacion;
	}
	
	
	public function getFechaCreacion()
	{
		return $this->fechaCreacion;
	}
	
	public function setFechaCreacion($fechaCreacion)
	{
		$this->fechaCreacion = $fechaCreacion;
	}
	
	public function getIdUsuarioModificacion()
	{
		return $this->idUsuarioModificacion;
	}
	
	public function setIdUsuarioModificacion($idUsuarioModificacion)
	{
		$this->idUsuarioModificacion = $idUsuarioModificacion;
	}
	
	
	public function getIdUsuarioCreacion()
	{
		return $this->idUsuarioCreacion;
	}
	
	public function setIdUsuarioCreacion($idUsuarioCreacion)
	{
		$this->idUsuarioCreacion = $idUsuarioCreacion;
	}
	
	    
	public function getCuidadoMedico() 
	{
	  return $this->cuidadoMedico;
	}
	
	public function setCuidadoMedico($cuidadoMedico) 
	{
	  $this->cuidadoMedico = $cuidadoMedico;
	}
	
	    
	public function getTallaCamiseta() 
	{
	  return $this->tallaCamiseta;
	}
	
	public function setTallaCamiseta($tallaCamiseta) 
	{
	  $this->tallaCamiseta = $tallaCamiseta;
	}
	
	    
	public function getRh() 
	{
	  return $this->rh;
	}
	
	public function setRh($rh) 
	{
	  $this->rh = $rh;
	}
	    
	public function getPersona() 
	{
	  return $this->persona;
	}
	
	public function setPersona(\entidad\Persona $persona) 
	{
	  $this->persona = $persona;
	}
	
	    
	public function getIdPersonaInfoAdicional() 
	{
	  return $this->idPersonaInfoAdicional;
	}
	
	public function setIdPersonaInfoAdicional($idPersonaInfoAdicional) 
	{
	  $this->idPersonaInfoAdicional = $idPersonaInfoAdicional;
	}
}
?>