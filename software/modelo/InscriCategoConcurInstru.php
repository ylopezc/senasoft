<?php
namespace  modelo;
session_start();
require_once('../entorno/Conexion.php');
class InscriCategoConcurInstru{
	
	public $conexion;
	private $whereAnd;
	private $condicion;
	
	private $idInscriCategoConcurInstru;
	private $categoriaConcurso;
	private $centroFormacion;
	private $instructor;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	private $estado;
	
	public function __construct(\entidad\InscriCategoConcurInstru $inscriCategoConcurInstru){
		
		$this->idInscriCategoConcurInstru = $inscriCategoConcurInstru->getIdInscriCategoConcurInstru();
		$this->categoriaConcurso = $inscriCategoConcurInstru->getCategoriaConcurso();
		$this->centroFormacion = $inscriCategoConcurInstru->getCentroFormacion();
		$this->instructor = $inscriCategoConcurInstru->getInstructor();
		$this->idUsuarioCreacion = $inscriCategoConcurInstru->getIdUsuarioCreacion();
		$this->idUsuarioModificacion = $inscriCategoConcurInstru->getIdUsuarioModificacion();
		$this->estado = $inscriCategoConcurInstru->getEstado();
		
		$this->conexion = new \Conexion();
	}
	public function adicionar(){
		$sentenciaSql = "
							INSERT INTO
								inscricategoconcurinstru
							(
								idCategoriaConcurso
								,idCentroFormacion
								,idInstructor
								,estado
								,idUsuarioCreacion
								,idUsuarioModificacion
								,fechaCreacion
								,fechaModificacion
								
							)
							VALUES
							(
								".$this->categoriaConcurso->getIdCategoriaConcurso()."
								,".$this->centroFormacion->getIdCentroFormacion()."
								,".$this->instructor->getIdInstructor()."
								,$this->estado
								,$this->idUsuarioCreacion
								,$this->idUsuarioModificacion
								,NOW()
								,NOW()
								
							)
						";
		$this->conexion->ejecutar($sentenciaSql);	
	}
	public function consultar(){
		$this->obtenerCondicion();
		$sentenciaSql = "
							SELECT 
								cf.centroFormacion
								,r.regional
								,p.foto
								,cc.categoriaConcurso
								,CONCAT_WS(' ',p.primerNombre,p.segundoNombre,p.primerApellido,p.segundoApellido) AS nombreCompleto
							FROM 
								inscricategoconcurinstru AS icci	
								INNER JOIN centroformacion AS cf ON cf.idCentroFormacion = icci.idCentroFormacion
								INNER JOIN instructor AS i ON i.idInstructor = icci.idInstructor
								INNER JOIN persona AS p ON p.idPersona = i.idPersona
								INNER JOIN regional AS r ON r.idRegional = cf.idRegional
								INNER JOIN categoriaconcurso AS cc ON cc.idCategoriaConcurso = icci.idCategoriaConcurso
								$this->condicion
							ORDER BY
								r.regional ASC , cf.centroFormacion ASC
						";
		$this->conexion->ejecutar($sentenciaSql);
		$contador = 0;
		while($fila = $this->conexion->obtenerObjeto()){
			$retorno[$contador]['centroFormacion'] = $fila->centroFormacion;
			$retorno[$contador]['regional'] = $fila->regional;
			$retorno[$contador]['foto'] = $fila->foto;
			$retorno[$contador]['categoriaConcurso'] = $fila->categoriaConcurso;
			$retorno[$contador]['nombreCompleto'] = $fila->nombreCompleto;
			$contador++;
		}
		return $retorno;
	}
	public function numerInstrInscrCateg(){
		$sentenciaSql="
						SELECT
							COUNT(icci.idCategoriaConcurso)  AS numeroInstructorCategoria,
							icci.idCategoriaConcurso
							,cc.limiteNumeroInstructor
						FROM
							inscricategoconcurinstru AS icci
							INNER JOIN categoriaconcurso AS cc ON cc.idCategoriaConcurso = icci.idCategoriaConcurso
						WHERE
							icci.idCategoriaConcurso = ".$this->categoriaConcurso->getIdCategoriaConcurso()."
						GROUP BY
							icci.idCategoriaConcurso
					";
		$this->conexion->ejecutar($sentenciaSql);
		$contador = 0;
		while($fila = $this->conexion->obtenerObjeto()){
			$retorno[$contador]['numeroInstructorCategoria'] = $fila->numeroInstructorCategoria;
			$retorno[$contador]['limiteNumeroInstructor'] = $fila->limiteNumeroInstructor;
			$contador++;
		}
		return $retorno;
	}
	public function validarLimitInstrCentrForma(){
		$sentenciaSql="
						SELECT
							COUNT(icca.idCentroFormacion) AS numeroInstructoresInscritos
							,cf.limiteInstructores
						FROM
							inscricategoconcurinstru AS icca
							INNER JOIN centroformacion AS cf ON cf.idCentroFormacion = icca.idCentroFormacion
						WHERE
							icca.idCentroFormacion  = ".$this->centroFormacion->getIdCentroFormacion()."
					";
		$this->conexion->ejecutar($sentenciaSql);
		$contador = 0;
		while($fila = $this->conexion->obtenerObjeto()){
			$retorno[$contador]['numeroInstructoresInscritos'] = $fila->numeroInstructoresInscritos;
			$retorno[$contador]['limiteInstructor'] = $fila->limiteInstructores;
			$contador++;
		}
		return $retorno;
	}
	public function validCategInstrCentrForma(){
		$sentenciaSql="
						SELECT
							COUNT(idCentroFormacion) as numeroInstructoresCategoria
						FROM
							inscricategoconcurinstru
						WHERE
							idCentroFormacion = ".$this->centroFormacion->getIdCentroFormacion()." AND idCategoriaConcurso = ".$this->categoriaConcurso->getIdCategoriaConcurso()."
					";
		$this->conexion->ejecutar($sentenciaSql);
		$contador = 0;
		while($fila = $this->conexion->obtenerObjeto()){
			$retorno[$contador]['numeroInstructoresCategoria'] = $fila->numeroInstructoresCategoria;
		}
		return $retorno;
	}
	public function validarInstrInscrCateg(){
		$sentenciaSql="
						SELECT
							icci.idInstructor
						FROM
							inscricategoconcurinstru AS icci
							INNER JOIN instructor AS i ON i.idInstructor = icci.idInstructor
							INNER JOIN persona AS p ON p.idPersona = i.idPersona
						WHERE
							p.numeroIdentificacion = ".$this->instructor->getPersona()->getNumeroIdentificacion()."
	
					";
		$this->conexion->ejecutar($sentenciaSql);
	}
	public function obtenerCondicion() {
		$this->condicion = '';
		$this->whereAnd = ' WHERE ';
	
		if(isset($this->categoriaConcurso)){
			if($this->categoriaConcurso->getIdCategoriaConcurso() != '' && $this->categoriaConcurso->getIdCategoriaConcurso() != 'null'){
				$this->condicion = $this->condicion.$this->whereAnd." icci.idCategoriaConcurso = ".$this->categoriaConcurso->getIdCategoriaConcurso();
				$this->whereAnd = ' AND ';
			}
		}
		if(isset($this->instructor)){
			if($this->instructor->getPersona()->getNumeroIdentificacion() != '' && $this->instructor->getPersona()->getNumeroIdentificacion() != 'null'){
				$this->condicion = $this->condicion.$this->whereAnd." p.numeroIdentificacion = ".$this->instructor->getPersona()->getNumeroIdentificacion();
				$this->whereAnd = ' AND ';
			}
		}
		if(isset($this->centroFormacion)){
			if($this->centroFormacion->getIdCentroFormacion() != '' && $this->centroFormacion->getIdCentroFormacion() != 'null'){
				$this->condicion = $this->condicion.$this->whereAnd." icci.idCentroFormacion = ".$this->centroFormacion->getIdCentroFormacion();
				$this->whereAnd = ' AND ';
			}
			$objetoEvaluar = $this->centroFormacion->getRegional();
			if(isset($objetoEvaluar)){
				if($this->centroFormacion->getRegional()->getIdRegional() != '' && $this->centroFormacion->getRegional()->getIdRegional() != 'null'){
					$this->condicion = $this->condicion.$this->whereAnd." cf.idRegional = ".$this->centroFormacion->getRegional()->getIdRegional();
					$this->whereAnd = ' AND ';
				}
			}
		}
	}
}
?>
