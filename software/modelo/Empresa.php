<?php
namespace modelo;
session_start();
require_once '../entorno/Conexion.php';
class Empresa {
    public $conexion = null;
    private $condicion;
    private $whereAnd;
    
    private $idEmpresa;
    private $nit;
    private $empresa;
    private $representanteLegal;
    private $direccion;
    private $telefonoCelular;
    private $telefonoFijo;
    private $correoElectronico;
    private $url;
    private $idMunicipio;
    private $estado;
    private $idRegimenEmpresa;
    private $idSectorEconomico;
    private $sectorEconomico;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    
    public function __construct(\entidad\Empresa $empresa) {
        $this->idEmpresa = $empresa->getIdEmpresa();
        $this->nit = $empresa->getNit();
        $this->empresa = $empresa->getEmpresa();
        $this->representanteLegal = $empresa->getRepresentanteLegal();
        $this->direccion = $empresa->getDireccion();
        $this->telefonoCelular = $empresa->getTelefonoCelular();
        $this->telefonoFijo = $empresa->getTelefonoFijo();
        $this->correoElectronico = $empresa->getCorreoElectronico();
        $this->url = $empresa->getUrl();
        $this->idMunicipio = $empresa->getIdMunicipio();
        $this->estado = $empresa->getEstado();
        $this->idUsuarioCreacion = $empresa->getIdUsuarioCreacion();
        $this->idUsuarioModificacion = $empresa->getIdUsuarioModificacion();
        $this->fechaCreacion = $empresa->getFechaCreacion();
        $this->fechaModificacion = $empresa->getFechaModificacion();
        $this->idRegimenEmpresa = $empresa->getIdRegimenEmpresa();
        $this->idSectorEconomico = $empresa->getIdSectorEconomico();
        $this->sectorEconomico = $empresa->getSectorEconomico();
        
        
        $this->conexion = new \Conexion();
    }
    
    public function adicionar() {
           $sentenciaSql = "
                        INSERT INTO 
                            empresa
                        (
                            nit
                            , empresa
                            , representanteLegal
                            , direccion
                            , telefonoCelular
                            , telefonoFijo
                            , correoElectronico
                            , url
                            , idMunicipio
                            ,idRegimenEmpresa
                            ,idSectorEconomico
                            ,sectorEconomico
                            , fechaCreacion
                            , fechaModificacion                           
                            
                        )
                        VALUES
                        (
                            
                            '$this->nit'
                            , '$this->empresa'
                            , '$this->representanteLegal'
                            , '$this->direccion'
                            ,  $this->telefonoCelular
                            ,  $this->telefonoFijo
                            , '$this->correoElectronico'
                            , '$this->url'
                            ,  $this->idMunicipio 
                            ,  $this->idRegimenEmpresa
                            ,  $this->idSectorEconomico
                            ,  '$this->sectorEconomico'
                            ,NOW()
                            ,NOW()                           
                               
                        )
                        ";
        $this->conexion->ejecutar($sentenciaSql);
    }
    public function modificar(){
       $sentenciaSql = "
                        UPDATE 
                            empresa
                        SET
                            nit = '$this->nit'
                            , empresa = '$this->empresa'
                            , representanteLegal = $this->representanteLegal
                            , direccion = '$this->direccion'
                            , telefonoCelular = $this->telefonoCelular
                            , telefonoFijo = $this->telefonoFijo    
                            , correoElectronico = '$this->correoElectronico'
                            , url = '$this->url'
                            , idMunicipio = $this->idMunicipio
                            , idRegimenEmpresa = $this->idRegimenEmpresa
                            , idSectorEconomico = $this->idSectorEconomico
                            , sectorEconomico = '$this->sectorEconomico'
                            , estado = '$this->estado'
                            , idUsuarioModificacion = $this->idUsuarioModificacion
                            , fechaModificacion = NOW()
                        WHERE
                            idEmpresa = $this->idEmpresa
                        ";
       $this->conexion->ejecutar($sentenciaSql);
    }
    public function consultar() {
        $this->obtenerCondicion();
         $sentenciaSql = "
                        SELECT
                            e. *
			    		,m.municipio
                        FROM
                            empresa as e
			    			INNER JOIN municipio AS m ON e.idMunicipio = m.idMunicipio
                            $this->condicion
                        ";

        $this->conexion->ejecutar($sentenciaSql);
    }
    public function consultarEmpresasOferentes(){
    	$this->obtenerCondicion();
    	$sentenciaSql = "
    				SELECT DISTINCT
    					e.*
    					,m.municipio
    				FROM
    					empresa AS e
    					INNER JOIN municipio AS m ON e.idMunicipio = m.idMunicipio
    					INNER JOIN empresaproducto  ep ON ep.idEmpresa = e.idEmpresa
    					$this->condicion
    					$this->whereAnd
    					ep.idTipoEmpresaProducto = 3
    		";
    	$this->conexion->ejecutar($sentenciaSql);
    }
    public function consultarEmpresasClientes(){
    	$this->obtenerCondicion();
    	$sentenciaSql = "
    					SELECT DISTINCT
					    	e.*
					    	,m.municipio
    					FROM
    						empresa AS e
    						INNER JOIN municipio AS m ON e.idMunicipio = m.idMunicipio
    						INNER JOIN empresaproducto  ep ON ep.idEmpresa = e.idEmpresa
    						$this->condicion
    						$this->whereAnd
    						(ep.idTipoEmpresaProducto = 1 OR ep.idTipoEmpresaProducto = 2)
    	";
    	$this->conexion->ejecutar($sentenciaSql);
    }
	
	function obtenerMaximo(){
       $sentenciaSql= "
                        SELECT 
                            MAX(idEmpresa) as maximo
                        FROM
                            empresa
                        ";
       $this->conexion->ejecutar($sentenciaSql);
       $fila = $this->conexion->obtenerObjeto();
       return $fila->maximo;   
    }
    function obtenerCondicion(){
        $this->condicion = '';
        $this->whereAnd = ' WHERE ';
        
        if($this->idEmpresa != '' && $this->idEmpresa != 'null'){
            $this->condicion = $this->condicion.$this->whereAnd." e.idEmpresa = $this->idEmpresa";
            $this->whereAnd = ' AND ';
        }
        if($this->nit != ''){
            $this->condicion = $this->condicion.$this->whereAnd." e.nit LIKE '%$this->nit%'";
            $this->whereAnd = ' AND ';
        }
        if($this->empresa != ''){
            $this->condicion = $this->condicion.$this->whereAnd." e.empresa LIKE '%$this->empresa%'";
            $this->whereAnd = ' AND ';
        }
        
        if($this->representanteLegal != ''){
            $this->condicion = $this->condicion.$this->whereAnd." e.representanteLegal LIKE '%$this->representanteLegal%'";
            $this->whereAnd = ' AND ';
        }
        if($this->direccion != ''){
            $this->condicion = $this->condicion.$this->whereAnd." e.direccion LIKE '%$this->direccion%'";
            $this->whereAnd = ' AND ';
        }
        if($this->telefonoCelular != '' && $this->telefonoCelular != 'null'){
            $this->condicion = $this->condicion.$this->whereAnd." e.telefonoCelular = $this->telefonoCelular";
            $this->whereAnd = ' AND ';
        }
        if($this->telefonoFijo != '' && $this->telefonoFijo != 'null'){
            $this->condicion = $this->condicion.$this->whereAnd." e.telefonoFijo = $this->telefonoFijo";
            $this->whereAnd = ' AND ';
        }
        if($this->correoElectronico != ''){
            $this->condicion = $this->condicion.$this->whereAnd." e.correoElectronico LIKE '%$this->correoElectronico%'";
            $this->whereAnd = ' AND ';
        }
        if($this->url != ''){
            $this->condicion = $this->condicion.$this->whereAnd." e.url LIKE '%$this->url%'";
            $this->whereAnd = ' AND ';
        }
        if($this->estado != ''){
            $this->condicion = $this->condicion.$this->whereAnd." e.estado = $this->estado";
            $this->whereAnd = ' AND ';
        }
		if($this->idMunicipio!= '' && $this->idMunicipio!= 'null'){
            $this->condicion = $this->condicion.$this->whereAnd." e.idMunicipio = $this->idMunicipio";
            $this->whereAnd = ' AND ';
        }
        if($this->municipio != ''){
            $this->condicion = $this->condicion.$this->whereAnd." m.municipio LIKE '%$this->municipio%'";
            $this->whereAnd = ' AND ';
        }
    }
}   
?>