<?php
namespace modelo;
require_once '../entorno/Conexion.php';
class Proceso {
    public $conexion = null;
    private $condicion;
    private $whereAnd;
    
    private $idProceso;
    private $proceso;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    private $estado;
    
    public function __construct(\entidad\Proceso $proceso) {
        $this->idProceso = $proceso->getIdProceso();
        $this->proceso = $proceso->getProceso();
        $this->idUsuarioCreacion = $proceso->getIdUsuarioCreacion();
        $this->idUsuarioModificacion = $proceso->getIdUsuarioModificacion();
        $this->fechaCreacion = $proceso->getFechaCreacion();
        $this->fechaModificacion = $proceso->getFechaModificacion();
        $this->estado = $proceso->getEstado();
        
        $this->conexion = new \Conexion();
    }
}
