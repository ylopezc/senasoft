<?php

namespace modelo;
class Departamento {
    public $conexion;
    private $whereAnd;
    private $condicion;
    
    private $idDepartamento;
    private $departamento;
    private $referencia;
    private $estado;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    
    public function __construct(\entidad\Departamento $departamento) {
       $this->idDepartamento = $departamento->getIdDepartamento();
       $this->departamento = $departamento->getDepartamento();
       $this->referencia = $departamento->getReferencia();
       
       $this->conexion = new \Conexion();
   }
   public function consultar(){
       $this->obtenerCondicion();
       $sentenciaSql = "
                            SELECT 
                                  d.idDepartamento
                                  ,d.departamento
                                  ,d.referencia
                            FROM
                                departamento as d
                            $this->condicion
                        ";
       $this->conexion->ejecutar($sentenciaSql);
       $contador = 0;
       while($fila = $this->conexion->obtenerObjeto()){
           $retorno[$contador]['idDepartamento'] = $fila->idDepartamento;
           $retorno[$contador]['departamento'] = $fila->departamento;
           $retorno[$contador]['referencia'] = $fila->referencia;
          
       }
   }
   
   public function obtenerCondicion() {
        $this->condicion = '';
        $this->whereAnd = ' WHERE ';
        
        if($this->idDepartamento != '' && $this->idDepartamento != 'null'){
            $this->condicion = $this->condicion.$this->whereAnd." d.idDepartamento = $this->idDepartamento";
            $this->whereAnd = ' AND ';
        }
	if($this->departamento != '' && $this->departamento != 'null'){
            $this->condicion = $this->condicion.$this->whereAnd." d.departamento = '$this->departamento'";
            $this->whereAnd = ' AND ';
        }
        if($this->referencia != '' && $this->referencia != 'null'){
            $this->condicion = $this->condicion.$this->whereAnd." d.referencia = $this->referencia";
            $this->whereAnd = ' AND ';
        }
    }
}

?>
