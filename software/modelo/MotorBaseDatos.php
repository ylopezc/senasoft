<?php
namespace modelo;
require_once '../entorno/Conexion.php';
class MotorBaseDatos {
    public $conexion = null;
    private $condicion;
    private $whereAnd;
    
    private $idMotorBaseDatos;
    private $motorBaseDatos;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    private $estado;
    
    public function __construct(\entidad\MotorBaseDatos $motorBaseDatos) {
        $this->idMotorBaseDatos = $motorBaseDatos->getIdMotorBaseDatos();
        $this->motorBaseDatos = $motorBaseDatos->getMotorBaseDatos();
        $this->idUsuarioCreacion = $motorBaseDatos->getIdUsuarioCreacion();
        $this->idUsuarioModificacion = $motorBaseDatos->getIdUsuarioModificacion();
        $this->fechaCreacion = $motorBaseDatos->getFechaCreacion();
        $this->fechaModificacion = $motorBaseDatos->getFechaModificacion();
        $this->estado = $motorBaseDatos->getEstado();
        
        $this->conexion = new \Conexion();
    }
}
