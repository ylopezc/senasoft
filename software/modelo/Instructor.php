<?php
namespace  modelo;
session_start();
require_once('../entorno/Conexion.php');
class Instructor{
	
	public $conexion;
	private $whereAnd;
	private $condicion;
	
	private $idInstructor;
	private $persona;
	private $centroFormacion;
	private $estado;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	
	public function __construct(\entidad\Instructor $instructor){
		$this->idInstructor = $instructor->getIdInstructor();
		$this->persona = $instructor->getPersona();
		$this->centroFormacion = $instructor->getCentroFormacion();
		$this->estado = $instructor->getEstado();
		$this->idUsuarioCreacion = $instructor->getIdUsuarioCreacion();
		$this->idUsuarioModificacion = $instructor->getIdUsuarioModificacion();
		
		$this->conexion = new \Conexion();
	}
	public function adicionar(){
		$sentenciaSql="
						INSERT INTO
							instructor
						(
							idPersona
							,idCentroFormacion
							,estado
							,idUsuarioCreacion
							,idUsuarioModificacion
							,fechaCreacion
							,fechaModificacion
						)
						VALUES
						(
							".$this->persona->getIdPersona()."
							,".$this->centroFormacion->getIdCentroFormacion()."
							,$this->estado
							,$this->idUsuarioCreacion
							,$this->idUsuarioModificacion
							,NOW()
							,NOW()
						)
							
					";
		$this->conexion->ejecutar($sentenciaSql);
	}
	function obtenerMaximo(){
		$sentenciaSql = "
                            SELECT
                                MAX(idInstructor) as maximo
                            FROM
                                instructor
                            ";
		$this->conexion->ejecutar($sentenciaSql);
		$fila = $this->conexion->obtenerObjeto();
		return $fila->maximo;
	}
}
?>