<?php
namespace modelo;
require_once '../entorno/Conexion.php';
class Municipio {
    public $conexion;
    private $whereAnd;
    private $condicion;
    
    private $idMunicipio;
    private $referencia;
    private $municipio;
    private $idDepartamento;
    private $estado;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    
    public function __construct(\entidad\Municipio $municipio) {
       $this->idMunicipio = $municipio->getIdMunicipio();
       $this->municipio = $municipio->getMunicipio();
       $this->referencia = $municipio->getReferencia();
       $this->idDepartamento = $municipio->getIdDepartamento();
       $this->idUsuarioCreacion = $municipio->getIdUsuarioCreacion();
       $this->idUsuarioModificacion = $municipio->getIdUsuarioModificacion();
       $this->fechaCreacion = $municipio->getFechaCreacion();
       $this->fechaModificacion = $municipio->getFechaModificacion();
       
       $this->conexion = new \Conexion();
   }
   public function consultar(){
       $this->obtenerCondicion();
       $sentenciaSql = "
                            SELECT 
                                  m.idMunicipio
                                  ,m.municipio
                                  ,m.referencia
                            FROM
                                municipio as m
                            $this->condicion
                        ";
       $this->conexion->ejecutar($sentenciaSql);
       $contador = 0;
       while($fila = $this->conexion->obtenerObjeto()){
           $retorno[$contador]['idMunicipio'] = $fila->idMunicipio;
           $retorno[$contador]['municipio'] = $fila->municipio;
           $retorno[$contador]['referencia'] = $fila->referencia;
          
       }
   }
   /*
   public function cargarMunicipios($idDepartamento) {
        $sentenciaSql = "
                        SELECT DISTINCT
                            m.idMunicipio
                            , m.municipio
                        FROM
                            municipio AS m
                            INNER JOIN departamento AS d ON d.idDepartamento = m.idDepartamento
                        WHERE
                            d.idDepartamento IN (
                                            SELECT
                                                idMunicipio
                                            FROM
                                                municipio
                                            WHERE
                                                idDepartamento = $this->idDepartamento
                                            )
                                AND m.idDepartamento = $idDepartamento
                        ";
        $this->conexion->ejecutar($sentenciaSql);
        $contador = 0;
        while ($fila = $this->conexion->obtenerObjeto()) {
            $retorno[$contador]['idMunicipio'] = $fila->idMunicipio;
            $retorno[$contador]['municipio'] = $fila->municipio;
            $contador++;
        }
        return $retorno;
    }
    */
   public function consultarMunicipios() {
       $sentenciaSql = "
                        SELECT
                            m.idMunicipio
                            , m.municipio
                        FROM
                            municipio AS m
                            INNER JOIN  departamento AS d ON d.idDepartamento = m.idDepartamento
                        WHERE
                            m.idDepartamento = $this->idDepartamento
                        ";
       $this->conexion->ejecutar($sentenciaSql);
        $contador = 0;
        while ($fila = $this->conexion->obtenerObjeto()) {
            $retorno[$contador]['idMunicipio'] = $fila->idMunicipio;
            $retorno[$contador]['municipio'] = $fila->municipio;
            $contador++;
        }
        return $retorno;
   }
   public function obtenerCondicion() {
        $this->condicion = '';
        $this->whereAnd = ' WHERE ';
        
        if($this->idMunicipio != '' && $this->idMunicipio != 'null'){
            $this->condicion = $this->condicion.$this->whereAnd." m.idTipoDocumentoIdentidad = $this->idMunicipio";
            $this->whereAnd = ' AND ';
        }
	if($this->municipio != '' && $this->municipio != 'null'){
            $this->condicion = $this->condicion.$this->whereAnd." m.municipio = '$this->municipio'";
            $this->whereAnd = ' AND ';
        }
        if($this->referencia != '' && $this->referencia != 'null'){
            $this->condicion = $this->condicion.$this->whereAnd." m.referencia = $this->referencia";
            $this->whereAnd = ' AND ';
        }
    }
}

?>
