<?php
namespace  modelo;
session_start();
require_once('../entorno/Conexion.php');
class ProgramaFormacion{
	
	public $conexion;
	private $whereAnd;
	private $condicion;
	
	private $idProgramaFormacion;
	private $nivelProgramaFormacion;
	private $programaFormacion;
	private $estado;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	
	public function __construct(\entidad\ProgramaFormacion $programaFormacion){
		$this->idProgramaFormacion = $programaFormacion->getIdProgramaFormacion();
		$this->nivelProgramaFormacion = $programaFormacion->getNivelProgramaFormacion();
		$this->programaFormacion = $programaFormacion->getProgramaFormacion();
		$this->estado = $programaFormacion->getEstado();
		$this->idUsuarioCreacion = $programaFormacion->getIdUsuarioCreacion();
		$this->idUsuarioModificacion = $programaFormacion->getIdUsuarioModificacion();
		
		$this->conexion = new \Conexion();
	}
	public function consultar(){
		$this->obtenerCondicion();
		$sentenciaSql="
						SELECT
							pf.idProgramaFormacion
							,pf.idNivelProgramaFormacion
							,pf.programaFormacion
							,pf.estado
							,npf.nivelProgramaFormacion
						FROM
							programaformacion AS pf
							INNER JOIN nivelprogramaformacion AS npf ON npf.idNivelProgramaFormacion = pf.idNivelProgramaFormacion
							$this->condicion
					";
		$this->conexion->ejecutar($sentenciaSql);
		$contador = 0;
		while($fila = $this->conexion->obtenerObjeto()){
			$retorno[$contador]['idProgramaFormacion'] = $fila->idProgramaFormacion;
			$retorno[$contador]['idNivelProgramaFormacion'] = $fila->idNivelProgramaFormacion;
			$retorno[$contador]['programaFormacion'] = $fila->programaFormacion;
			$retorno[$contador]['nivelProgramaFormacion'] = $fila->nivelProgramaFormacion;
			$retorno[$contador]['estado'] = $fila->estado;
			$contador++;
		}
		return $retorno;
	}
	public function obtenerCondicion() {
		$this->condicion = '';
		$this->whereAnd = ' WHERE ';
	
		if($this->idProgramaFormacion != '' && $this->idProgramaFormacion != 'null'){
			$this->condicion = $this->condicion.$this->whereAnd." pf.idProgramaFormacion = $this->idProgramaFormacion";
			$this->whereAnd = ' AND ';
		}
		if($this->programaFormacion != '' && $this->programaFormacion != 'null'){
			$this->condicion = $this->condicion.$this->whereAnd." pf.programaFormacion LIKE '%$this->programaFormacion%'";
			$this->whereAnd = ' AND ';
		}
		if($this->estado != '' && $this->estado != 'null'){
			$this->condicion = $this->condicion.$this->whereAnd." pf.estado = $this->estado";
			$this->whereAnd = ' AND ';
		}
	}
}
?>