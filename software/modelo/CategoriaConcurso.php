<?php
namespace  modelo;
session_start();
require_once('../entorno/Conexion.php');
class CategoriaConcurso{
	
	public $conexion;
	private $whereAnd;
	private $condicion;
	
	private $idCategoriaConcurso;
	private $categoriaConcurso;
	private $limiteNumeroAprendiz;
	private $limiteNumeroInstructor;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	private $estado;
	
	public function __construct(\entidad\CategoriaConcurso $categoriaConcurso){
		$this->idCategoriaConcurso = $categoriaConcurso->getIdCategoriaConcurso();
		$this->categoriaConcurso = $categoriaConcurso->getCategoriaConcurso();
		$this->limiteNumeroAprendiz =  $categoriaConcurso->getLimiteNumeroAprendiz();
		$this->limiteNumeroInstructor =  $categoriaConcurso->getLimiteNumeroInstructor();
		$this->idUsuarioCreacion =  $categoriaConcurso->getIdUsuarioCreacion();
		$this->idUsuarioModificacion =  $categoriaConcurso->getIdUsuarioModificacion();
		$this->estado =  $categoriaConcurso->getEstado();
		
		$this->conexion = new \Conexion();
	}
	public function consultar(){
		$this->obtenerCondicion();
		$sentenciaSql="
						SELECT
							cc.idCategoriaConcurso
							,cc.categoriaConcurso
							,cc.limiteNumeroAprendiz
							,cc.limiteNumeroInstructor
							,cc.estado
						FROM
							categoriaconcurso as cc
							$this->condicion
						";
		$this->conexion->ejecutar($sentenciaSql);
		$contador = 0;
		while($fila = $this->conexion->obtenerObjeto()){
			$retorno[$contador]['idCategoriaConcurso'] = $fila->idCategoriaConcurso;
			$retorno[$contador]['categoriaConcurso'] = $fila->categoriaConcurso;
			$retorno[$contador]['limiteNumeroAprendiz'] = $fila->limiteNumeroInstructor;
			$retorno[$contador]['limiteNumeroInstructor'] = $fila->limiteNumeroInstructor;
			$retorno[$contador]['estado'] = $fila->estado;
			$contador++;
		}
		return $retorno;
	}
	public function obtenerCondicion() {
		$this->condicion = '';
		$this->whereAnd = ' WHERE ';
	
		if($this->idCategoriaConcurso != '' && $this->idCategoriaConcurso != 'null'){
			$this->condicion = $this->condicion.$this->whereAnd." cc.idCategoriaConcurso = $this->idCategoriaConcurso";
			$this->whereAnd = ' AND ';
		}
		if($this->categoriaConcurso != '' && $this->categoriaConcurso != 'null'){
			$this->condicion = $this->condicion.$this->whereAnd." cc.categoriaConcurso LIKE '%$this->categoriaConcurso%'";
			$this->whereAnd = ' AND ';
		}
		if($this->estado != '' && $this->estado != 'null'){
			$this->condicion = $this->condicion.$this->whereAnd." cc.estado = $this->estado";
			$this->whereAnd = ' AND ';
		}
	}
}
?>