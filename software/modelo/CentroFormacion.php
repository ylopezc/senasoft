<?php
namespace  modelo;
session_start();
require_once('../entorno/Conexion.php');
class CentroFormacion{
	public $conexion;
	private $whereAnd;
	private $condicion;
	
	private $idCentroFormacion;
	private $centroFormacion;
	private $regional;
	private $codigo;
	private $estado;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	
	public function __construct(\entidad\CentroFormacion $centroFormacion){
		$this->idCentroFormacion = $centroFormacion->getIdCentroFormacion();
		$this->centroFormacion = $centroFormacion->getCentroFormacion();
		$this->regional = $centroFormacion->getRegional();
		$this->codigo = $centroFormacion->getCodigo();
		$this->estado = $centroFormacion->getEstado();
		$this->idUsuarioCreacion = $centroFormacion->getIdUsuarioCreacion();
		$this->idUsuarioModificacion = $centroFormacion->getIdUsuarioModificacion();
		
		$this->conexion = new \Conexion();
	}
	public function consultar(){
		$this->obtenerCondicion();
		$sentenciaSql= "
							SELECT
								cf.idCentroFormacion
								,cf.centroFormacion
								,cf.idRegional
								,cf.codigo
								,cf.estado
								,r.regional
							FROM
								centroformacion as cf
								INNER JOIN regional as r ON r.idRegional = cf.idRegional
								$this->condicion
						";
		$this->conexion->ejecutar($sentenciaSql);
		$contador = 0;
		while ($fila = $this->conexion->obtenerObjeto()){
			$retorno[$contador]['idCentroFormacion']=$fila->idCentroFormacion;
			$retorno[$contador]['centroFormacion']=$fila->centroFormacion;
			$retorno[$contador]['idRegional']=$fila->idRegional;
			$retorno[$contador]['codigo']=$fila->codigo;
			$retorno[$contador]['estado']=$fila->estado;
			$retorno[$contador]['regional']=$fila->regional;
			$contador++;
		}
		return $retorno;
	}
	public function obtenerCodigos() {
        $sentenciaSql = "
                        SELECT
                            codigo
                        FROM
                            centroformacion
                        ";
        $this->conexion->ejecutar($sentenciaSql);
        $contador = 0;
        while ($fila = $this->conexion->obtenerObjeto()) {
            $codigos[$contador] = $fila->codigo;
            $contador++;
        }
        return $codigos;
    }
    public function obtenerInformacionCentroFormacion() {
        $sentenciaSql = "
                        SELECT
                            cf.idCentroFormacion
                            , cf.idRegional
                            , r.regional
                            , cf.centroFormacion
                        FROM
                            centroformacion AS cf
                            INNER JOIN regional AS r ON r.idRegional = cf.idRegional
                        WHERE
                            cf.codigo = '$this->codigo'
                        ";
        $this->conexion->ejecutar($sentenciaSql);
        $fila = $this->conexion->obtenerObjeto();
        
        $retorno['idCentroFormacion'] = $fila->idCentroFormacion;
        $retorno['idRegional'] = $fila->idRegional;
        $retorno['regional'] = $fila->regional;
        $retorno['centroFormacion'] = $fila->centroFormacion;
        
        return $retorno;
    }
    public function buscarCentroFormacion($centroFormacion) {
        $sentenciaSql = "
                            SELECT 
                                cf.idCentroFormacion
                                , cf.idRegional
                                , cf.centroFormacion
                                , r.regional
                                , CONCAT_WS(' - ', r.regional, cf.centroFormacion) AS centro
                            FROM
                                centroformacion AS cf
                                INNER JOIN regional AS r ON r.idRegional = cf.idRegional
                            WHERE 
                                CONCAT_WS(' - ', r.regional, cf.centroFormacion) LIKE '%$centroFormacion%'
                        ";
        $this->conexion->ejecutar($sentenciaSql);
        while ($fila = $this->conexion->obtenerObjeto()) {
            $datos[] = array("idCentroFormacion" => $fila->idCentroFormacion,
                            "idRegional" => $fila->idRegional,
                            "value" => $fila->centro
                            );
        }
        return $datos;
    }
    public function consultarRegional() {
        $sentenciaSql = "
                        SELECT
                            idRegional
                            , regional
                        FROM
                            regional
                        WHERE
                            estado = 1
                        ORDER BY regional ASC
                        ";
        $this->conexion->ejecutar($sentenciaSql);
        while ($fila = $this->conexion->obtenerObjeto()) {
            $retorno[$contador]['idRegional'] = $fila->idRegional;
            $retorno[$contador]['regional'] = $fila->regional;
            $contador++;
        }
        return $retorno;
    }
	public function obtenerCondicion() {
		$this->condicion = '';
		$this->whereAnd = ' WHERE ';
	
		if($this->idCentroFormacion != '' && $this->idCentroFormacion != 'null'){
			$this->condicion = $this->condicion.$this->whereAnd." cf.idCentroFormacion = $this->idCentroFormacion";
			$this->whereAnd = ' AND ';
		}
		if($this->centroFormacion != '' && $this->centroFormacion != 'null'){
			$this->condicion = $this->condicion.$this->whereAnd." cf.centroFormacion LIKE '%$this->centroFormacion%'";
			$this->whereAnd = ' AND ';
		}
		if(isset($this->regional)){
			if($this->regional->getIdRegional() != '' && $this->regional->getIdRegional() != 'null'){
				$this->condicion = $this->condicion.$this->whereAnd." cf.idRegional = ".$this->regional->getIdRegional();
				$this->whereAnd = ' AND ';
			}
		}
		if($this->estado != '' && $this->estado != 'null'){
			$this->condicion = $this->condicion.$this->whereAnd." cf.estado = $this->estado";
			$this->whereAnd = ' AND ';
		}
		if($this->codigo != '' && $this->codigo!= 'null'){
			$this->condicion = $this->condicion.$this->whereAnd." cf.codigo = '$this->codigo'";
			$this->whereAnd = ' AND ';
		}
	}
}
?>