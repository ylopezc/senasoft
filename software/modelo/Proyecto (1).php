<?php
namespace modelo;
require_once '../entorno/Conexion.php';

class Proyecto {

    public $conexion = null;

    private $condicion;

    private $whereAnd;

    

    private $idProyecto;

    private $proyecto;

    private $descripcion;

    private $url;

    private $sistemaRelacionado;

    private $ubicacion;

    private $urlVideo;

    private $tiempoProduccion;

    private $procesoImpacto;

    private $tipoSolucion;

    private $motorBaseDatos;

    private $lenguajeProgramacion;

    private $tipoLicenciamiento;

    private $derechosAutor;

    private $nroImplementaciones;

    private $aprendiz;

    private $centroFormacion;

    private $cartaImplementacion;

    private $nroImplementacionFuturo;

    private $desarrolloFuturo;

    private $equipoDesarrollo;
    private $informacionAdicional;
    private $idUsuarioCreacion;

    private $idUsuarioModificacion;

    private $fechaCreacion;

    private $fechaModificacion;

    private $estado;

    

    public function __construct(\entidad\Proyecto $proyecto) {

        $this->idProyecto = $proyecto->getIdProyecto();

        $this->proyecto = $proyecto->getProyecto();

        $this->descripcion = $proyecto->getDescripcion();

        $this->url = $proyecto->getUrl();

        $this->sistemaRelacionado = $proyecto->getSistemaRelacionado();

        $this->ubicacion = $proyecto->getUbicacion();

        $this->urlVideo = $proyecto->getUrlVideo();

        $this->tiempoProduccion = $proyecto->getTiempoProduccion();

        $this->procesoImpacto = $proyecto->getProcesoImpacto();

        $this->tipoSolucion = $proyecto->getTipoSolucion();

        $this->motorBaseDatos = $proyecto->getMotorBaseDatos();

        $this->lenguajeProgramacion = $proyecto->getLenguajeProgramacion();

        $this->tipoLicenciamiento = $proyecto->getTipoLicenciamiento();

        $this->derechosAutor = $proyecto->getDerechosAutor();

        $this->nroImplementaciones = $proyecto->getNroImplementaciones();

        $this->aprendiz = $proyecto->getAprendiz();

        $this->centroFormacion = $proyecto->getCentroFormacion();

        $this->cartaImplementacion = $proyecto->getCartaImplementacion();

        $this->nroImplementacionFuturo = $proyecto->getNroImplementacionFuturo();

        $this->desarrolloFuturo = $proyecto->getDesarrolloFuturo();

        $this->equipoDesarrollo = $proyecto->getEquipoDesarrollo();
        $this->informacionAdicional = $proyecto->getInformacionAdicional();

        $this->idUsuarioCreacion = $proyecto->getIdUsuarioCreacion();

        $this->idUsuarioModificacion = $proyecto->getIdUsuarioModificacion();

        $this->fechaCreacion = $proyecto->getFechaCreacion();

        $this->fechaModificacion = $proyecto->getFechaModificacion();

        $this->estado = $proyecto->getEstado();

        

        $this->conexion = new \Conexion();

    }

    public function adicionar() {

    $sentenciaSql = "

                        INSERT INTO

                            proyecto

                        (

                            proyecto

                            , descripcion

                            , url

                            , sistemaRelacionado


                            , urlVideo

                            , tiempoProduccion

                            , idProcesoImpacto

                            , idTipoSolucion

                            , idMotorBaseDatos

                            , idLenguajeProgramacion

                            , NroImplementaciones

                            , idAprendiz

                            , idCentroFormacion

                            , cartaImplementacion

                            , nroImplementacionFuturo

                            , desarrolloFuturo

                            , equipoDesarroll
                            
                            , informacionAdicional

                            , fechaCreacion

                            , fechaModificacion

                            , estado

                        )

                        VALUES

                        (

                            '$this->proyecto'

                            , '$this->descripcion'

                            , '$this->url'

                            , '$this->sistemaRelacionado'

                            , '$this->urlVideo'

                            , '$this->tiempoProduccion'

                            , ".$this->procesoImpacto->getIdProceso()."

                            , ".$this->tipoSolucion->getIdTipoSolucion()."

                            , ".$this->motorBaseDatos->getIdMotorBaseDatos()."

                            , ".$this->lenguajeProgramacion->getIdLenguajeProgramacion()."

                            , $this->nroImplementaciones

                            , ".$this->aprendiz->getIdAprendiz()."

                            , ".$this->centroFormacion->getIdCentroFormacion()."

                            , '$this->cartaImplementacion'

                            , $this->nroImplementacionFuturo

                            , '$this->desarrolloFuturo'

                            , '$this->equipoDesarrollo'
                                
                            , '$this->informacionAdicional'

                            , NOW()

                            , NOW()

                            , 1

                        )

                        ";

        $this->conexion->ejecutar($sentenciaSql);

    }

    function obtenerMaximo(){

        $sentenciaSql = "

                            SELECT

                                MAX(idProyecto) as maximo

                            FROM

                                proyecto

                            ";

        $this->conexion->ejecutar($sentenciaSql);

        $fila = $this->conexion->obtenerObjeto();

        return $fila->maximo;

    }

    public function cargarProcesoImpacto() {

        $sentenciaSql = "

                        SELECT

                            idProceso

                            , proceso

                        FROM

                            proceso

                        WHERE

                            estado = 1

                        ";

        $this->conexion->ejecutar($sentenciaSql);

        while ($fila = $this->conexion->obtenerObjeto()) {

            $retorno[$contador]['idProceso'] = $fila->idProceso;

            $retorno[$contador]['proceso'] = $fila->proceso;

            $contador++;   

        }

        return $retorno;

    }

    public function cargarTipoSolucion() {

        $sentenciaSql = "

                        SELECT

                            idTipoSolucion

                            , tipoSolucion

                        FROM

                            tiposolucion

                        WHERE

                            estado = 1

                        ";

        $this->conexion->ejecutar($sentenciaSql);

        while ($fila = $this->conexion->obtenerObjeto()) {

            $retorno[$contador]['idTipoSolucion'] = $fila->idTipoSolucion;

            $retorno[$contador]['tipoSolucion'] = $fila->tipoSolucion;

            $contador++;   

        }

        return $retorno;

    }

    public function cargarMotorBaseDatos() {

        $sentenciaSql = "

                        SELECT

                            idMotorBaseDatos

                            , motorBaseDatos

                        FROM

                            motorbasedatos

                        WHERE

                            estado = 1

                        ";

        $this->conexion->ejecutar($sentenciaSql);

        while ($fila = $this->conexion->obtenerObjeto()) {

            $retorno[$contador]['idMotorBaseDatos'] = $fila->idMotorBaseDatos;

            $retorno[$contador]['motorBaseDatos'] = $fila->motorBaseDatos;

            $contador++;   

        }

        return $retorno;

    }

    public function cargarLenguajeProgramacion() {

        $sentenciaSql = "

                        SELECT

                            idLeguajeProgramacion

                            , leguajeProgramacion

                        FROM

                            leguajeprogramacion

                        WHERE

                            estado = 1

                        ";

        $this->conexion->ejecutar($sentenciaSql);

        while ($fila = $this->conexion->obtenerObjeto()) {

            $retorno[$contador]['idLeguajeProgramacion'] = $fila->idLeguajeProgramacion;

            $retorno[$contador]['leguajeProgramacion'] = $fila->leguajeProgramacion;

            $contador++;   

        }

        return $retorno;

    }

    public function actualizarCartaImplementacion() {

        $sentenciaSql = "

                        UPDATE

                            proyecto

                        SET

                            cartaImplementacion = '$this->cartaImplementacion'

                        WHERE

                            idProyecto = $this->idProyecto;

                        ";

        $this->conexion->ejecutar($sentenciaSql);

    }

    public function buscarProyecto($nombreProyecto) {

        $sentenciaSql = "

                            SELECT 

                                idProyecto

                                , proyecto

                            FROM

                                proyecto

                            WHERE 

                                proyecto LIKE '%$nombreProyecto%'

                        ";

        $this->conexion->ejecutar($sentenciaSql);

        while ($fila = $this->conexion->obtenerObjeto()) {

            $datos[] = array("idProyecto" => $fila->idProyecto,

                            "value" => $fila->proyecto,

                            );

        }

        return $datos;

    }

    public function consultar() {

        $this->obtenerCondicion();

        $sentenciaSql = "

                        SELECT

                            p.idProyecto

                            , p.proyecto

                            , p.descripcion

                            , p.url

                            , p.sistemaRelacionado

                            , p.ubicacion

                            , p.urlVideo

                            , p.tiempoProduccion

                            , p.idProcesoImpacto

                            , pi.proceso AS procesoImpacto

                            , p.idTipoSolucion

                            , ts.tipoSolucion

                            , p.idMotorBaseDatos

                            , mbd.motorBaseDatos

                            , p.idLenguajeProgramacion

                            , lp.leguajeProgramacion

                            , p.tipoLicenciamiento

                            , p.derechosAutor

                            , p.NroImplementaciones

                            , p.idAprendiz

                            , per.numeroIdentificacion

                            , CONCAT_WS(' ', per.primerNombre, per.segundoNombre, per.primerApellido, per.segundoApellido) AS nombreAprendiz

                            , re.idRegional

                            , re.regional

                            , p.idCentroFormacion

                            , cf.centroFormacion

                            , p.cartaImplementacion

                            , p.nroImplementacionFuturo

                            , p.desarrolloFuturo

                            , p.equipoDesarroll

                        FROM

                            proyecto AS p

                            INNER JOIN proceso AS pi ON pi.idProceso = p.idProcesoImpacto

                            INNER JOIN tiposolucion AS ts ON ts.idTipoSolucion = p.idTipoSolucion

                            INNER JOIN motorbasedatos AS mbd ON mbd.idMotorBaseDatos = p.idMotorBaseDatos

                            INNER JOIN leguajeprogramacion AS lp ON lp.idLeguajeProgramacion = p.idLenguajeProgramacion

                            INNER JOIN aprendiz AS a ON a.idAprendiz = p.idAprendiz

                            INNER JOIN persona AS per ON per.idPersona = a.idPersona

                            INNER JOIN centroformacion AS cf ON cf.idCentroFormacion = p.idCentroFormacion

                            INNER JOIN regional AS re ON re.idRegional = cf.idRegional

                            $this->condicion

                        ";

        $this->conexion->ejecutar($sentenciaSql);

        while ($fila = $this->conexion->obtenerObjeto()) {

            $retorno[$contador]['idProyecto'] = $fila->idProyecto;

            $retorno[$contador]['proyecto'] = $fila->proyecto;

            $retorno[$contador]['descripcion'] = $fila->descripcion;

            $retorno[$contador]['url'] = $fila->url;

            $retorno[$contador]['sistemaRelacionado'] = $fila->sistemaRelacionado;

            $retorno[$contador]['ubicacion'] = $fila->ubicacion;

            $retorno[$contador]['urlVideo'] = $fila->urlVideo;

            $retorno[$contador]['tiempoProduccion'] = $fila->tiempoProduccion;

            $retorno[$contador]['idProcesoImpacto'] = $fila->idProcesoImpacto;

            $retorno[$contador]['procesoImpacto'] = $fila->procesoImpacto;

            $retorno[$contador]['idTipoSolucion'] = $fila->idTipoSolucion;

            $retorno[$contador]['tipoSolucion'] = $fila->tipoSolucion;

            $retorno[$contador]['idMotorBaseDatos'] = $fila->idMotorBaseDatos;

            $retorno[$contador]['motorBaseDatos'] = $fila->motorBaseDatos;

            $retorno[$contador]['idLenguajeProgramacion'] = $fila->idLenguajeProgramacion;

            $retorno[$contador]['leguajeProgramacion'] = $fila->leguajeProgramacion;

            $retorno[$contador]['tipoLicenciamiento'] = $fila->tipoLicenciamiento;

            $retorno[$contador]['derechosAutor'] = $fila->derechosAutor;

            $retorno[$contador]['nroImplementaciones'] = $fila->NroImplementaciones;

            $retorno[$contador]['idAprendiz'] = $fila->idAprendiz;

            $retorno[$contador]['numeroIdentificacion'] = $fila->numeroIdentificacion;

            $retorno[$contador]['nombreAprendiz'] = $fila->nombreAprendiz;

            $retorno[$contador]['idRegional'] = $fila->idRegional;

            $retorno[$contador]['regional'] = $fila->regional;

            $retorno[$contador]['idCentroFormacion'] = $fila->idCentroFormacion;

            $retorno[$contador]['centroFormacion'] = $fila->centroFormacion;

            $retorno[$contador]['cartaImplementacion'] = $fila->cartaImplementacion;

            $retorno[$contador]['nroImplementacionFuturo'] = $fila->nroImplementacionFuturo;

            $retorno[$contador]['desarrolloFuturo'] = $fila->desarrolloFuturo;

            $retorno[$contador]['equipoDesarroll'] = $fila->equipoDesarroll;

            $contador++;

        }

        return $retorno;

    }

    function obtenerCondicion(){

        $this->condicion = "";

        $this->whereAnd = " WHERE ";

        if ($this->idProyecto != "" && $this->idProyecto != "null") {

            $this->condicion = $this->condicion.$this->whereAnd." p.idProyecto = $this->idProyecto";

            $this->whereAnd = " AND ";

        }

        if ($this->aprendiz->getPersona()->getNumeroIdentificacion() != "" && $this->aprendiz->getPersona()->getNumeroIdentificacion() != "null") {

            $this->condicion = $this->condicion.$this->whereAnd." per.numeroIdentificacion = ".$this->aprendiz->getPersona()->getNumeroIdentificacion();

            $this->whereAnd = " AND ";

        }

        if ($this->tipoSolucion->getIdTipoSolucion() != "" && $this->tipoSolucion->getIdTipoSolucion() != "null") {

            $this->condicion = $this->condicion.$this->whereAnd." p.idTipoSolucion = ".$this->tipoSolucion->getIdTipoSolucion();

            $this->whereAnd = " AND ";

        }

        if ($this->centroFormacion->getRegional()->getIdRegional() != "" && $this->centroFormacion->getRegional()->getIdRegional() != "null") {

            $this->condicion = $this->condicion.$this->whereAnd." re.idRegional = ".$this->centroFormacion->getRegional()->getIdRegional();

            $this->whereAnd = " AND ";

        }

        if ($this->centroFormacion->getIdCentroFormacion() != "" && $this->centroFormacion->getIdCentroFormacion() != "null") {

            $this->condicion = $this->condicion.$this->whereAnd." p.idCentroFormacion = ".$this->centroFormacion->getIdCentroFormacion();

            $this->whereAnd = " AND ";

        }

    }

}
?>