<?php
namespace modelo;
class Persona {
    
   public $conexion;
   private $whereAnd;
   private $condicion;
    
    private $idPersona;
    private $tipoDocumentoIdentidad;
    private $numeroIdentificacion;
    private $foto;
    private $primerNombre;
    private $segundoNombre;
    private $primerApellido;
    private $segundoApellido;
    private $fechaNacimiento;
    private $correoElectronico;
    private $telefonoCelular;
    private $telefonoFijo;
    private $estado;
    private $genero;
    private $idUsuarioCreacion;
    private $fechaCreacion;
    private $idUsuarioModificacion;
    private $fechaModificacion;
    
    public function __construct(\entidad\Persona $persona) {
        $this->idPersona = $persona->getIdPersona();
        $this->tipoDocumentoIdentidad = $persona->getTipoDocumentoIdentidad();
        $this->numeroIdentificacion = $persona->getNumeroIdentificacion();
        $this->foto = $persona->getFoto();
        $this->primerNombre = $persona->getPrimerNombre();
        $this->segundoNombre = $persona->getSegundoNombre();
        $this->primerApellido = $persona->getPrimerApellido();
        $this->segundoApellido = $persona->getSegundoApellido();
        $this->fechaNacimiento = $persona->getFechaNacimiento();
        $this->correoElectronico = $persona->getCorreoElectronico();
        $this->telefonoCelular = $persona->getTelefonoCelular();
        $this->telefonoFijo = $persona->getTelefonoFijo();
        $this->estado = $persona->getEstado();
        $this->genero = $persona->getGenero();
        $this->idUsuarioCreacion = $persona->getIdUsuarioCreacion();
        $this->idUsuarioModificacion = $persona->getIdUsuarioModificacion();
        
        $this->conexion = new \Conexion();
        
    }
    public function adicionar(){
        
        $sentenciaSql = "
                        INSERT INTO 
                            persona
                        (
                            idTipoDocumentoIdentidad
                            ,numeroIdentificacion
                            ,foto
                            ,primerNombre
                            ,segundoNombre
                            ,primerApellido
                            ,segundoApellido
                            ,fechaNacimiento
                            ,correoElectronico
                            ,telefonoCelular
                            ,telefonoFijo
                            ,estado
        					,genero
                            ,idUsuarioCreacion
                            ,fechaCreacion
                            ,idUsuarioModificacion
                            ,fechaModificacion
                        )
                        VALUES
                        (
                            ".$this->tipoDocumentoIdentidad->getIdTipoDocumentoIdentidad()."
                            ,$this->numeroIdentificacion
                            ,'$this->foto'
                            ,'$this->primerNombre'
                            ,'$this->segundoNombre'
                            ,'$this->primerApellido'
                            ,'$this->segundoApellido'
                            ,'$this->fechaNacimiento'
                            ,'$this->correoElectronico'
                            ,$this->telefonoCelular
                            ,$this->telefonoFijo
                            ,$this->estado
                            ,'$this->genero'
                            ,$this->idUsuarioCreacion
                            ,NOW()
                            ,$this->idUsuarioModificacion
                            ,NOW()
                        )
                        ";
        $this->conexion->ejecutar($sentenciaSql);
    }
    function obtenerMaximo(){
    	$sentenciaSql = "
                            SELECT
                                MAX(idPersona) as maximo
                            FROM
                                persona
                            ";
    	$this->conexion->ejecutar($sentenciaSql);
    	$fila = $this->conexion->obtenerObjeto();
    	return $fila->maximo;
    }
}
?>
