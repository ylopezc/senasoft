<?php
namespace  modelo;
session_start();
require_once('../entorno/Conexion.php');
class InscriCategoConcurAprend{
	
	public $conexion;
	private $whereAnd;
	private $condicion;
	
	private $idInscriCategoConcurAprend;
	private $programaFormacion;
	private $centroFormacion;
	private $categoriaConcurso;
	private $aprendiz;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	private $estado;
	
	public function __construct(\entidad\InscriCategoConcurAprend $inscriCategoConcurAprend){
		$this->idInscriCategoConcurAprend = $inscriCategoConcurAprend->getIdInscriCategoConcurAprend();
		$this->programaFormacion = $inscriCategoConcurAprend->getProgramaFormacion();
		$this->centroFormacion = $inscriCategoConcurAprend->getCentroFormacion();
		$this->categoriaConcurso = $inscriCategoConcurAprend->getCategoriaConcurso();
		$this->aprendiz = $inscriCategoConcurAprend->getAprendiz();
		$this->idUsuarioCreacion = $inscriCategoConcurAprend->getIdUsuarioCreacion();
		$this->idUsuarioModificacion = $inscriCategoConcurAprend->getIdUsuarioModificacion();
		$this->estado = $inscriCategoConcurAprend->getEstado();
		
		$this->conexion = new \Conexion();
	}
	public function adicionar(){
		$sentenciaSql="
						INSERT INTO
							inscricategoconcuraprend
						(
							
							idProgramaFormacion
							,idCentroFormacion
							,idCategoriaConcurso
							,idAprendiz
							,idUsuarioCreacion
							,idUsuarioModificacion
							,fechaCreacion
							,fechaModificacion
							,estado
						)
						VALUES
						(
							
							".$this->programaFormacion->getIdProgramaFormacion()."
							,".$this->centroFormacion->getIdCentroFormacion()."
							,".$this->categoriaConcurso->getIdCategoriaConcurso()."
							,".$this->aprendiz->getIdAprendiz()."
							,$this->idUsuarioCreacion
							,$this->idUsuarioModificacion
							,NOW()
							,NOW()
							,$this->estado
						)
					";
		$this->conexion->ejecutar($sentenciaSql);
	}
	public function numerAprenInscrCateg(){
		$sentenciaSql="
						SELECT
							COUNT(icca.idCategoriaConcurso)  AS numeroAprendizCategoria,
							icca.idCategoriaConcurso
							,cc.limiteNumeroAprendiz
						FROM
							inscricategoconcuraprend AS icca
							INNER JOIN categoriaconcurso AS cc ON cc.idCategoriaConcurso = icca.idCategoriaConcurso
						WHERE	
							icca.idCategoriaConcurso = ".$this->categoriaConcurso->getIdCategoriaConcurso()."
						GROUP BY
							icca.idCategoriaConcurso
					";
		$this->conexion->ejecutar($sentenciaSql);
		$contador = 0; 
		while($fila = $this->conexion->obtenerObjeto()){
			$retorno[$contador]['numeroAprendicesCategoria'] = $fila->numeroAprendizCategoria;
			$retorno[$contador]['limiteNumeroAprendiz'] = $fila->limiteNumeroAprendiz;
			$contador++;
		}
		return $retorno;
	}
	public function validarLimitAprenCentrForma(){
		$sentenciaSql="
						SELECT 
							COUNT(icca.idCentroFormacion) AS numeroAprendicesInscritos
							,cf.limiteAprendices
						FROM
							inscricategoconcuraprend AS icca
							INNER JOIN centroformacion AS cf ON cf.idCentroFormacion = icca.idCentroFormacion 
						WHERE
							icca.idCentroFormacion  = ".$this->centroFormacion->getIdCentroFormacion()."
					";
		$this->conexion->ejecutar($sentenciaSql);
		$contador = 0;
		while($fila = $this->conexion->obtenerObjeto()){
			$retorno[$contador]['numeroAprendicesInscritos'] = $fila->numeroAprendicesInscritos;
			$retorno[$contador]['limiteAprendices'] = $fila->limiteAprendices;
			$contador++;
		}
		return $retorno;
	}
	public function validCategAprendCentrForma(){
		$sentenciaSql="
						SELECT 
							COUNT(idCentroFormacion) as numeroAprendicesCategoria
						FROM
							inscricategoconcuraprend
						WHERE
							idCentroFormacion = ".$this->centroFormacion->getIdCentroFormacion()." AND idCategoriaConcurso = ".$this->categoriaConcurso->getIdCategoriaConcurso()."
					";
		$this->conexion->ejecutar($sentenciaSql);
		$contador = 0;
		while($fila = $this->conexion->obtenerObjeto()){
			$retorno[$contador]['numeroAprendicesCategoria'] = $fila->numeroAprendicesCategoria;
		}
		return $retorno;
	}
	public function validarAprenInscrCateg(){
		$sentenciaSql="
						SELECT
							icca.idAprendiz
						FROM
							inscricategoconcuraprend AS icca
							INNER JOIN aprendiz AS a ON a.idAprendiz = icca.idAprendiz
							INNER JOIN persona AS p ON p.idPersona = a.idPersona
						WHERE
							p.numeroIdentificacion = ".$this->aprendiz->getPersona()->getNumeroIdentificacion()."
	 
					";
		$this->conexion->ejecutar($sentenciaSql);
	}
	public function consultar(){
		$this->obtenerCondicion();
		$sentenciaSql = "
					SELECT
						icca.idInscricategoconcuraprend
						,cf.centroFormacion
						,r.regional
						,p.foto
						,cc.categoriaConcurso
						,CONCAT_WS(' ',p.primerNombre,p.segundoNombre,p.primerApellido,p.segundoApellido) AS nombreCompleto
					FROM
						inscricategoconcuraprend AS icca
						INNER JOIN centroformacion AS cf ON cf.idCentroFormacion = icca.idCentroFormacion
						INNER JOIN aprendiz AS a ON a.idAprendiz = icca.idAprendiz
						INNER JOIN persona AS p ON p.idPersona = a.idPersona
						INNER JOIN regional AS r ON r.idRegional = cf.idRegional
						INNER JOIN categoriaconcurso AS cc ON cc.idCategoriaConcurso = icca.idCategoriaConcurso
						$this->condicion
					ORDER BY
						r.regional ASC , cf.centroFormacion ASC,cc.categoriaConcurso ASC
					##icca.idInscricategoconcuraprend,r.regional , cf.centroFormacion  
				";
		$this->conexion->ejecutar($sentenciaSql);
		$contador = 0;
		$contadorAprendiz = 1;
		while($fila = $this->conexion->obtenerObjeto()){
			$retorno[$contador]["centroFormacionApren$contadorAprendiz"] = $fila->centroFormacion;
			$retorno[$contador]["regionalApren$contadorAprendiz"] = $fila->regional;
			$retorno[$contador]["fotoApren$contadorAprendiz"] = $fila->foto;
			$retorno[$contador]["categoriaConcursoApren$contadorAprendiz"] = $fila->categoriaConcurso;
			$retorno[$contador]["nombreCompletoApren$contadorAprendiz"] = $fila->nombreCompleto;
			if($contadorAprendiz == 2){
				$contadorAprendiz = 1;
				$contador++;
			}else{
				$contadorAprendiz++;
			}
		}
		return $retorno;
	}
	public function obtenerCondicion() {
		$this->condicion = '';
		$this->whereAnd = ' WHERE ';
	
		if(isset($this->categoriaConcurso)){
			if($this->categoriaConcurso->getIdCategoriaConcurso() != '' && $this->categoriaConcurso->getIdCategoriaConcurso() != 'null'){
				$this->condicion = $this->condicion.$this->whereAnd." icca.idCategoriaConcurso = ".$this->categoriaConcurso->getIdCategoriaConcurso();
				$this->whereAnd = ' AND ';
			}
		}
		if(isset($this->aprendiz)){
			if($this->aprendiz->getPersona()->getNumeroIdentificacion() != '' && $this->aprendiz->getPersona()->getNumeroIdentificacion() != 'null'){
				$this->condicion = $this->condicion.$this->whereAnd." p.numeroIdentificacion = ".$this->aprendiz->getPersona()->getNumeroIdentificacion();
				$this->whereAnd = ' AND ';
			}
		}
		if(isset($this->centroFormacion)){
			if($this->centroFormacion->getIdCentroFormacion() != '' && $this->centroFormacion->getIdCentroFormacion() != 'null'){
				$this->condicion = $this->condicion.$this->whereAnd." icca.idCentroFormacion = ".$this->centroFormacion->getIdCentroFormacion();
				$this->whereAnd = ' AND ';
			}
			$objetoEvaluar = $this->centroFormacion->getRegional(); 
			if(isset($objetoEvaluar)){
				if($this->centroFormacion->getRegional()->getIdRegional() != '' && $this->centroFormacion->getRegional()->getIdRegional() != 'null'){
					$this->condicion = $this->condicion.$this->whereAnd." cf.idRegional = ".$this->centroFormacion->getRegional()->getIdRegional();
					$this->whereAnd = ' AND ';
				}
			}
		}
	}
}
?>