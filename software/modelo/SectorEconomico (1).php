<?php
namespace  modelo;
session_start();
require_once('../entorno/Conexion.php');
class SectorEconomico{
	public $conexion = null;
	private $whereAnd = null;
	private $condicion = null;
	
	private $idSectorEconomico;
	private $sectorEconomico;
	private $estado;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	
	public function  __construct(\entidad\SectorEconomico $sectorEconomico){
		$this->idSectorEconomico =  $sectorEconomico->getIdSectorEconomico();
		$this->sectorEconomico = $sectorEconomico->getSectorEconomico();
		$this->estado = $sectorEconomico->getEstado();
		$this->idUsuarioCreacion = $sectorEconomico->getIdUsuarioCreacion();
		$this->idUsuarioModificacion = $sectorEconomico->getIdUsuarioModificacion();
		
		$this->conexion = new \Conexion();
	}
	public function consultar(){
		$this->obtenerCondicion();
		$sentenciaSql = "
							SELECT
								se.idSectorEconomico
								,se.sectorEconomico
								,se.estado
							FROM
								sectoreconomico as se
								$this->condicion
						";
		$this->conexion->ejecutar($sentenciaSql);
		while($fila = $this->conexion->obtenerObjeto()){
			$retorno[$contador]['idSectorEconomico'] =  $fila->idSectorEconomico;
			$retorno[$contador]['sectorEconomico'] =  $fila->sectorEconomico;
			$retorno[$contador]['estado'] =  $fila->estado;
			$contador++;
		}
		return $retorno;
	}
	
	function obtenerCondicion(){
		$this->condicion = '';
		$this->whereAnd = ' WHERE ';
	
		if($this->idSectorEconomico != '' && $this->idSectorEconomico != 'null'){
			$this->condicion = $this->condicion.$this->whereAnd." se.idSectorEconomico = $this->idSectorEconomico";
			$this->whereAnd = ' AND ';
		}
		if($this->sectorEconomico != ''){
			$this->condicion = $this->condicion.$this->whereAnd." se.sectorEconomico LIKE '%$this->sectorEconomico%'";
			$this->whereAnd = ' AND ';
		}
		if($this->estado != ''){
			$this->condicion = $this->condicion.$this->whereAnd." se.estado = $this->estado";
			$this->whereAnd = ' AND ';
		}
	}
}
?>