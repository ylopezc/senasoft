<?php
namespace modelo;
session_start();
require_once '../entorno/Conexion.php';
class AsistenteRuedaNegocio{
	
	public $conexion = null;
	private $condicion;
	private $whereAnd;
	
	private $idAsistenteRuedaNegocio;
	private $asistenteRuedaNegocio;
	private $cargo;
	private $correoElectronico;
	private $telefono;
	private $horaAsistencia;
	private $idEmpresa;
	private $estado;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	
	public function __construct(\entidad\AsistenteRuedaNegocio $asistenteRuedaNegocio){
		$this->idAsistenteRuedaNegocio = $asistenteRuedaNegocio->getIdAsistenteRuedaNegocio();
		$this->asistenteRuedaNegocio = $asistenteRuedaNegocio->getAsistenteRuedaNegocio();
		$this->cargo = $asistenteRuedaNegocio->getCargo();
		$this->correoElectronico = $asistenteRuedaNegocio->getCorreoElectronico();
		$this->telefono = $asistenteRuedaNegocio->getTelefono();
		$this->horaAsistencia = $asistenteRuedaNegocio->getHoraAsistencia();
		$this->idEmpresa = $asistenteRuedaNegocio->getIdEmpresa();
		$this->estado = $asistenteRuedaNegocio->getEstado();
		$this->idUsuarioCreacion = $asistenteRuedaNegocio->getIdUsuarioCreacion(); 
		$this->idUsuarioModificacion = $asistenteRuedaNegocio->getIdUsuarioModificacion();
		
		$this->conexion = new \Conexion();
	}
	public function adicionar(){
		$sentenciaSql = "
							INSERT INTO
								asistenteruedanegocio
							(
								asistenteRuedaNegocio
								,cargo
								,correoElectronico
								,telefono
								,horaAsistencia
								,idEmpresa
								,estado
								,idUsuarioCreacion
								,idUsuarioModificacion
								,fechaCreacion
								,fechaModificacion
							)
							VALUES
							(
								'$this->asistenteRuedaNegocio'
								,'$this->cargo'
								,'$this->correoElectronico'
								,$this->telefono
								,'$this->horaAsistencia'
								,$this->idEmpresa
								,$this->estado
								,$this->idUsuarioCreacion
								,$this->idUsuarioModificacion
								,NOW()
								,NOW()
							)
						";
		$this->conexion->ejecutar($sentenciaSql);
	}
}
?>