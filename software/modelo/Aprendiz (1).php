<?php
namespace  modelo;
session_start();
require_once('../entorno/Conexion.php');
class Aprendiz{
	
	public $conexion;
	private $whereAnd;
	private $condicion;
	
	private $idAprendiz;
	private $persona;
	private $programaFormacion;
	private $centroFormacion;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	private $estado;
	
	public function __construct(\entidad\Aprendiz $aprendiz){
		$this->idAprendiz = $aprendiz->getIdAprendiz();
		$this->persona = $aprendiz->getPersona();
		$this->programaFormacion = $aprendiz->getProgramaFormacion();
		$this->centroFormacion = $aprendiz->getCentroFormacion();
		$this->idUsuarioCreacion = $aprendiz->getIdUsuarioCreacion();
		$this->idUsuarioModificacion = $aprendiz->getIdUsuarioModificacion();
		$this->estado = $aprendiz->getEstado();
		
		$this->conexion = new \Conexion();
	}
	public function adicionar(){
		$sentenciaSql = "
							INSERT INTO
								aprendiz
							(
								idPersona
								,idProgramaFormacion
								,idCentroFormacion
								,estado
								,idUsuarioCreacion
								,idUsuarioModificacion
								,fechaCreacion
								,fechaModificacion
							)
							VALUES
							(
								".$this->persona->getIdPersona()."
								,".$this->programaFormacion->getIdProgramaFormacion()."
								,".$this->centroFormacion->getIdCentroFormacion()."
								,$this->estado
								,$this->idUsuarioCreacion
								,$this->idUsuarioModificacion
								,NOW()
								,NOW()		
							)
						";
		$this->conexion->ejecutar($sentenciaSql);
	}
	function obtenerMaximo(){
		$sentenciaSql = "
                            SELECT
                                MAX(idAprendiz) as maximo
                            FROM
                                aprendiz
                            ";
		$this->conexion->ejecutar($sentenciaSql);
		$fila = $this->conexion->obtenerObjeto();
		return $fila->maximo;
	}
        public function buscarAprendiz() {
          $sentenciaSql = "
                            SELECT
                                a.idAprendiz
                                , a.idPersona
                                , p.idTipoDocumentoIdentidad
                                , a.idProgramaFormacion
                                , p.numeroIdentificacion
                                , p.foto
                                , p.primerNombre AS nombre
                                , p.primerApellido
                                , p.segundoApellido
                                , p.fechaNacimiento
                                , p.correoElectronico
                                , p.telefonoCelular
                                , p.telefonoFijo
                                , p.genero
                                , pia.idPersonaInfoAdicional
                                , pia.rh
                                , pia.tallaCamiseta
                                , pia.cuidadoMedico
                            FROM
                                aprendiz AS a
                                INNER JOIN persona AS p ON p.idPersona = a.idPersona
                                INNER JOIN personainfoadicional AS pia ON pia.idPersona = p.idPersona
                            WHERE
                                p.numeroIdentificacion = ".$this->persona->getNumeroIdentificacion();
            
            $this->conexion->ejecutar($sentenciaSql);
            while ($fila = $this->conexion->obtenerObjeto()) {
                $retorno[$contador]['idAprendiz'] = $fila->idAprendiz;
                $retorno[$contador]['idPersona'] = $fila->idPersona;
                $retorno[$contador]['idTipoDocumentoIdentidad'] = $fila->idTipoDocumentoIdentidad;
                $retorno[$contador]['idProgramaFormacion'] = $fila->idProgramaFormacion;
                $retorno[$contador]['numeroIdentificacion'] = $fila->numeroIdentificacion;
                $retorno[$contador]['foto'] = $fila->foto;
                $retorno[$contador]['nombre'] = $fila->nombre;
                $retorno[$contador]['primerApellido'] = $fila->primerApellido;
                $retorno[$contador]['segundoApellido'] = $fila->segundoApellido;
                $retorno[$contador]['fechaNacimiento'] = $fila->fechaNacimiento;
                $retorno[$contador]['correoElectronico'] = $fila->correoElectronico;
                $retorno[$contador]['telefonoCelular'] = $fila->telefonoCelular;
                $retorno[$contador]['telefonoFijo'] = $fila->telefonoFijo;
                $retorno[$contador]['genero'] = $fila->genero;
                $retorno[$contador]['rh'] = $fila->rh;
                $retorno[$contador]['tallaCamiseta'] = $fila->tallaCamiseta;
                $retorno[$contador]['cuidadoMedico'] = $fila->cuidadoMedico;
                $contador++;
            }
            return $retorno;
        }
}
?>