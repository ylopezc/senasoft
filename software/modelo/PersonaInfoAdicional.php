<?php
namespace  modelo;
session_start();
require_once('../entorno/Conexion.php');
class PersonaInfoAdicional{
	
	public $conexion;
	private $whereAnd;
	private $condicion;
	
	private $idPersonaInfoAdicional;
	private $persona;
	private $rh;
	private $tallaCamiseta;
	private $cuidadoMedico;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	private $estado;
	
	public function __construct(\entidad\PersonaInfoAdicional $personaInfoAdicional){
		$this->idPersonaInfoAdicional = $personaInfoAdicional->getIdPersonaInfoAdicional() ; 
		$this->persona  = $personaInfoAdicional->getPersona();
		$this->rh  = $personaInfoAdicional->getRh();
		$this->tallaCamiseta  = $personaInfoAdicional->getTallaCamiseta();
		$this->cuidadoMedico  = $personaInfoAdicional->getCuidadoMedico();
		$this->idUsuarioCreacion  = $personaInfoAdicional->getIdUsuarioCreacion();
		$this->idUsuarioModificacion  = $personaInfoAdicional->getIdUsuarioModificacion();
		$this->estado  = $personaInfoAdicional->getEstado();
		
		$this->conexion = new \Conexion();
	}
	public function adicionar(){
		$sentenciaSql = "
							INSERT INTO
								personainfoadicional
							(
								idPersona
								,rh
								,tallaCamiseta
								,cuidadoMedico
								,estado
								,idUsuarioCreacion
								,idUsuarioModificacion
								,fechaCreacion
								,fechaModificacion
							)
							VALUES
							(
								".$this->persona->getIdPersona()."
								,'$this->rh'
								,'$this->tallaCamiseta'
								,'$this->cuidadoMedico'
								,$this->estado
								,$this->idUsuarioCreacion
								,$this->idUsuarioModificacion
								,NOW()
								,NOW()	
							)
						";
		$this->conexion->ejecutar($sentenciaSql);
	}
	
}
?>