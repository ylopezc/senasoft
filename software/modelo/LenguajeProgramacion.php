<?php
namespace modelo;
require_once '../entorno/Conexion.php';
class LenguajeProgramacion {
    public $conexion = null;
    private $condicion;
    private $whereAnd;
    
    private $idLenguajeProgramacion;
    private $lenguajeProgramacion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    private $estado;
    
    public function __construct(\entidad\LenguajeProgramacion $lenguajeProgramacion) {
        $this->idLenguajeProgramacion = $lenguajeProgramacion->getIdLenguajeProgramacion();
        $this->lenguajeProgramacion = $lenguajeProgramacion->getLenguajeProgramacion();
        $this->idUsuarioCreacion = $lenguajeProgramacion->getIdUsuarioCreacion();
        $this->idUsuarioModificacion = $lenguajeProgramacion->getIdUsuarioModificacion();
        $this->fechaCreacion = $lenguajeProgramacion->getFechaCreacion();
        $this->fechaModificacion = $lenguajeProgramacion->getFechaModificacion();
        $this->estado = $lenguajeProgramacion->getEstado();
        
        $this->conexion = new \Conexion();
    }
}
