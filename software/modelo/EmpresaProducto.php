<?php
namespace modelo;
session_start();
require_once '../entorno/Conexion.php';

class EmpresaProducto {
    public $conexion = null;
    private $condicion;
    private $whereAnd;
    
    private $idEmpresaProducto;
    private $empresaProducto;
    private $idEmpresa;
    private $idCategoriaProducto;
    private $estado;
    private $idTipoEmpresaProducto;
    private $frecuencia;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    
    public function __construct(\entidad\EmpresaProducto $empresaProducto) {
        $this->idEmpresaProducto = $empresaProducto->getIdEmpresa();
        $this->empresaProducto = $empresaProducto->getEmpresaProducto();
        $this->idEmpresa = $empresaProducto->getIdEmpresa();
        $this->idCategoriaProducto = $empresaProducto->getIdCategoriaProducto();       
        $this->estado = $empresaProducto->getEstado();
        $this->idTipoEmpresaProducto = $empresaProducto->getIdTipoEmpresaProducto();
        $this->frecuencia = $empresaProducto->getFrecuencia();
        $this->idUsuarioCreacion = $empresaProducto->getIdUsuarioCreacion();
        $this->idUsuarioModificacion = $empresaProducto->getIdUsuarioModificacion();
        $this->fechaCreacion = $empresaProducto->getFechaCreacion();
        $this->fechaModificacion = $empresaProducto->getFechaModificacion();
        
        
        $this->conexion = new \Conexion();
    }
    
    public function adicionar() {
          $sentenciaSql = "
                        INSERT INTO 
                            empresaproducto
                        (
                             empresaProducto
                            , idEmpresa
                            , idCategoriaProducto
                            ,idTipoEmpresaProducto
                            ,frecuencia
                            , fechaCreacion
                            , fechaModificacion                           
                        )
                        VALUES
                        (
                            
                             '$this->empresaProducto'
                            , $this->idEmpresa
                            , $this->idCategoriaProducto 
                            , $this->idTipoEmpresaProducto
                            , '$this->frecuencia'
                            , NOW()
                            , NOW()                           
                               
                        )
                        ";
        $this->conexion->ejecutar($sentenciaSql);
    }
    public function modificar(){
       $sentenciaSql = "
                        UPDATE 
                            empresaProducto
                        SET
                             empresaProducto = '$this->empresaProducto'
                            , idEmpresa = $this->idEmpresa
                            , idCategoriaProducto = $this->idCategoriaProducto
                            , estado = '$this->estado'
                            , idUsuarioModificacion = $this->idUsuarioModificacion
                            , fechaModificacion = NOW()
                        WHERE
                            idEmpresaProducto = $this->idEmpresaProducto
                        ";
       $this->conexion->ejecutar($sentenciaSql);
    }
    public function consultar() {
        $this->obtenerCondicion();
         $sentenciaSql = "
                        SELECT
                            *
                        FROM
                            empresaProducto
                            $this->condicion
                        ";
        $this->conexion->ejecutar($sentenciaSql);
    }
    function obtenerCondicion(){
        $this->condicion = '';
        $this->whereAnd = ' WHERE ';
        
        if($this->idEmpresaProducto!= '' && $this->idEmpresaProducto!= 'null'){
            $this->condicion = $this->condicion.$this->whereAnd." idEmpresaProducto = $this->idEmpresaProducto";
            $this->whereAnd = ' AND ';
        }
        if($this->empresaProducto != ''){
            $this->condicion = $this->condicion.$this->whereAnd." empresaProducto LIKE '%$this->empresaProducto%'";
            $this->whereAnd = ' AND ';
        }
        
        if($this->idEmpresa != '' && $this->idEmpresa != 'undefined'){
            $this->condicion = $this->condicion.$this->whereAnd." idEmpresa = $this->idEmpresa";
            $this->whereAnd = ' AND ';
        }
        if($this->estado != ''){
            $this->condicion = $this->condicion.$this->whereAnd." estado = $this->estado";
            $this->whereAnd = ' AND ';
        }
    }
}
?>