<?php
namespace modelo;
require_once '../entorno/Conexion.php';
class TipoSolucion {
    public $conexion = null;
    private $condicion;
    private $whereAnd;

    private $idTipoSolucion;
    private $tipoSolucion;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    private $estado;
    
    public function __construct(\entidad\TipoSolucion $tipoSolucion) {
        $this->idTipoSolucion = $tipoSolucion->getIdTipoSolucion();
        $this->tipoSolucion = $tipoSolucion->getTipoSolucion();
        $this->idUsuarioCreacion = $tipoSolucion->getIdUsuarioCreacion();
        $this->idUsuarioModificacion = $tipoSolucion->getIdUsuarioModificacion();
        $this->fechaCreacion = $tipoSolucion->getFechaCreacion();
        $this->fechaModificacion = $tipoSolucion->getFechaModificacion();
        $this->estado = $tipoSolucion->getEstado();
        
        $this->conexion = new \Conexion();
    }
}
