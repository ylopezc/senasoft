<?php
namespace  modelo;
session_start();
require_once('../entorno/Conexion.php');

class RegimenEmpresa{
	public $conexion = null;
	private $whereAnd = null;
	private $condicion = null;
	
	private $idRegimenEmpresa;
	private $regimenEmpresa;
	private $estado;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	private $fechaCreacion;
	private $fechaModificacion;
	
	public function __construct(\entidad\RegimenEmpresa $regimenEmpresa){
		$this->idRegimenEmpresa = $regimenEmpresa->getIdRegimenEmpresa() ;
		$this->regimenEmpresa  = $regimenEmpresa->getRegimenEmpresa();
		$this->estado =  $regimenEmpresa->getFechaCreacion();
		$this->idUsuarioCreacion  = $regimenEmpresa->getIdUsuarioCreacion();
		$this->idUsuarioModificacion =  $regimenEmpresa->getIdUsuarioModificacion();
		
		$this->conexion =  new \Conexion();
		
	}
	public function consultar(){
		$this->obtenerCondicion();
		$sentenciaSql = "
							SELECT
								re.idRegimenEmpresa
								,re.regimenEmpresa
								,re.estado
							FROM
								regimenempresa as re
								$this->condicion
						";
		$this->conexion->ejecutar($sentenciaSql);
		$contador = 0;
		while($fila = $this->conexion->obtenerObjeto()){
			$retorno[$contador]['idRegimenEmpresa'] = $fila->idRegimenEmpresa;
			$retorno[$contador]['regimenEmpresa'] = $fila->regimenEmpresa;
			$retorno[$contador]['estado'] = $fila->estado;
			
			$contador++;
		}
		return $retorno;
	}
	function obtenerCondicion(){
		$this->condicion = '';
		$this->whereAnd = ' WHERE ';
	
		if($this->idRegimenEmpresa != '' && $this->idRegimenEmpresa != 'null'){
			$this->condicion = $this->condicion.$this->whereAnd." re.idRegimenEmpresa = $this->idRegimenEmpresa";
			$this->whereAnd = ' AND ';
		}
		if($this->regimenEmpresa != ''){
			$this->condicion = $this->condicion.$this->whereAnd." re.regimenEmpresa LIKE '%$this->regimenEmpresa%'";
			$this->whereAnd = ' AND ';
		}
		if($this->estado != ''){
			$this->condicion = $this->condicion.$this->whereAnd." re.estado = $this->estado";
			$this->whereAnd = ' AND ';
		}
	}	
}
?>