<?php

namespace modelo;

class CategoriaProducto {
    public $conexion;
    private $whereAnd;
    private $condicion;
    
    private $idCategoriaProducto;
    private $categoriaProducto;
    private $estado;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    
    public function __construct(\entidad\CategoriaProducto $categoriaProducto) {
       $this->idCategoriaProducto = $categoriaProducto->getIdCategoriaProducto();
       $this->categoriaProducto = $categoriaProducto->getIdCategoriaProducto();
       $this->estado = $categoriaProducto->getEstado();
       $this->idUsuarioCreacion = $categoriaProducto->getIdUsuarioCreacion();
       $this->idUsuarioModificacion = $categoriaProducto->getIdUsuarioModificacion();
       $this->fechaCreacion = $categoriaProducto->getFechaCreacion();
       $this->fechaModificacion = $categoriaProducto->getFechaModificacion();
       
       $this->conexion = new \Conexion();
   }
   public function consultar(){
       $this->obtenerCondicion();
       $sentenciaSql = "
                            SELECT 
                                  cp.idCategoriaProducto
                                  ,cp.categoriaProducto
                            FROM
                                categoriaproducto as cp
                            $this->condicion
                        ";
       $this->conexion->ejecutar($sentenciaSql);
       $contador = 0;
       while($fila = $this->conexion->obtenerObjeto()){
           $retorno[$contador]['idCategoriaProducto'] = $fila->idCategoriaProducto;
           $retorno[$contador]['categoriaProducto'] = $fila->categoriaProducto;
          
       }
   }
   public function obtenerCondicion() {
        $this->condicion = '';
        $this->whereAnd = ' WHERE ';
        
        if($this->idCategoriaProducto != '' && $this->idCategoriaProducto != 'null'){
            $this->condicion = $this->condicion.$this->whereAnd." cp.idCategoriaProducto = $this->idCategoriaProducto";
            $this->whereAnd = ' AND ';
        }
	if($this->categoriaProducto != '' && $this->categoriaProducto != 'null'){
            $this->condicion = $this->condicion.$this->whereAnd." cp.categoriaProducto = '$this->categoriaProducto'";
            $this->whereAnd = ' AND ';
        }
        if($this->estado != ''){
            $this->condicion = $this->condicion.$this->whereAnd." estado = $this->estado";
            $this->whereAnd = ' AND ';
        }
    }
}

?>
