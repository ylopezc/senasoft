<?php
namespace  modelo;
require_once '../entorno/Conexion.php';
class PreguntaFrecuente{
	public  $conexion = null;
	private $whereAnd = '';
	private $condicion = '';
	
	private $idPreguntaFrecuente;
	private $preguntaFrecuente;
	private $respuesta;
	private $estado;
	private $fechaCreacion;
	private $fechaModificacion;
	private $idUsuarioCreacion;
	private $idUsuarioModificacion;
	
	public function __construct(\entidad\PreguntaFrecuente $preguntaFrecuente){
		$this->idPreguntaFrecuente =  $preguntaFrecuente->getIdPreguntaFrecuente();
		$this->preguntaFrecuente =  $preguntaFrecuente->getPreguntaFrecuente();
		$this->respuesta =  $preguntaFrecuente->getRespuesta();
		$this->estado =  $preguntaFrecuente->getEstado();
		$this->idUsuarioModificacion =  $preguntaFrecuente->getIdUsuarioModificacion();
		$this->idUsuarioCreacion =  $preguntaFrecuente->getIdUsuarioCreacion();
		
		$this->conexion =  new \Conexion(); 
		
	}
	public function adicionar(){
		$sentenciaSql = "
							INSERT INTO
								preguntafrecuente
							(
								preguntaFrecuente
								,respuesta
								,estado
								,idUsuarioCreacion
								,idUsuarioModificacion
								,fechaCreacion
								,fechaModificacion
				
							)
							VALUES
							(
								'$this->preguntaFrecuente'
								,'$this->respuesta'
								,$this->estado
								,$this->idUsuarioCreacion
								,$this->idUsuarioModificacion
								,NOW()
								,NOW()
							)
						";
		$this->conexion->ejecutar($sentenciaSql);
		
	}
	public function modificar(){
		$sentenciaSql = "
							UPDATE
								preguntafrecuente
							SET
								preguntaFrecuente = '$this->preguntaFrecuente'
								,respuesta = '$this->respuesta'
								,estado = $this->estado
							WHERE
								idPreguntaFrecuente = $this->idPreguntaFrecuente
						";
		$this->conexion->ejecutar($sentenciaSql);
	}
	public  function inactivar(){
		$sentenciaSql = "
							UPDATE
								preguntafrecuente
							SET
								estado = 0
							WHERE
								idPreguntaFrecuente = $this->idPreguntaFrecuente
						";
		$this->conexion->ejecutar($sentenciaSql);
	}
	public function consultar(){
		$this->obtenerCondicion();
		$sentenciaSql = "
							SELECT
								pf.idPreguntaFrecuente
								,preguntaFrecuente
								,pf.respuesta
								,pf.estado
							FROM
								preguntafrecuente as pf
								$this->condicion
						";
		$this->conexion->ejecutar($sentenciaSql);
		$contador = 0;
		while($fila = $this->conexion->obtenerObjeto()){
			$retorno[$contador]['idPreguntaFrecuente'] = $fila->idPreguntaFrecuente;
			$retorno[$contador]['preguntaFrecuente'] = $fila->preguntaFrecuente;
			$retorno[$contador]['respuesta'] = $fila->respuesta;
			$retorno[$contador]['estado'] = $fila->estado;
			$contador++;
		}
		return $retorno;
		
	}
	public function obtenerCondicion() {
		$this->condicion = '';
		$this->whereAnd = ' WHERE ';
		
		if($this->idPreguntaFrecuente != '' && $this->idPreguntaFrecuente != 'null'){
			$this->condicion = $this->condicion.$this->whereAnd." pf.idPreguntaFrecuente= $this->idPreguntaFrecuente";
			$this->whereAnd = ' AND ';
		}
	
		if($this->preguntaFrecuente != '' && $this->preguntaFrecuente != 'null'){
			$this->condicion = $this->condicion.$this->whereAnd." pf.preguntaFrecuente LIKE '%$this->preguntaFrecuente%'";
			$this->whereAnd = ' AND ';
		}
		if($this->respuesta != '' && $this->respuesta != 'null'){
			$this->condicion = $this->condicion.$this->whereAnd." pf.respuesta LIKE '%$this->respuesta%'";
			$this->whereAnd = ' AND ';
		}
		if($this->estado != '' && $this->estado != 'null'){
			$this->condicion = $this->condicion.$this->whereAnd." pf.estado = $this->estado";
			$this->whereAnd = ' AND ';
		}
	}	
}
?>