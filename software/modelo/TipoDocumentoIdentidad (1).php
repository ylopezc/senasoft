<?php
namespace modelo;
session_start();
require_once('../entorno/Conexion.php');
class TipoDocumentoIdentidad{
   
   public $conexion;
   private $whereAnd;
   private $condicion;
    
   private $idTipoDocumentoIdentidad;
   private $tipoDocumentoIdentidad;
   private $codigo;
   private $estado;
   private $idUsuarioCreacion;
   private $idUsuarioModificacion;
   private $fechaCreacion;
   private $fechaModificacion;
   
   public function __construct(\entidad\TipoDocumentoIdentidad $tipoDocumentoIdentidad) {
       $this->idTipoDocumentoIdentidad = $tipoDocumentoIdentidad->getIdTipoDocumentoIdentidad();
       $this->tipoDocumentoIdentidad = $tipoDocumentoIdentidad->getTipoDocumentoIdentidad();
       $this->codigo = $tipoDocumentoIdentidad->getCodigo();
       $this->estado = $tipoDocumentoIdentidad->getEstado();
       $this->idUsuarioCreacion = $tipoDocumentoIdentidad->getIdUsuarioCreacion();
       $this->idUsuarioModificacion = $tipoDocumentoIdentidad->getIdUsuarioModificacion();
       
       $this->conexion = new \Conexion();
   }
   public function consultar(){
       $this->obtenerCondicion();
       $sentenciaSql = "
                            SELECT 
                                  tdi.idTipoDocumentoIdentidad
                                  ,tdi.tipoDocumentoIdentidadCol
                                  ,tdi.codigo
                            FROM
                                tipodocumentoidentidad as tdi
                            $this->condicion
                        ";
       $this->conexion->ejecutar($sentenciaSql);
       $contador = 0;
       while($fila = $this->conexion->obtenerObjeto()){
           $retorno[$contador]['idTipoDocumIdent'] = $fila->idTipoDocumentoIdentidad;
           $retorno[$contador]['tipoDocumIdent'] = $fila->tipoDocumentoIdentidadCol;
           $retorno[$contador]['codigo'] = $fila->codigo;
           $contador++;
       }
       return $retorno;
   }
   public function obtenerCondicion() {
        $this->condicion = '';
        $this->whereAnd = ' WHERE ';
        
        if($this->idTipoDocumentoIdentidad != '' && $this->idTipoDocumentoIdentidad != 'null'){
            $this->condicion = $this->condicion.$this->whereAnd." tdi.idTipoDocumentoIdentidad = $this->idCanal";
            $this->whereAnd = ' AND ';
        }
		if($this->tipoDocumentoIdentidad != '' && $this->tipoDocumentoIdentidad != 'null'){
            $this->condicion = $this->condicion.$this->whereAnd." tdi.tipoDocumentoIdentidad = '$this->canal'";
            $this->whereAnd = ' AND ';
        }
        if($this->estado != '' && $this->estado != 'null'){
            $this->condicion = $this->condicion.$this->whereAnd." tdi.estado = $this->estado";
            $this->whereAnd = ' AND ';
        }
    }
}
?>
