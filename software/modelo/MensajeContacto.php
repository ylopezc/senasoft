<?php
namespace modelo;
require_once '../entorno/Conexion.php';
class MensajeContacto {
    public $conexion = null;
    private $condicion;
    private $whereAnd;
    
    private $idMensajeContacto;
    private $remitente;
    private $mensajeContacto;
    private $rol;
    private $asunto;
    private $centroFormacion;
    private $correoElectronico;
    private $estado;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    
    public function __construct(\entidad\MensajeContacto $mensajeContacto) {
        $this->idMensajeContacto = $mensajeContacto->getIdMensajeContacto();
        $this->remitente = $mensajeContacto->getRemitente();
        $this->mensajeContacto = $mensajeContacto->getMensajeContacto();
        $this->rol = $mensajeContacto->getRol();
        $this->asunto = $mensajeContacto->getAsunto();
        $this->centroFormacion = $mensajeContacto->getCentroFormacion();
        $this->correoElectronico = $mensajeContacto->getCorreoElectronico();
        $this->estado = $mensajeContacto->getEstado();
        $this->idUsuarioCreacion = $mensajeContacto->getIdUsuarioCreacion();
        $this->idUsuarioModificacion = $mensajeContacto->getIdUsuarioModificacion();
        $this->fechaCreacion = $mensajeContacto->getFechaCreacion();
        $this->fechaModificacion = $mensajeContacto->getFechaModificacion();
        
        $this->conexion = new \Conexion();
    }
    public function adicionar() {
        $sentenciaSql = "
                        INSERT INTO
                            mensajecontacto
                        (
                            remitente
                            , mensajeContacto
                            , rol
                            , asunto
                            , idCentroFormacion
                            , correoElectronico
                            , estado
                            , idUsuarioCreacion
                            , idUsuarioModificacion
                            , fechaCreacion
                            , fechaModificacion
                        )
                        VALUES
                        (
                            '$this->remitente'
                            , '$this->mensajeContacto'
                            , '$this->rol'
                            , '$this->asunto'
                            , ".$this->centroFormacion->getIdCentroFormacion()."
                            , '$this->correoElectronico'
                            , 1
                            , 1
                            , 1
                            , NOW()
                            , NOW()
                        )
                        ";
        $this->conexion->ejecutar($sentenciaSql);
    }
}
?>