<?php
namespace modelo;
class Frecuencia {
    private static $frecuencia;
    public static function getFrecuencia(){
        Frecuencia::$frecuencia = array("Diaria", "Semanal", "Quincenal", "Mensual", "Bimensual", "Trimestral", "Semestral", "Anual","Ninguna");
        return Frecuencia::$frecuencia;
    }
}
?>