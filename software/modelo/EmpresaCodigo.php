<?php
namespace modelo;
session_start();
require_once '../entorno/Conexion.php';
class EmpresaCodigo {
    public $conexion = null;
    private $condicion;
    private $whereAnd;
    
    private $idEmpresaCodigo;
    private $empresaCodigo;
    private $nit;
    private $empresa;
    private $correoElectronico;
    private $idEstadoEmpresa;
    private $estado;
    private $idUsuarioCreacion;
    private $idUsuarioModificacion;
    private $fechaCreacion;
    private $fechaModificacion;
    
    public function __construct(\entidad\EmpresaCodigo $empresaCodigo) {
        $this->idEmpresaCodigo = $empresaCodigo->getIdEmpresaCodigo();
        $this->empresaCodigo = $empresaCodigo->getEmpresaCodigo();
        $this->nit = $empresaCodigo->getNit();
        $this->empresa = $empresaCodigo->getEmpresa();
        $this->correoElectronico = $empresaCodigo->getCorreoElectronico();
        $this->idEstadoEmpresa = $empresaCodigo->getIdEstadoEmpresa();
        $this->estado = $empresaCodigo->getEstado();
        $this->idUsuarioCreacion = $empresaCodigo->getIdUsuarioCreacion();
        $this->idUsuarioModificacion = $empresaCodigo->getIdUsuarioModificacion();
        $this->fechaCreacion = $empresaCodigo->getFechaCreacion();
        $this->fechaModificacion = $empresaCodigo->getFechaModificacion();
        
        
        $this->conexion = new \Conexion();
    }
    
    public function adicionar() {
          $sentenciaSql = "
                        INSERT INTO 
                            empresacodigo
                        (
                             empresacodigo
                            ,nit
                            ,empresa
                            ,correoElectronico
                            ,idEstadoEmpresa
                            , fechaCreacion
                            , fechaModificacion                           
                        )
                        VALUES
                        (
                            
                             '$this->empresaCodigo'
                             ,'$this->nit'
                             ,'$this->empresa'
                             ,'$this->correoElectronico'
                             ,$this->idEstadoEmpresa
                            , NOW()
                            , NOW()                                                         
                        )
                        ";
        $this->conexion->ejecutar($sentenciaSql);
    }
    public function modificar(){
       $sentenciaSql = "
                        UPDATE 
                            empresacodigo
                        SET
                             empresaCodigo = '$this->empresaCodigo'
                            , estado = '$this->estado'
                            , fechaModificacion = NOW()
                        WHERE
                            idEmpresaCodigo = $this->idEmpresaProducto
                        ";
       $this->conexion->ejecutar($sentenciaSql);
    }
    public function consultar() {
        $this->obtenerCondicion();
        $sentenciaSql = "
                        SELECT
                            ec.idEmpresaCodigo
                            ,ec.empresaCodigo
                            ,ec.idEstadoEmpresa
                            ,ec.empresa
                            ,ec.nit
                            ,ec.correoElectronico
                            ,ee.estadoEmpresa
                        FROM
                            empresacodigo as ec
                            INNER JOIN estadoempresa as ee ON ee.idEstadoEmpresa = ec.idEstadoEmpresa 
                            $this->condicion
                        ";
        $this->conexion->ejecutar($sentenciaSql);
        $contador = 0;

        while ($fila = $this->conexion->obtenerObjeto()) {
            $retorno[$contador]['idEmpresaCodigo'] = $fila->idEmpresaCodigo;
            $retorno[$contador]['empresaCodigo'] = $fila->empresaCodigo;
            $retorno[$contador]['empresa'] = $fila->empresa;
            $retorno[$contador]['idEstadoEmpresa'] = $fila->idEstadoEmpresa;
            $retorno[$contador]['nit'] = $fila->nit;
            $retorno[$contador]['correoElectronico'] = $fila->correoElectronico;
            $retorno[$contador]['estadoEmpresa'] = $fila->estadoEmpresa;

            $contador++;
        }
        return $retorno;
    }
    public function validarCodigo() {
        $sentenciaSql = "
                        SELECT
                            empresaCodigo
                        FROM
                            empresacodigo
                        WHERE
                            empresaCodigo = '$this->empresaCodigo' AND idEstadoEmpresa = 2
                        ";
        $this->conexion->ejecutar($sentenciaSql);
    }
    function obtenerCondicion(){
        $this->condicion = '';
        $this->whereAnd = ' WHERE ';
        
        if($this->idEmpresaCodigo!= '' && $this->idEmpresaCodigo!= 'null'){
            $this->condicion = $this->condicion.$this->whereAnd." ec.idEmpresaCodigo = $this->idEmpresaCodigo";
            $this->whereAnd = ' AND ';
        }
        if($this->empresaCodigo != ''){
            $this->condicion = $this->condicion.$this->whereAnd." ec.empresaCodigo =  '$this->empresaCodigo'";
            $this->whereAnd = ' AND ';
        }
        if($this->nit!= ''){
        	$this->condicion = $this->condicion.$this->whereAnd." ec.nit  = $this->nit";
        	$this->whereAnd = ' AND ';
        }
        if($this->empresa!= ''){
        	$this->condicion = $this->condicion.$this->whereAnd." ec.empresa LIKE  '%$this->empresa%'";
        	$this->whereAnd = ' AND ';
        }
        if($this->empresaCodigo != ''){
        	$this->condicion = $this->condicion.$this->whereAnd." ec.empresaCodigo LIKE '%$this->empresaCodigo%'";
        	$this->whereAnd = ' AND ';
        }
        
        if($this->idEstadoEmpresa != '' && $this->idEstadoEmpresa != 'null'){
        	$this->condicion = $this->condicion.$this->whereAnd." ec.idEstadoEmpresa  = $this->idEstadoEmpresa";
        	$this->whereAnd = ' AND ';
        }
        if($this->estado != ''){
            $this->condicion = $this->condicion.$this->whereAnd." ec.estado = $this->estado";
            $this->whereAnd = ' AND ';
        }
    }
    public function generarCodigoEmpresa(){
    	//Se define una cadena de caractares. Te recomiendo que uses esta.
    	$cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    	//Obtenemos la longitud de la cadena de caracteres
    	$longitudCadena=strlen($cadena);
    	//Se define la variable que va a contener la contrase�a
    	$codigo = "";
    	//Se define la longitud de la contrase�a, en mi caso 10, pero puedes poner la longitud que quieras
    	$longitudCodigo=10;
    	//Creamos la contrase�a
    	for($i=1 ; $i<=$longitudCodigo ; $i++){
    		//Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
    		$pos=rand(0,$longitudCodigo-1);
    	
    		//Vamos formando la contrase�a en cada iteraccion del bucle, a�adiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
    		$codigo .= substr($cadena,$pos,1);
    	}
    	$this->empresaCodigo = md5($codigo);
    	
    }
    public function consultarEstadoEmpresa(){
    	$sentenciaSql = "
                        SELECT
                            idEstadoEmpresa
                            , estadoEmpresa
                        FROM
                            estadoempresa
                        WHERE
                            estado = 1
                        ";
    	$this->conexion->ejecutar($sentenciaSql);
    	while ($fila = $this->conexion->obtenerObjeto()) {
    		$retorno[$contador]['idEstadoEmpresa'] = $fila->idEstadoEmpresa;
    		$retorno[$contador]['estadoEmpresa'] = $fila->estadoEmpresa;
    		$contador++;
    	}
    	return $retorno;
    }
    public function actualizarEstado(){
    	$sentenciaSql = "
                        UPDATE
                        	empresacodigo    
                        SET idEstadoEmpresa = $this->idEstadoEmpresa
                        
                        WHERE
                            idEmpresaCodigo = $this->idEmpresaCodigo
                        ";
    	$this->conexion->ejecutar($sentenciaSql);
    }
    public function consultarCodigoEmpresa(){
    	$sentenciaSql = "
                        SELECT
                        	correoElectronico
    						,empresaCodigo
                        FROM
                            empresacodigo
                        WHERE
                            idEmpresaCodigo = $this->idEmpresaCodigo
                        ";
    	$this->conexion->ejecutar($sentenciaSql);
    	return $retorno;
    }
}
?>
