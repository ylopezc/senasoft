<?php  
 $connect = mysqli_connect("localhost", "root", "", "senasoft");  
 $output = '';  
 if(isset($_POST["export_excel"]))  
 {  
      $sql = "SELECT * FROM persona ORDER BY id DESC";  
      $result = mysqli_query($connect, $sql);  
      if($result)  
      {  
           $output .= '  
                <table class="table" bordered="1">  
                     <tr>  
                          <th>Id</th>  
                          <th>First Name</th>  
                          <th>Last Name</th>  
                     </tr>  
           ';  
           while($row = mysqli_fetch_array($result))  
           {  
                $output .= '  
                     <tr>  
                          <td>'.$row["primerNombre"].'</td>  
                          <td>'.$row["primerApellido"].'</td>  
                          <td>'.$row["genero"].'</td>  
                     </tr>  
                ';  
           }  
           $output .= '</table>';  
           header("Content-Type: application/xls");   
           header("Content-Disposition: attachment; filename=download.xls");  
           echo $output;  
      }  
 }  
 ?>  