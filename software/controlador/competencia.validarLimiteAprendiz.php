<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/CategoriaConcurso.php';
require_once '../modelo/CategoriaConcurso.php';
require_once '../entidad/InscriCategoConcurAprend.php';
require_once '../modelo/InscriCategoConcurAprend.php';

$retorno = array("exito"=>1,"mensaje"=>"","data"=>null);
try{
	$idCategoria = $_POST['idCategoria'];
	//$idCategoria = 1;
	
	$categoriaConcursoE = new \entidad\CategoriaConcurso();
	$categoriaConcursoE->setIdCategoriaConcurso($idCategoria);
	
    $inscriCategoConcurAprendE = new \entidad\InscriCategoConcurAprend();
    $inscriCategoConcurAprendE->setCategoriaConcurso($categoriaConcursoE);
    
    $inscriCategoConcurAprendM = new \modelo\InscriCategoConcurAprend($inscriCategoConcurAprendE);
    $retorno['data'] = $inscriCategoConcurAprendM->numerAprenInscrCateg();

}catch(Exception $e){
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
?>
