<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/Proyecto.php';
require_once '../modelo/Proyecto.php';
$retorno = array('exito'=>1, 'mensaje'=>'', 'data'=>null, 'numeroRegistros'=>0);
try {
    $proyectoE = new \entidad\Proyecto();
    
    $proyectoM = new \modelo\Proyecto($proyectoE);
    $retorno['data'] = $proyectoM->cargarMotorBaseDatos();
    $retorno['numeroRegistros'] = $proyectoM->conexion->obtenerNumeroRegistros();
} catch (Exception $exc) {
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $exc->getMessage();
}
echo json_encode($retorno);
?>