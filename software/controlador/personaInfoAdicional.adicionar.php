<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/PersonaInfoAdicional.php';
require_once '../modelo/PersonaInfoAdicional.php';
require_once '../entidad/Persona.php';
require_once '../modelo/Persona.php';
$retorno = array('exito'=>1,"mensaje"=>'',"idPersona"=>null);

try {
	$idPersona = $_POST['idPersona'];
	$rh = $_POST['rh'];
	$posicion = strrpos($rh,"1");
	if($posicion != false){
		$rh = trim($rh,'1');
		$rh = $rh."+";
	}
	
	$tallaCamiseta = $_POST['tallaCamiseta'];
	$cuidadoMedico = $_POST['cuidadoMedico'];
	$idUsuarioCreacion = 1;
	$idUsuarioModificacion = 1;
	$estado = 1;
	
	$personaE = new \entidad\Persona();
	$personaE->setIdPersona($idPersona);
	
	$personaInfoAdicionalE = new \entidad\PersonaInfoAdicional();
	$personaInfoAdicionalE->setPersona($personaE);
	$personaInfoAdicionalE->setRh($rh);
	$personaInfoAdicionalE->setTallaCamiseta($tallaCamiseta);
	$personaInfoAdicionalE->setCuidadoMedico($cuidadoMedico);
	$personaInfoAdicionalE->setEstado($estado);
	$personaInfoAdicionalE->setIdUsuarioCreacion($idUsuarioCreacion);
	$personaInfoAdicionalE->setIdUsuarioModificacion($idUsuarioModificacion);
	
	
	$personaInfoAdicionalM = new \modelo\PersonaInfoAdicional($personaInfoAdicionalE);
	$personaInfoAdicionalM->adicionar();
	
} catch (Exception $e) {
	$retorno["exito"] = 0;
	$retorno["mensaje"] = $e->getMessage();
}
echo json_encode($retorno);

?>