<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/ProgramaFormacion.php';
require_once '../modelo/ProgramaFormacion.php';

$retorno = array("exito"=>1,"mensaje"=>"","data"=>null,"numeroRegistros"=>0);
try{
    $programaFormacionE = new \entidad\ProgramaFormacion();
    $programaFormacionE->setEstado(1);

    $programaFormacionM = new \modelo\ProgramaFormacion($programaFormacionE);
    $retorno['data'] = $programaFormacionM->consultar();
    $retorno['numeroRegistros'] = $programaFormacionM->conexion->obtenerNumeroRegistros();

}catch(Exception $e){
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
?>
