<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/InscriCategoConcurAprend.php';
require_once '../modelo/InscriCategoConcurAprend.php';
require_once '../entidad/CategoriaConcurso.php';
require_once '../modelo/CategoriaConcurso.php';
require_once '../entidad/Aprendiz.php';
require_once '../modelo/Aprendiz.php';
require_once '../entidad/CentroFormacion.php';
require_once '../modelo/CentroFormacion.php';
require_once '../entidad/ProgramaFormacion.php';
require_once '../modelo/ProgramaFormacion.php';



$retorno = array('exito'=>1,"mensaje"=>'Los aprendices se registraron correctamente',"idAprendiz"=>null);

try {
	$idAprendiz = $_POST['idAprendiz'];
	$idProgramaFormacion = $_POST['idProgramaFormacion'];
	$idCentroFormacion = $_POST['idCentroFormacion'];
	$idCategoriaConcurso = $_POST['idCategoriaConcurso'];;
	$estado = 1;
	$idUsuarioCreacion = 1;
	$idUsuarioModificacion = 1;
	
	$programaFormacionE = new \entidad\ProgramaFormacion();
	$programaFormacionE ->setIdProgramaFormacion($idProgramaFormacion);
	
	$centroFormacionE = new \entidad\CentroFormacion();
	$centroFormacionE ->setIdCentroFormacion($idCentroFormacion);
	
	$categoriaConcursoE = new \entidad\CategoriaConcurso();
	$categoriaConcursoE->setIdCategoriaConcurso($idCategoriaConcurso);
	
	$aprendizE = new \entidad\Aprendiz();
	$aprendizE->setIdAprendiz($idAprendiz);
	
	$inscriCategoConcurAprendE = new \entidad\InscriCategoConcurAprend();
	$inscriCategoConcurAprendE->setProgramaFormacion($programaFormacionE);
	$inscriCategoConcurAprendE->setCentroFormacion($centroFormacionE);
	$inscriCategoConcurAprendE->setAprendiz($aprendizE);
	$inscriCategoConcurAprendE->setCategoriaConcurso($categoriaConcursoE);
	$inscriCategoConcurAprendE->setEstado($estado);
	$inscriCategoConcurAprendE->setIdUsuarioCreacion($idUsuarioCreacion);
	$inscriCategoConcurAprendE->setIdUsuarioModificacion($idUsuarioModificacion);
	
	$inscriCategoConcurAprendM = new \modelo\InscriCategoConcurAprend($inscriCategoConcurAprendE);
	$inscriCategoConcurAprendM->adicionar();

	
} catch (Exception $e) {
	$retorno["exito"] = 0;
	$retorno["mensaje"] = $e->getMessage();
}
echo json_encode($retorno);

?>