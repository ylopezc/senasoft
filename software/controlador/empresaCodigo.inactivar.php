<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/EmpresaCodigo.php';
require_once '../modelo/EmpresaCodigo.php';
$retorno = array('exito'=>1,"mensaje"=>'La solicitud de la empresa se canceló correctamente.', 'numeroRegistros'=>0);

try {
	$idEmpresaCodigo = $_POST['idEmpresaCodigo'];
	$idEstadoEmpresa = $_POST['idEstadoEmpresa'];
    
    $empresaCodigoE = new \entidad\EmpresaCodigo();
    $empresaCodigoE->setIdEmpresaCodigo($idEmpresaCodigo);
    $empresaCodigoE->setIdEstadoEmpresa($idEstadoEmpresa);
    
    $empresaCodigoM = new \modelo\EmpresaCodigo($empresaCodigoE);
    $empresaCodigoM->actualizarEstado();
    
    $codigoEmpresa = $empresaCodigoM->consultarCodigoEmpresa();
    
    while ($fila = $empresaCodigoM->conexion->obtenerObjeto()) {
    	$correoElectronico  = $fila->correoElectronico;
    	$codigoEmpresa = $fila->empresaCodigo;
    	$contador++;
    }
    date_default_timezone_set('America/Bogota');
    $destinatario = $correoElectronico;
    
    $asunto = "Solicitud código inscripción";
    
    $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    
    $cabeceras .= "From: SENASOFT Huila 2014<contacto@senasoft.co>\r\n";
    
    $mensajeEnviar = "Lo sentimos , hemos revisado los datos ingresados y no cumplen para el proceso  de inscripción . Gracias";
    
    $mensajeEnviar .= '<br><br>'.$mensaje;
    
    if(mail($destinatario, $asunto, $mensajeEnviar , $cabeceras)){
    	$retorno['enviado'] = 1;
    }else{
    	$retorno['enviado'] = 0;
    }
} catch (Exception $e) {
	$retorno["exito"] = 0;
	$retorno["mensaje"] = "Error adicionando la empresa .".$e->getMessage();
}
echo json_encode($retorno);
?>