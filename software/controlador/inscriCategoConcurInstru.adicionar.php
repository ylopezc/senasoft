<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/InscriCategoConcurInstru.php';
require_once '../modelo/InscriCategoConcurInstru.php';
require_once '../entidad/CategoriaConcurso.php';
require_once '../modelo/CategoriaConcurso.php';
require_once '../entidad/Instructor.php';
require_once '../modelo/Instructor.php';
require_once '../entidad/CentroFormacion.php';
require_once '../modelo/CentroFormacion.php';

$retorno = array('exito'=>1,"mensaje"=>'El instructor se registró correctamente',"idInstructor"=>null);

try {
	$idInstructor = $_POST['idInstructor'];
	$idCentroFormacion = $_POST['idCentroFormacion'];
	$idCategoriaConcurso = $_POST['idCategoriaConcurso'];;
	$estado = 1;
	$idUsuarioCreacion = 1;
	$idUsuarioModificacion = 1;
	
	$centroFormacionE = new \entidad\CentroFormacion();
	$centroFormacionE ->setIdCentroFormacion($idCentroFormacion);
	
	$categoriaConcursoE = new \entidad\CategoriaConcurso();
	$categoriaConcursoE->setIdCategoriaConcurso($idCategoriaConcurso);
	
	$instructorE = new \entidad\Instructor();
	$instructorE->setIdInstructor($idInstructor);
	
	$inscriCategoConcurInstruE = new \entidad\InscriCategoConcurInstru();
	$inscriCategoConcurInstruE->setCentroFormacion($centroFormacionE);
	$inscriCategoConcurInstruE->setInstructor($instructorE);
	$inscriCategoConcurInstruE->setCategoriaConcurso($categoriaConcursoE);
	$inscriCategoConcurInstruE->setEstado($estado);
	$inscriCategoConcurInstruE->setIdUsuarioCreacion($idUsuarioCreacion);
	$inscriCategoConcurInstruE->setIdUsuarioModificacion($idUsuarioModificacion);
	
	$inscriCategoConcurInstruM = new \modelo\InscriCategoConcurInstru($inscriCategoConcurInstruE);
	$inscriCategoConcurInstruM->adicionar();

	
} catch (Exception $e) {
	$retorno["exito"] = 0;
	$retorno["mensaje"] = $e->getMessage();
}
echo json_encode($retorno);

?>