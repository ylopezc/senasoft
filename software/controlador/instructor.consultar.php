<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/Persona.php';
require_once '../modelo/Persona.php';
require_once '../entidad/CategoriaConcurso.php';
require_once '../modelo/CategoriaConcurso.php';
require_once '../entidad/Instructor.php';
require_once '../modelo/Instructor.php';
require_once '../entidad/InscriCategoConcurInstru.php';
require_once '../modelo/InscriCategoConcurInstru.php';
require_once '../entidad/CentroFormacion.php';
require_once '../modelo/CentroFormacion.php';
require_once '../entidad/Regional.php';

$retorno = array('exito'=>1,"mensaje"=>'',"data"=>null,"numeroRegistros"=>0);

try {
	$documentoIdentidad = $_POST['documentoIdentidad'];
	$idCentroFormacion = $_POST['idCentroFormacion'];
	$idCategoria = $_POST['idCategoria'];
	$idRegional = $_POST['idRegional'];
	
	
	$regionalE = new \entidad\Regional();
	$regionalE->setIdRegional($idRegional);
	
	$centroFormacionE = new \entidad\CentroFormacion();
	$centroFormacionE ->setIdCentroFormacion($idCentroFormacion);
	$centroFormacionE ->setRegional($regionalE);
	
	$categoriaConcursoE = new \entidad\CategoriaConcurso();
	$categoriaConcursoE ->setIdCategoriaConcurso($idCategoria);
	
	$personaE = new \entidad\Persona();
	$personaE->setNumeroIdentificacion($documentoIdentidad);
	
	$instructorE = new \entidad\Instructor();
	$instructorE->setPersona($personaE);
	
	$inscriCategoConcurInstruE = new \entidad\InscriCategoConcurInstru();
	$inscriCategoConcurInstruE->setCentroFormacion($centroFormacionE);
	$inscriCategoConcurInstruE->setCategoriaConcurso($categoriaConcursoE);
	$inscriCategoConcurInstruE->setInstructor($instructorE);
	
	$inscriCategoConcurInstruM = new \modelo\InscriCategoConcurInstru($inscriCategoConcurInstruE);
	$retorno['data'] = $inscriCategoConcurInstruM->consultar();
	$retorno['numeroRegistros'] = $inscriCategoConcurInstruM->conexion->obtenerNumeroRegistros();
} catch (Exception $e) {
	$retorno["exito"] = 0;
	$retorno["mensaje"] = $e->getMessage();
}
echo json_encode($retorno);

?>