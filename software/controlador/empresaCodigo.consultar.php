<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/EmpresaCodigo.php';
require_once '../modelo/EmpresaCodigo.php';

$retorno = array('exito'=>1,"mensaje"=>'', 'numeroRegistros'=>0,"data"=>'');

try {
    $nit = $_POST['nit'];
    $idEstadoEmpresa = $_POST['idEstadoEmpresa'];
    $empresa = $_POST['empresa'];
    
    $empresaCodigoE = new \entidad\EmpresaCodigo();
    $empresaCodigoE->setEmpresa($empresa);
    $empresaCodigoE->setNit($nit);
    $empresaCodigoE->setIdEstadoEmpresa($idEstadoEmpresa);
    
    $empresaCodigoM = new \modelo\EmpresaCodigo($empresaCodigoE);
    $retorno['data'] = $empresaCodigoM->consultar();
    $retorno['numeroRegistros'] = $empresaCodigoM->conexion->obtenerNumeroRegistros();
} catch (Exception $e) {
	$retorno["exito"] = 0;
	$retorno["mensaje"] = "Error al consultar la empresa  .".$e->getMessage();
}
echo json_encode($retorno);
?>