<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/Persona.php';
require_once '../modelo/Persona.php';
require_once '../entidad/Aprendiz.php';
require_once '../modelo/Aprendiz.php';
require_once '../entidad/InscriCategoConcurAprend.php';
require_once '../modelo/InscriCategoConcurAprend.php';



$retorno = array("exito"=>1,"mensaje"=>"","numeroRegistros"=>null);
try{
	$numeroIdentificacion = $_POST['numeroIdentificacion'];
	//$numeroIdentificacion = 123;
	
	//$idCategoria = 1;
	//$idCentroFormacion = 1;
	
	$personaE = new \entidad\Persona();
	$personaE->setNumeroIdentificacion($numeroIdentificacion);
	
	
	$aprendizE = new \entidad\Aprendiz();
	$aprendizE->setPersona($personaE);
	
    $inscriCategoConcurAprendE = new \entidad\InscriCategoConcurAprend();
    $inscriCategoConcurAprendE->setAprendiz($aprendizE);
    
    $inscriCategoConcurAprendM = new \modelo\InscriCategoConcurAprend($inscriCategoConcurAprendE);
    $inscriCategoConcurAprendM->validarAprenInscrCateg();
    $retorno['numeroRegistros'] = $inscriCategoConcurAprendM->conexion->obtenerNumeroRegistros();

}catch(Exception $e){
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
?>