<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/Persona.php';
require_once '../modelo/Persona.php';
require_once '../entidad/Aprendiz.php';
require_once '../modelo/Aprendiz.php';
require_once '../entidad/CentroFormacion.php';
require_once '../modelo/CentroFormacion.php';
require_once '../entidad/ProgramaFormacion.php';
require_once '../modelo/ProgramaFormacion.php';

$retorno = array('exito'=>1,"mensaje"=>'',"idAprendiz"=>null);

try {
	$idPersona = $_POST['idPersona'];
	$idProgramaFormacion = $_POST['idProgramaFormacion'];
	$idCentroFormacion = $_POST['idCentroFormacion'];;
	$estado = 1;
	$idUsuarioCreacion = 1;
	$idUsuarioModificacion = 1;
	
	$programaFormacionE = new \entidad\ProgramaFormacion();
	$programaFormacionE ->setIdProgramaFormacion($idProgramaFormacion);
	
	$centroFormacionE = new \entidad\CentroFormacion();
	$centroFormacionE ->setIdCentroFormacion($idCentroFormacion);
	
	$personaE = new \entidad\Persona();
	$personaE->setIdPersona($idPersona);
	
	$aprendizE = new \entidad\Aprendiz();
	$aprendizE->setPersona($personaE);
	$aprendizE->setProgramaFormacion($programaFormacionE);
	$aprendizE->setCentroFormacion($centroFormacionE);
	$aprendizE->setIdUsuarioCreacion($idUsuarioCreacion);
	$aprendizE->setIdUsuarioModificacion($idUsuarioModificacion);
	$aprendizE->setEstado($estado);
	
	$aprendizM = new \modelo\Aprendiz($aprendizE);
	$aprendizM->adicionar();
	$retorno["idAprendiz"] = $aprendizM->obtenerMaximo();
	
} catch (Exception $e) {
	$retorno["exito"] = 0;
	$retorno["mensaje"] = $e->getMessage();
}
echo json_encode($retorno);

?>