<?php
ini_set("display_errors", 0);
require_once '../modelo/Frecuencia.php';
$retorno = array("exito"=>1, "mensaje"=>'', "data"=>null);
try {
    $frecuencia = new modelo\Frecuencia();
    $retorno["data"] = $frecuencia->getFrecuencia();
} catch (Exception $e) {
    $retorno["exito"] = 0;
    $retorno["mensaje"] = $e->getMessage();
}
echo json_encode($retorno);
?>

