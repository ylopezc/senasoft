<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/EmpresaCodigo.php';
require_once '../modelo/EmpresaCodigo.php';
$retorno = array('exito'=>1, 'mensaje'=>'','data'=>'');
try {
    if($_SESSION['codigoInscripcion'] == ''){
        $retorno['exito'] = 0;
        $retorno['mensaje'] = "Debe ingresar el código de inscripción de su empresa.";
    }else{
    	$codigoEmpresa = $_SESSION['codigoInscripcion'];
    	$empresaCodigoE = new \entidad\EmpresaCodigo();
    	$empresaCodigoE->setEmpresaCodigo($codigoEmpresa);
    	$empresaCodigoE->setIdEstadoEmpresa(2);
    	
    	$empresaCodigoM = new \modelo\EmpresaCodigo($empresaCodigoE);
    	$retorno['data'] = $empresaCodigoM->consultar();	
    }
} catch (Exception $exc) {
    echo $exc->getTraceAsString();
}
echo json_encode($retorno);
?>
