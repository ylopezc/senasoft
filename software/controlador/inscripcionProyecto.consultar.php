<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/Proyecto.php';
require_once '../modelo/Proyecto.php';
require_once '../entidad/Aprendiz.php';
require_once '../entidad/Persona.php';
require_once '../entidad/TipoSolucion.php';
require_once '../entidad/Regional.php';
require_once '../entidad/CentroFormacion.php';


$retorno = array('exito'=>1, 'mensaje'=>'', 'data'=>null, 'numeroRegistros'=>0);
try {
    $idProyecto = $_POST['idProyecto'];
    $numeroIdentificacion = $_POST['numeroIdentificacion'];
    $idTipoSolucion = $_POST['idTipoSolucion'];
    $idRegional = $_POST['idRegional'];
    $idCentroFormacion = $_POST['idCentroFormacion'];
    
    $proyectoE = new \entidad\Proyecto();
    $proyectoE->setIdProyecto($idProyecto);
    
    $personaE = new \entidad\Persona();
    $personaE->setNumeroIdentificacion($numeroIdentificacion);
    
    $aprendizE = new \entidad\Aprendiz();
    $aprendizE->setPersona($personaE);
    $proyectoE->setAprendiz($aprendizE);
    
    $tipoSolucionE = new \entidad\TipoSolucion();
    $tipoSolucionE->setIdTipoSolucion($idTipoSolucion);
    $proyectoE->setTipoSolucion($tipoSolucionE);
    
    
    $centroFormacionE = new \entidad\CentroFormacion();
    $centroFormacionE->setIdCentroFormacion($idCentroFormacion);
    
    $regionalE = new \entidad\Regional();
    $regionalE->setIdRegional($idRegional);
    $centroFormacionE->setRegional($regionalE);
    
    $proyectoE->setCentroFormacion($centroFormacionE);
    
    $proyectoM = new \modelo\Proyecto($proyectoE);
    $retorno['data'] = $proyectoM->consultar();
    $retorno['numeroRegistros'] = $proyectoM->conexion->obtenerNumeroRegistros();
} catch (Exception $exc) {
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $exc->getMessage();
}
echo json_encode($retorno);
?>