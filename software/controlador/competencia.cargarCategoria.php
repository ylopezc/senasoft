<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/CategoriaConcurso.php';
require_once '../modelo/CategoriaConcurso.php';
$retorno = array("exito"=>1,"mensaje"=>"","data"=>null,"numeroRegistros"=>0);
try{
    $categoriaConcursoE = new \entidad\CategoriaConcurso();
    $categoriaConcursoE->setEstado(1);

    $categoriaConcursoM = new \modelo\CategoriaConcurso($categoriaConcursoE);
    $retorno['data'] = $categoriaConcursoM->consultar();
    $retorno['numeroRegistros'] = $categoriaConcursoM->conexion->obtenerNumeroRegistros();

}catch(Exception $e){
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
?>
