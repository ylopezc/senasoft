<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/Proyecto.php';

require_once '../modelo/Proyecto.php';



require_once '../entidad/Proceso.php';

require_once '../entidad/TipoSolucion.php';

require_once '../entidad/MotorBaseDatos.php';

require_once '../entidad/LenguajeProgramacion.php';

require_once '../entidad/Aprendiz.php';

require_once '../entidad/CentroFormacion.php';



$retorno = array('exito'=>1, 'mensaje'=>'El proyecto se registró correctamente.');

try {

    $idProyecto = $_POST['idProyecto'];

    $proyecto = $_POST['proyecto'];

    $descripcion = $_POST['descripcion'];

    $url = $_POST['url'];

    $sistemaRelacionado = $_POST['sistemaRelacionado'];

    $ubicacion = $_POST['ubicacion'];

    $urlVideo = $_POST['urlVideo'];

    $tiempoProduccion = $_POST['tiempoProduccion'];

    $idProcesoImpacto = $_POST['idProcesoImpacto'];

    $idTipoSolucion = $_POST['idTipoSolucion'];

    $idMotorBaseDatos = $_POST['idMotorBaseDatos'];

    $idLenguajeProgramacion = $_POST['idLenguajeProgramacion'];

    $tipoLicenciamiento = $_POST['tipoLicenciamiento'];

    $derechosAutor = $_POST['derechosAutor'];

    $nroImplementaciones = $_POST['nroImplementaciones'];

    $idAprendiz = $_POST['idAprendiz'];

    $idCentroFormacion = $_POST['idCentroFormacion'];

    $cartaImplementacion = $_POST['cartaImplementacion'];

    $nroImplementacionFuturo = $_POST['nroImplementacionFuturo'];

    $desarrolloFuturo = $_POST['desarrolloFuturo'];

    $equipoDesarrollo = $_POST['equipoDesarrollo'];
    $informacionAdicional = $_POST['informacionAdicional'];
    $idUsuarioCreacion = $_SESSION['idUsuario'];

    $idUsuarioModificacion = $_SESSION['idUsuario'];    

    

    $proyectoE = new \entidad\Proyecto();

    $proyectoE->setIdProyecto($idProyecto);

    $proyectoE->setProyecto($proyecto);

    $proyectoE->setDescripcion($descripcion);

    $proyectoE->setUrl($url);

    $proyectoE->setSistemaRelacionado($sistemaRelacionado);

    $proyectoE->setUbicacion($ubicacion);

    $proyectoE->setUrlVideo($urlVideo);

    $proyectoE->setTiempoProduccion($tiempoProduccion);

    

    $procesoE = new \entidad\Proceso();

    $procesoE->setIdProceso($idProcesoImpacto);

    $proyectoE->setProcesoImpacto($procesoE);

    

    $tipoSolucionE = new \entidad\TipoSolucion();

    $tipoSolucionE->setIdTipoSolucion($idTipoSolucion);

    $proyectoE->setTipoSolucion($tipoSolucionE);

    

    $motorBaseDatosE = new \entidad\MotorBaseDatos();

    $motorBaseDatosE->setIdMotorBaseDatos($idMotorBaseDatos);

    $proyectoE->setMotorBaseDatos($motorBaseDatosE);

    

    $lenguajeProgramacionE = new \entidad\LenguajeProgramacion();

    $lenguajeProgramacionE->setIdLenguajeProgramacion($idLenguajeProgramacion);

    $proyectoE->setLenguajeProgramacion($lenguajeProgramacionE);

    

    $proyectoE->setTipoLicenciamiento($tipoLicenciamiento);

    $proyectoE->setDerechosAutor($derechosAutor);

    $proyectoE->setNroImplementaciones($nroImplementaciones);
    $proyectoE->setInformacionAdicional($informacionAdicional);
    

    $aprendizE = new \entidad\Aprendiz();

    $aprendizE->setIdAprendiz($idAprendiz);

    $proyectoE->setAprendiz($aprendizE);

    

    $centroFormacionE = new \entidad\CentroFormacion();

    $centroFormacionE->setIdCentroFormacion($idCentroFormacion);

    $proyectoE->setCentroFormacion($centroFormacionE);

    

    $proyectoE->setCartaImplementacion($cartaImplementacion);

    $proyectoE->setNroImplementacionFuturo($nroImplementacionFuturo);

    $proyectoE->setDesarrolloFuturo($desarrolloFuturo);

    $proyectoE->setEquipoDesarrollo($equipoDesarrollo);

    $proyectoE->setIdUsuarioCreacion($idUsuarioCreacion);

    $proyectoE->setIdUsuarioModificacion($idUsuarioModificacion);

    

    $proyectoM = new \modelo\Proyecto($proyectoE);

    $proyectoM->adicionar();

    

    if($cartaImplementacion != "" && $cartaImplementacion != "null"){

        $idProyecto = $proyectoM->obtenerMaximo();

        $extension = substr(strrchr($cartaImplementacion, "."), 1);

        $cartaImplementacionNueva = '../archivos/certificadosImplementacion/'.$idProyecto.'-'.$_SESSION['nombreCertificado'];

        rename($cartaImplementacion, $cartaImplementacionNueva);

        $proyectoE = new \entidad\Proyecto();

        $proyectoE->setIdProyecto($idProyecto);

        $proyectoE->setCartaImplementacion($cartaImplementacionNueva);



        $proyectoM = new \modelo\Proyecto($proyectoE);

        $proyectoM->actualizarCartaImplementacion();

    }

} catch (Exception $exc) {

    $retorno['exito'] = 0;

    $retorno['mensaje'] = $exc->getMessage();

}

echo json_encode($retorno);

?>