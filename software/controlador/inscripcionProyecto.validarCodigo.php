<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/CentroFormacion.php';
require_once '../modelo/CentroFormacion.php';

$retorno = array('exito'=>1, 'mensaje'=>'', 'validarCodigo'=>0);
try {
    $codigoInscripcion = $_POST['codigoInscripcion'];
    
    $centroFormacionE = new \entidad\CentroFormacion();
    
    $centroFormacionM = new \modelo\CentroFormacion($centroFormacionE);
    $codigos = $centroFormacionM->obtenerCodigos();
    
    foreach ($codigos as $codigo) {
        if($codigoInscripcion == $codigo){
            $retorno['validarCodigo'] = 1;
            $_SESSION['codigoInscripcion'] = $codigoInscripcion;
            break;
        }
    }
} catch (Exception $exc) {
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $exc->getMessage();
}
echo json_encode($retorno);
?>