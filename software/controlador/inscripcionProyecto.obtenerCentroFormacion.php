<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/CentroFormacion.php';
require_once '../modelo/CentroFormacion.php';

$retorno = array('exito'=>1, 'mensaje'=>'', 'informacionCentro'=>null);
try {
    if($_SESSION['codigoInscripcion'] == ''){
        $retorno['exito'] = 0;
        $retorno['mensaje'] = "Debe ingresar el código de inscripción.";
    }
    $codigoInscripcion = $_SESSION['codigoInscripcion'];
    
    $centroFormacionE = new \entidad\CentroFormacion();
    $centroFormacionE->setCodigo($codigoInscripcion);
    
    $centroFormacionM = new \modelo\CentroFormacion($centroFormacionE);
    $retorno['informacionCentro'] = $centroFormacionM->obtenerInformacionCentroFormacion();
} catch (Exception $exc) {
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $exc->getMessage();
}
echo json_encode($retorno);
?>