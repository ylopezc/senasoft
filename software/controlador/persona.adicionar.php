<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/Persona.php';
require_once '../modelo/Persona.php';
require_once '../entidad/TipoDocumentoIdentidad.php';
require_once '../modelo/TipoDocumentoIdentidad.php';

$retorno = array('exito'=>1,"mensaje"=>'',"idPersona"=>null);

try {
	$idTipoDocumentoIdentidad = $_POST["idTipoDocumentoIdentidad"];
	$numeroIdentificacion = $_POST["numeroIdentificacion"];
	$foto = $_POST["foto"];
	$primerNombre = $_POST["primerNombre"];
	$segundoNombre = $_POST["segundoNombre"];
	$primerApellido = $_POST["primerApellido"];
	$segundoApellido = $_POST["segundoApellido"];
	$fechaNacimiento = $_POST["fechaNacimiento"];
	$correoElectronico = $_POST["correoElectronico"];
	$telefonoCelular = $_POST["telefonoCelular"];
	$telefonoFijo = $_POST["telefonoFijo"];
	if($telefonoFijo == null || $telefonoFijo==''){
		$telefonoFijo = 'NULL';
	}
	$genero = $_POST["genero"];
	$estado = 1;
	$idUsuarioCreacion = 1;
	$idUsuarioModificacion = 1;
	
	$tipoDocumentoIdentidadE = new \entidad\TipoDocumentoIdentidad();
	$tipoDocumentoIdentidadE->setIdTipoDocumentoIdentidad($idTipoDocumentoIdentidad);
	
	
	$extension = substr(strrchr($foto, "."), 1);
	$fotoNueva = "../archivos/fotos/foto.png";
	rename($foto, $fotoNueva);
	
	
	$personaE = new \entidad\Persona();
	$personaE->setTipoDocumentoIdentidad($tipoDocumentoIdentidadE);
	$personaE->setNumeroIdentificacion($numeroIdentificacion);
	$personaE->setFoto($fotoNueva);
	$personaE->setPrimerNombre($primerNombre);
	$personaE->setSegundoNombre($segundoNombre);
	$personaE->setPrimerApellido($primerApellido);
	$personaE->setSegundoApellido($segundoApellido);
	$personaE->setFechaNacimiento($fechaNacimiento);
	$personaE->setCorreoElectronico($correoElectronico);
	$personaE->setTelefonoCelular($telefonoCelular);
	$personaE->setTelefonoFijo($telefonoFijo);
	$personaE->setEstado($estado);
	$personaE->setGenero($genero);
	$personaE->setIdUsuarioCreacion($idUsuarioCreacion);
	$personaE->setIdUsuarioModificacion($idUsuarioModificacion);
	
	$personaM = new \modelo\Persona($personaE);
	$personaM->adicionar();
	$retorno["idPersona"] = $personaM->obtenerMaximo();
	
} catch (Exception $e) {
	$retorno["exito"] = 0;
	$retorno["mensaje"] = $e->getMessage();
}
echo json_encode($retorno);

?>