<?php
ini_set("display_errors", 0);
require_once '../entorno/Conexion.php';
$retorno = array('exito'=>1, 'mensaje'=>'', 'data'=>null,'numeroRegistros'=>0);
try{

    $sentenciaSql = "SELECT
                        idCategoriaProducto
                        ,categoriaProducto
                     FROM
                        categoriaproducto
                     WHERE 
                     	estado = 1
                     ";
     $conexion = new Conexion();
     $conexion->ejecutar($sentenciaSql);
    
     $contador = 0;
    while ($fila = $conexion->obtenerObjeto()){
       $retorno['data'][$contador]['idCategoriaProducto'] = $fila->idCategoriaProducto;
       $retorno['data'][$contador]['categoriaProducto'] = $fila->categoriaProducto;
       $contador++;
    }
    
}catch (Exception $e){
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);

?>