<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/Empresa.php';
require_once '../modelo/Empresa.php';

$retorno = array('exito'=>1, 'mensaje'=>'La empresa se adicionó satisfactoriamente.', 'idEmpresa'=>0);

try {
    $idEmpresa = $_POST['idEmpresa'];
    $nit = $_POST['nit'];
    $empresa = $_POST['empresa'];
    $representanteLegal = $_POST['representanteLegal'];
    $direccion = $_POST['direccion'];
    $telefonoCelular = $_POST['telefonoCelular'];
    $telefonoFijo = $_POST['telefonoFijo'];
    $correoElectronico = $_POST['correoElectronico'];
    $url = $_POST['url'];
    $idMunicipio = $_POST['idMunicipio'];
    $idRegimenEmpresa = $_POST['idRegimenEmpresa'];
    $idSectorEconomico = $_POST['idSectorEconomico'];
    $sectorEconomico = $_POST['sectorEconomico'];
    $estado = $_POST['estado'];
    $idUsuarioCreacion = $_SESSION['idUsuario'];
    $idUsuarioModificacion = $_SESSION['idUsuario'];

    $empresaE = new \entidad\Empresa();
    $empresaE->setIdEmpresa($idEmpresa);
    $empresaE->setNit($nit);
    $empresaE->setEmpresa($empresa);
    $empresaE->setRepresentanteLegal($representanteLegal);
    $empresaE->setDireccion($direccion);
    $empresaE->setTelefonoCelular($telefonoCelular);
    $empresaE->setTelefonoFijo($telefonoFijo);
    $empresaE->setCorreoElectronico($correoElectronico);
    $empresaE->setUrl($url);
    $empresaE->setIdMunicipio($idMunicipio);
    $empresaE->setIdSectorEconomico($idSectorEconomico);
    $empresaE->setIdRegimenEmpresa($idRegimenEmpresa);
    $empresaE->setSectorEconomico($sectorEconomico);
    $empresaE->setEstado($estado);
    $empresaE->setIdUsuarioCreacion($idUsuarioCreacion);
    $empresaE->setIdUsuarioModificacion($idUsuarioModificacion);

    $empresaM = new \modelo\Empresa($empresaE);	
    $empresaM->adicionar();
    $idEmpresa = $empresaM->obtenerMaximo();
    $retorno['idEmpresa'] = $idEmpresa;
} catch (Exception $e) {
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);



?>