<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/Persona.php';
require_once '../modelo/Persona.php';
require_once '../entidad/Instructor.php';
require_once '../modelo/Instructor.php';
require_once '../entidad/CentroFormacion.php';
require_once '../modelo/CentroFormacion.php';


$retorno = array('exito'=>1,"mensaje"=>'',"idInstructor"=>null);

try {
	$idPersona = $_POST['idPersona'];
	$idCentroFormacion = $_POST['idCentroFormacion'];
	$estado = 1;
	$idUsuarioCreacion = 1;
	$idUsuarioModificacion = 1;
	
	$centroFormacionE = new \entidad\CentroFormacion();
	$centroFormacionE ->setIdCentroFormacion($idCentroFormacion);
	
	$personaE = new \entidad\Persona();
	$personaE->setIdPersona($idPersona);
	
	$instructorE = new \entidad\Instructor();
	$instructorE->setPersona($personaE);
	$instructorE->setCentroFormacion($centroFormacionE);
	$instructorE->setIdUsuarioCreacion($idUsuarioCreacion);
	$instructorE->setIdUsuarioModificacion($idUsuarioModificacion);
	$instructorE->setEstado($estado);
	
	$instructorM = new \modelo\Instructor($instructorE);
	$instructorM->adicionar();
	$retorno["idInstructor"] = $instructorM->obtenerMaximo();
	
} catch (Exception $e) {
	$retorno["exito"] = 0;
	$retorno["mensaje"] = $e->getMessage();
}
echo json_encode($retorno);

?>