<?php
ini_set("display_errors", 0);
require_once '../entorno/Conexion.php';

$retorno = array('exito'=>1, 'mensaje'=>'', 'data'=>null,'numeroRegistros'=>0);
try{

    $sentenciaSql = "SELECT
                        idDepartamento
                        ,departamento
                     FROM
                        departamento
                     ";
     $conexion = new Conexion();
     $conexion->ejecutar($sentenciaSql);
    
     $contador = 0;
    while ($fila = $conexion->obtenerObjeto()){
       $retorno['data'][$contador]['idDepartamento'] = $fila->idDepartamento;
       $retorno['data'][$contador]['departamento'] = $fila->departamento;
       $contador++;
    }
    
}catch (Exception $e){
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
   

?>
