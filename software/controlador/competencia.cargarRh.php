<?php
ini_set("display_errors", 0);
require_once '../modelo/Rh.php';
$retorno = array("exito"=>1, "mensaje"=>'', "data"=>null);
try {
    $rh = new modelo\Rh();
    $retorno["data"] = $rh->getRh();
} catch (Exception $e) {
    $retorno["exito"] = 0;
    $retorno["mensaje"] = $e->getMessage();
}
echo json_encode($retorno);
?>

