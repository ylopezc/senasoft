<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/Municipio.php';
require_once '../modelo/Municipio.php';

$retorno = array('exito'=>1, 'mensaje'=>'', 'municipio'=>null, 'numeroRegistros'=>0);
try {
    $idDepartamento = $_POST['idDepartamento'];
    
    $departamentoMunicipioE= new \entidad\Municipio();
    $departamentoMunicipioE->setIdDepartamento($idDepartamento);
    
    $departamentoMunicipioM = new \modelo\Municipio($departamentoMunicipioE);
    $retorno['municipio'] = $departamentoMunicipioM->consultarMunicipios();
    $retorno['numeroRegistros'] = $departamentoMunicipioM->conexion->obtenerNumeroRegistros();
} catch (Exception $e) {
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
?>
