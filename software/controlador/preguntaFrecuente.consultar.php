<?php
ini_set("display_errors", 0);
require_once '../entidad/PreguntaFrecuente.php';
require_once '../modelo/PreguntaFrecuente.php';

$retorno = array('exito'=>1,'data'=>null,'numeroRegistros'=>0,'mensaje'=>'');

try{
	$preguntaFrecuente = $_POST['preguntaFrecuente'];
	$respuesta = $_POST['respuesta'];
	$estado = $_POST['estado'];
	
	$preguntaFrecuenteE = new \entidad\PreguntaFrecuente();
	$preguntaFrecuenteE->setPreguntaFrecuente($preguntaFrecuente);
	$preguntaFrecuenteE->setRespuesta($respuesta);
	$preguntaFrecuenteE->setEstado($estado);
	
	$preguntaFrecuenteM = new \modelo\PreguntaFrecuente($preguntaFrecuenteE);
	$retorno['data'] = $preguntaFrecuenteM->consultar();
	$retorno['numeroRegistros'] = $preguntaFrecuenteM->conexion->obtenerNumeroRegistros();	
		
}catch(Exception $e){
	$retorno['exito'] = 0;
	$retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
?>