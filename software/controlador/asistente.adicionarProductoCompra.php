<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/EmpresaProducto.php';
require_once '../modelo/EmpresaProducto.php';

$retorno = array('exito'=>1, 'mensaje'=>'El producto se adicionó satisfactoriamente.');

try {
    $idEmpresa = $_POST['idEmpresa'];
    $idCategoriaProducto = $_POST['idCategoriaProductoCompra'];
    $estado = $_POST['estado'];
    $frecuencia = $_POST['frecuencia'];
    
    $idTipoEmpresaProducto = 1;
    
    if(count($idCategoriaProducto) > 1){
        $contador = 0;
        foreach ($idCategoriaProducto as $idCategoriaProducto) {
            $empresaProductoE = new \entidad\EmpresaProducto();
            $empresaProductoE->setIdEmpresa($idEmpresa);
            $empresaProductoE->setIdCategoriaProducto($idCategoriaProducto);
            $empresaProductoE->setEstado($estado);
            $empresaProductoE->setFrecuencia($frecuencia[$contador]);
            $empresaProductoE->setIdTipoEmpresaProducto($idTipoEmpresaProducto);
            $empresaProductoE->setIdUsuarioCreacion($idUsuarioCreacion);
            $empresaProductoE->setIdUsuarioModificacion($idUsuarioModificacion);

            $empresaProductoM = new \modelo\EmpresaProducto($empresaProductoE);
            $empresaProductoM->adicionar();
            $contador++;
        }
    }else{
        $empresaProductoE = new \entidad\EmpresaProducto();
        $empresaProductoE->setIdEmpresa($idEmpresa);
        $empresaProductoE->setIdTipoEmpresaProducto($idTipoEmpresaProducto);
        $empresaProductoE->setIdCategoriaProducto($idCategoriaProducto[0]);
        $empresaProductoE->setEstado($estado);
        $empresaProductoE->setFrecuencia($frecuencia[0]);
        $empresaProductoE->setIdUsuarioCreacion($idUsuarioCreacion);
        $empresaProductoE->setIdUsuarioModificacion($idUsuarioModificacion);

        $empresaProductoM = new \modelo\EmpresaProducto($empresaProductoE);
        $empresaProductoM->adicionar();
    }
} catch (Exception $e) {
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);

?>