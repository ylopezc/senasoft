<?php
ini_set("display_errors", 0);
require_once '../entidad/Municipio.php';
require_once '../modelo/Municipio.php';

$retorno = array("exito"=>0,"mensaje"=>"","data"=>null);
try{
    $municipioE = new \entidad\Municipio();
    $municipioE->setMunicipio(1);

    $municipioM = new \modelo\Municipio($municipioE);
    $retorno['data'] = $municipioM->consultar();

}catch(Exception $e){
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
?>
