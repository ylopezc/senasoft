<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/TipoDocumentoIdentidad.php';
require_once '../modelo/TipoDocumentoIdentidad.php';



$retorno = array("exito"=>1,"mensaje"=>"","data"=>null,"numeroRegistros"=>0);
try{
    $tipoDocumentoIdentidadE = new \entidad\TipoDocumentoIdentidad();
    $tipoDocumentoIdentidadE->setEstado(1);

    $tipoDocumentoIdentidadM = new \modelo\TipoDocumentoIdentidad($tipoDocumentoIdentidadE);
    $retorno['data'] = $tipoDocumentoIdentidadM->consultar();
    $retorno['numeroRegistros'] = $tipoDocumentoIdentidadM->conexion->obtenerNumeroRegistros();

}catch(Exception $e){
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
?>
