<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/CentroFormacion.php';
require_once '../modelo/CentroFormacion.php';

$retorno = array('exito'=>1,"numeroRegistros"=>0,"mensaje"=>'');

try{
	$codigo = $_POST['codigoCentroFormacion'];
	
	$centroFormacionE = new \entidad\CentroFormacion();
	$centroFormacionE->setCodigo($codigo);
	
	$centroFormacionM = new \modelo\CentroFormacion($centroFormacionE);
	$centroFormacionM->consultar();
	$retorno['numeroRegistros'] = $centroFormacionM->conexion->obtenerNumeroRegistros();
	if($centroFormacionM->conexion->obtenerNumeroRegistros() == 1){
		$_SESSION['codigoCentroFormacion'] = $codigo;
	}else{
		$retorno['exito'] = 0;
		$retorno['mensaje'] = "Por favor verifique que el código ingresado corresponde a su centro de formación ";
	}
}catch (Exception $e){
	$retorno['exito'] = 0;
	$retorno['mensaje'] = "Por favor verifique que el código ingresado corresponde a su centro de formación ".$e->getMessage();
}
echo json_encode($retorno);
?>