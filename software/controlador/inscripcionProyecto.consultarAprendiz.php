<?php
ini_set("display_errors", 0);
require_once '../entidad/Persona.php';
require_once '../entidad/Aprendiz.php';
require_once '../modelo/Aprendiz.php';

$retorno = array('exito'=>1, 'mensaje'=>'', 'numeroRegistros'=>0,'data'=>null);
try {
    $numeroIdentificacion = $_POST['numeroDocumento'];
    
    $aprendizE = new \entidad\Aprendiz();
    $personaE = new \entidad\Persona();
    $personaE->setNumeroIdentificacion($numeroIdentificacion);
    $aprendizE->setPersona($personaE);
    
    $aprendizM = new \modelo\Aprendiz($aprendizE);
    $retorno['data'] = $aprendizM->buscarAprendiz();
    $retorno['numeroRegistros'] = $aprendizM->conexion->obtenerNumeroRegistros();
} catch (Exception $exc) {
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $exc->getMessage();
}
echo json_encode($retorno);
?>