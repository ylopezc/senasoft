<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/CentroFormacion.php';
require_once '../modelo/CentroFormacion.php';
require_once '../entidad/InscriCategoConcurAprend.php';
require_once '../modelo/InscriCategoConcurAprend.php';

$retorno = array("exito"=>1,"mensaje"=>"","data"=>null);
try{
	$idCentroFormacion = $_POST['idCentroFormacion'];
	//$idCategoria = 1;
	//$idCentroFormacion = 1;
	
	$centroFormacionE = new \entidad\CentroFormacion();
	$centroFormacionE->setIdCentroFormacion($idCentroFormacion);
	
    $inscriCategoConcurAprendE = new \entidad\InscriCategoConcurAprend();
    $inscriCategoConcurAprendE->setCentroFormacion($centroFormacionE);
    
    $inscriCategoConcurAprendM = new \modelo\InscriCategoConcurAprend($inscriCategoConcurAprendE);
    $retorno['data'] = $inscriCategoConcurAprendM->validarLimitAprenCentrForma();

}catch(Exception $e){
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
?>