<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/CentroFormacion.php';
require_once '../modelo/CentroFormacion.php';
require_once '../entidad/InscriCategoConcurInstru.php';
require_once '../modelo/InscriCategoConcurInstru.php';

$retorno = array("exito"=>1,"mensaje"=>"","data"=>null);
try{
	$idCentroFormacion = $_POST['idCentroFormacion'];
	//$idCategoria = 1;
	//$idCentroFormacion = 1;
	
	$centroFormacionE = new \entidad\CentroFormacion();
	$centroFormacionE->setIdCentroFormacion($idCentroFormacion);
	
    $inscriCategoConcurInstruE = new \entidad\InscriCategoConcurInstru();
    $inscriCategoConcurInstruE->setCentroFormacion($centroFormacionE);
    
    $inscriCategoConcurInstruM = new \modelo\InscriCategoConcurInstru($inscriCategoConcurInstruE);
    $retorno['data'] = $inscriCategoConcurInstruM->validarLimitInstrCentrForma();

}catch(Exception $e){
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
?>