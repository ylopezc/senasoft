<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/Persona.php';
require_once '../modelo/Persona.php';
require_once '../entidad/CategoriaConcurso.php';
require_once '../modelo/CategoriaConcurso.php';
require_once '../entidad/Aprendiz.php';
require_once '../modelo/Aprendiz.php';
require_once '../entidad/InscriCategoConcurAprend.php';
require_once '../modelo/InscriCategoConcurAprend.php';
require_once '../entidad/CentroFormacion.php';
require_once '../modelo/CentroFormacion.php';
require_once '../entidad/Regional.php';

$retorno = array('exito'=>1,"mensaje"=>'',"data"=>null,"numeroRegistros"=>0);

try {
	$documentoIdentidad = $_POST['documentoIdentidad'];
	$idCentroFormacion = $_POST['idCentroFormacion'];
	$idCategoria = $_POST['idCategoria'];
	$idRegional = $_POST['idRegional'];
	
	$categoriaConcursoE = new \entidad\CategoriaConcurso();
	$categoriaConcursoE ->setIdCategoriaConcurso($idCategoria);
	
	$personaE = new \entidad\Persona();
	$personaE->setNumeroIdentificacion($documentoIdentidad);
	
	$aprendizE = new \entidad\Aprendiz();
	$aprendizE->setPersona($personaE);
	
	$regionalE = new \entidad\Regional();
	$regionalE->setIdRegional($idRegional);
	
	$centroFormacionE = new \entidad\CentroFormacion();
	$centroFormacionE ->setIdCentroFormacion($idCentroFormacion);
	$centroFormacionE ->setRegional($regionalE);
	
	$inscriCategoConcurAprendE = new \entidad\InscriCategoConcurAprend();
	$inscriCategoConcurAprendE->setCentroFormacion($centroFormacionE);
	$inscriCategoConcurAprendE->setCategoriaConcurso($categoriaConcursoE);
	$inscriCategoConcurAprendE->setAprendiz($aprendizE);
	
	$inscriCategoConcurAprendM = new \modelo\InscriCategoConcurAprend	($inscriCategoConcurAprendE);
	$retorno['data'] = $inscriCategoConcurAprendM->consultar();
	$retorno['numeroRegistros'] = $inscriCategoConcurAprendM->conexion->obtenerNumeroRegistros();
} catch (Exception $e) {
	$retorno["exito"] = 0;
	$retorno["mensaje"] = $e->getMessage();
}
echo json_encode($retorno);

?>