<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/CategoriaConcurso.php';
require_once '../modelo/CategoriaConcurso.php';
require_once '../entidad/CentroFormacion.php';
require_once '../modelo/CentroFormacion.php';
require_once '../entidad/InscriCategoConcurInstru.php';
require_once '../modelo/InscriCategoConcurInstru.php';

$retorno = array("exito"=>1,"mensaje"=>"","data"=>null);
try{
	$idCategoria = $_POST['idCategoria'];
	$idCentroFormacion = $_POST['idCentroFormacion'];
	
	$centroFormacionE = new \entidad\CentroFormacion();
	$centroFormacionE->setIdCentroFormacion($idCentroFormacion);
	
	
	$categoriaConcursoE = new \entidad\CategoriaConcurso();
	$categoriaConcursoE->setIdCategoriaConcurso($idCategoria);
	
    $inscriCategoConcurInstruE = new \entidad\InscriCategoConcurInstru();
    $inscriCategoConcurInstruE->setCategoriaConcurso($categoriaConcursoE);
    $inscriCategoConcurInstruE->setCentroFormacion($centroFormacionE);
    
    $inscriCategoConcurInstruM = new \modelo\InscriCategoConcurInstru($inscriCategoConcurInstruE);
    $retorno['data'] = $inscriCategoConcurInstruM->validCategInstrCentrForma();

}catch(Exception $e){
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
?>