<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/Empresa.php';
require_once '../modelo/Empresa.php';
$retorno = array('exito'=>1,'mensaje'=> '', 'data'=>null, 'numeroRegistros'=>0);

try {
    $idEmpresa = $_POST['idEmpresa'];
    $nit = $_POST['nit'];
    $empresa = $_POST['empresa'];
    $idMunicipio = $_POST['idMunicipio'];
    //$municipio = $_POST['municipio'];
	    
    $empresaE = new \entidad\Empresa();
    $empresaE->setNit($nit);
    $empresaE->setIdEmpresa($idEmpresa);
    $empresaE->setIdMunicipio($idMunicipio);
    $empresaE->setEmpresa($empresa);
    
    $empresaM = new \modelo\Empresa($empresaE);
    $empresaM->consultarEmpresasOferentes();
    $retorno['numeroRegistros'] = $empresaM->conexion->obtenerNumeroRegistros();
	$contador = 0;
	
    while ($fila = $empresaM->conexion->obtenerObjeto()) {
		$retorno['data'][$contador]['idEmpresa'] = $fila->idEmpresa;
		$retorno['data'][$contador]['nit'] = $fila->nit;
        $retorno['data'][$contador]['empresa'] = $fila->empresa;
		$retorno['data'][$contador]['representanteLegal'] = $fila->representanteLegal;
		$retorno['data'][$contador]['direccion'] = $fila->direccion;
		$retorno['data'][$contador]['telefonoCelular'] = $fila->telefonoCelular;
		$retorno['data'][$contador]['telefonoFijo'] = $fila->telefonoFijo;
		$retorno['data'][$contador]['url'] = $fila->url;
		$retorno['data'][$contador]['correoElectronico'] = $fila->correoElectronico;
		$retorno['data'][$contador]['idMunicipio'] = $fila->idMunicipio;
		$retorno['data'][$contador]['municipio'] = $fila->municipio;
        $contador++;
    }
    
} catch (Exception $e) {
    $retorno['exito'] =0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);

?>