<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/CentroFormacion.php';
require_once '../modelo/CentroFormacion.php';
$retorno = array('exito'=>1,"numeroRegistros"=>0,"mensaje"=>'',"data"=>null);

try{
	$codigo = $_SESSION['codigoCentroFormacion'];
	
	$centroFormacionE = new \entidad\CentroFormacion();
	$centroFormacionE->setCodigo($codigo);
	
	$centroFormacionM = new \modelo\CentroFormacion($centroFormacionE);
	$retorno['data'] =  $centroFormacionM->consultar();
	$retorno['numeroRegistros'] = $centroFormacionM->conexion->obtenerNumeroRegistros();
	
}catch (Exception $e){
	$retorno['exito'] = 0;
	$retorno['mensaje'] = "El código ingresado no existe".$e->getMessage();
}
echo json_encode($retorno);
?>