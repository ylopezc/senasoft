<?php
ini_set("display_errors", 0);
require_once '../modelo/TallaCamiseta.php';
$retorno = array("exito"=>1, "mensaje"=>'', "data"=>null);
try {
    $tallaCamiseta = new modelo\TallaCamiseta();
    $retorno["data"] = $tallaCamiseta->getTallaCamiseta();
} catch (Exception $e) {
    $retorno["exito"] = 0;
    $retorno["mensaje"] = $e->getMessage();
}
echo json_encode($retorno);
?>

