<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/EmpresaCodigo.php';
require_once '../modelo/EmpresaCodigo.php';
$retorno = array('exito'=>1,"mensaje"=>'', 'numeroRegistros'=>0);

try {
    $codigoInscripcion = $_POST['codigoInscripcion'];
    
    $empresaCodigoE = new \entidad\EmpresaCodigo();
    $empresaCodigoE->setEmpresaCodigo($codigoInscripcion);
    
    $empresaCodigoM = new \modelo\EmpresaCodigo($empresaCodigoE);
    $empresaCodigoM->validarCodigo();
    $retorno['numeroRegistros'] = $empresaCodigoM->conexion->obtenerNumeroRegistros();
	
	if($empresaCodigoM->conexion->obtenerNumeroRegistros() == 1){
		$_SESSION['codigoInscripcion'] = $codigoInscripcion;
	}else{
		$retorno['exito'] = 0;
		$retorno['mensaje'] = "Por favor ingrese un código válido para la inscripción de su empresa.";
	}
} catch (Exception $e) {
	$retorno["exito"] = 0;
	$retorno["mensaje"] = "Por favor ingrese un código válido para la inscripción de su empresa.".$e->getMessage();
}
echo json_encode($retorno);
?>