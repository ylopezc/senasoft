<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/AsistenteRuedaNegocio.php';
require_once '../modelo/AsistenteRuedaNegocio.php';

$retorno = array('exito'=>1,"mensaje"=>'El asistente a la rueda de negocios se registró correctamente',"idPersona"=>null);

try {
	$asistenteRuedaNegocio = $_POST["asistenteRuedaNegocio"];
	$cargo = $_POST["cargo"];
	$correoElectronico = $_POST["correoElectronico"];
	$telefono = $_POST["telefono"];
	$idEmpresa = $_POST["idEmpresa"];
	$horaAsistencia = $_POST["horaAsistencia"];	
	$estado = 1;
	$idUsuarioCreacion = 1;
	$idUsuarioModificacion = 1;
	
	
	$asistenteRuedaNegocioE = new \entidad\AsistenteRuedaNegocio();
	$asistenteRuedaNegocioE->setAsistenteRuedaNegocio($asistenteRuedaNegocio);
	$asistenteRuedaNegocioE->setCargo($cargo);
	$asistenteRuedaNegocioE->setCorreoElectronico($correoElectronico);
	$asistenteRuedaNegocioE->setTelefono($telefono);
	$asistenteRuedaNegocioE->setHoraAsistencia($horaAsistencia);
	$asistenteRuedaNegocioE->setEstado($estado);
	$asistenteRuedaNegocioE->setIdEmpresa($idEmpresa);
	$asistenteRuedaNegocioE->setIdUsuarioCreacion($idUsuarioCreacion);
	$asistenteRuedaNegocioE->setIdUsuarioModificacion($idUsuarioModificacion);
	
	$asistenteRuedaNegocioM = new \modelo\AsistenteRuedaNegocio($asistenteRuedaNegocioE);;
	$asistenteRuedaNegocioM->adicionar();
	
} catch (Exception $e) {
	$retorno["exito"] = 0;
	$retorno["mensaje"] = $e->getMessage();
}
echo json_encode($retorno);

?>