<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/EmpresaCodigo.php';
require_once '../modelo/EmpresaCodigo.php';

$retorno = array('exito'=>1,"mensaje"=>'La solicitud se envió correctamente , validaremos los datos ingresados y posteriormente enviaremos un correo electrónico con su código de inscripción', 'numeroRegistros'=>0);

try {
    $nit = $_POST['nit'];
    $correoElectronico = $_POST['correoElectronico'];
    $empresa = $_POST['empresa'];
    
    $empresaCodigoE = new \entidad\EmpresaCodigo();
    $empresaCodigoE->setEmpresa($empresa);
    $empresaCodigoE->setNit($nit);
    $empresaCodigoE->setCorreoElectronico($correoElectronico);
    $empresaCodigoE->setIdEstadoEmpresa(1);
    
    $empresaCodigoM = new \modelo\EmpresaCodigo($empresaCodigoE);
    $empresaCodigoM->generarCodigoEmpresa();
    $empresaCodigoM->adicionar();
	
	date_default_timezone_set('America/Bogota');
	
    $destinatario = "alejandrodiazp@misena.edu.co ";
    
    $asunto = "Solicitud código inscripción";
    
    $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    
    $cabeceras .= "From: SENASOFT Huila 2014<contacto@senasoft.co>\r\n";
    
    $mensajeEnviar = "Se ha realizado una solicitud de código por parte de la empresa $empresa , con el correo electrónico $correoElectronico . Por favor revise el módulo de administración de la página web en el siguiente link http://senasoft.co/Senasoft/indexAdmin.html";
    
    $mensajeEnviar .= '<br><br>'.$mensaje;
    
    if(mail($destinatario, $asunto, $mensajeEnviar , $cabeceras)){
    	$retorno['enviado'] = 1;
    }else{
    	$retorno['enviado'] = 0;
    }
	
} catch (Exception $e) {
	$retorno["exito"] = 0;
	$retorno["mensaje"] = "Error adicionando la empresa .".$e->getMessage();
}
echo json_encode($retorno);
?>