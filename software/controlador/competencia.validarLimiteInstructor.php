<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/CategoriaConcurso.php';
require_once '../modelo/CategoriaConcurso.php';
require_once '../entidad/InscriCategoConcurInstru.php';
require_once '../modelo/InscriCategoConcurInstru.php';

$retorno = array("exito"=>1,"mensaje"=>"","data"=>null);
try{
	$idCategoria = $_POST['idCategoria'];
	//$idCategoria = 1;
	
	$categoriaConcursoE = new \entidad\CategoriaConcurso();
	$categoriaConcursoE->setIdCategoriaConcurso($idCategoria);
	
    $inscriCategoConcurInstruE = new \entidad\InscriCategoConcurInstru();
    $inscriCategoConcurInstruE->setCategoriaConcurso($categoriaConcursoE);
    
    $inscriCategoConcurInstruM = new \modelo\InscriCategoConcurInstru($inscriCategoConcurInstruE);
    $retorno['data'] = $inscriCategoConcurInstruM->numerInstrInscrCateg();

}catch(Exception $e){
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
?>
