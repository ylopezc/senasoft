<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/CategoriaConcurso.php';
require_once '../modelo/CategoriaConcurso.php';
require_once '../entidad/CentroFormacion.php';
require_once '../modelo/CentroFormacion.php';
require_once '../entidad/InscriCategoConcurAprend.php';
require_once '../modelo/InscriCategoConcurAprend.php';



$retorno = array("exito"=>1,"mensaje"=>"","data"=>null);
try{
	$idCategoria = $_POST['idCategoria'];
	$idCentroFormacion = $_POST['idCentroFormacion'];
	//$idCategoria = 1;
	//$idCentroFormacion = 1;
	
	$centroFormacionE = new \entidad\CentroFormacion();
	$centroFormacionE->setIdCentroFormacion($idCentroFormacion);
	
	
	$categoriaConcursoE = new \entidad\CategoriaConcurso();
	$categoriaConcursoE->setIdCategoriaConcurso($idCategoria);
	
    $inscriCategoConcurAprendE = new \entidad\InscriCategoConcurAprend();
    $inscriCategoConcurAprendE->setCategoriaConcurso($categoriaConcursoE);
    $inscriCategoConcurAprendE->setCentroFormacion($centroFormacionE);
    
    $inscriCategoConcurAprendM = new \modelo\InscriCategoConcurAprend($inscriCategoConcurAprendE);
    $retorno['data'] = $inscriCategoConcurAprendM->validCategAprendCentrForma();

}catch(Exception $e){
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
?>