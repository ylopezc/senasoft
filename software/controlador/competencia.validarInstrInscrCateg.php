<?php
ini_set("display_errors", 0);
session_start();
require_once '../entidad/Persona.php';
require_once '../modelo/Persona.php';
require_once '../entidad/Instructor.php';
require_once '../modelo/Instructor.php';
require_once '../entidad/InscriCategoConcurInstru.php';
require_once '../modelo/InscriCategoConcurInstru.php';



$retorno = array("exito"=>1,"mensaje"=>"","numeroRegistros"=>null);
try{
	$numeroIdentificacion = $_POST['numeroIdentificacion'];
	//$numeroIdentificacion = 123;
	
	//$idCategoria = 1;
	//$idCentroFormacion = 1;
	
	$personaE = new \entidad\Persona();
	$personaE->setNumeroIdentificacion($numeroIdentificacion);
	
	
	$instructorE = new \entidad\Instructor();
	$instructorE->setPersona($personaE);
	
    $inscriCategoConcurInstrE = new \entidad\InscriCategoConcurInstru();
    $inscriCategoConcurInstrE->setInstructor($instructorE);
    
    $inscriCategoConcurInstrM = new \modelo\InscriCategoConcurInstru($inscriCategoConcurInstrE);
    $inscriCategoConcurInstrM->validarInstrInscrCateg();
    $retorno['numeroRegistros'] = $inscriCategoConcurInstrM->conexion->obtenerNumeroRegistros();

}catch(Exception $e){
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $e->getMessage();
}
echo json_encode($retorno);
?>