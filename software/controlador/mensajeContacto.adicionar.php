<?php
ini_set("display_errors", 0);
require_once '../entidad/MensajeContacto.php';
require_once '../modelo/MensajeContacto.php';
require_once '../entidad/CentroFormacion.php';

$retorno = array('exito'=>1 , 'mensaje'=>'El mensaje se guardó correctamente.');
try {    
    $nombres = $_POST['nombres'];
    $correoElectronico = $_POST['correoElectronico'];   
    $asunto = $_POST['asunto'];
    $rol = $_POST['rol'];
    $regional = $_POST['regional'];
    $centroFormacion = $_POST['centroFormacion'];
    $idCentroFormacion = $_POST['idCentroFormacion'];
    $mensaje = $_POST['mensaje'];
	
    $mensajeContactoE = new \entidad\MensajeContacto();
    $mensajeContactoE->setRemitente($nombres);
    $mensajeContactoE->setMensajeContacto($mensaje);
    $mensajeContactoE->setRol($rol);
    $mensajeContactoE->setAsunto($asunto);
    
    $centroFormacionE = new \entidad\CentroFormacion();
    $centroFormacionE->setIdCentroFormacion($idCentroFormacion);
    $mensajeContactoE->setCentroFormacion($centroFormacionE);
    
    $mensajeContactoE->setCorreoElectronico($correoElectronico);
    
    $mensajeContactoM = new \modelo\MensajeContacto($mensajeContactoE);
    $mensajeContactoM->adicionar();
}catch (Exception $ex) {
    $retorno['mensaje'] = $ex->getMessage();
    $retorno['exito'] = 0;
}
echo json_encode($retorno);
?>