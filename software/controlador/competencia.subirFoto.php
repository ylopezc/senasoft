<?php
ini_set("display_errors", 0);
$retorno = array('exito'=>1, 'mensaje'=>'la foto se ha subió correctamente', 'ruta'=>'');
try {
	$nombreTemporal = $_POST['nombreTemporal'];
	if(!empty($_SERVER['HTTP_CLIENT_IP'])) {
	    $direccionIp=$_SERVER['HTTP_CLIENT_IP'];
	} elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	    $direccionIp=$_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
	    $direccionIp=$_SERVER['REMOTE_ADDR'];
	}
    foreach ($_FILES as $key){
        $nombre = $key['name'];
        $temporal = $key['tmp_name'];
        $tamano = $key['size']/1024;
        $extension = substr(strrchr($nombre, "."), 1);
        $ruta = "../archivos/fotos/$direccionIp-$nombreTemporal.".$extension;
        $retorno['ruta'] = $ruta;
        if($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'gif' || $extension == 'JPG' || $extension == 'JPEG' || $extension == 'PNG' || $extension == 'GIF'){
             if($tamano <= 200){	
                if(!move_uploaded_file($temporal, $ruta)){
                    $retorno['exito'] = 0;
                    $retorno['mensaje'] = 'Ocurrio un error subiendo el certificado.';
                }
            }else{
                $retorno['exito'] = 0;
                $retorno['ruta']= "";
                $retorno['mensaje'] = 'El peso del archivo supera los 200 Kb';
            } 
        }else{
            $retorno['exito'] = 0;
            $retorno['ruta']="";
            $retorno['mensaje'] = 'Extensión inválida';
        }
    }
} catch (Exception $exc) {
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $exc->getMessage();
}
echo json_encode($retorno);
?>