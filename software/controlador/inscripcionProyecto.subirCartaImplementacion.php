<?php
ini_set("display_errors", 0);
session_start();
$retorno = array('exito'=>1, 'mensaje'=>'El certificado de implementación se subió correctamente', 'ruta'=>'');
try {
    foreach ($_FILES as $key){
        $nombre = $key['name'];
        $temporal = $key['tmp_name'];
        $tamano = $key['size']/1024;
        $extension = substr(strrchr($nombre, "."), 1);
        $_SESSION['nombreCertificado'] = $nombre;
        $ruta = '../archivos/certificadosImplementacion/temporal.'.$extension;
        $retorno['ruta'] = $ruta;
        if($extension == 'pdf' || $extension == 'PDF'){
            if($tamano <= 500){	
                if(!move_uploaded_file($temporal, $ruta)){
                    $retorno['exito'] = 0;
                    $retorno['mensaje'] = 'Ocurrio un error subiendo el certificado.';
                }
            }else{
                $retorno['exito'] = 0;
                $retorno['ruta']= "";
                $retorno['mensaje'] = 'El peso del archivo supera los 500 Kb';
            }
        }else{
            $retorno['ruta'] = '';
            $retorno['exito'] = 0;
            $retorno['mensaje'] = 'Extensión inválida';
        }
    }
} catch (Exception $exc) {
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $exc->getMessage();
}
echo json_encode($retorno);
?>