<?php
    require_once ('configuracion.php');

    $conexion = new mysqli($SERVER_NAME,$USER,$PASSWORD,$DATABASE,$PORT);
    //$conexion = new mysqli('localhost','root','','senasoft',3306);
	if (mysqli_connect_errno()) {
    	printf("La conexión con el servidor de base de datos falló: %s\n", mysqli_connect_error());
    	exit();
	}
	$consulta = "
		SELECT 
		    persona.numeroIdentificacion AS documento, 
		    persona.primerNombre AS primerNombre,
		    persona.segundoNombre AS segundoNombre,
		    persona.primerApellido AS primerApellido,
		    persona.segundoApellido AS segundoApellido,
		    persona.fechaNacimiento AS fechaNacimiento,
		    persona.correoElectronico AS correo,
		    persona.telefonoCelular AS celular,
		    persona.telefonoFijo AS fijo,
		    persona.fechaCreacion AS fecha,
		    persona.genero AS genero,
		    categoriaconcurso.categoriaConcurso AS categoria,
		    tipodocumentoidentidad.codigo AS tipodocumento,
		    centroformacion.centroFormacion AS centro,
		    regional.regional AS regional,
		    programaformacion.programaFormacion AS programa,
		    nivelprogramaformacion.nivelProgramaFormacion AS nivel,
		    personainfoadicional.rh AS rh,
		    personainfoadicional.tallaCamiseta AS talla,
		    personainfoadicional.cuidadoMedico AS cuidados
		FROM
			inscricategoconcuraprend
		INNER JOIN aprendiz 
			ON inscricategoconcuraprend.idAprendiz=aprendiz.idAprendiz 
		INNER JOIN programaformacion
		    ON	inscricategoconcuraprend.idProgramaFormacion=programaformacion.idProgramaFormacion
		INNER JOIN nivelprogramaformacion
		    ON programaformacion.idNivelProgramaFormacion=nivelprogramaformacion.idNivelProgramaFormacion
		INNER JOIN persona
		    ON	aprendiz.idPersona=persona.idPersona
		INNER JOIN categoriaconcurso
		   	ON	inscricategoconcuraprend.idCategoriaConcurso=categoriaconcurso.idCategoriaConcurso
		INNER JOIN tipodocumentoidentidad
		   	ON	persona.idTipoDocumentoIdentidad=tipodocumentoidentidad.idTipoDocumentoIdentidad
		INNER JOIN centroformacion
		   	ON	inscricategoconcuraprend.idCentroFormacion=centroformacion.idCentroFormacion
		INNER JOIN regional
		   	ON centroformacion.idRegional=regional.idRegional
		INNER JOIN personainfoadicional
			ON persona.idPersona=personainfoadicional.idPersona
	";

	$resultado = $conexion->query($consulta);
	if($resultado->num_rows > 0 ){
						
		date_default_timezone_set('America/Bogota');

		if (PHP_SAPI == 'cli')
			die('Este archivo solo se puede ver desde un navegador web');

		/** Se agrega la libreria PHPExcel */
		require_once 'lib/PHPExcel/PHPExcel.php';

		// Se crea el objeto PHPExcel
		$objPHPExcel = new PHPExcel();

		// Se asignan las propiedades del libro
		$objPHPExcel->getProperties()->setCreator("Codedrinks") //Autor
							 ->setLastModifiedBy("Yovanny López") //Ultimo usuario que lo modificó
							 ->setTitle("Reporte Excel")
							 ->setSubject("Reporte Excel")
							 ->setDescription("Reporte de registros")
							 ->setKeywords("reporte de registros")
							 ->setCategory("Reporte excel");

		$tituloReporte = "Reporte de Aprendices inscritos en SENASoft Quindío 2016";
		$titulosColumnas = array(
				'FECHA DE INSCRIPCIÓN', 
				'REGIONAL',
				'CENTRO DE FORMACIÓN',
				'CATEGORÍA',
				'TIPO DE DOCUMENTO',
				'DOCUMENTO DE IDENTIDAD',
				'NOMBRE',
				'PRIMER APELLIDO',
				'SEGUNDO APELLIDO',
				'FECHA DE NACIMIENTO',
				'CORREO ELECTRÓNICO',
				'CELULAR',
				'FIJO',
				'GENERO',
				'RH',
				'TALLA',
				'CUIDADOS MEDICOS',
				'PROGRAMA DE FORMACIÓN',
				'NIVEL DE FORMACIÓN'
			);
		
		$objPHPExcel->setActiveSheetIndex(0)
        		    ->mergeCells('A1:F1');
						
		// Se agregan los titulos del reporte
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1',  $tituloReporte)
        		    ->setCellValue('A3',  $titulosColumnas[0])
        		    ->setCellValue('B3',  $titulosColumnas[1])
        		    ->setCellValue('C3',  $titulosColumnas[2])
        		    ->setCellValue('D3',  $titulosColumnas[3])
        		    ->setCellValue('E3',  $titulosColumnas[4])
        		    ->setCellValue('F3',  $titulosColumnas[5])
            		->setCellValue('G3',  $titulosColumnas[6])
            		->setCellValue('H3',  $titulosColumnas[7])
            		->setCellValue('I3',  $titulosColumnas[8])
            		->setCellValue('J3',  $titulosColumnas[9])
            		->setCellValue('K3',  $titulosColumnas[10])
            		->setCellValue('L3',  $titulosColumnas[11])
            		->setCellValue('M3',  $titulosColumnas[12])
            		->setCellValue('N3',  $titulosColumnas[13])
            		->setCellValue('O3',  $titulosColumnas[14])
            		->setCellValue('P3',  $titulosColumnas[15])
            		->setCellValue('Q3',  $titulosColumnas[16])
            		->setCellValue('R3',  $titulosColumnas[17])
            		->setCellValue('S3',  $titulosColumnas[18]);
		
		//Se agregan los datos de los alumnos
		$i = 4;
		while ($fila = $resultado->fetch_array()) {
			$objPHPExcel->setActiveSheetIndex(0)
        		    ->setCellValue('A'.$i,  $fila['fecha'])
        		    ->setCellValue('B'.$i,  utf8_encode($fila['regional']))
        		    ->setCellValue('C'.$i,  utf8_encode($fila['centro']))
        		    ->setCellValue('D'.$i,  utf8_encode($fila['categoria']))
        		    ->setCellValue('E'.$i,  $fila['tipodocumento'])
		            ->setCellValue('F'.$i,  $fila['documento'])
		            ->setCellValue('G'.$i,  utf8_encode($fila['primerNombre']))
		            ->setCellValue('H'.$i,  utf8_encode($fila['primerApellido']))
		            ->setCellValue('I'.$i,  utf8_encode($fila['segundoApellido']))
		            ->setCellValue('J'.$i,  utf8_encode($fila['fechaNacimiento']))
		            ->setCellValue('K'.$i,  utf8_encode($fila['correo']))
		            ->setCellValue('L'.$i,  $fila['celular'])
		            ->setCellValue('M'.$i,  $fila['fijo'])
		            ->setCellValue('N'.$i,  $fila['genero'])
		            ->setCellValue('O'.$i,  $fila['rh'])
		            ->setCellValue('P'.$i,  $fila['talla'])
		            ->setCellValue('Q'.$i,  utf8_encode($fila['cuidados']))
		            ->setCellValue('R'.$i,  utf8_encode($fila['programa']))
		            ->setCellValue('S'.$i,  utf8_encode($fila['nivel']));
					$i++;
		}
		
		$estiloTituloReporte = array(
        	'font' => array(
	        	'name'      => 'Verdana',
    	        'bold'      => true,
        	    'italic'    => false,
                'strike'    => false,
               	'size' =>16,
	            	'color'     => array(
    	            	'rgb' => 'FFFFFF'
        	       	)
            ),
	        'fill' => array(
				'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
				'color'	=> array('argb' => '009781')
			),
            'borders' => array(
               	'allborders' => array(
                	'style' => PHPExcel_Style_Border::BORDER_NONE                    
               	)
            ), 
            'alignment' =>  array(
        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        			'rotation'   => 0,
        			'wrap'          => TRUE
    		)
        );

		$estiloTituloColumnas = array(
            'font' => array(
                'name'      => 'Arial',
                'bold'      => true,                          
                'color'     => array(
                    'rgb' => 'FFFFFF'
                )
            ),
            'fill' 	=> array(
				'type'		=> PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
				'rotation'   => 90,
        		'startcolor' => array(
            		'rgb' => '8bb63c'
        		),
        		'endcolor'   => array(
            		'argb' => '009781'
        		)
			),
            'borders' => array(
            	'top'     => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                    'color' => array(
                        'rgb' => '000000'
                    )
                ),
                'bottom'     => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                    'color' => array(
                        'rgb' => '000000'
                    )
                )
            ),
			'alignment' =>  array(
        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        			'wrap'          => TRUE
    		));
			
		$estiloInformacion = new PHPExcel_Style();
		$estiloInformacion->applyFromArray(
			array(
           		'font' => array(
               	'name'      => 'Arial',               
               	'color'     => array(
                   	'rgb' => '000000'
               	)
           	),
           	'fill' 	=> array(
				'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
				'color'		=> array('argb' => 'FFFFFF')
			),
           	'borders' => array(
               	'left'     => array(
                   	'style' => PHPExcel_Style_Border::BORDER_THIN ,
	                'color' => array(
    	            	'rgb' => '000000'
                   	)
               	)             
           	)
        ));
		 
		$objPHPExcel->getActiveSheet()->getStyle('A1:S1')->applyFromArray($estiloTituloReporte);
		$objPHPExcel->getActiveSheet()->getStyle('A3:S3')->applyFromArray($estiloTituloColumnas);		
		$objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:S".($i-1));
				
		for($i = 'A'; $i <= 'S'; $i++){
			$objPHPExcel->setActiveSheetIndex(0)			
				->getColumnDimension($i)->setAutoSize(TRUE);
		}
		
		// Se asigna el nombre a la hoja
		$objPHPExcel->getActiveSheet()->setTitle('Inscripciones aprendices');

		// Se activa la hoja para que sea la que se muestre cuando el archivo se abre
		$objPHPExcel->setActiveSheetIndex(0);
		// Inmovilizar paneles 
		//$objPHPExcel->getActiveSheet(0)->freezePane('A4');
		$objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,4);

		// Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="ReporteAprendices.xls"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit;
		
	}
	else{
		print_r('No hay resultados para mostrar');
	}
?>