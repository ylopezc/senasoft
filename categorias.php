<?php include('header.php') ?>

    <div class="portfolio_box">
        <div class="container">
            <section class="title-section">
            	<h1 class="title-header">Categorías y lineamientos técnicos</h1>
            </section>

            
            
            <div class="row service_box">
                <div class="col-md-3 grid_box">
                    <a href="archivos/algoritmia.pdf" target="_blank" class="swipebox"  title="Image Title"> <img src="images/algoritmia.jpg" class="img-responsive" alt=""></a>
                    <h4>Objetivo de la categoría</h4>
                    <p>Plantear soluciones algorítmicas a problemas diversos, en el menor tiempo posible.</p>
                    <a href="archivos/algoritmia.pdf" target="_blank" class="link">Lineamientos técnicos</a>
                </div>
                <div class="col-md-3 grid_box">
                    <a href="archivos/aplicaciones_net.pdf" target="_blank" class="swipebox"  title="Image Title"><img src="images/net.jpg" class="img-responsive" alt=""></a>
                    <h4>Objetivo de la categoría</h4>
                    <p>Desarrollar aplicaciones Windows, utilizando la suite de Visual Studio.NET.</p>
                    <a href="archivos/aplicaciones_net.pdf" target="_blank" class="link">Lineamientos técnicos</a>
                </div>
                <div class="col-md-3 grid_box">
                    <a href="archivos/aplicaciones_moviles_android.pdf" target="_blank" class="swipebox"  title="Image Title"> <img src="images/android.jpg" class="img-responsive" alt=""></a>
                    <h4>Objetivo de la categoría</h4>
                    <p>Desarrollar aplicaciones móviles para Android, usando buenas prácticas. </p>
                    <a href="archivos/aplicaciones_moviles_android.pdf" target="_blank" class="link">Lineamientos técnicos</a>
                </div>
                <div class="col-md-3 grid_box">
                    <a href="archivos/aplicaciones_web_php.pdf" target="_blank" class="swipebox"  title="Image Title"> <img src="images/php.jpg" class="img-responsive" alt=""></a>
                    <h4>Objetivo de la categoría</h4>
                    <p>Desarrollar sitios web dinámicos, incorporando bibliotecas de componentes.</p>
                    <a href="archivos/aplicaciones_web_php.pdf" target="_blank" class="link">Lineamientos técnicos</a>
                </div>
            </div>
            <div class="row service_box">
                <div class="col-md-3 grid_box">
                    <a href="archivos/bases_datos.pdf" target="_blank" class="swipebox"  title="Image Title"> <img src="images/bd.jpg" class="img-responsive" alt=""></a>
                    <h4>Objetivo de la categoría</h4>
                    <p>Construir sentencias SQL a partir de un diseño de base de datos.</p>
                    <a href="archivos/bases_datos.pdf" target="_blank" class="link">Lineamientos técnicos</a>
                </div>
                <div class="col-md-3 grid_box">
                    <a href="archivos/diseno_orientado_objetos.pdf" target="_blank" class="swipebox"  title="Image Title"> <img src="images/poo.jpg" class="img-responsive" alt=""></a>
                    <h4>Objetivo de la categoría</h4>
                    <p>Generar diseños orientados a objetos, usando los elementos y los conceptos fundamentales de UML.</p>
                    <a href="archivos/diseno_orientado_objetos.pdf" target="_blank" class="link">Lineamientos técnicos</a>
                </div>
                <div class="col-md-3 grid_box">
                    <a href="archivos/java_web.pdf" target="_blank" class="swipebox"  title="Image Title"> <img src="images/java.jpg" class="img-responsive" alt=""></a>
                    <h4>Objetivo de la categoría</h4>
                    <p>Desarrollar aplicaciones web funcionales, basadas en Java.</p>
                    <a href="archivos/java_web.pdf" target="_blank" class="link">Lineamientos técnicos</a>
                </div>
                <div class="col-md-3 grid_box">
                    <a href="archivos/proyectos_agiles_scrum.pdf" target="_blank" class="swipebox"  title="Image Title"> <img src="images/Scrom.jpg" class="img-responsive" alt=""></a>
                    <h4>Objetivo de la categoría</h4>
                    <p>Definir los requisitos de carácter técnico que regirán la ejecución de las pruebas de la categoría PROYECTOS ágiles con scrum.</p>
                    <a href="archivos/proyectos_agiles_scrum.pdf" target="_blank" class="link">Lineamientos técnicos</a>
                </div>
            </div>
                	
            <div class="row service_box">
                <div class="col-md-3 grid_box">
                    <a href="archivos/animacion_3d.pdf" target="_blank" class="swipebox"  title="Image Title"> <img src="images/3d.jpg" class="img-responsive" alt=""></a>
                    <h4>Objetivo de la categoría</h4>
                    <p>Diseñar, modelar y animar un personaje con su escenario.</p>
                    <a href="archivos/animacion_3d.pdf" target="_blank" class="link">Lineamientos técnicos</a>
                </div>
                <div class="col-md-3 grid_box">
                    <a href="archivos/produccion_medios_audiovisuales.pdf" target="_blank" class="swipebox"  title="Image Title"> <img src="images/audiovisual.jpg" class="img-responsive" alt=""></a>
                    <h4>Objetivo de la categoría</h4>
                    <p>Realizar una producción audiovisual, teniendo en cuenta las tres fases.</p>
                    <a href="archivos/produccion_medios_audiovisuales.pdf" target="_blank" class="link">Lineamientos técnicos</a>
                </div>
                <div class="col-md-3 grid_box">
                    <a href="archivos/produccion_multimedia.pdf" target="_blank" class="swipebox"  title="Image Title"> <img src="images/multimedia.jpg" class="img-responsive" alt=""></a>
                    <h4>Objetivo de la categoría</h4>
                    <p>Diseñar e implementar soluciones de comunicación visual e interactiva.</p>
                    <a href="archivos/produccion_multimedia.pdf" target="_blank" class="link">Lineamientos técnicos</a>
                </div>
                <div class="col-md-3 grid_box">
                    <a href="archivos/videojuegos.pdf" target="_blank" class="swipebox"  title="Image Title"> <img src="images/videojuego.jpg" class="img-responsive" alt=""></a>
                    <h4>Objetivo de la categoría</h4>
                    <p>Diseñar, desarrollar e implementar un demo funcional de un videojuego.</p>
                    <a href="archivos/videojuegos.pdf" target="_blank" class="link">Lineamientos técnicos</a>
                </div>
            </div>
            
            <div class="row service_box">
                <div class="col-md-3 grid_box">
                    <a href="archivos/instalacion_hardening_sistemas_operativos.pdf" class="swipebox"  title="Image Title"> <img src="images/hardening.jpg" class="img-responsive" alt=""></a>
                    <h4>Objetivo de la categoría</h4>
                    <p>Instalar un sistema operativo, con su respectivo aseguramiento.</p>
                    <a href="archivos/instalacion_hardening_sistemas_operativos.pdf" target="_blank" class="link">Lineamientos técnicos</a>
                </div>
                <div class="col-md-3 grid_box">
                    <a href="archivos/redes_datos.pdf" target="_blank" class="swipebox"  title="Image Title"> <img src="images/redes.jpg" class="img-responsive" alt=""></a>
                    <h4>Objetivo de la categoría</h4>
                    <p>Diseñar e implementar soluciones de interconexión y servicios de red.</p>
                    <a href="archivos/redes_datos.pdf" target="_blank" class="link">Lineamientos técnicos</a>
                </div>
                <div class="col-md-3 grid_box">
                    <a href="archivos/sistemas_operativos_red.pdf" target="_blank" class="swipebox"  title="Image Title"> <img src="images/so.jpg" class="img-responsive" alt=""></a>
                    <h4>Objetivo de la categoría</h4>
                    <p>Instalar sistemas operativos de red y utilizar software de administración.</p>
                    <a href="archivos/sistemas_operativos_red.pdf" target="_blank" class="link">Lineamientos técnicos</a>
                </div>
                <div class="col-md-3 grid_box">
                    <a href="archivos/lineamientos.pdf" target="_blank" class="swipebox"  title="Image Title"> <img src="images/creamas.jpg" class="img-responsive" alt=""></a>
                    <h4>Objetivo de la categoría</h4>
                    <p>Rueda de proyección empresarial para la comunidad SENA en la que se busca promover la gestión comercial de productos y servicios consolidados.</p>
                    <a href="archivos/lineamientos.pdf" target="_blank" class="link">Lineamientos técnicos</a>
                </div>
            </div>
        </div>	
    </div>
 
<?php include('footer.php') ?>